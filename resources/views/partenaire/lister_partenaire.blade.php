@extends('layout.adminlayout.design')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Partenaire</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom du Partenaire</th>
                        <th>Image du Partenaire</th>
                        <th>Date de Création</th>
                        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($partenaires as $partenaire)
                        <tr>
                        <td>{{ $partenaire->id }}</td>
                        <td>{{ $partenaire->nom}}</td>
                        <td><img src="{{url('partenaireimagess', $partenaire->image) }}" style="width:50px;"></td>
                        <td>{{ $partenaire->created_at }}</td>
                        <td>@if($partenaire->status==0)
                                      <b style="color:green"> Activer</b>
                                      @else
                                    <b style="color:red">  Desactiver</b>
                                      @endif
                                      <br>
                                      <button id="showSelectDiv{{$partenaire->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>
                                      <div id="selectDiv{{$partenaire->id}}">
                                      <input type="hidden" id="userID{{$partenaire->id}}" value="{{$partenaire->id}}">
                                      <select id="partenaireStatus{{$partenaire->id}}">
                                        <option value="">select a option</option>
                                        <option value="0">Activer</option>
                                        <option value="1">Desactiver</option>
                                      </select>
                                      </div>   
                                    </td>
                                   <td><a href="" class="btn btn-fill btn-warning">Actions</a></td>

                        

                        </tr>
                        @endforeach

                      </tbody>
                      </table>

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script>
$(document).ready(function(){

@foreach($partenaires as $partenaire)
$("#selectDiv{{$partenaire->id}}").hide();
$("#showSelectDiv{{$partenaire->id}}").click(function(){
$("#selectDiv{{$partenaire->id}}").show();
});
$("#partenaireStatus{{$partenaire->id}}").change(function(){
var status = $("#partenaireStatus{{$partenaire->id}}").val();
var userID = $("#userID{{$partenaire->id}}").val()
if(status==""){
alert("please select an option");
}else{
$.ajax({
url: '{{url("/admin/banPartenaire")}}',
data: 'status=' + status + '&userID=' + userID,
type: 'get',
success:function(response){
console.log(response);
}
});
}

});
@endforeach
});
</script>

 @endsection
         
