@extends('layout.adminlayout.design')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Partenaires</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                        
                                  {!! Form::open(['url'=>'save_partenaire', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">Nom du Partenaire</label>
                                          <input id="nom" name="nom" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="image" class="control-label mb-1">Image Partenaire</label>
                                          <input id="image" name="image" type="file" class="form-control cc-number identified visa" value="" data-val="" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">Description du Partenaire</label>
                                          <textarea name="description" id="description" cols="30" rows="10" class="form-control" aria-required="true" aria-invalid="false" required></textarea>
                                      </div>
                                      
                                      <div>
                                          <button id="submit" name="submit" style="background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection