<!DOCTYPE html>
<html>
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Dropdowns</h2>
  <p>The .dropdown class is used to indicate a dropdown menu.</p>
  
  <div class="dropdown">
 
    <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">drop
    <span class="caret"></span></button>

		
		
    <ul class="dropdown-menu">

    @foreach(App\Category::with('childs')
    ->where('product_id',0)->get() as $item)

            @if($item->childs->count()>0)
					
								<li><a href="{{url('catalogues')}}/{{$item->name}}"><h4>{{$item->name}}</h4></a></li>

								    @foreach($item->childs as $subMenu)
										
												<li><a href="{{url('catalogues')}}/{{$subMenu->name}}">{{$subMenu->name}}</a></li>
									
										@endforeach

						
				@else

				<li><a href="{{url('catalogues')}}/{{$item->name}}"><h4>{{$item->name}}</h4></a></li>
				@endif
				
		@endforeach

    </ul>
	


</div>
<div class="row">
<form action="">
        <div class="row row-filter">
          
          <div class="small-12 medium-6 large-4 p-0-15">
            <label for="">
              <strong>Catégorie</strong>
              <select id="catID">
							   <option value="">Select a Category</option>
                
                 <option class="option" value=""></option>
               </select>
            </label>
          </div>
          
          <div class="small-12 medium-6 large-2 p-0-15">
            <label for="">
              <strong>Prix</strong>
              <select id="priceID">
                <option value="0" selected>Croissant / Décroissant</option>
                <option value="0-100000">Décroissant</option>
						    <option value="100000-200000">Croissant</option>
						    
              </select>
            </label>
          </div>

          <div class="small-12 medium-12 large-2 p-0-15">
          <button id="findBtn" onClick="find()" class="filter-button">Filtrer</button>
          </div>

        </div>

      </div>
</form>
</div>

</body>
</html>
