<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Jalo</title>
<link rel="stylesheet" href="../css/app.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>


<body>

<header class="header">
<div class="header-top">
<div class="row">
<div class="small-12 medium-4 large-2">
<img src="../images/logo-yellow.jpeg" class="logo" alt="">
</div>
<div class="small-12 medium-8 large-6">
<div class="search">
<form action="">
<input type="search" class="search-input" placeholder="Rechercher un mot clé">
<button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
</form>
</div>
</div>
<div class="small-12 medium-12 large-3 large-offset-1">
<div class="user-session">
<ul class="dropdown menu" data-dropdown-menu>
<li><a href="" class="notification-is-active"><i class="tiny material-icons">notifications</i><span></span></a></li>
<li>
<a href="#" class="user-name"><i class="tiny material-icons">person</i>Client</a>
<ul class="menu">
<li><a href="profile.html"><i class="tiny material-icons">person</i> Profil</a></li>
<li><a href="#"><i class="tiny material-icons">settings</i> Paramettre</a></li>
<li><a href="/sign-in.html"><i class="tiny material-icons">exit_to_app</i> Se connecter</a></li>
</ul>
</li>
</ul>
</div>
</div>
</div>
</div>
<div class="row">
<div class="title-bar" data-responsive-toggle="example-animated-menu" data-hide-for="medium">
<button class="menu-icon" type="button" data-toggle></button>
<div class="title-bar-title">Menu</div>
</div>

<div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">
<div class="top-bar-left">
<ul class="menu">
<li><a href="catalogue.html" class="is-active waves-effect waves-yellow"><i class="tiny material-icons">view_module</i> <span>Catalogue</span></a></li>
<li><a href="ma-boutique.html" class="waves-effect waves-yellow"><i class="tiny material-icons">shop</i> <span>Ma boutique</span></a></li>
<li><a href="commandes.html" class="waves-effect waves-yellow"><i class="tiny material-icons">shopping_cart</i> <span>Commandes</span></a></li>
<li><a href="boutiques.html" class="waves-effect waves-yellow"><i class="tiny material-icons">shop</i> <span>Boutiques</span></a></li>
<li><a href="promotions.html" class="waves-effect waves-yellow"><i class="tiny material-icons">shopping_basket</i> <span>Promo</span></a></li>
<li><a href="fournisseurs.html" class="waves-effect waves-yellow"><i class="tiny material-icons">local_shipping</i> <span>Fournisseurs</span></a></li>
</ul>
</div>
<div class="top-bar-right">
<ul class="menu">
<li><a href="#bucket-container" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span>3</span></a></li>
</ul>
</div>
</div>
</div>
</header>

<main class="main">
<div class="row">
<header class="header-main-content">
<div class="top-bar">
<div class="top-bar-left">
<ul class="menu">
<li> <a href="" class="button">Toutes les catégories</a> </li>
<li> <a href="" class="hollow button secondary">Matériels électronique</a> </li>
<li> <a href="" class="hollow button secondary">Vétements</a> </li>
<li> <a href="" class="hollow button secondary">Consommation courante</a> </li>
</ul>
</div>
<div class="top-bar-right pos-rltv">
<div class="fixed-action-btn horizontal">
<a class="waves-effect waves-yellow btn-floating btn-large black">
<i class="material-icons">add_shopping_cart</i>
</a>
<!-- Modal Trigger -->
<ul>
<li>
<a class="waves-effect waves-black btn-floating green darken-1 modal-trigger" href="#create-shop" data-position="bottom" data-delay="50" data-tooltip="Faire une commande">
<i class="material-icons">add_shopping_cart</i>
</a>
</li>
</ul>
</div>
</div>
</div>
</header>
</div>

        <div class="row">
        @foreach($catalogues as $catalogue)

                <div class="small-12 medium-6 large-3 p-10">
                    <div class="card card-center">
                        <img src="https://source.unsplash.com/collection/190727" alt="">
                            <div class="card-section">
                            <a href="/produit/{{$catalogue->id}}"><h4 class="card-title">{{ ucfirst($catalogue->produit) }}</h4></a>
                            <small class="card-sub-title">{{ ucfirst($catalogue->prenom) }} {{$catalogue->nom}}</small>

                                    <div class="card-wrap-price">
                                        @if($catalogue->promo_prix != 0)
                                            <strong class="card-price-old">{{$catalogue->prix}}<span>F CFA</span></strong>
                                            <strong class="card-price">{{$catalogue->promo_prix}}<span>F CFA</span></strong>
                                        @else
                                            <strong class="card-price">{{$catalogue->prix}}<span>F CFA</span></strong>
                                        @endif
                                    </div>
                                    
          <button class="add-bucket-btn waves-effect waves-black add_to_cart" id="add_btn">tine</button>

                                    <div class="alert alert-success" id="successMsg"></div>
                            </div>
                    </div>
                </div>

@endforeach
        </div>


<div class="row">
<div class="small-12 medium-12 large-12">
<ul class="pagination text-center" role="navigation" aria-label="Pagination">
<li class="pagination-previous disabled">Précédent</li>
<li class="current"><span class="show-for-sr">Page courante</span> 1</li>
<li><a href="#" aria-label="Page 2">2</a></li>
<li><a href="#" aria-label="Page 3">3</a></li>
<li><a href="#" aria-label="Page 4">4</a></li>
<li class="ellipsis"></li>
<li><a href="#" aria-label="Page 12">12</a></li>
<li><a href="#" aria-label="Page 13">13</a></li>
<li class="pagination-next"><a href="#" aria-label="Next page">Suivant</a></li>
</ul>
</div>
</div>






                
                   
        
                
           

               

         
</body>
</html>



