<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  
<style>

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;  
}  

img.avatar {
    width: 40%;
    border-radius: 50%;
}


/* Change styles for span and cancel button on extra small screens */

</style>
</head>
  <body>

    <!--header-->
       
    <header class="header">
         @include('version3.header.header2')
  
        </header>
    <!-- end header-->   
    <main class="main">

        <p>&nbsp;</p>
        <p>&nbsp;</p>

         <section class="row">
<div class="row">
            <div class="small-12 medium-12 large-3">
            <div style="background-color: yellow; border-radius:3px; box-shadow: 0 3px 9px rgba(black, 0.2); padding: 15px">

            <form action="/login" method="post">
                        {{ csrf_field() }}

                                        <h2 style="font-size:20px;">Déjà client, connectez-vous</h2>

                                         
                                        @if(Session::has('flash_message_error'))
                                        <div class="alert alert-error alert-block" style="color:red">
                                        <button type="button" class="close" data-dismiss="alert"></button>
                                            <strong>
                                            {!! session('flash_message_error') !!}
                                            </strong>
                                        </div>
                                        @endif   
   

                                        <div class="input-field">

                                                <label for="uname" data-error="votre numéro de téléphone est invalide" ><b>Votre Numéro de Téléphone*</b></label>
                                                <input type="text" placeholder="votre numéro de téléphone" name="phone" value="{{ old('phone') }}" required>
                                                <span style="color: red">@if($errors->has('phone')) {{ $errors->first('phone') }}@endif</span>


                                        </div>


                                    <div class="input-field">
                                            <label for="psw" data-error="Votre mot de passe est incorect"><b>Mot de Passe*</b></label>
                                            <input type="password" placeholder="Entrer votre mot de passe" name="password" required>
                                            <span style="color: red">@if($errors->has('password')) {{ $errors->first('password') }}@endif</span>

                                    </div>

                                       <button type="submit" value="submit" class="button" style="background-color:black; color:white;">Connexion</button>
                        <a class="btn btn-link" style="color:black" href="{{ route('password.request') }}">
                                    {{ __('Mot de passe oublié?') }}
                        </a> 
                                <div class="form-group row">
                                    <div class="col-md-6 offset-md-4">
                                        <div class="form-check">
                                            <input class="form-check-input" type="hidden" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                            <label class="form-check-label" for="remember">
                                                <!-- {{ __('Se Souvenir de moi') }} -->
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <!-- <div class="container" style="background-color:#f1f1f1">
            <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
            </div> -->
                    </form>            </div>

</div>



                            <div class="small-12 medium-12 large-8 large-offset-1">


                                <form action="/register" method="post">
                                                {{ csrf_field() }}

                                    <h2  style="font-size:20px;">Créer votre compte pour continuer</h2> 
 <div class="row">

                                                        <div class="large-6 p-0-15">

                                                            <label for="usr">Genre*</label>
                                                            <select name="genre" required>  
                                                                        <option value="" disabled selected>Sélectionner Votre Genre</option>
                                                                    
                                                                            <option value="Monsieur">Monsieur</option>
                                                                            <option value="Madame">Madame</option>
                                                                            <option value="Mademoiselle">Mademoiselle</option>
                                                                            <option value="Célibataire">Célibataire</option>
                                                                            <span style="color: red">@if($errors->has('genre')) {{ $errors->first('genre') }}@endif</span>
                
                                                            </select>   
                                                        </div>

                                                            <div class="large-6 p-0-15">
                                                            <label for="usr">Prénom & Nom*</label>
                                                                                    <input type="text" name="name" class="validate" class="form-control" id="usr" placeholder="Nom" value="{{ old('name') }}" required>

                                                                                    <span style="color: red">@if($errors->has('name')) {{ $errors->first('name') }}@endif</span>

                                                            </div>


                                                    </div>


                                                        <div class="row">

                                                                                <div class="large-6 p-0-15">

                                                                                 <label for="usr">Tranche d'âge*</label>
                                                                                <select name="tranche_age" required>  
                                                                                                <option value="" disabled selected>Sélectionner Votre Tranche d'âge</option>
                                                                                            
                                                                                                    <option value="18-30">18-30</option>
                                                                                                    <option value="30-60">30-60</option>
                                                                                                    <option value="60-90">60-90</option>
                                                                                                    <span style="color: red">@if($errors->has('tranche_age')) {{ $errors->first('tranche_age') }}@endif</span>
                                        
                                                                                </select>  

                                                                                </div>

                                                                                <div class="large-6 p-0-15">
                                                                                <label for="usr"  data-error="votre adresse eamil est invalide">Email*</label>
                                                                                <input type="text" name="email" value="{{ old('email') }}" class="validate" class="form-control" id="usr"
                                                                                placeholder="exemple@gmail.com" required>
                                                                                <span style="color: red">@if($errors->has('email')) {{ $errors->first('email') }}@endif</span>

                                                                                </div>
                                                                        

                                                        </div>

                                                    <div class="row">
  
                                                        <div class="large-6 p-0-15">
                                                            <label for="usr">Téléphone*</label>
                                                            <input type="text" name="phone" value="{{ old('phone') }}" class="validate" class="form-control" id="usr" placeholder="Téléphone" required>
                                                            <span style="color: red">@if($errors->has('phone')) {{ $errors->first('phone') }}@endif</span>                                                           
                                                        </div>

                                                            <div class="large-6 p-0-15">
                                                                <label for="quartier_id">Votre quartier*</label>
                                                                <select name="quartier_id" required>  
                                                                    <option value="" disabled selected>Sélectioner votre quartier</option>
                                                                    @foreach($quartiers as $quartier)
                                                                        <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                                                                    @endforeach
                                                                    <span style="color: red">@if($errors->has('quartier_id')) {{ $errors->first('quartier_id') }}@endif</span>
                                                                </select>   
                                                            </div>
                                                    </div>

                                                <div class="row">

                                                                        <div class="large-6 p-0-15">
                                                                        <label for="usr" data-error="Votre mot de passe est incorect">Mot de passe(min:6 caractères)*</label>
                                                                        <input type="password" class="validate" name="password" placeholder="mot de passe" class="form-control" id="usr" required>
                                                                        <span style="color: red">@if($errors->has('password')) {{ $errors->first('password') }}@endif</span>
                                                                        </div>

                                                                                <div class="large-6 p-0-15">

                                                                            <label for="usr" data-error="Votre mot de passe est incorect">Confirmer mot de passe*</label>
                                                                            <input type="password" name="password_confirmation" placeholder="confirmer mot de passe" class="validate" class="form-control" id="usr" required>
                                                                            <span style="color: red">@if($errors->has('password_confirmation')) {{ $errors->first('password_confirmation') }}@endif</span>



                                                                            </div>

                                                </div>

                                                    <div class="form-group row p-0-15">
                                                                                <div class="col-md-6 offset-md-4">
                                                                                    <div class="form-check">
                                                                                        <input class="form-check-input" type="checkbox" name="condiction" value="J'accepte les conditions générales de vente" id="remember" required>

                                                                                        <label class="form-check-label" for="remember">
                                                                                            J'accepte les conditions générales de vente
                                                                                        </label>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="col-md-6 offset-md-4" style="margin-top:10px;">
                                                                                    <div class="form-check">

                                                                                        <label class="form-check-label" for="remember">
                                                                                        <input class="form-check-input" type="checkbox" name="" id="remember">

                                                                                            J'autorise JALÔ à utliser mes informations de contact pour m'informer sur des opportunités dans le cadre strict de ses activités
                                                                                        </label>
                                                                                    </div>
                                                                                </div>

                                                        </div>


                                    
                                    <button type="submit" value="submit" class="button yellow"  style="background-color:yellow; color:black">Créer mon compte</button>
                                </form>


                           </div>           
               
                           </div>  
        </section>
   

        <p>&nbsp;</p>

<p>&nbsp;</p>
  </main>
 <!-- foooter -->
 @include('version3.footer.footer')

<!-- end footer-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171493377-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171493377-1');
</script>

</body>
</html>
