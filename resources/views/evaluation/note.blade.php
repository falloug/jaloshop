<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

    <!--header-->
@include('version3.header.header1')
<!-- end header--> 
    <main class="main">
      <p>&nbsp;</p>
      <div class="row">
        <div class="small-12 medium-12 large-6 large-offset-3">
        <h4 style="margin-left:150px;">Évaluation</h4>
          
          {!! Form::open(['route'=>'evaluation.store', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
            <div class="forms">


              <div class="note" style="display:flex; margin-top:30px;">

                                            <label class="containers" style="font-size:13px;"><strong style="color:yellow; font-size:15px;"><b><img src="/images/trés_mauvaise.png" style="width:50px;" alt=""></b></strong>
                                              <input type="radio" name="note" value="1">
                                              <span class="checkmark"></span>
                                            </label>

                                            <label class="containers" style="font-size:13px;"><strong style="color:yellow; font-size:15px;"><b><img src="/images/mauvaises.png" style="width:50px;" alt=""></b></strong>
                                              <input type="radio" name="note" value="2">
                                              <span class="checkmark"></span>
                                            </label>

                                            <label class="containers" style="font-size:13px;"><strong style="color:yellow; font-size:15px;"><b><img src="/images/acceptable.png" style="width:50px;" alt=""></b></strong>
                                              <input type="radio" name="note" value="3">
                                              <span class="checkmark"></span>
                                            </label>
                                            <label class="containers" style="font-size:13px;"><strong style="color:yellow; font-size:15px;"><b><img src="/images/bonne.png" style="width:50px;" alt=""></b></strong>
                                              <input type="radio" name="note" value="4">
                                              <span class="checkmark"></span>
                                            </label>
                                            <label class="containers" style="font-size:13px;"><strong style="color:yellow; font-size:15px;"><b><img src="/images/exceptionnelle.png" style="width:50px;" alt=""></b></strong>
                                              <input type="radio" name="note" value="5">
                                              <span class="checkmark"></span>
                                            </label>
                 </div><br>
                
                
              <button class="submit-button" style="width:80px; margin-top:30px; margin-left:150px;">OK</button>
             
            </div>
            {!! Form::close() !!} 
            
        </div>
      </div>
      <p>&nbsp;</p>
    </main>
  
    <!-- foooter -->
    @include('version3.footer.footer')

<!-- end footer-->

    <script src="js/jalo/jquery.min.js"></script>
    <script src="js/jalo/what-input.min.js"></script>
    <script src="js/jalo/foundation.min.js"></script>
    <script src="js/jalo/slick.min.js"></script>
    <script src="js/jalo/app.js"></script>
  </body>
</html>