<div class="container">
  <div id="fbCommentCount">
  <span class="fb-comment-count" data-href="{{ Request::url() }}"></span>
  </div>
      <form action="{{ route('posts.update', ['id'=>$id])}}" method="POST">
      {{ csrf_field() }}
      <input type="hidden" name="_method" value="PUT">
      <input type="text" name="commentCount" id="fbFormCommentCount">
      <input type="submit" value="submit comment count">
      <input type="text" name="visitCount" value="{{ $post->visit_count }}" id="postVisitCount">
      </form>
   

    <script>
    var fbCommentCount = document.getElementById('fbCommentCount');
    .getElementByClassName('fb_comments_count');

    setTimeout(function(){
        document.getElementById('fbFormCommentCount').value = fbCommentCount[0].innerHTML;

        var visitCount = document.getElementById('postVisitCount').value;

        var visitCountPlusOne = parseInt(visitCount) + 1;

        document.getElementById('postVisitCount').value = visitCountPlusOne; 

        var $formVar = $('form');

        $.ajax({
            url: $formVar.prop('{{ route('posts.update', ['id'=>$id]) }}'),
            method: 'PUT',
            data: $formVar.serialize()

        });
    }, 1000);
    
    </script>
 </div>