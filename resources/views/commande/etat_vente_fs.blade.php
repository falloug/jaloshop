@extends('layout.adminlayout.design6')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"> 
                            <center><strong class="card-title">Etat de vente {{$freelance->name}}</strong></center>
                        </div>
                        <div class="card-body">
            
                          <!-- Credit Card -->
                          <center><strong>Le nombre de commande de {{$freelance->name}} est: <b>{{count($products)}}</b></strong></center>
                           <center><strong>La marge de {{$freelance->name}} est: <b>{{$marges * 0.2}}</b></strong></center> 

                          <div id="pay-invoice">
                              <div class="card-body">  
                                  
                                  <hr>
                                  <table bproduct="3" class="table table-bproducted table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>Identifiant</th>
                        <th>Produit</th>
                        <th>Montant</th>
                        <th>Freelance</th>
                        <th>Client</th>                       
                        <th>Date de la commande</th>
                        <th>Marge Jalo</th>
                        <th>Marge Freelance</th>
                        

                        </tr>
                        </thead>

                        <tbody>


                        @foreach($products as $product)
                        <tr>
                        <td>
                           {{ $product->ID }}

                        </td>
                        <td>
                        
                            <img src="{{$product->image}}" style="width:50px;"> <br>
                            <b>{{ $product->name }}
                            </b>
                                
                              
                            
                        </td>
                        <td>  
                        <br>
                                Prix: {{ $product->price }}
                                <br>
                            Quantité: {{ $product->quantity }}
                          <br>
                          <b>Total: {{ $product->price * $product->quantity }}</b>
                        </td>
                        <td>{{ $product->Myname }} 
                          <br> 
                           {{ $product->Myphone }}
                        </td>
                        <td>{{ $product->clientName }} 
                          <br> 
                           {{ $product->clientPhone }}
                           <br>
                           {{ $product->ClientAdresse }}
                        </td>
                        
                        <td>{{ date('F d, Y', strtotime($product->date))}} at {{ date('g:ia', strtotime($product->date))}}</td>
                        <td>
                           {{ $product->marge }}

                        </td>
                        <td>
                        {{ $product->marge * 0.2}}

                        </td>
                        
                        </tr>
                        @endforeach

                      </tbody>
                      </table>

                              </div>
                          </div>


                          <div id="pay-invoice">
                              <div class="card-body">  
                                  <h4>Les clients de {{$freelance->name}}</h4>

                                  <br><hr>
                                  <p><b style="color:black;"> Nombres de clients:</b> <strong style="color:yellow;"><b>{{count($clients)}}</b></strong></p>

                                  <hr>
                                  <table bproduct="3" class="table table-bproducted table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>Identifiant</th>
                        <th>Prénom & Nom du client</th>
                        <th>Téléphone du client</th>
                        <th>Adresse du client</th>
                
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($clients as $client)
                        <tr>
                        <td>
                           {{ $client->id }}

                        </td>
                        
                        <td>
                        
                         {{ $client->nom }} 
                         
                        </td>

                        <td>
                        
                        {{ $client->phone }} 
                        
                       </td>

                        <td>
                        
                         {{ $client->adresse }} 
                         
                        </td>
                        
                       
                        
                        </tr>
                        @endforeach

                      </tbody>
                      </table>

                              </div>
                          </div>
                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
                       
        <script>
        $(document).ready(function(){
        
        @foreach($products as $product)
        $("#selectDiv{{$product->id}}").hide();
        $("#showSelectDiv{{$product->id}}").click(function(){  
        $("#selectDiv{{$product->id}}").show();
        });
        $("#productStatus{{$product->id}}").change(function(){
        var status = $("#productStatus{{$product->id}}").val();
        var userID = $("#userID{{$product->id}}").val()
        if(status==""){
        alert("please select an option");
        }else{
        $.ajax({
        url: '{{url("/order/banOrderCommercial")}}',
        data: 'status=' + status + '&userID=' + userID,
        type: 'get',
        success:function(response){
        console.log(response);
        }
        });
        }
        
        });
        @endforeach
        });
        </script>
                  @endsection
         
