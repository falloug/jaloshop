@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"> 
                            <center><strong class="card-title">Etat de vente {{$commercial->name}}</strong></center>
                        </div>
                        <div class="card-body">
            
                          <!-- Credit Card -->
                          <center><strong>Le nombre de commande de {{$commercial->name}} est: <b>{{count($products)}}</b></strong></center>
                           <center><strong>La marge de {{$commercial->name}} est: <b>{{$marges * 0.2}}</b></strong></center> 
                          <div id="pay-invoice">
                              <div class="card-body">  
                                  
                                  <hr>
                                  <table bproduct="3" class="table table-bproducted table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>Identifiant</th>
                        <th>Produit</th>
                        <th>Montant</th>
                        <th>Commercial</th>
                        <th>Client</th>                       
                        <th>Date de la commande</th>
                        <th>Marge Jalo</th>
                        <th>Marge Commercial</th>

                        </tr>
                        </thead>

                        <tbody>


                        @foreach($products as $product)
                        <tr>
                        <td>
                           {{ $product->ID }}

                        </td>
                        <td>
                        
                            <img src="{{$product->image}}" style="width:50px;"> <br>
                            <b>{{ $product->name }}
                            </b>
                                
                              
                            
                        </td>
                        <td>  
                        <br>
                                Prix: {{ $product->price }}
                                <br>
                            Quantité: {{ $product->quantity }}
                          <br>
                          <b>Total: {{ $product->price * $product->quantity }}</b>
                        </td>
                        <td>{{ $product->Myname }} 
                          <br> 
                           {{ $product->Myphone }}
                        </td>
                        <td>{{ $product->clientName }} 
                          <br> 
                           {{ $product->clientPhone }}
                           <br>
                           {{ $product->ClientAdresse }}
                        </td>
                        
                        <td>{{ date('F d, Y', strtotime($product->date))}} at {{ date('g:ia', strtotime($product->date))}}</td>
                        <td>
                           {{ $product->marge }}

                        </td>
                        <td>
                           {{ $product->marge * 0.2}}

                        </td>
                      
                        
                        </tr>
                        @endforeach

                      </tbody>
                      </table>

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
                       
        <script>
        $(document).ready(function(){
        
        @foreach($products as $product)
        $("#selectDiv{{$product->id}}").hide();
        $("#showSelectDiv{{$product->id}}").click(function(){  
        $("#selectDiv{{$product->id}}").show();
        });
        $("#productStatus{{$product->id}}").change(function(){
        var status = $("#productStatus{{$product->id}}").val();
        var userID = $("#userID{{$product->id}}").val()
        if(status==""){
        alert("please select an option");
        }else{
        $.ajax({
        url: '{{url("/order/banOrderCommercial")}}',
        data: 'status=' + status + '&userID=' + userID,
        type: 'get',
        success:function(response){
        console.log(response);
        }
        });
        }
        
        });
        @endforeach
        });
        </script>
                  @endsection
         
