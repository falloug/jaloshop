@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')

  
<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"> 
                            <center><strong class="card-title">Commande des boutiquiers</strong></center>
                        </div>
                        <div class="card-body">
                        <button type="button" style="background:yellow;" class="button"><a href="/exporter_bou/xls">Exporter les clients des boutiquiers</a></button>

                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">  
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>Identifiant</th>
                        <th>Produit</th>
                        <th>Montant</th>
                        <th>Boutiquier</th>
                        <th>Client</th>
                        <th>Adresse de livraison</th>
                        <th>Date de la commande</th>
                        <th>Livreur</th>  
                        <th>Select Livreur</th>
                        <th>Etat</th>
                        <th>Options</th>


                        </tr>
                        </thead>

                        <tbody>


                        @foreach($products as $product)
                        <tr>
                        <td>
                           {{ $product->ID }}

                        </td>
                        <td>
                        
                            <img src="{{$product->image}}" style="width:50px;"> <br>
                            <b>{{ $product->name }}
                               
                            </b>
                               
                              
                            
                        </td>
                        <td> 
                            Prix: {{ $product->price }}
                                    <br> 
                            Quantité: {{ $product->quantity }}
                          <br>
                           <b>Total: {{ $product->price * $product->quantity }}</b>
                        </td>
                        <td>{{ $product->Myname }} 
                          <br> 
                           {{ $product->Myphone }}
                        </td>
                        <td>{{ $product->clientName }} 
                          <br> 
                           {{ $product->clientPhone }}
                           <br>
                           {{ $product->ClientAdresse }}
                        </td>
                        <td>{{ $product->AddressLivraison }} 
                         
                        </td>
                        <td>{{ date('F d, Y', strtotime($product->date))}} at {{ date('g:ia', strtotime($product->date))}}</td>

                        @foreach($livreurs as $livreur)
                        @if($product->livreur_id ==  $livreur->id)
                        <td>{{$livreur->prenom}} {{$livreur->nom}}</td> 
                        @endif
                        @endforeach
                        <td>
                        {!! Form::open(['method'=>'post', 'route'=>['status.boutiquier', $product->ID]]) !!}
                        <div class="liv" style="display:flex;">
                        <select name="livreur_id">
                            @foreach($livreurs as $livreur)
                              <option value="">Selectionner le Livreur</option>
                              <option value="{{$livreur->id}}">{{$livreur->prenom}}{{$livreur->nom}}</option>
                            @endforeach                                       
                        </select>
                         <button type="submit" class="btn btn-fill btn-success">Valider</button>
                         {!! Form::close() !!}
                         </div>
                        </td>
                        <td>@if($product->status=="")
                                      <b style="color:yellow">Commande en cours</b>
                                      @elseif($product->status==0)
                                      <b style="color:yellow">Commande en cours</b>
                                      @elseif($product->status==1)
                                      <b style="color:green">Livraison terminée</b>
                                      @elseif($product->status==2)
                                      <b style="color:red">Commande Annulée</b>
                                      @elseif($product->status==3)
                                      <b style="color:green">Paiement reçu</b>
                                      @elseif($product->status==4)
                                      <b style="color:yellow">Livraison en cours</b>
                                      @elseif($product->status==5)
                                      <b style="color:green">Paiement en attente</b>
          
                                      @else
                                    <b style="color:green">Prêt pour expédition</b>
                                      @endif
                                      <br>
                                      <button id="showSelectDiv{{$product->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>
                                      <div id="selectDiv{{$product->id}}">
                                      <input type="hidden" id="userID{{$product->id}}" value="{{$product->id}}">
                                      <select id="productStatus{{$product->id}}">
                                      <option value="">En cours</option>
                                        <option value="0">Commande en cours</option>
                                        <option value="1">Livraison terminée</option>
                                        <option value="2">Commande Annulée</option>
                                        <option value="3">Paiement reçu</option>
                                        <option value="4">Livraison en cours</option>
                                        <option value="5">Paiement en attente</option>
                                        <option value="6">Prêt pour expédition</option>

                                      </select>
                                      </div>   
                                    </td>
                                    <td><a href=""><button class="btn btn-fill btn-warning">Valider</button></a></td>
                        
                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                 {{ $products->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
                       
                       <script>
                       $(document).ready(function(){
                       
                       @foreach($products as $product)
                       $("#selectDiv{{$product->id}}").hide();
                       $("#showSelectDiv{{$product->id}}").click(function(){  
                       $("#selectDiv{{$product->id}}").show();
                       });
                       $("#productStatus{{$product->id}}").change(function(){
                       var status = $("#productStatus{{$product->id}}").val();
                       var userID = $("#userID{{$product->id}}").val()
                       if(status==""){
                       alert("please select an option");
                       }else{
                       $.ajax({
                       url: '{{url("/order/banOrderBoutiquier")}}',
                       data: 'status=' + status + '&userID=' + userID,
                       type: 'get',
                       success:function(response){
                       console.log(response);
                       }
                       });
                       }
                       
                       });
                       @endforeach
                       });
                       </script>
                  @endsection
         
