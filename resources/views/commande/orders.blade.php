<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Jalo</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="../css/app.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<style>
0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}  

.container1{

  background-color: lightyellow;
}
</style>

<body>

<header class="header">

            <div class="header-top">
                <div class="row">
                    <div class="small-12 medium-4 large-2">
                    <img src="../images/logo-yellow.jpeg" class="logo" alt="">
                    </div>
                        
                </div>
            </div>

        <div class="row">

            <div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">

                <div class="top-bar-right">
                    <ul class="menu">
                    <li><a href="#bucket-container" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span>{{Cart::count()}}</span></a></li>
                    </ul>
                </div>
            </div>
        </div>

</header>

<main class="main">



          <div class="container1">

<div class="row" style="margin-left:50px; margin-top:50px;">
  <div class="col-lg-3">

  </div>

  <div class="col-lg-7" style="margin-left:100px;">
<h3>Les Commandes</h3>

<ul>
    @foreach($orders as $order)

    <li>
         <h4>Commander par  <br> Prix Total {{$order->total}}</h4><hr>

         <form action="{{ route('toggle.deliver', $order->id)}}" method="POST" class="pull-right" id="deliver-toggle">
         
         {{csrf_field()}}
         <div class="col-md-6 offset-md-4">

         <div class="form-check">
             <input class="form-check-input" type="checkbox" name="status" value="1" {{$order->status==1?"checked":"" }}>
                <label class="form-check-label" for="status">Livrer</label>
                <input type="submit" value="submit"> 
 
        </div>

        </div>


         
         </form>

         <h5>Les Items</h5>
         <table class="table table-bordered">
          <tr>
          <th>Quantité</th>
          <th>Prix</th>

          </tr>
          @foreach($orders as $item)

          <tr>
          <td>{{ $item->qty }}</td>
          <td>{{ $item->total }}</td>

          </tr>
         @endforeach
         </table>
    </li>
    @endforeach
</ul>
  </div>
</div>
</div>
  

        </main>
         
</body>
</html>


