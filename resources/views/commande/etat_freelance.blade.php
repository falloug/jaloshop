@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Freelances</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom du freelance</th>
                        <th>Téléphone du freelance</th>
                        <th>Quartier du freelance</th>
                        <th colspan="2"><span style="margin-left:40px;">Etat de vente</span></th>

                        </tr>
                        </thead>

                        <tbody>


                        @foreach($freelances as $freelance)
                        <tr>
                        <td>{{ $freelance->id }}</td>
                        <td>{{ $freelance->name}}</td>
                        <td>{{ $freelance->phone}}</td>
                        <td>{{ $freelance->quartier_nom}}</td>
                        <td>
                        <a href="{{route('etat_vente_f.vente', $freelance->id)}}" class="btn btn-warning">
                           ETAT
                         </a>   
                        </td>
                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      {{ $freelances->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
         
