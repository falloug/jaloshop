<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  </head>   
  <body>

    <!--header-->
       
    <header class="header">
        @include('version3.header.header2')
  
        </header>
    <!-- end header-->   

                                

    <main class="main">
      <p>&nbsp;</p>
      @if(Cart::count()!="0")

      <div class="row">
      <table class="unstriped">
  @foreach($cartItems as $pro)
  <tbody>
    <tr>
      <td style="width: 180px;"><img src="{{url('jaloimage', $pro->options->img) }}"
                                     class="img-responsive pull-left" width="100px" /></td>
      <td><a href="{{url('details')}}/{{$pro->id}}" style="color:black">
                                          <b>{{ucwords($pro->name)}}</b>
                                        </a></td>

      <td>
      <h6>Prix ​​unitaire</h6>
                                      <p>{{$pro->price}} F CFA
                                      </p>
                                     
      </td>
      
      <td  style="width: 120px;">

          {!! Form::open(['route'=> ['cart.update', $pro->rowId], 'method' => 'PUT']) !!}
          <div style="display: flex; justify-content: flex-start; align-items: center;">


          <span>


            <input type="number" name="qty" max="10" min="1" style="width:80px; margin-bottom:0; margin-right:15px;"
                                              value="{{$pro->qty}}" class="qty-fill"
                                              id="upCart{{$pro->id}}">
            </span>

            <span>
            <button type="submit" style="float: left; border:radius:50%;" class="qty-fill"><i class="material-icons" style="background-color:black; color:yellow">&#xe90a;</i></button>

            </span>
          </div>
                                            




          {!! form::close() !!}

      </td>
      <td style="width: 80px;">


          <form action="{{route('cart.destroy', $pro->rowId)}}" method="Post">
                    {{ csrf_field()}}
                    {{ Method_field('DELETE')}}
                    <button type="submit" class="qty-fill" ><i class="material-icons" style="background-color:yellow; color:black; margin-left:10px;">close</i>
              </button>
          </form>
      </td>
      <td>
                                    <h6 class="redtext">
                                      Total Achat: {{$pro->subtotal}}
                                        <br>
                                      </h6>
      </td>
    </tr>
  </tbody>
  @endforeach

  <tr>
    <td>
                  <div class="col-xs-12 col-sm-12 col-md-6">
                          
                  </div>
    </td>

    <td>
                          <div class="col-sm-4" id="cartTotal">
                            <div class="cart-total" >
                                <table>
                                  <tbody>
                                    <tr>
                                      <td>Total Achat</td>
                                      <td>{{Cart::subtotal()}} F CFA</td>
                                    </tr>
                                    <tr>
                                      <!-- <td>Tax (%)</td>
                                      <td>$ {{Cart::tax()}}</td> -->
                                    </tr>
                                   
                                  
                                    <!-- <tr>
                                      <td>Montant total</td>
                                      <td>{{Cart::total()}} F CFA</td>
                                    </tr> -->
                                  </tbody>
                                </table>
                          
                        <button><a href="{{url('checkouts')}}" class="button" style="background-color:black; color:yellow; font-size:18px;">Passer Commande</a></button> 
                                    <button><a href="{{url('/catalogues')}}"
                              class="button" style="background-color:black; color:yellow; font-size:18px;" font-size:18px;>Continuer vos achats</a></button>     



                         </div>
                          </div>
                              @else
                <div class="row">
                   <div class="col-md-2 col-md-offset-5 top25">
                    <img src="images/empty-cart-page-doodle.png"
                    class="img-response"/>
                    <br><br>
                    <p style="text-align:center">Votre panier est vide<br><br>
                     <button><a href="{{url('/catalogues')}}"
                   class="button" style="background-color:black; color:yellow; font-size:18px;">Continuer vos achats</a></button> 
                    </p>

                  </div>
                </div>
                @endif
    </td>

  </tr>
</table>
      </div>

           

      <p>&nbsp;</p>
    </main>

    <!-- foooter -->
    @include('version3.footer.footer')

    <!-- end footer-->

   <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/flickity.pkgd.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>

  $(document).ready(function(){
    $("#CartMsg").hide();
    //$('#CartTotal').hide();
    @foreach($cartItems as $pro)
    $("#upCart{{$pro->id}}").on('change keyup', function(){
      var newQty = $("#upCart{{$pro->id}}").val();
      var rowID = $("#rowID{{$pro->id}}").val();
      $.ajax({
          url:'{{url("/cart/modified")}}',
          data:'rowID=' + rowID + '&newQty=' + newQty,
          type:'get',
          success:function(response){
            $("#CartMsg").show();
            console.log(response);
            $("#CartMsg").html(response);
          }
      });
    });
    @endforeach
  $('#coupon_btn').click(function(){
      var coupon_id = $('#coupon_id').val();
      //alert(coupon_id);
      $.ajax({
        url:'{{url('/checkCoupon')}}',
        data: 'code=' + coupon_id,
        success:function(res){
       // alert(res);
       $('#cartTotal').html(res);
        }
      })
  });
   
  });
   </script> 
  </body>
</html>
