<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Jalo</title>
<link rel="stylesheet" href="../css/app.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<style>
0.8;
}
    
.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}

.container1{

  background-color: lightyellow;
}
</style>

<body>

<header class="header">

            <div class="header-top">
                <div class="row">
                    <div class="small-12 medium-4 large-2">
                    <a href="/"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
                    </div>
                        
                </div>
            </div>

        <div class="row">

            <div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">

                <div class="top-bar-right">
                    <ul class="menu">
                    <li><a href="{{route('cart.index')}}" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span>{{Cart::count()}}</span></a></li>
                    </ul>
                </div>
            </div>
        </div>

</header>

<main class="main">



         <div class="row">
         <form action="your-server-side-code" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class=""
    data-key="pk_test_TYooMQauvdEDq54NiTphI7jx"
    data-amount="999"
    data-name="Stripe.com"
    data-description="Example charge"
    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
    data-locale="auto"
    data-zip-code="true">
  </script>
</form>
<div class="small-12 medium-6 large-10 p-10">
<center><h4 style="margin-top:50px;">Récapulatif de la commande</h4></center>
<table border="2" class="table table-bordered table-responsive" style="margin-top:50px; margin-left:100px;">

<thead>
<tr>
<th>Produit</th>
<th>Prix</th>
<th>Quantité</th>
<th colspan="2">Actions</th>
</tr>
</thead>

<tbody>
@foreach($cartItems as $cartItem)
<tr>
<td>{{ $cartItem->name }}</td>
<td>{{ $cartItem->price }}</td>
<td width="50px">

{!! Form::open(['route'=> ['cart.update', $cartItem->rowId], 'method' => 'PUT']) !!}
<input name="qty" type="text" value="{{ $cartItem->qty }}">

</td>
  
<td>
<button type="submit" style="float: left; border:radius:50%;  width:50px; height:50px;" class="button success small"><i class="material-icons" style="margin-left:-12px;">&#xe90a;</i>
</button>

        {!! form::close() !!}

                <form action="{{route('cart.destroy', $cartItem->rowId)}}" method="Post">
                {{ csrf_field()}}
                {{ Method_field('DELETE')}}
                <button type="submit" class="button danger small" style="background:red; border-radius:50%; margin-right:0px; width:50px; height:50px;"><i class="material-icons" style="margin-left:-12px;">close</i>
</button>
</form>
</td>
</tr>
<tr>
<td>Montant: 1500 F CFA</td>
</tr>
@endforeach
<tr>
<td></td>

<td>

Total: {{Cart::total()}} F CFA


</td>
<td colspan="2">Total Produit: {{Cart::count()}}</td>
<td></td>
</tr>
</tbody>
</table>
<strong style="margin-left:200px;">MERCI!  <p style="margin-left:200px;">{{ Auth::user()->name }}</p></strong>

<div class="button" style="display:flex; margin-left:400px; background-color:white; width:400px;">
<a href="#"><button class="add-bucket-btn waves-effect waves-black add_to_cart" id="add_btn" onClick="compteOM()" style="margin-left:30px;">Payer maintenant</button>
<p><span>Total:Total: {{Cart::total()}} F CFA</span></p>
<div class="compte">
<ol>
<li>Compte Orange Money</li>
<li>Api Orange Money</li>
<li>Verification Solde</li>
<li>Approuver la livraison</li>
</ol>
</div>
</a> 
<a href="{{route('home')}}"><button class="add-bucket-btn waves-effect waves-black add_to_cart" id="add_btn" style="margin-left:10px;">Payer à la livraison</button>
<span>Total:{{Cart::total()}} F CFA</span>
</a> 
</div>
                    <div class="alert alert-success" id="successMsg"></div>
                    </div>
                    </div>
                </div>

        </div>

  

        </main>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>

         <script>
         $('.compte').hide();
         function compteOM(){
             $('.compte').show();
         }
         </script>
</body>
</html>


