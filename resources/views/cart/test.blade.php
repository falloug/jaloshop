<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

  </head>   
  <body>

    <!--header-->
       
    <header class="header">
         @include('version3.header.header')
  
        </header>
    <!-- end header-->   

                                

    <main class="main">

                                <div class="row">
                                          <div class="col-sm-12">
                                                       <div class="breadcrumbs">
                                                        <ul>
                                                            <li><a href="{{url('/')}}" style="color:black">Home </a></li>
                                                            <li><span class="dot">/</span>
                                                              <a href="" style="color:black">cart</a>
                                                          </ul>
                                                        </div>
                                          </div>
                                </div><br>

                                 <!-- design of cart page -->

                <section class="row">
                <div class="small-12 medium-12 large-8 large-offset-2" style="display:flex;">

                        <div class="col-sm-3">
                            <div class="blk-box">
                                <div class="blk-boxHd">Chariot</div>
                                <div class="blk-boxTxt hidden-sm">Voulez-vous regarder sur commande?</div>
                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="wht-box">
                              <div class="wht-boxHd">Facturation &amp; livraison</div>
                              <div class="wht-boxTxt hidden-sm">Où devrions-nous envoyer cette commande?</div>
                            </div>
                        </div>
                        
                          <div class="col-sm-3">
                            <div class="wht-box">
                              <div class="wht-boxHd">Examen de la commande</div>
                              <div class="wht-boxTxt hidden-sm">Comment voulez-vous payer votre commande?</div>
                            </div>
                          </div>
                         
                        <div class="col-sm-3">
                          <div class="wht-box">
                            <div class="wht-boxHd">Confirmation</div>
                            <div class="wht-boxTxt hidden-sm">Confirmer votre commande</div>
                          </div>
                        </div>
                        
                        </div>
          </section>
          <hr>
          @if(Cart::count()!="0")
          <section class="row">

                  <div class="cart">
                      <div class="col-sm-12">
                        <h2 align="center">Mon Panier</h2>
                        <div class="row" style="margin-top:50px;">
                            <div class="col-sm-8">
                              @if(isset($msg))
                              <div class="alert alert-info">{{$msg}}</div>
                              @endif
                              <div class="alert alert-info" id="CartMsg"></div>

                            @foreach($cartItems as $pro)

                              <div class="cart-row">
                                  <div class="row" style="margin-left:180px;">

                                    <div class="col-xs-12 col-sm-6 col-md-6 text-center">
                                    <img src="{{url('images', $pro->options->img) }}"
                                     class="img-responsive pull-left" width="100px" />
                                      <span class="pull-left top20">
                                      <a href="{{url('details')}}/{{$pro->id}}" style="color:black">
                                          <b>{{ucwords($pro->name)}}</b>
                                        </a>
                                      </span>

                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3" style="margin-left:50px;">
                                      <input type="hidden" value="{{$pro->rowId}}"
                                       id="rowID{{$pro->id}}"/>
                                    <div class="cart-qty"> <span>Quantité : </span>
                                    {!! Form::open(['route'=> ['cart.update', $pro->rowId], 'method' => 'PUT']) !!}
                                        <input type="number" name="qty" max="10" min="1" style="width:80px;"
                                        value="{{$pro->qty}}" class="qty-fill"
                                        id="upCart{{$pro->id}}">
                                        <button type="submit" style="float: left; border:radius:50%;" class="qty-fill"><i class="material-icons" style="background-color:black; color:yellow">&#xe90a;</i>

                                      </div>
                                      {!! form::close() !!}
                                      <form action="{{route('cart.destroy', $pro->rowId)}}" method="Post">
                {{ csrf_field()}}
                {{ Method_field('DELETE')}}
                <button type="submit" class="qty-fill" ><i class="material-icons" style="background-color:yellow; color:black; margin-left:10px;">close</i>
              </button>
              </form>
<!--                                       <a class="cart-remove btn" ><i class="material-icons" style="background-color:black; color:yellow" >&#xe90a;</i></a>
 -->                                      <!-- <a href="{{url('cart/removes')}}/{{$pro->rowId}}"
                                         class="cart-remove btn" ><i class="material-icons" style="background-color:yellow; color:black">close</i></a> -->
                                    </div>
                                    <div class="col-xs-12 col-sm-3 col-md-3" style="margin-left:150px;">
                                      <h6>Prix ​​unitaire</h6>
                                      <p>{{$pro->price}} F CFA
                                      </p>

                                      <hr/>
                                      <h6 class="redtext">
                                      Total Achat: {{$pro->subtotal}}
                                        <br>
                                        Total (taxe incluse): {{$pro->total}}
                                      </h6>
                                    </div>
                                  </div>
                              </div>
                              @endforeach

                              <div class="col-xs-12 col-sm-12 col-md-6" style="margin-left:180px;">
                                <div class="chk-coupon">
                                  <label>Code de coupon (si possible)</label>
                                  <div class="input-group">
                                      <input type="text" class="form-control" id="coupon_id" style="width:100px;">
                                      <span class="input-group-btn">
                                      <input type="button" class="btn btn fld-btn" style="background-color:yellow; color:black; margin-top:40px; margin-left:-110px;"
                                      value="Utiliser ce code" id="coupon_btn"/>
                                      </span>
                                  </div>
                                </div>
                            </div>
                              <div class="clearfix"></div>
                            </div>
																	<div class="col-sm-4" id="cartTotal" style="margin-left:80px;">
																	<div class="cart-total" >
																			<center><h4>Montant total</h4></center>
																			<table>
																				<tbody>
																					<tr>
																						<td>Total Achat</td>
																						<td>{{Cart::subtotal()}} F CFA</td>
																					</tr>
																					<tr>
																						<!-- <td>Tax (%)</td>
																						<td>$ {{Cart::tax()}}</td> -->
																					</tr>
																				
																				
																					<tr>
																						<td>Montant total</td>
																						<td>{{Cart::total()}} F CFA</td>
																					</tr>
																				</tbody>
																			</table>
																
															<center><a href="{{url('checkouts')}}" class="btn check_out btn-block" style="background-color:black; color:yellow">Passer Commande</a></center>
																			</div>
																		</div>
                        </div>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                </div>
                @else
                <div class="row">
                   <div class="col-md-2 col-md-offset-5 top25">
                    <img src="images/empty-cart-page-doodle.png"
                    class="img-response"/>
                    <br><br>
                    <p style="text-align:center">Rien dans le sac<br><br>
                    <a href="{{url('/catalogues')}}"
                    class="btn btn-fill btn-primary">Continuer vos achats</a>
                    </p>

                  </div>
                </div>
                @endif
          </section>

    </main>

    <!-- foooter -->
    @include('version3.footer.footer')

    <!-- end footer-->

   <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/flickity.pkgd.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script>
  $(document).ready(function(){
    $("#CartMsg").hide();
    //$('#CartTotal').hide();
    @foreach($cartItems as $pro)
    $("#upCart{{$pro->id}}").on('change keyup', function(){
      var newQty = $("#upCart{{$pro->id}}").val();
      var rowID = $("#rowID{{$pro->id}}").val();
      $.ajax({
          url:'{{url("/cart/modified")}}',
          data:'rowID=' + rowID + '&newQty=' + newQty,
          type:'get',
          success:function(response){
            $("#CartMsg").show();
            console.log(response);
            $("#CartMsg").html(response);
          }
      });
    });
    @endforeach
  $('#coupon_btn').click(function(){
      var coupon_id = $('#coupon_id').val();
      //alert(coupon_id);
      $.ajax({
        url:'{{url('/checkCoupon')}}',
        data: 'code=' + coupon_id,
        success:function(res){
       // alert(res);
       $('#cartTotal').html(res);
        }
      })
  });
   
  });
    
  </body>
</html>
