<div class="greyBg" style="width:400px;">
      <div class="container" >
  		<div class="wrapper">
                            
<table border="2" class="table table-bordered table-responsive" style="margin-top:50px;">

<thead>
<tr>
<th>Image</th>
<th>Produit</th>
<th>Prix</th>
<th>Quantité</th>
<th colspan="2">Actions</th>
</tr>
</thead>

<tbody>
@foreach($cartItems as $pro)
<tr>
<td><img src="{{url('jaloimage', $pro->options->img) }}"
                                     class="img-responsive pull-left" width="50px" /></td>
<td>{{ $pro->name }}</td>
<td>{{ $pro->price }}</td>
<td width="50px">

{!! Form::open(['route'=> ['cart.update', $pro->rowId], 'method' => 'PUT']) !!}
<input name="qty" type="text" value="{{ $pro->qty }}">

</td>
  
<td>
            <button type="submit" class="button success small" style="background-color:black;"><i class="material-icons" style="color:yellow;">&#xe90a;</i>
            </button>

              {!! form::close() !!}

                <form action="{{route('cart.destroy', $pro->rowId)}}" method="Post">
                {{ csrf_field()}}
                {{ Method_field('DELETE')}}
                <button type="submit" class="button danger small" style="background-color:yellow;"><i class="material-icons" style="color:black;">close</i>
                </button>
                </form>
</td>
</tr>
@endforeach
</table>
<p>Montant: {{Cart::subtotal()}} F CFA</p>
<p>Somme Finale: {{Cart::total()}}</p>
<p colspan="2">Total Produit: {{Cart::count()}}</p>
<a href="{{url('checkouts')}}"><button class="button" id="add_btn" style="margin-left:250px; background-color:yellow; color:black;">Passer Commande</button></a>


                           


  <!-- design of cart page  end -->
</div>
</div>
</div>
