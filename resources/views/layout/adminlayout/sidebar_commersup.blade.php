<!-- Left Panel -->

    <aside id="left-panel" class="left-panel">
        <nav class="navbar navbar-expand-sm navbar-default">

            <div class="navbar-header">
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#main-menu" aria-controls="main-menu" aria-expanded="false" aria-label="Toggle navigation">
                    <i class="fa fa-bars"></i>
                </button>
                <a class="navbar-brand" href="./"><img src="../images/logo-yellow.jpeg" alt="Logo"></a>
                <a class="navbar-brand hidden" href="./"><img src="../images/logo-yellow.jpeg" alt="Logo"></a>
            </div>

            <div id="main-menu" class="main-menu collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li class="active">
                        <a href="/admin/dashboard"> <i class="menu-icon fa fa-dashboard"></i>Dashboard </a>
                    </li>
                    <h3 class="menu-title">UI elements</h3><!-- /.menu-title -->

                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="/listerProduct" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><i class="menu-icon fa fa-laptop"></i>Produits</a>
                        <ul class="sub-menu children dropdown-menu">
                           
                            <li><i class="fa fa-bars"></i><a href="/product/create">Ajouter Produit</a></li>
                            <li><i class="fa fa-bars"></i><a href="/listerProduct">Les Produits</a></li>
                            <li><i class="fa fa-bars"></i><a href="/status_product">Status Produit</a></li>


                        
                             <li><i class="fa fa-fire"></i><a href="/users">Utilisateurs</a></li>
                            
                        </ul>
                    </li>
 -->
                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="/listerCategory" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Categories</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/category/create">Ajouter Categorie</a></li>
                            <li><i class="fa fa-table"></i><a href="/listerCategory">Categories</a></li>
                        </ul>
                    </li> -->

                     <!-- <li class="menu-item-has-children dropdown">
                        <a href="/listerCategory" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Visuals</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/ajout_visual">Ajouter Visual</a></li>
                            <li><i class="fa fa-table"></i><a href="/lister_visual">Les Visuals</a></li>
                        </ul>
                    </li> -->

                     <!-- <li class="menu-item-has-children dropdown">
                        <a href="/listerCategory" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Partenaires</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/ajout_partenaire">Ajouter Partenaire</a></li>
                            <li><i class="fa fa-table"></i><a href="/lister_partenaire">Les Partenaires</a></li>
                        </ul>
                    </li>
 -->
                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Commandes</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/commandes_pro">Les Commandes</a></li>
                        </ul>
                    </li> -->

                     <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Utilisateurs</a>
                        <ul class="sub-menu children dropdown-menu">
                        <li><i class="fa fa-fire"></i><a href="/users">Les Utilisateurs</a></li>
                        </ul>
                    </li> -->

                    <li class="menu-item-has-children dropdown">
                        <a href="/lister/b_commercesup" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Boutiquier</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/ajout/b_commercesup">Ajouter Boutiquier</a></li>
                            <li><i class="fa fa-table"></i><a href="/lister/b_commercesup">Boutiquiers</a></li>
                        </ul>
                    </li>

                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="/lister/c_commercesup" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Quartier</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/ajout/c_commercesup">Ajouter Quartier</a></li>
                            <li><i class="fa fa-table"></i><a href="/lister/c_commercesup">Quartiers</a></li>
                        </ul>
                    </li> -->

                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="/listerFournisseur" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Fournisseur</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/fournisseur/create">Ajouter Fournisseur</a></li>
                            <li><i class="fa fa-table"></i><a href="/listerFournisseur">Fournisseurs</a></li>
                        </ul>
                    </li> -->

                    <li class="menu-item-has-children dropdown">
                        <a href="/lister/c_commercesup" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Commerciaux</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/ajout/c_commercesup">Ajouter Commercial</a></li>
                            <li><i class="fa fa-table"></i><a href="/lister/c_commercesup">Commerciaux</a></li>
                        </ul>
                    </li>

                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="/listerFourni_Vente" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Vente des Fournisseurs</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/fournisseur_vente/create">Ajouter des Ventes</a></li>
                            <li><i class="fa fa-table"></i><a href="/listerFourni_Vente">Vente Fournisseur</a></li>
                            <li><i class="fa fa-table"></i><a href="/my-searchs">Vente d'un Fournisseur</a></li>

                        </ul>
                    </li> -->

                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="/listerFourni_Vente" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-table"></i>Vente des Boutiquiers</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="fa fa-table"></i><a href="/boutique_vente/create">Ajouter des Ventes</a></li>
                            <li><i class="fa fa-table"></i><a href="/listerBoutique_Vente">Vente Boutiquier</a></li>
                            <li><i class="fa fa-table"></i><a href="/my-search">Vente d'un Boutiquier</a></li>

                        </ul>
                    </li> -->

                    <h3 class="menu-title"></h3><!-- /.menu-title -->
                    <!-- <li class="menu-item-has-children dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="menu-icon fa fa-glass"></i>Pages</a>
                        <ul class="sub-menu children dropdown-menu">
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-login.html">Login</a></li>
                            <li><i class="menu-icon fa fa-sign-in"></i><a href="page-register.html">Register</a></li>
                            <li><i class="menu-icon fa fa-paper-plane"></i><a href="pages-forget.html">Forget Pass</a></li>
                        </ul>
                    </li> -->
                </ul>
            </div><!-- /.navbar-collapse -->
        </nav>
    </aside>

    <!-- /#left-panel -->