<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">


<style>
* {box-sizing: border-box}
body {font-family: "Lato", sans-serif;}

/* Style the tab */
.tab {
    float: left;
    border: 1px solid #ccc;
    background-color: #f1f1f1;
    width: 100%;
    height: 60px;
}

/* Style the buttons inside the tab */
.tab button {
    display: flex;
    background-color: black;
    color: white;
    padding: 22px 16px;
    width: 100%;
    border: none;
    outline: none;
    text-align: left;
    cursor: pointer;
    font-size: 10px;
    height:30px;
}

/* Change background color of buttons on hover */
.tab button:hover {
    color: #ddd;
}

/* Create an active/current "tab button" class */
.tab button.active {
   color: yellow;
}

/* Style the tab content */
.tabcontent {
    float: left;
    padding: 0px 12px;
    border: 1px solid #ccc;
    border-left: none;
    height: 380px;
    display: none;
    margin-left: 130px;
    background-color:white;
    color:yellow;
    width:350px;
}

/* Clear floats after the tab */
.clearfix::after {
    content: "";
    clear: both;
    display: table;
}
ul li{
    color:gray;
    font-size:11px;
}
a :hover{
    color:yellow;
}
</style>

</head>
<body>
<div class="tab" style="dsiplay:flex;">
  <button class="tablinks" onmouseover="openCity(event, 'London')">Électroménager</button>
  <button class="tablinks" onmouseover="openCity(event, 'Paris')">Papeterie</button>
  <button class="tablinks" onmouseover="openCity(event, 'Tokyos')">Mobilier</button>
  <button class="tablinks" onmouseover="openCity(event, 'Bureautique')">Bureautique</button>
  <button class="tablinks" onmouseover="openCity(event, 'High-tech')">High-tech</button>
  <button class="tablinks" onmouseover="openCity(event, 'Informatique')">Informatique</button>
  <button class="tablinks" onmouseover="openCity(event, 'Alimentaires')">Alimentaires</button>
  <button class="tablinks" onmouseover="openCity(event, 'Jouets')">Jouets,Enfants,Bébé</button>
  <button class="tablinks" onmouseover="openCity(event, 'Beauté')">Beauté,Corps,Cheveux</button>
  <button class="tablinks" onmouseover="openCity(event, 'Mode')">Mode et Habillement</button>


</div>

<div id="London" class="tabcontent">
  <ul>
  <a href="#"><li>Réfrigérateur</li></a>
  <a href="#"><li>Congélateur</li></a>
  <a href="#"><li>Cuisinière</li></a>
  <a href="#"><li>Plaque chauffante</li></a>
  <a href="#"><li>Micro-onde</li></a>
  <a href="#"><li>Hotte aspirante</li></a>
  <a href="#"><li>Machine à laver</li></a>
  <a href="#"><li>Bouilloire</li></a>
  <a href="#"><li>Blindeur</li></a>
  <a href="#"><li>Ventilateur</li></a>
  <a href="#"><li>Slip</li></a>
  <a href="#"><li>Télévision</li></a>
  <a href="#"><li>Aspirateur</li></a>
  <a href="#"><li>Machine à café</li></a>
  <a href="#"><li>Faire à repasser</li></a>
  </ul>
</div>

<div id="Paris" class="tabcontent">
  <ul>
  <a href="#"><li>Cahiers</li></a>
  <a href="#"><li>Livres scolaires</li></a>
  <a href="#"><li>Stylos,crayons,gommes</li></a>
  <a href="#"><li>Matériels géométriques</li></a>
  <a href="#"><li>Protège-Cahiers et Couvre-Livres</li></a>
  <a href="#"><li>Enveloppe</li></a>
  <a href="#"><li>Bloc-note</li></a>
  <a href="#"><li>Agenda</li></a>
  <a href="#"><li>Classeurs,Intercalaires</li></a>
  <a href="#"><li>Chemisiers, Trieurs</li></a>
  </ul>
</div>
<div id="Tokyos" class="tabcontent">
  <ul>
  <a href="#"><li>Rangement</li></a>
  <a href="#"><li>Sièges et fauteuils</li></a>
  <a href="#"><li>Salle de bain</li></a>
  <a href="#"><li>Bureau</li></a>
  <a href="#"><li>Luminaires</li></a>
  </ul>
</div>
<div id="Bureautique" class="tabcontent">
  <ul>
  <a href="#"><li>Ordinateurs fixes</li></a>
  <a href="#"><li>Imprimantes</li></a>
  <a href="#"><li>Scanners</li></a>
  <a href="#"><li>Photocopieurs</li></a>
  <a href="#"><li>Clé USB, Carte mémoire, disque dure</li></a>
  <a href="#"><li>Cartouches,Tonner</li></a>
  
  </ul>
</div>
<div id="High-tech" class="tabcontent">
  <ul>
  <a href="#"><li>Téléphone portable et fixe</li></a>
  <a href="#"><li>Tv et Home cinéma</li></a>
  <a href="#"><li>Photos et Caméscopes</li></a>
  <a href="#"><li>Accessoires High-tech</li></a>
  
  </ul>
</div>

<div id="Informatique" class="tabcontent">
  <ul>
  <a href="#"><li>Ordinateurs portables</li></a>
  <a href="#"><li>Tablettes Tactiles</li></a>
  <a href="#"><li>Accessoires Informatiques</li></a>
  <a href="#"><li>Fournitures Scolaires et Bureau</li></a>
  </ul>
</div>

<div id="Alimentaires" class="tabcontent">
  <ul>
  <a href="#"><li>Viandes et Poissons</li></a>
  <a href="#"><li>Produits Laitiers</li></a>
  <a href="#"><li>Épices et Condiments</li></a>
  <a href="#"><li>Fruits et Légumes</li></a>
  <a href="#"><li>Petit-Déjeuner</li></a>
  <a href="#"><li>Gouter</li></a>
  <a href="#"><li>Pattes Alimentaires</li></a>
  <a href="#"><li>Produits Frais</li></a>
  <a href="#"><li>Céréales</li></a>
  <a href="#"><li>Huile</li></a>
  <a href="#"><li>Sucrerie</li></a>
  <a href="#"><li>Conserve</li></a>
  <a href="#"><li>Riz</li></a>

  </ul>
</div>

<div id="Jouets" class="tabcontent">
  <ul>
  <a href="#"><li>Jouets,</li></a>
  <a href="#"><li>Bébés et puéricultures</li></a>
  <a href="#"><li>Vétements et Chaussures</li></a>
  <a href="#"><li>Livre D'éveil </li></a>
  </ul>
</div>

<div id="Beauté" class="tabcontent">
  <ul>
  <a href="#"><li>Maquillage</li></a>
  <a href="#"><li>Soins du corps</li></a>
  <a href="#"><li>Soins du visage</li></a>
  <a href="#"><li>Parfums</li></a>
  <a href="#"><li>Huiles</li></a>
  <a href="#"><li>Pommade</li></a>
  <a href="#"><li>Shampoings et Leave-in</li></a>
  <a href="#"><li>Mèches,greffage</li></a>

  </ul>
</div>

<div id="Mode" class="tabcontent">
  <h5>Femme</h5>
          <ul>
          <a href="#"><li>Vetements</li></a>
          <a href="#"><li>Chaussures</li></a>
          <a href="#"><li>Accessoires</li></a>
          </ul>


  <h5>Homme</h5>
          <ul>
          <a href="#"><li>Vetements</li></a>
          <a href="#"><li>Chaussures</li></a>
          <a href="#"><li>Accessoires</li></a>
          </ul>



</div>



<div class="clearfix"></div>

<script>
function openCity(evt, cityName) {
    var i, tabcontent, tablinks;
    tabcontent = document.getElementsByClassName("tabcontent");
    for (i = 0; i < tabcontent.length; i++) {
        tabcontent[i].style.display = "none";
    }
    tablinks = document.getElementsByClassName("tablinks");
    for (i = 0; i < tablinks.length; i++) {
        tablinks[i].className = tablinks[i].className.replace(" active", "");
    }
    document.getElementById(cityName).style.display = "block";
    evt.currentTarget.className += " active";
}
</script>
</body>
</html> 

