@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Approvisionnements</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>


                                <table bapprovisionnement="3" class="table table-bapprovisionnemented table-responsive" style="margin-top:50px;">

                                <thead>
                                <tr>
                                <th>Produit</th>
                                <th>Catégorie</th>
                                <th>Quantité</th>
                                <th>Date</th>
                                <th>Boutiquier</th>
                                <th>Boutiquier phone</th>
                                <th>Boutiquier quartier</th>
                                <th>status</th>  
                                <th>Détails</th>
                                </tr>
                                </thead>

                                <tbody>

                    @foreach($approvisionnements as $approvisionnement)

                        <td><img src="{{$approvisionnement->image}}" style="width:50px;"> <br>
                         {{$approvisionnement->libelle_produit }} 
                       
                        </td>
                        <td>{{$approvisionnement->categorie}}</td>
                        <td>{{$approvisionnement->quantite}}</td>
                        <td>{{$approvisionnement->date_ajout}}</td>
                        <td>{{$approvisionnement->boutiquier}}</td>
                        <td>{{$approvisionnement->boutiquier_phone}}</td>
                        <td>{{$approvisionnement->boutiquier_quartier}}</td>

                        <td>@if($approvisionnement->status=="")
                                      <b style="color:yellow">Approvisionnement en cours</b>
                                      @elseif($approvisionnement->status==0)
                                      <b style="color:yellow">Approvisionnement en cours</b>
                                      @elseif($approvisionnement->status==1)
                                      <b style="color:green">Livraison terminée</b>
                                      @elseif($approvisionnement->status==2)
                                      <b style="color:red">Approvisionnement Annulée</b>
                                      @elseif($approvisionnement->status==3)
                                      <b style="color:green">Paiement reçu</b>
                                      @elseif($approvisionnement->status==4)
                                      <b style="color:yellow">Livraison en cours</b>
                                      @elseif($approvisionnement->status==5)
                                      <b style="color:green">Paiement en attente</b>
          
                                      @else
                                    <b style="color:green">Prêt pour expédition</b>
                                      @endif
                                      <br>
                                      <button id="showSelectDiv{{$approvisionnement->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>
                                      <div id="selectDiv{{$approvisionnement->id}}">
                                      <input type="hidden" id="userID{{$approvisionnement->id}}" value="{{$approvisionnement->id}}">
                                      <select id="approvisionnementStatus{{$approvisionnement->id}}">
                                      <option value="">Approvisionnement En cours</option>
                                        <option value="0">Approvisionnement en cours</option>
                                        <option value="1">Livraison terminée</option>
                                        <option value="2">Approvisionnement Annulée</option>
                                        <option value="3">Paiement reçu</option>
                                        <option value="4">Livraison en cours</option>
                                        <option value="5">Paiement en attente</option>
                                        <option value="6">Prêt pour expédition</option>

                                      </select>
                                      </div>   
                                    </td>
                                    <td><a href=""><button class="btn btn-fill btn-warning">Actions</button></a></td>

                          </div>
                          </tr>
                        @endforeach

                      </tbody>
                      </table>
                  {{$approvisionnements->links()}}

                    
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        
                  

<script>
$(document).ready(function(){

@foreach($approvisionnements as $approvisionnement)
$("#selectDiv{{$approvisionnement->id}}").hide();
$("#showSelectDiv{{$approvisionnement->id}}").click(function(){  
$("#selectDiv{{$approvisionnement->id}}").show();
});
$("#approvisionnementStatus{{$approvisionnement->id}}").change(function(){
var status = $("#approvisionnementStatus{{$approvisionnement->id}}").val();
var userID = $("#userID{{$approvisionnement->id}}").val()
if(status==""){
alert("please select an option");
}else{
$.ajax({
url: '{{url("/admin/banApprovisionnement")}}',
data: 'status=' + status + '&userID=' + userID,
type: 'get',
success:function(response){
console.log(response);
}
});
}

});
@endforeach
});
</script>
                  @endsection
         

