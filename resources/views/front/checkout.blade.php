<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">


</head>
  <body>

    <!--header-->
       
    <header class="header">
         @include('version3.header.header2')
  
        </header>
    <!-- end header-->   


  <main class="main">

                      <div class="row">
                                          <div class="col-sm-12">
                                                       <div class="breadcrumbs" style="padding: 8px; blackground-color: white;">
                                                        <ul>
                                                            <li><a href="{{url('/')}}" style="color:black">Home </a></li>
                                                            <li><span class="dot">/</span>
                                                              <a href="" style="color:black">cart</a>
                                                          </ul>
                                                        </div>
                                          </div>
                      </div><br>
          <!-- design of cart page -->
          @if(Cart::count()!="0")

          <div class="row">

            <div class="small-12 medium-12 large-4">
            <h4 Style="Color:black;">Procedure</h4>
                        <div class="col-sm-3">
                            <div class="blk-box">
                                <div class="blk-boxHd"><b Style="Color:black; font-size:15px;">Livraison</b></div>
                                <div class="blk-boxTxt hidden-sm" Style="Color:black; font-size:12px;">Confirmer le quartier de livraison (si différent du vôtre).</div>
                                <div class="blk-boxTxt hidden-sm" Style="Color:black; font-size:12px;">Précisez l’adresse pour faciliter la livraison (ex: à côté du SAMU)</div>

                            </div>
                        </div>

                        <div class="col-sm-3">
                            <div class="wht-box">
                              <div class="wht-boxHd"><b Style="Color:black; font-size:15px;">Zone &amp; livraison</b></div>
                              <div class="wht-boxTxt hidden-sm" Style="Color:black; font-size:12px;">Votre zone de livraison et le prix de la livraison sont affichés</div>
                            </div>
                        </div>
                        
                          <div class="col-sm-3">
                            <div class="wht-box">
                              <div class="wht-boxHd"><b Style="Color:black; font-size:15px;">Commande</b></div>
                              <div class="wht-boxTxt hidden-sm" Style="Color:black; font-size:12px;">Confirmez votre commande</div>
                            </div>
                          </div>
                         
                        <div class="col-sm-3">
                          <div class="wht-box">
                            <div class="wht-boxHd"><b Style="Color:black; font-size:15px;">Orange Money</b></div>
                            <div class="wht-boxTxt hidden-sm" Style="Color:black; font-size:12px;">Payez par Orange Money (vous devrez cocher la case “Payer maintenant” et cliquer immédiatement sur le bouton “Orange Money” ). En effet, Orange rejettera les paiements avec des délais entre les deux actions et vous aurez le message “Jeton invalide”</div>
                          </div>
                        </div>

            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="om-sidebar">
            <h5></h5>
           <img src="/images/om.png" alt="" style="width: 100px">
            
            </div>
    
            </div>


            <div class="small-12 medium-12 large-8">
                 <input type="hidden" value="{{csrf_token()}}" name="_token">
                        <h4 Style="Color:black;"><b>Adresse de livraison</b></h4>

                        <form class="w3-container" action="{{url('/placeOrder')}}" method="post">

                             <input type="hidden" value="{{csrf_token()}}" name="_token"/>


                            <div class="row">

                            <div class="large-6 p-0-15">

                            <label for="usr" Style="Color:black; font-size:18px;"><b>Prénom & Nom du Récipiendaire<b style="color:red;">*</b></b></label>
                            <input type="text" name="fullname" class="validate" class="form-control" id="usr" placeholder="Prénom & Nom" value="{{Auth::user()->name}}">

                            <span style="color: red">@if($errors->has('name')) {{ $errors->first('name') }}@endif</span>

                            </div>   

                            <div class="large-6 p-0-15">
                            <label for="usr"  data-error="votre adresse eamil est invalide" Style="Color:black; font-size:18px;"><b>Téléphone du Récipiendaire<b style="color:red;">*</b></b></label>
                            <input type="text" name="phone" class="validate" class="form-control" id="usr"
                            placeholder="Téléphone" value="{{Auth::user()->phone}}">
                            <span style="color: red">@if($errors->has('phone')) {{ $errors->first('phone') }}@endif</span>

                            </div>


                            </div>

                          <div class="row">

                           
                            <!-- <div class="large-6 p-0-15">
                                          {!! Form::select('quartier_id',$quartiers, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner quartier du fournisseur']) !!}
                                          {!! $errors->has('quartier_id')?$errors->first('quartier_id'):'' !!}
                                          </div> -->  
                                          
                          <div class="large-6 p-0-15">
                        
                          <label for="quartier_id" Style="Color:black; font-size:18px;"><b>Confirmer le quartier de la livraison<b style="color:red;">*</b></b></label>
                          
                            <select name="quartier_id" onchange="location = this.value;"   required>
                            <option value="{{Auth::user()->quartier_id}}">Sélectionner</option>
                              @foreach(App\Quartier::all() as $zone)
                                <option value="{{route('quartier.vue', $zone->id)}}">{{$zone->nom}}</option>

                                @endforeach  
                            </select> 

<label for="usr" Style="Color:black; font-size:18px;"><b>Adresse du Récipiendaire<b style="color:red;">*</b></b></label>
                            <input type="text" name="adresse" class="validate" class="form-control" id="usr"
                             value="{{Auth::user()->adresse}}" placeholder="Adresse du Récipiendaire" required>
                            <span style="color: red">@if($errors->has('adresse')) {{ $errors->first('adresse') }}@endif</span>

                              <p Style="Color:black; font-size:18px;"><b>Ce quartier correspond à la zone de livraison :</b>

<p><B><strong Style="Color:black; font-size:18px;">{{$frais->nom ?? '' }}  :   {{$frais->zone->nom ?? '' }}</strong></B></p>
 <input type="hidden" name="quartier" class="validate" class="form-control" id="usr"
                             value="{{$frais->nom ?? '' }}  :   {{$frais->zone->nom ?? '' }}" placeholder="Quartier et Zone"></p>
                             <p Style="Color:black; font-size:18px;"><b>Le prix de la livraison dans cette zone est de :</b>

 <p><B><strong Style="Color:black; font-size:18px;">{{$frais->zone->frais ?? '' }} F</strong></B></p>
 <input type="hidden" name="livraison" class="validate" class="form-control" id="usr"
                             value="{{$frais->zone->frais ?? '' }} FCFA" placeholder="Les Frais de Livraison"></p>
                      
                            </div>  

<div class="large-6 p-0-15">
                         <label for="usr"  data-error="votre adresse eamil est invalide" Style="Color:black; font-size:18px;"><b>Votre Quartier<b style="color:red;">*</b></b></label>

                          <input type="text" name="quartier" class="validate" class="form-control" id="usr"
                            placeholder="Téléphone" value="{{Auth::user()->quartier->nom}}">

                            </div> 
                                                   <!--      <p>   
                                            <label class="containers">livraison
                                              <input type="radio" checked="checked" name="livraison" value="1500 F CFA">
                                              <span class="checkmark"></span>
                                            </label>

                                            <label class="containers" style="font-size:13px;"><strong style="color:yellow; font-size:15px;"><b>1500</b></strong>: <b>zone 1: Plateau-Point E-HLM-Amitié-Sacré coeur-Mermoz-Liberté-Castor-Médina
                                              -Fass-Colobane-Ouakam-Foire-Sipres-Hlm  grand yoff-Zone de captage</b>
                                              <input type="radio" name="livraison" value="1500 F CFA">
                                              <span class="checkmark"></span>
                                            </label>

                                              <label class="containers" style="font-size:13px;"><strong style="color:yellow; font-size:15px;"><b>2000</b></strong>: <b>zone_2: Almadie-Ngor-Mamelle-Yoff-Parcelle assainie-Pâte d'oie-Mariste
                                              -Cambérène-Zone industrielle</b>
                                              <input type="radio" name="livraison" value="2000 F CFA">
                                              <span class="checkmark"></span>
                                            </label>

                                            <label class="containers" style="font-size:13px;"><strong style="color:yellow; font-size:15px;"><b>2500</b></strong>: <b>zone_3: Guédiawaye-Golf-Pikine-Mbao-Rufisque-Keur massar-Thiaroye-Route de boune</b>
                                              <input type="radio" name="livraison" value="2500 F CFA">
                                              <span class="checkmark"></span>
                                            </label>
  
                                                          </p> -->
                                                        
                

                <p>&nbsp;</p>

      <div class="row">

                  <table class="unstriped">
                      @foreach($data as $pro)
                      <tbody>
                        <tr>
                            <td style="width: 180px;"><img src="{{url('jaloimage', $pro->options->img) }}"
                                                          class="img-responsive pull-left" width="100px" /></td>
                            <td><a href="{{url('details')}}/{{$pro->id}}" style="color:black">
                                                                <b>{{ucwords($pro->name)}}</b>
                                                              </a></td>

                              <td>
                              <h6>Prix ​​unitaire</h6>
                                                              <p>{{$pro->price}} F CFA
                                                              </p>
                                                            
                              </td>
                  
                          <td  style="width: 120px;">

                              <div style="display: flex; justify-content: flex-start; align-items: center;">


                              <span>


                                <input type="number" name="qty" max="10" min="1" style="width:80px; margin-bottom:0; margin-right:15px;"
                                                                  value="{{$pro->qty}}" class="qty-fill"
                                                                  id="upCart{{$pro->id}}">
                                </span>

                                <span>
                                <button type="submit" style="float: left; border:radius:50%;" class="qty-fill"><i class="material-icons" style="background-color:black; color:yellow">&#xe90a;</i></button>

                                </span>
                              </div>
                                                                





                          </td>
                        <td style="width: 80px;">

                              <a href="{{url('cart/removes')}}/{{$pro->rowId}}"
                                  ><button type="submit" class="qty-fill" ><i class="material-icons" style="background-color:yellow; color:black; margin-left:10px;">close</i>
                                </button></a>

                        </td>
                        <td>
                                                      <h6 class="redtext">
                                                        Total Achat: {{$pro->subtotal}}
                                                          <br>
                                                        </h6>
                        </td>
                   </tr>
                </tbody>
              @endforeach
 
                 </table>
          <p>&nbsp;</p>

      <div class="row">


                <div class="small-12 medium-6 large-6">
                  <div style="">
                <strong>Montant total: </strong><span>
                    {{Cart::total()}} F CFA
                    </span>
                  </div>
                </div>
      
        

                <div class="small-12 medium-12 large-6">
                  <div class="continuer" style="text-align: right">
                  <a href="/catalogues"><button class="">....</button></a>
                  </div>
                </div>

                  

                 <!--  <div class="small">
                    <p>&nbsp;</p>
                    <div class="om">
                     <p id="p">Payer à la livraison
                    <input type="radio" name="submit" alt="Payer"/></p> 
                    </div>
                  </div> -->

<div class="form-group" hidden>
                                                                                <label style="color:black;">Votre Prénomet Nom</label>
                                                                            <input type="text" style="border: solid 1px gray;" name="name" value="{{ Auth::user()->name }}" class="form-control" id="name" placeholder="Votre Prénom & Nom" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                                                            <div class="validation"></div>
                                                                            </div>
                                                                             
                                                                            <div class="form-group" hidden>    
                                                                          
                                                                                <label style="color:black;">Votre Email</label>
                                                                            <input type="email" style="border: solid 1px gray;" class="form-control" name="email[]" id="email" placeholder="Votre Email" data-rule="email" data-msg="Please enter a valid email" />
                                                                            <div class="validation"></div>
                                                                          
                                                                            </div>
                                                                          
                                                                            <div class="form-group" hidden>
                                                                                <label style="color:black;">Votre Subject</label>
                                                                            <input type="text" style="border: solid 1px gray;" class="form-control" name="subject" value="Commande" id="subject" placeholder="Votre Subject" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                                                            <div class="validation"></div>
                                                                            </div>
                                                                            <div class="form-group" hidden>
                                                                                <label style="color:black;">Votre Message</label>
                                                                            <input class="form-control" style="border: solid 1px gray;" name="message" rows="5" value="{{ Auth::user()->name }} vient de passer une commande, voici son numéro de téléphone : {{Auth::user()->phone}} et le produit Commandé: , Produit: {{ucwords($pro->name)}} , Prix: {{($pro->price)}} , Total Achat: {{$pro->subtotal}}" data-rule="required" data-msg="Please write something for us" placeholder="Votre Message">
                                                                            <div class="validation"></div>
                                                                            </div>

                  <div class="small-12 medium-12 large-12">
                    <p>&nbsp;</p>
                   <div style="text-align:">
                      <input id="OMoney" class="button yellow" style="margin-top:00px;font-size:18px;" type="submit" value="Confirmer votre Commande">
                      <button><a href="/catalogues" class="button" style="background-color:black; color:yellow;margin-top: 16px; font-size:18px;">Continuer Vos achats</a></button>
                      </form>
                    </div>
                  </div>

                   
                  
                 <!--  <div class="small-12 medium-12 large-12">
                    <p>&nbsp;</p>
                    <div style="text-align: right">
                   
                   
                     
                     <p>&nbsp;</p>                      
                    </div>
                  </div> -->
                  
                 

      </div>

          <p>&nbsp;</p>
          <p>&nbsp;</p>


      @else

                    <div class="row">
                              <div class="col-md-2 col-md-offset-5 top25">
                                    <img src="images/empty-cart-page-doodle.png"
                                    class="img-response"/>
                                    <br><br>
                        <p style="text-align:center" style="font-size:18px;">Votre panier est vide<br><br>
                   <button><a href="{{url('/catalogues')}}"
                   class="button" style="background-color:black; color:yellow; font-size:18px;">Continuer vos achats</a></button> 
                    </p>

                              </div>
                    </div>
      @endif
      </div>

            

          </div>

    
    </main> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

    <script>
    $(document).ready(function(){
      $("OMoney").hide();
      $("p").click(function(){
        $("OMoney").show();
      });
    
    });
   
    
    </script>                  
                   
<style>
/* The container */
.containers {
  display: block;
  position: relative;
  padding-left: 35px;
  margin-bottom: 12px;
  cursor: pointer;
  font-size: 22px;
  -webkit-user-select: none;
  -moz-user-select: none;
  -ms-user-select: none;
  user-select: none;
}

/* Hide the browser's default radio button */
.containers input {
  position: absolute;
  opacity: 0;
  cursor: pointer;
}

/* Create a custom radio button */
.checkmark {
  position: absolute;
  top: 0;
  left: 0;
  height: 25px;
  width: 25px;
  background-color: black;
  border-radius: 50%;
}

/* On mouse-over, add a grey background color */
.containers:hover input ~ .checkmark {
  background-color: black;
}
.button:hover{
   background-color:black;
   color:yellow;
}

/* When the radio button is checked, add a blue background */
.containers input:checked ~ .checkmark {
  background-color: yellow;
}

/* Create the indicator (the dot/circle - hidden when not checked) */
.checkmark:after {
  content: "";
  position: absolute;
  display: none;
}

/* Show the indicator (dot/circle) when checked */
.containers input:checked ~ .checkmark:after {
  display: block;
}

.yellow {
  background-color: yellow;
  color: black;
  margin-bottom: 0;
}

/* Style the indicator (dot/circle) */
.containers .checkmark:after {
 	top: 9px;
	left: 9px;
	width: 8px;
	height: 8px;
	border-radius: 50%;
	background: black;
}

.om {
  display: flex;
  justify-content: flex-end;
  align-items: center;
}

.om input {
margin-bottom: 0;
margin-right: 8px;
}

.om > img {
  height: 60px;
  object-fit: contain;
  margin-right: 15px
}
.button{
  background-color:yellow;
  color:black;  
}
input:required {
  box-shadow: 4px 4px 20px rgba(200, 0, 0, 0.85);
}

/**
 * style input elements that have a required
 * attribute and a focus state
 */
input:required:focus {
  border: 1px solid red;
  outline: none;
}

/**
 * style input elements that have a required
 * attribute and a hover state
 */
input:required:hover {
  opacity: 1;
}
</style>

  <!-- design of cart page  end -->


<!-- foooter -->
@include('version3.footer.footer')

<!-- end footer-->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171493377-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171493377-1');
</script>

</body>
</html>
