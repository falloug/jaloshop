@foreach($data as $p)
<div class="col-xs-6 col-sm-4" >
  <div class="itemBox">
    <div class="prod">
      <img
      src="{{Config::get('app.url')}}/public/img/{{$p->image}}" alt=""
      width="400px" height="360px" /></div>
    <label>{{$p->name}}</label>
    <br>
    {{$p->description}}</span>
    <div class="addcart">
      <div class="price">Rs {{$p->price}}</div>
      <div class="cartIco hidden-xs"><a href="/"></a></div>
    </div>
  </div>
</div>
@endforeach
