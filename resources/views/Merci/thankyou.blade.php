@extends('layout.deco1')

@section('content')
<div class="greyBg">
    <div class="container">
		<div class="wrapper">
      <div class="row">
				<div class="col-sm-12">
				 <div class="breadcrumbs">
			        <ul style="display:flex; margin-left:420px;">
			          <li ><a href="{{url('/')}}" style="color:black;"><strong><b></b></strong></a></li>
                 <li><span class="dot"></span>
			          <a href="{{url('/myaccount')}}" style="color:black;"><strong><b></b></strong></a></li>
                <li><span class="dot"></span>
                  <a href="" style="color:black;"><strong><b></b></strong></a>
			        </ul>
                        </div>
                    </div>
         </div> 
        
                          <div class="small-12 medium-12 large-12">
                    <div style="">
                  <center>  <img src="/images/om.png" style="border: 0px solid; width:100px"/></center>

                    </div>
                  </div>
         <div class="callout secondary" style="margin-top:20px;">

          <center>  <h5><strong Style="Color:black; font-size:18px;">MERCI  <p><b>{{ Auth::user()->name }} pour votre commande !</b></p></strong></h5>
            <p><b Style="Color:black; font-size:16px;">Votre livraison sera effective dans 48 h maximum. Nos commerciaux vous contacteront si nécessaire. Vous pouvez payer à la livraison, ou immédiatement par Orange Money" et bénéficier d'un traitement prioritaire !.</b><</center>                
           
      
             <!-- Material form login -->
            <div class="card">

            <h5 class="card-header info-color white-text text-center py-4">
            <img src="/images/om.png" style="border: 0px solid; width:20px"/>

              <strong>Payer Par Orange Money</strong>
            </h5>

<!--Card content-->
<div class="card-body px-lg-5 pt-0">

  <!-- Form -->
  <form method="POST" action="https://api.paiementorangemoney.com" class="text-center" style="color: #757575;">
    <input type="hidden" value="{{csrf_token()}}" name="_token"/>

    <!-- Email -->
    <div class="md-form">
    <input type="hidden" name="S2M_IDENTIFIANT" value="{{$identifiant}}" class="form-control">
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_SITE" value="{{$site}}" class="form-control">
    </div>
    
    <div class="md-form">
    <input type="hidden" name="S2M_TOTAL"  value="{{$total}}" class="form-control"> 
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_REF_COMMANDE" value="{{$ref_commande}}" class="form-control">
    </div>
    <div class="md-form">
           <input type="hidden" name="S2M_COMMANDE" value="{{$commande}}" class="form-control">   
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_DATEH" value="{{$dateh}}" class="form-control">
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_HTYPE" value="{{$algo}}" class="form-control">
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_HMAC" value="{{$hmac}}" class="form-control">      
    </div>
    <div class="d-flex justify-content-around">
      <div>
        <!-- Remember me -->
        <div class="form-check">
        </div>
      </div>
      <div>
        <!-- Forgot password -->
       
      </div>
    </div>

    <!-- Sign in button -->
   <!--  <button class="btn btn-outline-warning btn-rounded btn-block my-4 waves-effect z-depth-0" type="submit">
    <img src="/images/om.png" style="border: 0px solid; width:20px"/>
    </button> -->

    <!-- Register -->
    

    <!-- Social login -->

  </form>
  <!-- <table>
              @foreach($commandes as $commande)
              <tbody>
              <tr>
              <td><p><strong Style="Color:black; font-size:15px;">Référence de la Commande: {{$commande->id}}</b></strong></p></td>

              <td><p><strong Style="Color:black; font-size:15px;">Total Commande: {{$commande->total}}</b></strong></p></td>
              
              <td><p><strong Style="Color:black; font-size:15px;">Date de Votre Commande: {{$commande->created_at}}</strong></p></td>
              
              <a href="{{route('orange.money', $commande->id)}}" class="button" Style="Color:black; font-size:20px;"><b>Je confirme mon paiement par Orange Money</b></a>

              <td>
              {!! Form::open(['method'=>'delete', 'route'=>['annuler.destroyed', $commande->id]]) !!}
              <button type="submit" class="button"  class="pull-right">Annuler</button>
              {!! Form::close() !!}
              </td>
              </tr>                          
            </tbody>
              @endforeach 
              </table> Form -->

</div>

</div>
<!-- Material form login -->

         
            <table>
              @foreach($commandes as $commande)
              <tbody>
              <tr>
              <td><p><strong Style="Color:black; font-size:15px;" >Référence de la Commande: {{$commande->id}}</b></strong></p></td>

              <td><p><strong Style="Color:black; font-size:15px;" >Total Commande: {{$commande->total}}</b></strong></p></td>
              
              <td><p><strong Style="Color:black; font-size:15px;" >Date de Votre Commande: {{$commande->created_at}}</strong></p></td>
              <center>
              <a href="{{route('orange.money', $commande->id)}}" class="button" Style="Color:black; font-size:20px;"><b>Je règle par Orange Money</b></a>

              <a href="/" class="button" Style="Color:black; font-size:20px; margin-left: 50px;"><b>Je retourne à l'acceuil</b></a>

</center>

              <td>
              {!! Form::open(['method'=>'delete', 'route'=>['annuler.destroyed', $commande->id]]) !!}
              <button type="submit" class="button"  class="pull-right">Annuler</button>
              {!! Form::close() !!}
              </td>
              </tr>                          
            </tbody>
              @endforeach 
              </table>
              
      

  
          </div>      
        </div>
    </div>
  </div>
</div>
<style>
ul li {
}
.button{
  background-color:yellow;
  color:black;
}

.button:hover{
  background-color:black;
  color:yellow;
}

</style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>

</script>
@endsection
