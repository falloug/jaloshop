@extends('layout.deco1')

@section('content')
<div class="greyBg">
   <div class="container">
		<div class="wrapper">
      <div class="row">
				<div class="col-sm-12">
				 <div class="breadcrumbs">
			        <ul style="display:flex; margin-left:420px;">
			          <li ><a href="{{url('/')}}" style="color:black;"><strong><b></b></strong></a></li>
                 <li><span class="dot"></span>
			          <a href="{{url('/myaccount')}}" style="color:black;"><strong><b></b></strong></a></li>
                <li><span class="dot"></span>
                  <a href="" style="color:black;"><strong><b></b></strong></a>
			        </ul>
                        </div>
                    </div>
         </div> 
        
                          <div class="small-12 medium-12 large-12">
                    <div style="">
                  <center>  <img src="/images/om.png" style="border: 0px solid; width:100px"/></center>

                    </div>
                  </div>
         <div class="callout secondary" style="margin-top:20px;">

          <center>  <h5><strong Style="Color:black; font-size:18px;">MERCI!!!
           <b>{{$user->name }}</b> 
 <p><b> pour votre commande !</b></p></strong></h5>
            <p><b Style="Color:black; font-size:16px;">Merci d'avoir choisi le paiement par Orange Money qui donne une priorité à votre commande.</b></p> </center>
      
             <!-- Material form login -->
            <div class="card">

         

<!--Card content-->
<div class="card-body px-lg-5 pt-0">

  <!-- Form -->
  <form method="POST" action="https://api.paiementorangemoney.com" class="text-center" style="color: #757575;">
    <input type="hidden" value="{{csrf_token()}}" name="_token"/>

    <!-- Email -->
    <div class="md-form">
    <input type="hidden" name="S2M_IDENTIFIANT" value="{{$identifiant}}" class="form-control">
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_SITE" value="{{$site}}" class="form-control">
    </div>
    
    <div class="md-form">
    <input type="hidden" name="S2M_TOTAL"  value="{{$montant}}" class="form-control"> 
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_REF_COMMANDE" value="{{$ref_commande}}" class="form-control">
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_COMMANDE" value="{{$commande}}" class="form-control">   
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_DATEH" value="{{$dateh}}" class="form-control">
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_HTYPE" value="{{$algo}}" class="form-control">
    </div>
    <div class="md-form">
    <input type="hidden" name="S2M_HMAC" value="{{$hmac}}" class="form-control">      
    </div>

    <div class="d-flex justify-content-around">
      <div>
        <!-- Remember me -->
        <div class="form-check">
          <input type="hidden" class="form-check-input" id="materialLoginFormRemember">
          <label class="form-check-label" for="materialLoginFormRemember"></label>
        </div>
      </div>
      <div>
        <!-- Forgot password -->
       
      </div>
    </div>

    <!-- Sign in button -->
    <center>
    <button class="btn btn-outline-warning btn-rounded btn-block my-4 waves-effect z-depth-0" style="width:480px; background-color:yellow;" type="submit">
    <img src="/images/om.png" style="border: 0px solid; width:50px"/>

              <strong style="font-size:20px; color:black;">Payer maintenant par Orange Money</strong>
    </button>
    </center>

    <!-- Register -->
    

    <!-- Social login -->

  </form>
  <!-- Form -->

</div>

</div>
<!-- Material form login -->

         
            <table>
              @foreach($commandes as $commande)
              <tbody>
              <tr>
              <td><p><strong Style="Color:black; font-size:15px;" >Référence de la Commande: {{$commande->id}}</b></strong></p></td>

              <td><p><strong Style="Color:black; font-size:15px;" >Total Commande: {{$commande->total}}</b></strong></p></td>
              
              <td><p><strong Style="Color:black; font-size:15px;" >Date de Votre Commande: {{$commande->created_at}}</strong></p></td>
              
              
              <td>
              {!! Form::open(['method'=>'delete', 'route'=>['annuler.destroyed', $commande->id]]) !!}
              <button type="submit" class="button"  class="pull-right">Annuler</button>
              {!! Form::close() !!}
              </td>
              </tr>                          
            </tbody>
              @endforeach 
              </table>
              
  

  
          </div>      
        </div>
    </div>
  </div>
</div>
<style>
ul li {
}
.button{
  background-color:yellow;
  color:black;
}

.button:hover{
  background-color:black;
  color:yellow;
}

</style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script>

</script>
@endsection
