<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  </head>
  <body>
        <form method="POST" action="https://api.paiementorangemoney.com">
        <input type="hidden" value="{{csrf_token()}}" name="_token"/>

        <input type="hidden" name="S2M_IDENTIFIANT" value="{{$identifiant}}">
        <input type="hidden" name="S2M_SITE" value="{{$site}}">
        <input type="hidden" name="S2M_TOTAL" value="{{$total}}">
        <input type="hidden" name="S2M_REF_COMMANDE" value="{{$ref_commande}}">
        <input type="hidden" name="S2M_COMMANDE" value="{{$commande}}">   
        <input type="hidden" name="S2M_DATEH" value="{{$dateh}}">
        <input type="hidden" name="S2M_HTYPE" value="{{$algo}}">
        <input type="hidden" name="S2M_HMAC" value="{{$hmac}}">
        <input type="image" name="submit"  
          src="/images/om.png" style="border: 0px solid
          black;border-radius: 10px; width:50px; -moz-border-radius: 10px; -
          khtml-
          border-radius: 10px; -webkit-border-radius:
          10px;" alt="Payer"/>
          </form>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>