@extends('layout.adminlayout.design1')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Commerciaux</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom du commercial</th>
                        <th>Téléphone du commercial</th>
                        <th>Adresse du commercial</th>
                        <th>Quartier du commercial</th>
                        <th>Date de Création</th>
                        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($commercials as $commercial)
                        <tr>
                        <td>{{ $commercial->id }}</td>
                        <td>{{ $commercial->nom}}</td>
                        <td>{{ $commercial->phone}}</td>
                        <td>{{ $commercial->adresse}}</td>
                        <td>{{ $commercial->quartier->nom}}</td>
                        <td>{{ $commercial->created_at }}</td>

                        

                        <td>
                        <a href="{{route('commercial.edit', $commercial->id)}}" class="btn btn-warning"><i class="fa fa-pencil-square-o" style="font-size:24px"></i>

                         </a>   
                                        </td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['commercial.destroy', $commercial->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>

                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      {{ $commercials->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
         
