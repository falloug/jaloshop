
<header class="header">

<div class="row">
  <div class="header-container">

    <a href="/" class="logo"><img src="{{asset('images/jalo/logo.png')}}" alt=""></a>

    <form action="" class="search">
      <input type="text" placeholder="Rechercher un article" class="search-input" name="search" value="{{ old('search') }}">
      <button class="search-submit" id="findBtn">
        <img src="{{asset('images/jalo/search-icon.svg')}}" alt="">
      </button>
    </form>


    <!-- <div class="header-right" > -->

     <!--  <div class="session">
        <div class="session-user"> -->  

          <a href="/myaccount"><strong style="color:black;">Mon Compte</strong></a>
        
       <!--  </div>

      </div> -->
            <div class="bucket">
              <a href="{{route('cart.index')}}"><strong style="color:black;">Mon panier</strong></a>
              <a href="{{route('cart.index')}}"><img src="{{asset('images/jalo/bucket.svg')}}" alt=""><strong style="color:black;">{{Cart::count()}}</strong></a>
              <!-- <div class="bucket-number"><strong>{{Cart::count()}}</strong></div> -->
            </div>
     
            <div class="bucket-content hide">
              <strong>@include('cart.cart_index')</strong>
            </div>

      <!-- </div> -->

    <!-- </div> -->
    
  </div>
</div>

</header>