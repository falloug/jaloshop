
<header class="header">

<div class="row">
  <div class="header-container">

    <a href="/" class="logo"><img src="{{asset('images/jalo/logo.png')}}" alt=""></a>

    <form action="" class="search">
      <input type="text" placeholder="Rechercher un article" class="search-input" name="search" value="{{ old('search') }}">
      <button class="search-submit" id="findBtn">
        <img src="{{asset('images/jalo/search-icon.svg')}}" alt="">
      </button>
      </form>


    <div class="header-right" style="margin-right:-2px;">
    <ul class="drop">
  
  <li class="dropdown down">
    <a href="javascript:void(0)" class="dropbtn"><b style="color:black;">Connecter</b></a>
    <div class="dropdown-content">
    <a href="/myaccount"><strong style="color:black;">Mon Compte</strong></a>     
      <a href="/deconnexion"><strong style="color:black;">Deconnecter</strong></a>
    </div>
  </li>
</ul>

      <div class="session">
        <div class="session-user">
<!--           <a href="/myaccount"><strong style="color:black;">Mon Compte</strong></a>     
 -->        </div>

      </div>
      <div class="bucket">
        <a href="{{route('cart.index')}}"><strong style="color:black;">Mon panier</strong></a>
        <a href="{{route('cart.index')}}"><img src="{{asset('images/jalo/bucket.svg')}}" alt=""></a>
        <div class="bucket-number"><strong>{{Cart::count()}}</strong></div>
      </div>
     
        <div class="bucket-content hide">
          <strong>@include('cart.cart_index')</strong>
        </div>

      </div>

    </div>
    
  </div>
</div>
<style>
.drop {
  list-style-type: none;
  margin: 0;
  padding: 0;
  overflow: hidden;
  background-color: white;
 
}

.down {
  float: left;
}

.down a, .dropbtn {
  display: inline-block;
  color: white;
  text-align: center;
  padding: 14px 16px;
  text-decoration: none;
}

.down a:hover, .dropdown:hover .dropbtn {
  background-color: yellow;
  color: black;
}

.down.dropdown {
  display: inline-block;
}

.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}

.dropdown-content a {
  color: black;
  padding: 12px 16px;
  text-decoration: none;
  display: block;
  text-align: left;
}

.dropdown-content a:hover {background-color: #f1f1f1}

.dropdown:hover .dropdown-content {
  display: block;
}
</style>

</header>