 <!-- category and slider -->
 <p>&nbsp;</p>
      <div class="row">
        <div class="small-12 medium-12 xmedium-4 large-3">
          <ul class="multilevels-accordion-menu vertical menu" style="background-color:black" data-accordion-menu>
          @foreach(App\Category::with('childs')
             ->where('product_id',0)->get() as $item)
               @if($item->childs->count()>0)

                        <li>
                        <a href="{{url('catalogue')}}/{{$item->name}}" style="color:white"><h6><i style="font-size:24px; color:yellow;" class="fa">&#9658;</i><b style="margin-left:8px;">{{$item->name}}</b></h6></a>
                              <ul class="menu vertical sublevel-1">
                                    @foreach($item->childs as $subMenu)
                                      <li>
                                      <a class="subitem" href="{{url('catalogues')}}/{{$subMenu->name}}" style="color:white; margin-left:35px; font-size:18px;"><b>{{$subMenu->name}}</b></a>
                                       </li>
                                       @endforeach
                                </ul>
    
                                @else  

                          <li><a href="{{url('catalogue')}}/{{$item->name}}" style="color:white"><h6><i style='font-size:24px; color:yellow;' class='far'>&#9658;</i><b>{{$item->name}}</b></h6></a></li>
                        </li>
                        @endif
          @endforeach

          </ul>  


        </div>
        <div class="small-12 medium-12 xmedium-8 large-9">

          <div class="carousel carousel-main" data-flickity='{"cellAlign": "left"}'>
            @foreach( $visuals as $visual )

              <div class="carousel-cell"  style="positin: absolute; width:120%"><img src="{{url('ima', $visual->image)}}" alt=""></div>
            @endforeach

          </div>

          <div class="carousel carousel-nav" data-flickity='{ "asNavFor": ".carousel-main", "contain": true, "pageDots": false }'>
          @foreach( $visuals as $visual )

             <div class="carousel-cell"><img src="{{url('ima', $visual->image)}}" alt=""></div>
             
          @endforeach
          </div>
          
        </div>
      </div>
      <!-- En category and slider -->
      <p>&nbsp;</p>
      