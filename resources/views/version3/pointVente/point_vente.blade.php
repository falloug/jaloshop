<!-- Nos points de ventes -->
<section class="section" id="shops">
<hr>
        <div class="row">
          <h4 class="section-title">Nos Points de vente</h4>
        </div>
        <div class="row">
          
          <div class="shops">
          @foreach($boutiquiers as $boutiquier)

                    <div class="shops-item">
                    <img src="images/jalo/shops/shop1.jpg" alt="" class="shops-item-img">
                            <div class="shops-item-info">
                                        <div class="shops-item-info-avatar">
                                        <img src="images/jalo/shops/av1.jpg" alt="">
                                        </div>
                                        <div class="shops-item-info-details">
                                        <p>{{$boutiquier->nom}}</p>
                                        <span>Adresse : {{ str_limit(ucfirst($boutiquier->adresse), 40)}}</span>
                                                <div class="phone">
                                                    <img src="images/jalo/icons/phone.svg" alt="" class="phone-icon">
                                                    <span class="phone-number">{{$boutiquier->phone}}</span>
                                                </div>
                                        </div>
                            </div>
                    <div class="shops-item-badge"><img src="{{asset('images/jalo/icons/badge.svg')}}" alt=""></div>
                    </div>
                    @endforeach

            
            <hr>
        

          </div>
          <div class="see-more">
            <a href="/boutique" class="see-more-button">Voir tous nos points de vente</a>
          </div>

        </div>
      </section>

