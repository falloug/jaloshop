<section class="section" id="products" style="margin-top:-150px;">
<div class="row">

<div class="title">
  <h4 class="section-title">Made In Sénégal</h4>
  <a href="/made_senegal" class="title-see-more">Voir Tous Les Produits<img src="images/jalo/icons/arrow-right.svg" alt=""></a>
</div>

</div>

</div>
        <div class="row">

 @if(count($made_sénégal)=="0") 
        <div class="col-md-12" align="center">

                               <div class="col-md-2 col-md-offset-5">
                                    <img src="images/empty-cart-page-doodle.png"
                                    class="img-response"/>
                                    <br><br>
                                    <p style="text-align:center">Aucun Produit<br><br>
                                    
                                    </p>

                 
                               </div>
        @else 
          <div class="products" style="display:flex;">
          
          @foreach($made_sénégal as $produit)

            <div class="products-item">  
              <a href="{{route('details.produit', $produit->id)}}"><img src="{{ $produit->image }}" style="width:300px;" alt="" class="products-item-img"></a>
              <div class="products-item-info">
                <a href="{{route('details.produit', $produit->id)}}"><h5 class="products-item-info-title">{{ $produit->name}}</h5></a>
                <div class="products-item-info-footer">
                  <span class="price">{{ $produit->price}} F CFA</span>
                  <a href="{{route('details.produit', $produit->id)}}"> Détails</a>
                </div>
              </div>
              <div class="add-bucket">
                <form action="">
                <a href="{{route('cart.addItem', $produit->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
                </form>
              </div>
            </div>
            @endforeach

            
          </div>
          @endif
        </div>
      </section>
