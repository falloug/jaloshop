<section class="section" id="products" style="margin-top:-50px;">
        <div class="row">
          <h4 class="section-title">Top ventes</h4>
        </div>
        <div class="row">
        @if(count($commandes)=="0")
        <div class="col-md-12" align="center">

                               <div class="col-md-2 col-md-offset-5">
                                    <img src="images/empty-cart-page-doodle.png"
                                    class="img-response"/>
                                    <br><br>
                                    <p style="text-align:center">Aucun Produit Trouvé<br><br>
                                    
                                    </p>

              
                               </div>
        @else 

          <div class="products" style="display:flex;">
          @foreach($commandes as $vente)

            <div class="products-item">
            <a href="{{route('details.vente', $vente->id)}}"><img src="{{ $vente->image }}" style="width:300px;" alt="" class="products-item-img"></a>
              <div class="products-item-info">
              <a href="{{route('details.vente', $vente->id)}}"><h5 class="products-item-info-title">{{ $vente->name}}</h5></a>
       <div class="products-item-info-footer">
                  <span class="price">Prix: {{ $vente->price}} F CFA</span>
                  <a href="{{route('details.vente', $vente->id)}}"> Détails</a>

                </div>
              </div>
              <div class="add-bucket">
                <form action="">
                <a href="{{route('cart.addItem', $vente->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
                </form>
              </div>    
            </div>

            @endforeach
            <div class="see-more">
         {{ $commandes->links() }}
        </div>
            
          </div>
          @endif

          <div class="see-more">
        </div>
        </div>
      </section>
