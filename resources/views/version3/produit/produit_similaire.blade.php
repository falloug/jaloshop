<section class="section" id="products">
        <div class="row">
          <h4 class="section-title">Produits Similaires</h4>
        </div>
        <div class="row">

 @if(count($produit_jours)=="0")
        <div class="col-md-12" align="center">

 <div class="col-md-2 col-md-offset-5">
                                    <img src="images/empty-cart-page-doodle.png"
                                    class="img-response"/>
                                    <br><br>
                                    <p style="text-align:center">Aucun Produit Trouvé<br><br>
                                    
                                    </p>

              
                               </div>
              
        </div>
        @else 
          <div class="products" style="display:flex;">
          @foreach($produit_jours as $produit_jour)

            <div class="products-item">
              <a href="{{route('quartier.show', $produit_jour->id)}}"><img src="{{ $produit_jour->image }}" style="width:300px;" alt="" class="products-item-img"></a>
              <div class="products-item-info">
              <a href="{{route('quartier.show', $produit_jour->id)}}"><h5 class="products-item-info-title">{{ $produit_jour->name}}</h5></a>
                <div class="products-item-info-footer">
                  <span class="price">Prix: {{ $produit_jour->price}} F CFA</span>
                  <a href="{{route('quartier.show', $produit_jour->id)}}">Détails</a>

                </div>
              </div>
              <div class="add-bucket">
                <form action="">
                <a href="{{route('cart.addItem', $produit_jour->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
                </form>
              </div>
            </div>
            @endforeach

            
          </div>
          @endif
          <div class="see-more" >
         <p class="pagi">{{ $produit_jours->links() }}</p>
        </div>
        </div>
        <style>
        .see-more{
          color:yellow;
        }
        </style>
      </section>
