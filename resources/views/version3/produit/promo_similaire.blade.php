<section class="section" id="products" style="margin-top:-50px;">
        <div class="row">
          <h4 class="section-title">Produits en promo</h4>
        </div>
        <div class="row">
        @if(count($top_promos)=="0")
        <div class="col-md-12" align="center">

 <div class="col-md-2 col-md-offset-5">
                                    <img src="images/empty-cart-page-doodle.png"
                                    class="img-response"/>
                                    <br><br>
                                    <p style="text-align:center">Aucun Produit en Promotion<br><br>
                                    
                                    </p>

              
                               </div>
              
        </div>
        @else 

          <div class="products" style="display:flex;">
          @foreach($top_promos as $top_promo)

            <div class="products-item">
              <a href="{{route('boutique.show', $top_promo->id)}}"><img src="{{ $top_promo->image }}" style="width:300px;" alt="" class="products-item-img"></a>
              <div class="products-item-info">
              <a href="{{route('boutique.show', $top_promo->id)}}"><h5 class="products-item-info-title">{{ $top_promo->name}}</h5></a>
                         <div class="products-item-info-footer">
              
                   @if($top_promo->promo_prix != 0)  
                   <strike><strong class="card-price-old">{{$top_promo->price}}<span>F cfa</span></strong></strike>
                                    <strong class="card-price">{{$top_promo->promo_prix}}<span>F cfa</span></strong>
                                @else
                                    <strong class="card-price">{{$top_promo->price}}<span>F cfa</span></strong>
                                @endif

                  <a href="{{route('boutique.show', $top_promo->id)}}"> Détails</a>
                </div>
              </div>
              <div class="add-bucket">
                <form action="">
                <a href="{{route('cart.addItem', $top_promo->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
                </form>
              </div>
            </div>
            @endforeach

            
          </div>
          @endif
          <div class="see-more">
         {{ $top_promos->links() }}
        </div>
        </div>
      </section>
