<section class="section" id="products">
        <div class="row">
          <h4 class="section-title">Nos Produits</h4>
        </div>
        <div class="row">

 @if(count($produits)=="0")
        <div class="col-md-12" align="center">

 <div class="col-md-2 col-md-offset-5">
                                    <img src="images/empty-cart-page-doodle.png"
                                    class="img-response"/>
                                    <br><br>
                                    <p style="text-align:center">Aucun Produit Trouvé<br><br>
                                    
                                    </p>

              
                               </div>
              
        </div>
        @else 
          <div class="products" style="display:flex;">
          @foreach($produits as $produit)

            <div class="products-item">
            <a href="{{route('product.show', $produit->id)}}"><img src="{{url('jaloimage', $produit->image)}}" alt="" class="products-item-img"></a>
              <div class="products-item-info">
              <a href="{{route('product.show', $produit->id)}}"><h5 class="products-item-info-title">{{ $produit->name}}</h5></a>
                <div class="products-item-info-footer">
                  <span class="price">Prix: {{ $produit->price}} F CFA</span>
                  <a href="{{route('product.show', $produit->id)}}">Détails</a>

                </div>
              </div>
              <div class="add-bucket">
                <form action="">
                <a href="{{route('cart.addItem', $produit->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
                </form>
              </div>
            </div>
            @endforeach

            
          </div>
          @endif
          <div class="see-more">
         {{ $produits->links() }}
        </div>
        </div>
      </section>
