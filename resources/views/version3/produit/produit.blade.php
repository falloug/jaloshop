
<section class="section section-white" id="products">
        <div class="row">
          <h4 class="section-title">Produits du jour</h4>
        </div>
        <div class="row">


          <div class="products" style="display:flex;">
          <?php if($produit_jours->isEmpty()){?>
            <div class="col-md-2 col-md-offset-5">
                                    <img src="images/empty-cart-page-doodle.png"
                                    class="img-response"/>
                                    <br><br>
                                    <p style="text-align:center">Aucun Produit Trouvé<br><br>
                                    
                                    </p>

                
                               </div>
          <?php } else {
          $countP=0;?>
          @foreach($produit_jours as $produit_jour)
            <input type="hidden" id="pro_id<?php echo $countP;?>" value="{{ $produit_jour->id}}"/>
            <div class="products-item">
              <a href="{{route('quartier.show', $produit_jour->id)}}"><img src="{{ $produit_jour->image }}" style="width:300px;" alt="" class="products-item-img"></a>
              <div class="products-item-info">
              <a href="{{route('quartier.show', $produit_jour->id)}}"><h5 class="products-item-info-title">{{ $produit_jour->name}}</h5></a>
               <div class="products-item-info-footer">
                  <span class="price">{{ $produit_jour->price}} F CFA</span>

                  <a href="{{route('quartier.show', $produit_jour->id)}}"> Détails</a>

                </div>
              </div>
              <div class="add-bucket">
 <a href="{{route('cart.addItem', $produit_jour->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
               <!-- <button class="add-bucket-icon" id="cartBtn<?php echo $countP;?>"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></button>
                <div id="successMsg<?php echo $countP;?>" class="alert alert-success"></div> -->

              </div>

            </div>
            <?php $countP ++?>
            @endforeach
<?php }?>

            
          </div>
          
        </div>

      </section>
      <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>

            <script>
            $(document).ready(function(){
            <?php $maxP = count($produit_jours);
            for($i=0;$i<$maxP;$i++){?>
            $('#successMsg<?php echo $i;?>').hide();
            $('#cartBtn<?php echo $i;?>').click(function(){
            var pro_id<?php echo $i;?> = $('#pro_id<?php echo $i;?>').val();
/*             alert(pro_id<)
 */            $.ajax({
            type: 'get',
            url: '<?php echo url('/cart/add-item/');?>/' + pro_id<?php echo $i;?>,
            success: function(){
            $('#cartBtn<?php echo $i;?>').hide();
            $('#successMsg<?php echo $i;?>').show();
            $('#successMsg<?php echo $i;?>').append('Ajouté');
            }  
            });
            });
            <?php }?>
            });
           
            </script>
