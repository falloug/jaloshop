<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

    <!--header-->
@include('version3.header.header1')
<!-- end header--> 
    <main class="main">
      <p>&nbsp;</p>
      <div class="row">
        <div class="small-12 medium-12 large-6 large-offset-3">
          <h4 align="center">commande</h4>
          
        <table border="1">
            <tr>
         
         <th>Id</th>
         <th>product</th>
         <th>order</th>
        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>

            </tr>
            <tbody>
            @foreach($commandes as $commande)
            <tr>
            
            <td>{{$commande->id}}</td>
            <td>{{$commande->product_id}}</td>
            <td>{{$commande->order_id}}</td>

            <td>
                {!! Form::open(['method'=>'delete', 'route'=>['commander.destroyed', $commande->id]]) !!}
                    <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i></button>
                {!! Form::close() !!}
            </td>
            
            </tr>
            @endforeach
            </tbody>
        </table>
        {{ $commandes->links() }}

        </div>
      </div>
      <p>&nbsp;</p>
    </main>

    <!-- foooter -->
    @include('version3.footer.footer')

<!-- end footer-->

    <script src="js/jalo/jquery.min.js"></script>
    <script src="js/jalo/what-input.min.js"></script>
    <script src="js/jalo/foundation.min.js"></script>
    <script src="js/jalo/slick.min.js"></script>
    <script src="js/jalo/app.js"></script>
    
  </body>
</html>
