<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">

  </head>
  <style>
  .button:hover{
    background-color:black;
    color:white;

  }
  .button{
    background-color:yellow;
    color:black;

  }
  
  </style>
  <body>

    <!--header-->
@include('version3.header.header1')

<!-- end header--> 

    <main class="main">
    @include('version3.layouts.ourJs')

      <!-- Filter -->
      <form action="">
        <div class="row row-filter">
          <!-- <div class="small-12 medium-6 large-4 p-0-15">
            <label for="">  
              <strong>Nom produit</strong>
              <input type="text" placeholder="Nom du produit" name="search" value="{{ old('search') }}">
            </label>
          </div> -->
         <!--  <div class="small-12 medium-6 large-4 p-0-15">
            <label for="">
              <strong>Catégorie</strong>
              <select id="catID">
							   <option value="">Selectionner Categorie</option>
                 @foreach(App\Category::where('product_id', '=',0)->get() as $cList)
                 <option class="option" value="{{$cList->id}}">{{$cList->name}}</option>
                 @endforeach
               </select>
            </label>
          </div> -->
          <ul class="menus" style="display:flex; margin-top:20px; margin-left:100px; background-color:white;">
                        <li><a href="/top_promo" class="button">Toutes les catégories</a></li>
                        @if($categories != [])
                        @foreach(App\Category::where('product_id', '=',0)->get() as $categorie)                                <li><a href="{{url('catalogues')}}/{{$categorie->name}}"
                                       class="button">{{$categorie->name}}</a></li>
                            @endforeach
                        @endif  
                    </ul>
          
         <!--  <div class="small-12 medium-6 large-2 p-0-15">
            <label for="">
              <strong>Prix</strong>
              <select id="priceID">
                <option value="0" selected>Croissant / Décroissant</option>
                <option value="0-100000">Décroissant</option>
						    <option value="100000-200000">Croissant</option>
						    
              </select>
            </label>
          </div>

          <div class="small-12 medium-12 large-2 p-0-15">
          <button id="findBtn" onClick="find()" class="filter-button">Filtrer</button>
          </div> -->

        </div>

      </form>
      <p>&nbsp;</p>
      <!-- End filter -->
      
      <div class="row">

@if(count($promos)=="0")
        <div class="col-md-12" align="center">

 <div class="col-md-2 col-md-offset-5">
                                    <img src="images/empty-cart-page-doodle.png"
                                    class="img-response"/>
                                    <br><br>
                                    <p style="text-align:center">Aucun Produit Trouvé<br><br>
                                    
                                    </p>

              
                               </div>
              
        </div>
        @else 
        <div class="products">
        @foreach($promos as $promo)

                <div class="products-item">
                <a href="{{route('boutique.show', $promo->id)}}"><img src="{{ $promo->image }}" alt="" class="products-item-img"></a>
                            <div class="products-item-info">
                            <a href="{{route('boutique.show', $promo->id)}}"><h5 class="products-item-info-title">{{ $promo->name}}</h5></a>
                                       <div class="products-item-info-footer">
               
                   @if($promo->promo_prix != 0)  
                   <strike><strong class="card-price-old">{{$promo->price}}<span>F cfa</span></strong></strike>
                                    <strong class="card-price">{{$promo->promo_prix}}<span>F cfa</span></strong>
                                @else
                                    <strong class="card-price">{{$promo->price}}<span>F cfa</span></strong>
                                @endif

                  <a href="{{route('boutique.show', $promo->id)}}"> Détails</a>
                </div>
                            </div>
                            <div class="add-bucket">
                            <form action="">
                            <a href="{{route('cart.addItem', $promo->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
                            </form>
                            </div>
                            <p>&nbsp;</p>

                </div>
                @endforeach

        </div>
@endif
        <div class="see-more">
         {{ $promos->links() }}
        </div>

      </div>
      
    </main>

   <!-- foooter -->
   @include('version3.footer.footer')

<!-- end footer-->

    <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>

  </body>
</html>
