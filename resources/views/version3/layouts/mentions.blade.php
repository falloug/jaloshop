<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

     <!--header-->
       
         <header class="header">
         @include('version3.header.header')
     
        </header>
    <!-- end header-->    


    <main class="main">
      <!-- Jumbotrun -->
     <h2 align="center">JALÔ SAS</h2> 
     <h2 align="center">MENTIONS LÉGALES</h2>

     <div class="row">
     <div class="small-6 medium-6 large-6 large-offset-3">
        <h4>1. Editrice :</h4>
                                <p>●JALÔ SAS au capital de 10 000 000F CFA
                                   ●N°RCCM : SN DKR-2017-B-20254
                                   ● NINEA : 006480165
                                   ● Siège social : 85B, Sacré Cœur III, Résidence Fatou Kasse, Dakar Sénégal
                                   ● Téléphone : +221 33 824 81 95 et email : <a href="#">info@jaloshops.com</a>
                                   ● Directeur de publications : Roland Kyedrebeogo <a>(roland.k@jaloshops.com)</a>..</p>

                         <h3 align="center">2. Hébergeur :</h3> 
                         <p>●Forme juridique et capital social : DIGITAL OCEAN
                            ● Siège social : 101 Avenue of the Americas (6 156,62 km) 10013 New York.
                            ● Téléphone : +1 212-226-2794
                            ● Site web : www.digitalocean.com</p>

                           <h3 align="center">3. Concepteur graphique :</h3> 
                           <p>●Roland Kyedrebeogo
                              ● Contact : +221 77 349 38 88</p>

                            <h3 align="center">Crédits photos et vidéos :</h3>
                            <p> ●OuagaDakar (+221 77 860 50 14 - <a>www.ouagadakar.com)</a>;
                                ●Serge N. DALEBA (+221772745252) ; et,
                                ●JALÔ SAS.</p>

                          <h3 align="center">4. Traitement des données à caractère personnel</h3>
                          <p>Tout utilisateur de la plateforme, en remplissant les formulaires mis en ligne par JALÔ SAS,
                                consent et accepte que cette dernière traite les informations personnelles qui le
                                concerne, conformément à la législation et à la réglementation en vigueur au Sénégal.
                                Aucune information personnelle n’est collectée à votre insu ou cédée à des tiers. Aussi, la
                                société offre à tout utilisateur la possibilité de demander la modification, la rectification
                                et/ou la suppression des données le concernant en envoyant simplement un e-mail sur
                                <a href="#">info@jaloshops.com</a> ou un courrier postal à l’adresse suivante : Jalô SAS, 85B, Sacré Cœur
                                III, Résidence Fatou Kasse, Dakar, 2ème étage.</p>

                                <h2 align="center">JALÔ SAS</h2>
                                  <strong><h4>CONDITIONS GÉNÉRALES D’UTILISATION (CGU)</h4></strong>
                                  <h5 align="center">Mise en ligne le .... / Décembre / 2018</h5>
                                  <h5>1. Objet</h5>
                                <p>La société éditrice JALÔ SAS a mis en ligne la présente plateforme (ci-après
                                    dénommée la « plateforme ») afin de vous présenter ses produits et services en
                                    vente ainsi que des informations et recommandations liés à votre activité ou à vos
                                    centres d'intérêt.
                                    L'utilisation de cette plateforme implique que vous acceptiez d'être lié par les
                                    termes des conditions générales d’utilisations suivantes (ci-après dénommée les
                                    « CGU ») et de respecter l'ensemble des lois applicables. Elles constituent donc un
                                    contrat entre JALÔ et l’Utilisateur et ou le Client.</p>

                                    <h5 align="center">2. Définitions</h5>
                                    <p> ●Utilisateur :

                                    L'Utilisateur s’entend comme toute personne utilisant la
                                    plateforme JALÔ Shops (site web ou application) ou l'un des services
                                    proposés sur celui-ci.</p>
                                    <br> 

                                   <h5>●Utilisateur identifié :</h5>
                                   <p>L'Utilisateur s’entend comme toute personne utilisant la
                                      plateforme JALÔ Shops (site web ou application) ou l'un des services
                                      proposés sur celui-ci et possédant un compte JALÔ.</p>
                                    <br>
                                    <h5>●Client :</h5>
                                    <p>S’entend comme toute personne physique qui agit à des fins qui
                                        n’entrent pas dans le cadre de son activité commerciale, industrielle,
                                        artisanale ou libérale, âgée au moins de 18 ans et ayant commandé au
                                        moins une fois sur la plateforme JALÔ via un compte d’utilisateur identifié.</p>

                                  <br>
                                  <h5>●Identifiant :</h5>
                                  <p>Est l’adresse e-mail nécessaire à l'identification d'un Utilisateur sur
                                      le Site pour accéder à son compte.</p>
                                  <br>
                                  <h5>●Mot de passe :</h5> 
                                  <p>S’entend comme la succession de caractères dont
                                      l'Utilisateur doit garder secret et lui permettant, avec son Identifiant,
                                      d’accéder à son compte.</p>
                                      <br>
                                      <h5>●Boutiquier :</h5>
                                      <p>Est tout commerçant disposant d’un terminal d’accès à la
                                          plateforme JALÔ.</p>
                                    <br>
                                    <h5>●Telecommercial :</h5>
                                    <p>Après la validation d’une commande, c’est le commercial
                                        qui prend en charge la ou les commandes clients par téléphone pour confirmation</p>
                                        <br>
                                    <h5>●Fournisseur :</h5>
                                    <p>S’entend comme tout partenaire fournissant des produits et
                                        services à JALÔ SAS à l’endroit de ces clients.</p>
                                        <br>
                                        <h5>●Livraison :</h5>
                                        <p>C’est la remise matérielle de la commande effectuée sur
                                            jaloshops au client.</p>
                                            <br>
                                        <strong><h4 align="center">Conditions d’accès et utilisation de la plateforme</h4></strong>
                                        <p>La plateforme est accessible gratuitement à tout Utilisateur disposant d’un accès
                                              à internet et auprès de nos boutiquiers partenaires. 
                                              <br> <br>
                                              L’Utilisateur est responsable de son équipement informatique ainsi que de son
                                              accès à internet. Tous les coûts relatifs à l’accès à la plateforme restent à la
                                              charge de l’Utilisateur et ou Client exception faite des accès auprès d’un
                                              boutiquier JALÔ.
                                              <br> <br>
                                              L’Utilisateur ne pourra accéder à son compte qu’après identification à l’aide de
                                              son Identifiant et de son Mot de passe. L’Utilisateur est tenu de garder secret ses
                                              Identifiant et Mot de passe et ne doit pas les communiquer à un tiers.
                                              <br> <br>
                                              JALÔ mettra en œuvre tous les moyens afin d’assurer un accès à la plateforme
                                              24h/24, 7 jours/7, mais ne peut être tenue responsable de toute problème,
                                              dysfonctionnement de la plateforme qui empêcherait tout accès à la plateforme
                                              et/ou aux différents services proposés par JALÔ.
                                              JALÔ fera son possible afin de résoudre tout problème dans les meilleurs délais.</p>
                                              <br>
                                              <h4 align="center">4. Création de compte</h4>
                                              <p>L’Utilisateur a la possibilité de créer un compte sur le site s’il souhaite accéder et
                                              profiter des services/offres proposés par JALÔ SAS.
                                              <br> <br>
                                              En créant son compte, l’Utilisateur accepte la présentes CGU et notre politique de
                                              protection des données personnelles.
                                              <br> <br>
                                              La création de compte sur la plateforme est réservée à toute personne physique
                                              âgée au moins de 18 ans. Chaque Utilisateur ne peut créer qu’un seul compte
                                              Jaloshops.
                                              <br> <br> 
                                              Tous les champs présents dans le formulaire de création de compte doivent être
                                              renseignés à l’exception des champs indiqués comme facultatifs. Tout défaut de
                                              réponse aura pour conséquence la non création de compte. Toutes les
                                              informations renseignées par l’Utilisateur devront être exactes. Le Client peut
                                              renseigner un numéro de téléphone au choix : bureau et/ou portable et/ou
                                              domicile.
                                              <br> <br>
                                              Lorsque l’Utilisateur crée un compte sur le Site, il reçoit un e-mail de confirmation
                                              de création de compte le jour même. JALÔ SAS se réserve le droit de supprimer
                                              tout compte ne respectant pas les présentes conditions contractuelles.</p>
                                              <br>

                                              <strong><h5 align="center">5. Droits de la propriété intellectuelle</h5></strong>
                                              <p>La structure générale de la plateforme Jaloshops, ainsi que son contenu (textes,
                                                  graphiques, images, sons et vidéos), sont la propriété de JALÔ SAS et de ses
                                                  partenaires à l’exception des contributions envoyées par les utilisateurs ou
                                                  contributeurs.
                                                  <br><br>
                                                Toute représentation, reproduction, exploitation partielle ou totale, des contenus
                                                et services proposés par la plateforme Jaloshops, quel que soit le procédé, sans
                                                l’autorisation préalable et par écrit de JALÔ SAS ou de ses partenaires est
                                                strictement interdite.
                                                <br> <br>
                                                </p>
                                                <strong><h4 align="center">6. Responsabilité des parties</h4></strong>
                                                 <h5>●Création de liens hypertextes vers la plateforme Jaloshops</h5>
                                                <p> La création de liens hypertextes vers le site www.jaloshops.com est autorisée à des
                                                    fins purement informationnelles ou publicitaires, afférentes aux produits et services
                                                    de la plateforme Jaloshops.
                                                    <br> <br></p>

                                                    <strong><h5 align="center">Cas de Force majeure</h5></strong>
                                                    <p>La responsabilité de JALÔ SAS ne pourra être engagée en cas de force majeure
                                                      ou de faits indépendants de sa volonté.</p>
                                                      <br> <br>
                                                      <h5 align="center">7. Utilisation des cookies </h5>
                                                      <p>Un cookie est un petit fichier texte envoyé à votre navigateur via le site Web pour
                                                        permettre l’échange de microdonnées entre nos serveurs et votre navigateur.
                                                        Grâce à ce cookie, des informations sur votre visite et d’autres paramètres sont
                                                        enregistrés pour faciliter vos visites suivantes et nous permettre d’améliorer nos
                                                        services pour votre satisfaction
                                                        <br> <br>
                                                        Le site jaloshops.com utilise des cookies pour récupérer les informations statistiques
                                                         avec des outils d’analyse. Certains modules liés à des sites de réseaux sociaux
                                                         (Facebook, Twitter, LinkedIn...) peuvent aussi attribuer des cookies lors de la
                                                       navigation sur nos pages Internet.</p>

                                                      <br> <br>
                                                      <strong><h5 align="center">Comment désactiver les cookies de votre navigateur</h5></strong>
                                                      <p>Ces informations sont généralistes et peuvent être faussées selon la version et
                                                        l’ancienneté de votre navigateur.</p>
                                                        <h4>Google Chrome :</h4>
                                                        <ul>
                                                        <li>●       Ouvrez Google Chrome</li>
                                                        <li>●       Cliquez sur l’icône « Personnaliser et contrôler Google Chrome »</li>
                                                        <li>●       Sélectionnez « Paramètre «</li>
                                                        <li>●       Cliquez sur l’onglet « Paramètre de contenu... »</li>
                                                        <li>●       Cliquez sur « Bloquer les cookies et les données de site tiers »</li>
                                                        <li>●       Cliquez sur « ok »</li>
                                                        </ul>

                                                        <br> <br>
                                                       <strong> <h4>Firefox :</h4></strong>
                                                        <ul>
                                                        <li>● Ouvrez Firefox</li>
                                                        <li>● Cliquez sur l'icône « ouvrir le menu «</li>
                                                        <li>● Cliquez sur « Options «</li>
                                                        <li>● Sélectionnez l’onglet « Vie privée «</li>
                                                        <li>● Dans le menu déroulant à droite de «Règles de conservation « , cliquez sur « utiliser les paramètres personnalisés pour l’historique «</li>
                                                        <li>● Un peu plus bas, décochez « Accepter les cookies «</li>
                                                        <li>● Sauvegardez vos préférences en cliquant sur « OK «</li>


                                                        </ul>
                                                        <h4>Internet Explorer :</h4>
                                                        <ul>
                                                        <li>● Ouvrez Internet Explorer</li>
                                                        <li>● Dans le menu « Outils », sélectionnez « Options Internet «</li>
                                                        <li>● Cliquez sur l’onglet « Confidentialité «</li>
                                                        <li>● Cliquez sur « Avancé » et cochez « ignorer la gestion automatique des cookies »</li>
                                                        <li>● Cliquez sur Accepter, Refuser ou demander.</li>
                                                        <li>● Sauvegardez vos préférences en cliquant sur « OK «</li>
                                                        </ul>
                                                        <br> <br>
                                                        <h4>Safari :</h4>
                                                        <ul>
                                                        <li>●Ouvrez Safari</li>
                                                        <li>●Dans la barre de menu en haut, cliquez sur « Safari », puis « Préférences «</li>
                                                        <li>●À côté de « Accepter les cookies « cochez « Jamais »Sélectionnez l’icône « Sécurité »</li>
                                                        <li>●Si vous souhaitez voir les cookies qui sont déjà sauvegardés sur votre ordinateur, cliquez sur « Afficher les cookies «</li>
                                                        </ul>
                                                        <br> <br>
                                                        <h2>8. Règlement des litiges</h2>
                                                        <p>Tout différend survenant du contenu et ou de l’utilisation de la plateforme
                                                            Jaloshops sera tranché définitivement par le règlement d’arbitrage du Centre
                                                            d’Arbitrage, de Médiation et de Conciliation de Dakar.</p>
        </div> 
        </div>
     <!--partenaires-->     
     @include('version3.partenaire.partenaire')
    <!--end partenaires-->     
 
</main>

    <!-- foooter -->
    @include('version3.footer.footer')

    <!-- end footer-->

    <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/flickity.pkgd.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>
  </body>
</html>