<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

    <!--header-->
@include('version3.header.header1')
<!-- end header--> 
    <main class="main">
      <p>&nbsp;</p>
      <div class="row">
        <div class="small-12 medium-12 large-6 large-offset-3">
        <h4 align="center">Vous connaissez un boutiquier ? Parrainez-le !</h4>
          
          {!! Form::open(['route'=>'ajout.boutis', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
            <div class="forms">

              <label for="">
                <strong>Prénom & Nom du Boutiquier</strong>
                <input type="text" name="nom" placeholder="Prénom & Nom du Boutiquier">
              </label>
              <label for="">
                <strong>Téléphone du Boutiquier</strong>
                <input type="text" name="phone" placeholder="Téléphone du Boutiquier">
              </label>
              <label for="">
                <strong>Adresse du Boutiquier</strong>
                <input type="text" name="adresse" placeholder="Adresse du Boutiquier">
              </label>
              <label for="">
                <strong>Quartier du Boutiquier</strong>
                {!! Form::select('quartier_id',$quartiers, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner le quartier du boutiquier']) !!}
                {!! $errors->has('quartier_id')?$errors->first('quartier_id'):'' !!}
                    </label>
                           <!--  <label for="">
                <strong>Commercial du Boutiquier</strong>
                {!! Form::select('commercial_id',$commercials, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner un commercial']) !!}
                {!! $errors->has('commercial_id')?$errors->first('commercial_id'):'' !!}
                            </label> -->

              <button class="submit-button">Parrainez-le !</button>

            </div>
            {!! Form::close() !!} 
            
        </div>
      </div>
      <p>&nbsp;</p>
    </main>

    <!-- foooter -->
    @include('version3.footer.footer')

<!-- end footer-->

    <script src="js/jalo/jquery.min.js"></script>
    <script src="js/jalo/what-input.min.js"></script>
    <script src="js/jalo/foundation.min.js"></script>
    <script src="js/jalo/slick.min.js"></script>
    <script src="js/jalo/app.js"></script>0
  </body>
</html>