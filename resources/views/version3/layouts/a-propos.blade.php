<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

     <!--header-->
       
         <header class="header">
         @include('version3.header.header')

        </header>
    <!-- end header-->    


    <main class="main">
      <!-- Jumbotrun -->
      <div class="jumbo">
        <div class="row">  
          <div class="small-12 medium-12 large-6">
            <h1 class="jumbo-title" style="color:black;"><b>Tout près de chez vous !</b></h1>
            <p class="jumbo-sumary" >
              <b>JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines</b>
            </p>

            <div class="jumbo-video">
              <div class="jumbo-video-item" data-open="exampleModal1">
                <div class="jumbo-video-item-img">
                  <div class="cover"><img src="{{asset('images/jalo/play.svg')}}" alt=""></div>
                  <img src="{{asset('images/jalo/vignette/v4.png')}}" alt="">
                </div>
                <p class="jumbo-video-item-name">Vidéo 1</p>
              </div>
              <div class="jumbo-video-item" data-open="exampleModal2">
                <div class="jumbo-video-item-img">
                  <div class="cover"><img src="{{asset('images/jalo/play.svg')}}" alt=""></div>
                  <img src="{{asset('images/jalo/vignette/v5.png')}}" alt="">
                </div>
                <p class="jumbo-video-item-name">Vidéo 2</p>
              </div>
              <div class="jumbo-video-item" data-open="exampleModal3">
                <div class="jumbo-video-item-img">
                  <div class="cover"><img src="{{asset('images/jalo/play.svg')}}" alt=""></div>
                  <img src="{{asset('images/jalo/vignette/v6.png')}}" alt="">
                </div>
                <p class="jumbo-video-item-name">Vidéo 3</p>
              </div>
            </div>
            <!-- Videos -->

            <div class="reveal" id="exampleModal1" data-reveal>
              <h1>Video 1.</h1>
              <div class="responsive-embed widescreen">
                <iframe width="560" height="315" src="https://youtu.be/embed/RUjnq-LTxdQ" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
              <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="reveal" id="exampleModal2" data-reveal>
              <h1>Video 2.</h1>
              <div class="responsive-embed widescreen">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/lVpEHi1cKYk" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
              <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

            <div class="reveal" id="exampleModal3" data-reveal>
              <h1>Video 3.</h1>
              <div class="responsive-embed widescreen">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/Jx9-pOPCv1Y" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
              <button class="close-button" data-close aria-label="Close modal" type="button">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>

          </div>
          <div class="small-12 medium-12 large-6">
            <div class="jumbo-devise">
              <img src="{{asset('images/jalo/nexus5x.png')}}" alt="">
            </div>
          </div>
        </div>
      </div>
      <!-- End jumbotrun -->

      <section class="section">
        <div class="row">
          <h4 class="section-title">Qui sommes-nous ?</h4>
        </div>
        
        <div class="row">
          <div class="team">

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/rlnd.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Roland kyedrebeogo</strong>
                  <br>
                  <span>Directeur Général</span>
                  <ul>
                    <li><a href="https://www.facebook.com/RolandKyedrebeogo" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/mapauline.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Pauline sané</strong>
                  <br>
                  <span>Résponsable Finance</span>
                  <ul>
                    <li><a href="https://www.facebook.com/pauline.sane" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="https://www.linkedin.com/feed/" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul>
                </div>
              </div>
            </div>
            
            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/rassoul.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Rassoul Fall</strong>
                  <br>  
                  <span>Résponsable Commercial</span>
                  <ul>
                    <li><a href="https://www.facebook.com/lamine.fall.90" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul>
                </div>
              </div>
            </div>  

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/edith.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Edith Sylva-Amangoua</strong>
                  <br>
                  <span>Résponsable Fournisseur</span>
                  <ul>
                    <li><a href="/" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="https://twitter.com/EdviJumbo" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="https://www.linkedin.com/in/edith-virginie-sylva-amangoua-053aa5112/" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul>
                </div>
              </div>
            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/amary.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Issakha diouf</strong>
                  <br>
                  <span>Commercial Superviseur</span>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/fallou.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Fallou Gueye</strong>
                  <br>
                  <span>Technicien</span>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/emily.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Bernadette Emilie Diouf</strong>
                  <br>
                  <span>Assistante</span>
                 <!--  <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/katy.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Katy</strong>
                  <br>
                  <span>Assistante</span>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
            </div>

              <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/equipe.jpg')}}" class="team-item-img" alt="">

                <div class="team-item-info">
                  <strong>Équipe</strong>
                  <br>
                  <span>JALÔ</span>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/jalo.jpg')}}" class="team-item-img" alt="">
                
                <div class="team-item-info">
                  <strong>JALÔ</strong>
                  <br>
                  <span>JALÔ</span>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
            </div>



          </div>
        </div>
      </section>

      <section class="section section-black" id="presentation">
        <div class="row">
          <h4 class="section-title section-black-title">Que faisons-nous ?</h4>
        </div>
        <div class="row">
          <div class="small-12 medium-12 large-6">
            <img src="{{asset('images/jalo/team/equipe.jpg')}}" class="team-small" alt="">
          </div>
          <div class="small-12 medium-12 large-6">
            <div class="about">
<p>
A JALÔ, nous sommes avant tout une famille de jeunes hommes et femmes passionnés et déterminés pour la sauvegarde des commerces de proximité en Afrique ! Notre engagement est total et permanent au service de nos boutiques de quartiers et de nos clients
</p>
              <ul class="about-list">
                <li><img src="{{asset('images/jalo/icons/i1.svg')}}" alt=""><span>Protective Preventative Maintenance</span></li>
                <li><img src="{{asset('images/jalo/icons/i2.svg')}}" alt=""><span>Becoming A Dvd Repair Expert Online</span></li>
                <li><img src="{{asset('images/jalo/icons/i3.svg')}}" alt=""><span>Make Myspace Your Best Designed Space</span></li>
                <li><img src="{{asset('images/jalo/icons/i4.svg')}}" alt=""><span>Becoming A Dvd Repair Expert Online</span></li>
              </ul>
            </div>
          </div>
        </div>
      </section>

      <!-- Join-us -->
     <!-- Join-us -->
     @include('version3.rejoindre.rejoindre')
      <!-- end join-->

     <!--partenaires-->     
     @include('version3.partenaire.partenaire')
    <!--end partenaires-->     

</main>

    <!-- foooter -->
    @include('version3.footer.footer')

    <!-- end footer-->

    <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/flickity.pkgd.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>
  </body>
</html>