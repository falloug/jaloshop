<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

    <!--header-->
@include('version3.header.header1')
<!-- end header--> 
    <main class="main">
      <p>&nbsp;</p>
      <div class="row">
        <div class="small-12 medium-12 large-6 large-offset-3">
          <h4 align="center">Vous êtes fournisseur, Rejoignez-nous</h4>
          
          {!! Form::open(['route'=>'ajout.fournis', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
            <div class="forms">

              <label for="">
                <strong>Prénom & Nom du fournisseur</strong>
                <input type="text" name="nom" placeholder="Prénom & Nom du fournisseur">
              </label>
              <label for="">
                <strong>Téléphone du Fournisseur</strong>
                <input type="text" name="phone" placeholder="Téléphone du Fournisseur">
              </label>
              <label for="">
                <strong>Adresse du Fournisseur</strong>
                <input type="text" name="adresse" placeholder="Adresse du Fournisseur">
              </label>
              <label for="">
                <strong>Quartier du Fournisseur</strong>
                {!! Form::select('quartier_id',$quartiers, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner le quartier du fournisseur']) !!}
                {!! $errors->has('quartier_id')?$errors->first('quartier_id'):'' !!}
                            </label>

              <button class="submit-button">S'enregistrer</button>

            </div>
            {!! Form::close() !!} 
            
        </div>
      </div>
      <p>&nbsp;</p>
    </main>

    <!-- foooter -->
    @include('version3.footer.footer')

<!-- end footer-->

    <script src="js/jalo/jquery.min.js"></script>
    <script src="js/jalo/what-input.min.js"></script>
    <script src="js/jalo/foundation.min.js"></script>
    <script src="js/jalo/slick.min.js"></script>
    <script src="js/jalo/app.js"></script>0
  </body>
</html>