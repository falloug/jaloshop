<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

    <!--header-->
@include('version3.header.header1')

<!-- end header--> 

    <main class="main">

      <!-- Filter -->
      <form action="">
        <div class="row row-filter">
          <div class="small-12 medium-6 large-5 p-0-15">
            <label for="">
             <!--  <strong>Quartier</strong>
              <select id="catID">
							   <option value="">Selectionner un Quartier</option>
                 @foreach(App\Quartier::all() as $cList)
                 <option class="option" value="{{$cList->id}}">{{$cList->nom}}</option>
                 @endforeach
               </select> -->
            </label>
          </div>
          
          <div class="small-12 medium-6 large-5 p-0-15">
            <label for="">
              <strong>Rechercher par Nom ou Quartier du Vendeur</strong>
              <input type="text" placeholder="Rechercher par Nom ou Quartier du vendeur" class="search-input" name="search" value="{{ old('search') }}">
            </label>
          </div>

          <div class="small-12 medium-12 large-2 p-0-15">
            <input type="submit" value="Filtrer" class="filter-button">
          </div>

        </div>
      </form>
      <p>&nbsp;</p>
      <!-- End filter -->
      <div class="row">
      @if(count($vendeurs)=="0")
        <div class="col-md-12" align="center">

          <h1 style="font-size:20px;">Le vendeur n'existe pas</h1>

              
        </div>
        @else 
          <div class="shops">
          @foreach($vendeurs as $vendeur)

                    <div class="shops-item">
                    <img src="images/jalo/shops/shop1.jpg" alt="" class="shops-item-img">
                            <div class="shops-item-info">
                                        <div class="shops-item-info-avatar">
                                        <img src="images/jalo/shops/boutique.jpeg" alt="">
                                        </div>
                                        <div class="shops-item-info-details">
                                        <p>{{$vendeur->nom}}</p>
                                       
                                                <div class="phone">
                                                    <span class="phone-number"></span>
                                                </div>
                                        </div>
                            </div>
                    <div class="shops-item-badge"><img src="{{asset('images/jalo/icons/badge.svg')}}" alt=""></div>
                    </div>
                    @endforeach

            

        

          </div>
@endif
        <div class="see-more">
        {{ $vendeurs->links() }}
        </div>

      </div>
    </main>

<!-- foooter -->
@include('version3.footer.footer')

<!-- end footer-->

    <script src="{{asset('js/jalo/jquery.min.js')}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}"></script>
    <script src="{{asset('js/jalo/app.js')}"></script>
  </body>
</html>