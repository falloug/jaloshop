<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

<!--header-->
@include('version3.header.header1')

<!-- end header--> 

   <main class="main"> 

                    <div class="row">

                                <div class="small-12 medium-12 large-8 large-offset-2">

                                        <div class="products-details">
                                            <img src="{{ $produit_jour->image }}" alt="" class="products-details-img">

                                                    <div class="products-details-info">

                                                    <h2 class="products-details-info-title">{{$produit_jour->name}}</h2>
                                                          <p>Description: {{$produit_jour->description}}</p>                                   
                                                 <strong class="products-details-info-prix">Prix: {{$produit_jour->price}} F CFA</strong>
                                                    <p>&nbsp;</p>

                                                            <div class="add-bucket">


                                                                <form action="">
                                                                <a href="{{route('cart.addItem', $produit_jour->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
                                                                </form>

                                                            </div>

                                                    </div>
                                        </div>

                                </div>

                    </div>


                 <!--section similaires-->
                 @include('version3.produit.produit_similaire')
                 <!--end section similaires-->

    </main>

    <!-- foooter -->
    @include('version3.footer.footer')

    <!-- end footer-->

    <script src="js/jalo/jquery.min.js"></script>
    <script src="js/jalo/what-input.min.js"></script>
    <script src="js/jalo/foundation.min.js"></script>
    <script src="js/jalo/slick.min.js"></script>
    <script src="js/jalo/app.js"></script>
  </body>
</html>
