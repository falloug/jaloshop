<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
  </head>
  <body>

     <!--header-->
       
         <header class="header">
         @include('version3.header.header')

        </header>
    <!-- end header-->    


   
      <!-- End jumbotrun -->

     
     <p>&nbsp;</p>
     <p>&nbsp;</p>
     <p>&nbsp;</p>

      <section class="section section-black" id="presentation">
        <div class="row">
          <h3 class="section-title section-black-title">À la Une</h3>
        </div>
        <div class="row">
          <div class="small-12 medium-12 large-6">
            <img src="{{asset('images/jalo/team/rolfa.png')}}" class="team-small" alt="">
          </div>
          <div class="small-12 medium-12 large-6">
            <div class="about">
            <p><b>
            Fatoumata BA est PDG de Janngo,
             le premier studio pour startups sociales en Afrique, 
             après un passage a JUMIA (Directrice Côte d’Ivoire,
              puis co-fondatrice Nigeria). 
              Elle est la marraine du #SIBC 2018 et a inspiré l’ensemble des
               participants par sa grande générosité, son énergie, et sa 
               passion pour les startups. J’ai eu le privilège de bénéficier 
               de ses conseils sur les facteurs clés de succès de l’e-commerce 
               inclusif. #YesWeCan #SIBC2018 – incroyablement bien.

            </b></p>
              <ul class="about-list">
                <li><img src="{{asset('images/jalo/icons/i1.svg')}}" alt=""><span>Protective Preventative Maintenance</span></li>
                <li><img src="{{asset('images/jalo/icons/i2.svg')}}" alt=""><span>Becoming A Dvd Repair Expert Online</span></li>
                <li><img src="{{asset('images/jalo/icons/i3.svg')}}" alt=""><span>Make Myspace Your Best Designed Space</span></li>
                <li><img src="{{asset('images/jalo/icons/i4.svg')}}" alt=""><span>Becoming A Dvd Repair Expert Online</span></li>
              </ul>
            </div>
          </div>
        </div>
      </section>

       <section class="section">
        <div class="row">
          <h3 class="section-title">Autres Actualités </h3>
        </div>
        
        <div class="row">
          <div class="team">

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/equipe.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualite</strong>
                  <br>
                  <p><b><span>Vous appartenez a une famille, 
                  vous faites partie de jeunes qui ont de l'ambition,
                   qui veulent transformer ;
                    et qui vont transformer.
                     Vous travaillez dur, 
                     vous imaginez des solutions pour lever les obstacles ;
                      tout en ayant du fun et des fous-rires....
                      vous ne rêvez pas. Ici, c'est JALÔ. Merci 2018 ! Bonjour 2019.
                       #LeMeilleurEstLa #DigitalTransformation #JALO</span></b></p>
                 <!--  <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/rassoupol.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualite</strong>
                  <br>
                  <p><b><span>A JALÔ, nous servons la cause des boutiquiers de quartiers,
                   avec enthousiasme, détermination, et avec le sourire !
                    Faites connaissance avec Rassoul Fall Responsable Réseau et Ventes ;
                     et Pauline Sane Responsable Administrative et Financière ;
                      et rejoignez le mouvement ; avec le sourire.
                       #JALO #SocialBusiness #Smile</span></b></p>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>

            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/jalo.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualité</strong>
                  <br>  
                  <p><b><span>Vous appartenez a une famille, 
                  vous faites partie de jeunes qui ont de l'ambition,
                   qui veulent transformer ;
                    et qui vont transformer.
                     Vous travaillez dur, 
                     vous imaginez des solutions pour lever les obstacles ;
                      tout en ayant du fun et des fous-rires....
                      vous ne rêvez pas. Ici, c'est JALÔ. Merci 2018 ! Bonjour 2019.
                       #LeMeilleurEstLa #DigitalTransformation #JALO</span></b></p>
                 <!--  <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jaloicons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jaloicons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>  

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/actu.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualité</strong>
                  <br>
                  <p><b><span>Vous appartenez a une famille, 
                  vous faites partie de jeunes qui ont de l'ambition,
                   qui veulent transformer ;
                    et qui vont transformer.
                     Vous travaillez dur, 
                     vous imaginez des solutions pour lever les obstacles ;
                      tout en ayant du fun et des fous-rires....
                      vous ne rêvez pas. Ici, c'est JALÔ. Merci 2018 ! Bonjour 2019.
                       #LeMeilleurEstLa #DigitalTransformation #JALO</span></b></p>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/rolfa.png')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualité</strong>
                  <br>
                  <p><b><span>Fatoumata BA est PDG de Janngo,
                   le premier studio pour startups sociales en Afrique,
                    après un passage a JUMIA (Directrice Côte d’Ivoire, puis co-fondatrice Nigeria).
                     Elle est la marraine du #SIBC 2018 et a inspiré l’ensemble des participants par
                      sa grande générosité, son énergie, et sa passion pour les startups.
                       J’ai eu le privilège de bénéficier de ses conseils.
                    </span></b></p>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>

            </div>


            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/bouti.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualité</strong>
                  <br>
                  <p><b><span>
                  
                  Avec JALÔ, j'offre à mes clients une large gamme de produits
                   et services non disponibles dans ma boutique.
                    Je renforce ainsi la relation de confiance qui
                     nous lie car les livraisons sont toujours 
                     réalisées dans un délai maximum de 48H,
                      à la satisfaction de mes clients.
                       (Djibril Thiaw, Boutiquier à Guediawaye, Dakar, Sénégal).
                       j'offre à mes clients des produits
                   

                    </span></b></p>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>

            </div>


            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/stp.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualité</strong>
                  <br>
                  <p><b><span>
                  
                  Le stand de la startup JALÔ en direct de Vivatech Paris.
                   En 3 jours, nous avons reçu plus de 200 visiteurs
                    intéressés de découvrir comment le digital pouvait améliorer 
                    la vie et le statut des commerces de proximité en Afrique.
                     #DigitalAfrica #SocialBusiness
                     Le stand de la startup JALÔ en direct de Vivatech Paris.
                   En 3 jours, nous avons reçu plus de 200 visiteurs.
                    


                    </span></b></p>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>

            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/neng.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualité</strong>
                  <br>
                  <p><b><span>
                  
                  Notre engagement est de vous livrer en 48H maximum.
                   Notre engagement est de vous faire gagner du temps. 
                   Notre engagement est de vous fournir des produits de qualité.
                    Nous sommes JALÔ, nous sommes engagés.
                     #TransformationDigitale #SocialBusiness #Marketplace #Kebetu
                     Notre engagement est de vous livrer en 48H.


                    </span></b></p>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>

            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/jaloshop.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualité</strong>
                  <br>
                  <p><b><span>
                  
                  A JALÔ, nous croyons au potentiel et en la créativité de la jeunesse Africaine.
                   Notre engagement social nous commande de trouver des solutions pour cette jeunesse.
                    Nous recrutons 100 vendeurs freelance a Dakar,
                     pour formation, coaching et renforcement de l'équipe existante !
                      Contactez nous au (221) 77 225 94 32 ou (221) 78 384 01 94

                    </span></b></p>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>

            </div>

            <div class="team-item">
              <div class="team-content">
                <img src="{{asset('images/jalo/team/rnd.jpg')}}" class="team-item-img" alt="">
                <div class="team-item-info">
                  <strong>Actualité</strong>
                  <br>
                  <p><b><span>
                  
                  Nous sommes fiers d'avoir été sélectionné par Orange Sénégal
                   comme l'une des 4 Startups les plus innovantes du Sénégal en 2018,
                    dans le cadre de la compétition de startups Orange Fab Saison 3.
                     Merci à Orange pour la confiance.
                      Plus de Détails sur http://www.orangefab.sn/ – reconnaissant.
                      Merci à Orange pour la confiance.
                       http://www.orangefab.sn/



                    </span></b></p>
                  <!-- <ul>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/fcbk.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/tw.svg')}}" alt=""></a></li>
                    <li><a href="" target="_blank"><img src="{{asset('images/jalo/icons/lnkd.svg')}}" alt=""></a></li>
                  </ul> -->
                </div>
              </div>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>
              <p>&nbsp;</p>

            </div>


            


          </div>
        </div>
      </section>

      <!-- Join-us -->
     <!-- Join-us -->
     @include('version3.rejoindre.rejoindre')
      <!-- end join-->

     <!--partenaires-->     
     @include('version3.partenaire.partenaire')
    <!--end partenaires-->     

</main>

    <!-- foooter -->
    @include('version3.footer.footer')

    <!-- end footer-->

    <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/flickity.pkgd.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>
  </body>
</html>