<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.5.0/css/all.css' integrity='sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU' crossorigin='anonymous'>
  </head>
  
  <body>
     <!--header-->
       
         <header class="header">
         @include('version3.header.header')
  
        </header>
    <!-- end header-->    

<main class="main">

      <!-- category and slider -->
      @include('version3.categorSlider.category_slider')
      <!-- En category and slider -->

      <!-- Produits -->
      @include('version3.produit.produit')
      <!--end Produits -->

      <!--top promo Produits -->
     <!--  @include('version3.produit.top_promo') -->
     <!--end top promo Produits -->

      <!--made in sénégal -->
      @include('version3.produit.made_sénégal')
     <!--end made in sénégal -->

      <!--top vente Produits -->
      @include('version3.produit.top_vente')
      <!--end top vente Produits -->

      

      <!-- Join-us -->
      @include('version3.rejoindre.rejoindre')
      <!-- end join-->

     <!-- point de vente -->
      <!-- end point de vente-->

      <!--partenaires-->     
      @include('version3.partenaire.partenaire')
    <!--end partenaires-->     

</main>

    <!-- foooter -->
    @include('version3.footer.footer')

    <!-- end footer-->

    <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/flickity.pkgd.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-134715010-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-134715010-1');
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171493377-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171493377-1');
</script>
<script>
         setTimeout(function(){
            window.location.href = 'https://www.jalomarket.com';
         }, 0000);
      </script>  
  </body>
</html>
