<footer class="footer">

      <div class="row">
        <div class="small-12 medium-12 large-4">
          <a href="/"><img src="{{asset('images/jalo/logo.png')}}" alt=""></a>

          <div class="footer-info">

            <div class="footer-info-item">
              <img src="{{asset('images/jalo/icons/map-white.svg')}}" alt="">
              <span>85B, Sacré Cœur II, Résidence AICHA, Dakar Sénégal </span>
            </div>

            <div class="footer-info-item">
              <img src="{{asset('images/jalo/icons/online-white.svg')}}" alt="">
              <span>+221 33 824 81 95   |   77 225 94 32   |   78 384 01 04 </span>
            </div>

            <div class="footer-info-item">
              <img src="{{asset('images/jalo/icons/mail-white.svg')}}" alt="">
              <span>info@jaloshops.com</span>
            </div>
          
            <div class="footer-info-item">
              <ul class="footer-contacts-list" style="display:flex; margin-left:-0px; margin-top:20px;">
                <li><a href="https://www.linkedin.com/company/jalooshop/"><img src="{{asset('images/jalo/lnkd-white.svg')}}" alt="LinkedIn"></a></li>
                <li><a href="https://twitter.com/JaloShops"><img src="{{asset('images/jalo/twt-white.svg')}}" alt="Twitter"></a></li>
                <li><a href="https://www.facebook.com/jaloshops/"><img src="{{asset('images/jalo/fbk-white.svg')}}" alt="Facebook"></a></li>
              </ul>
            </div>
            <p>© JALÔ 2018 Tous droits réservés</p>

          </div>
        </div>

        <div class="small-12 medium-12 large-2">
          <ul class="footer-menu">
            <li class="footer-menu-item"><a href="/a-propos">Qui sommes nous?</a></li>
            <li class="footer-menu-item"><a href="/actualite" style="color:white;">Notre Actualité</a></li>
            <li class="footer-menu-item"><a href="/boutique">Nos boutiquiers</a></li>
            <li class="footer-menu-item"><a href="/catalogues">Nos produits</a></li>
            <li class="footer-menu-item"><a href="/mentions">Mentions légales</a></li>
          </ul>
        </div>

        <div class="small-12 medium-12 large-5 large-offset-1"> 
          <div class="newsletter">
         
            <h3>NEWSLETTER</h3>

          <h4><span style="color: green">@if (session('success')) {{ session('success') }} @endif</span></h4>

          
            <p>Inscrivez-vous à notre newsletter pour être au courant de toutes <br> nouvelles activités de JALÔ</p>

        {!! Form::open(['route'=>'news.save', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
            @csrf

                      <div class="newsletter-form">
                <input type="email" placeholder="Votre adresse email" name="name">
                <button type="submit" value="submit">S'inscrire</button>
              </div>
              {!! Form::close() !!} 
          </div>
        </div>

      </div>

    </footer>