<section class="section" id="join-us">
        <div class="row">
          <div class="join-us">
            
            <div class="join-us-item">
              <h5 class="join-us-item-title">Vous connaissez un boutiquier ? <br>Parrainez-le !</h5>
              <p class="join-us-item-sumary">
                Vous connaissez un boutiquier ? Vous souhaitez contribuer à son essor ? Recommandez le dans ce formulaire (en cliquant sur le lien, un formulaire flottant apparait pour capturer votre nom, téléphone, email et le nom, téléphone et quartier et ville du boutiquier)
              </p>
              <a href="ajout/bouti" class="join-us-item-button">Parrainer</a><br><br><br>
              <a href="/boutique" class="title-see-more"> Voir tous nos points de vente <img src="{{asset('images/jalo/icons/arrow-right.svg')}}" alt=""></a>

              <p>&nbsp;</p>
            </div>
            
            <div class="join-us-item">
              <h5 class="join-us-item-title">Vous êtes fournisseur, vos produits nous intéressent !</h5>
              <p class="join-us-item-sumary">
                Vous êtes fournisseur de biens et services ? Vous souhaitez réduire votre time-to-market ? Rejoignez notre réseau (en cliquant sur le lien, un formulaire flottant apparait pour capturer le nom de la société, le nom du contact, téléphone, email, description des produits, quartier et ville)
              </p>
              <a href="/ajout/fourni" class="join-us-item-button">Rejoignez-nous</a><br><br><br>
              <a href="/fournisseurss" class="title-see-more">Voir tous nos fournisseurs <img src="{{asset('images/jalo/icons/arrow-right.svg')}}" alt=""></a>

              <p>&nbsp;</p>
            </div>
            
            <div class="join-us-item">
              <h5 class="join-us-item-title">Vous êtes vendeur, votre talent nous intéresse !</h5>
              <p class="join-us-item-sumary">
                Vous avez du talent pour la vente? Vous vendez deja sur les réseaux sociaux? Vous avez un bon réseau de vente ? Rejoignez notre réseau de freelance (en cliquant sur le lien, un formulaire flottant apparait pour capturer le nom et prénoms du freelance, son téléphone, email,  quartier et ville)
              </p>
              <a href="/ajout/commerci" class="join-us-item-button">Rejoignez-nous</a><br><br><br>
              <a href="/vendeur" class="title-see-more">Voir tous nos vendeurs <img src="{{asset('images/jalo/icons/arrow-right.svg')}}" alt=""></a>

              <p>&nbsp;</p>
            </div>

          </div>
        </div>
      </section>