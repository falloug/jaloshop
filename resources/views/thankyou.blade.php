@extends('layout.deco1')

@section('content')
<div class="greyBg">
    <div class="container">
		<div class="wrapper">
      <div class="row">
				<div class="col-sm-12">
				 <div class="breadcrumbs" style="margin-top:50px;">
			        <ul style="display:flex; margin-left:420px;">
			          <li ><a href="{{url('/')}}" style="color:black;"><strong><b>Home</b></strong></a></li>
                 <li><span class="dot">/</span>
			          <a href="{{url('/myaccount')}}" style="color:black;"><strong><b>{{Auth::user()->name}}</b></strong></a></li>
                <li><span class="dot">/</span>
                  <a href="" style="color:black;"><strong><b>On vous remercie</b></strong></a>
			        </ul>
                        </div>
                    </div>
         </div> 

         <div class="callout secondary">
            <h5><strong>MERCI!  <p><b>{{ Auth::user()->name }}</b></p></strong></h5>
            <p><b>Votre livraison sera faite d'ici 48H.</b></p>
            <a href="{{url('/myaccount')}}" class="button"><strong>Votre Commande se trouve ici</strong></a>

  
          </div>

               
                        
        </div>
    </div>
  </div>
</div>
<style>
ul li {
}
.button{
  background-color:yellow;
  color:black;
}

.button:hover{
  background-color:black;
  color:yellow;
}

</style>
@endsection
