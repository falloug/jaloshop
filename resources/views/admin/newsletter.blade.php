@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">   


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Categories</strong></center>
                        </div>
                          <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                       <th>Email</th>
                       <th>Date de Création</th>

                        </thead>

                        <tbody>


                        @foreach($news as $new)
                        <tr>
                        <td>{{ $new->id }}</td>
                        <td>{{ $new->name}}</td>
                        
                        <td>{{ $new->created_at }}</td>

                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      <div class="see-more">
         {{ $news->links() }}
        </div>

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  
                  @endsection
         
