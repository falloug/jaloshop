@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')

<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">  
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Produits</strong></center>
                        </div>  
                        <div class="card-body">
                        <form method="GET" action="{{ url('status_product') }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="search" class="form-control" placeholder="Rechercher un produit" value="{{ old('search') }}">
                                </div>
                                <div class="col-md-6">
                                <button style="background-color:yellow; color:black" class="btn">Rechercher</button>
                                </div>
                            </div>
                        </form>
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>Nom du Produit</th>
                           <th>Description du Produit</th>
                        <th>Prix du Produit</th>  
                        <th>promo_prix</th>
                      <th>Image</th>
                     <th>Date de Création</th>
                        <th>Produit Status</th>

                       </tr>
                        </thead>

                        <tbody>


                        @foreach($products as $product)
                        <tr>
                        <td>{{ $product->name }}</td>
                       <td>{{ $product->description }}</td>
                        <td>{{ $product->price }}</td>
                       <td>{{ $product->promo_prix }}</td>
                        


                        <td><img src="{{ $product->image }}" style="width:50px;"></td>

                        <td>{{ $product->created_at }}</td>
                        <td>@if($product->status=="") 
                                      <b style="color:yellow"> Produits</b>
                                      @elseif($product->status==0)
                                      <b style="color:green">Produit du Jour</b>
                                      @elseif($product->status==1)
                                      <b style="color:red">Desactiver</b>
                                      @elseif($product->status==2)
                                      <b style="color:green">Produit en Promo</b>
                                      @elseif($product->status==3)
                                      <b style="color:green">Top Vente</b>
                                      @elseif($product->status==4)
                                      <b style="color:green">Produit Boutiquier</b>
                                      @elseif($product->status==5)
                                      <b style="color:green">Made In Sénégal</b>
                                      @elseif($product->status==6)
                                      <b style="color:green">En promo cette semaine</b>
                                      @elseif($product->status==7)
                                      <b style="color:green">À la une !</b>
                                      @elseif($product->status==8)
                                      <b style="color:green">Jouets pour Enfants</b>
                                      @elseif($product->status==9)
                                      <b style="color:green">Jeux Vidéos</b>
                                      @else
                                    <b style="color:green">SMART PHONES</b>
                                      @endif
                                      <br>
                                      <button id="showSelectDiv{{$product->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>  
                                      <div id="selectDiv{{$product->id}}">
                                      <input type="hidden" id="userID{{$product->id}}" value="{{$product->id}}">
                                      <select id="productStatus{{$product->id}}">
                                        <option value="">select a option</option>
                                        <option value="0">Produit du Jour</option>
                                        <option value="1">Desactiver</option>
                                        <option value="2">Produit en Promo</option>
                                        <option value="3">Top Vente</option>
                                        <option value="4">Produit Boutiquier</option>
                                        <option value="5">Made In Sénégal</option>
                                        <option value="6">En promo cette semaine</option>
                                        <option value="7">À la une !</option>
                                        <option value="8">Jouets pour Enfants</option>
                                        <option value="9">Jeux Vidéos</option>
                                        <option value="10">SMART PHONES</option>




                                      </select>
                                      </div>   
                                    </td>
                                    <td><a href=""><button class="btn btn-fill btn-warning">Actions</button></a></td>

                       <!--  <td>

                        <a href="{{route('product.edit', $product->id)}}" class="btn btn-warning"><i class="fa fa-pencil-square-o" style="font-size:24px"></i>

                         </a>                  </td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['product.destroy', $product->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td> -->

                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      {{$products->links()}}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script>
$(document).ready(function(){

@foreach($products as $product)
$("#selectDiv{{$product->id}}").hide();
$("#showSelectDiv{{$product->id}}").click(function(){
$("#selectDiv{{$product->id}}").show();
});
$("#productStatus{{$product->id}}").change(function(){
var status = $("#productStatus{{$product->id}}").val();
var userID = $("#userID{{$product->id}}").val()
if(status==""){
alert("please select an option");
}else{
$.ajax({
url: '{{url("/admin/banProduct")}}',
data: 'status=' + status + '&userID=' + userID,
type: 'get',
success:function(response){
console.log(response);
}
});
}

});
@endforeach
});
</script>
                  @endsection
         
