@extends('layout.adminlayout.design')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Mes Commandes</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <div class="row row_head">
                        <div class="col-md-2 col-xs-2 col-sm-2">    numéro de commande</div>
                        <div class="col-md-2 col-xs-2 col-sm-2">    Date</div>
                        <div class="col-md-2 col-xs-2 col-sm-2">    Identifiant d'utilisateur)</div>
                        <div class="col-md-2 col-xs-2 col-sm-2">    Total de la commande</div>
                        <div class="col-md-2 col-xs-2 col-sm-2">    status</div>
                        <div class="col-md-2 col-xs-2 col-sm-2">    Détails</div>
                    </div>
                    <?php $countOrder ="";?>
                    @foreach($orders as $order)

                    <div class="row row_body">

                          <div class="col-md-2 col-xs-2 col-sm-2">#{{$order->id}}</div>
                            <div class="col-md-2 col-xs-2 col-sm-2">
                            {{date('d M Y', strtotime($order->created_at))}}<br>
                              {{date('H:i A', strtotime($order->created_at))}}
                             
                            </div>

                          <div class="col-md-2 col-xs-2 col-sm-2">{{$order->username}}(#{{$order->userId}})</div>


                          <div class="col-md-2 col-xs-2 col-sm-2">  {{$order->total}}</div>

                          <div class="col-md-2 col-xs-2 col-sm-2">

                            <input type="hidden" id="order_id<?php echo $countOrder;?>" value="{{$order->id}}"/>
                              <select class="form-control" id="order_status<?php echo $countOrder;?>">
                                  <option value="pending"
                                  <?php if($order->status=='pending'){?> selected="selected"<?php }?>>en attendant</option>

                                  <option value="dispatched"
                                    <?php if($order->status=='dispatched'){?> selected="selected"<?php }?>>expédié</option>

                                  <option value="processed"
                                  <?php if($order->status=='processed'){?> selected="selected"<?php }?>>traité</option>

                                  <option value="shipping"
                                  <?php if($order->status=='shipping'){?> selected="selected"<?php }?>>livraison</option>

                                  <option value="cancelled"
                                  <?php if($order->status=='cancelled'){?> selected="selected"<?php }?>>annulé</option>

                                  <option value="delivered"
                                  <?php if($order->status=='delivered'){?> selected="selected"<?php }?>>livré</option>
                              </select>
                              <div align="center" id="successMsg<?php echo $countOrder;?>"></div>
                            </div>

                        <div class="col-md-2 col-xs-2 col-sm-2">
                           <a href=""><i style="font-size:24px" class="fa">&#xf019;</i></span></a>
                         </div>

                          </div>
                          <?php $countOrder++;?>
                    @endforeach


                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        
                  <script>
$(document).ready(function(){
<?php for($i=1;$i<15;$i++){?>
  $('#order_status<?php echo $i;?>').change(function(){
    var order_id<?php echo $i;?> = $('#order_id<?php echo $i;?>').val();
    var order_status<?php echo $i;?> = $('#order_status<?php echo $i;?>').val();

      $.ajax({
       type: 'get',
       data: 'order_id=' + order_id<?php echo $i;?> + '&order_status=' + order_status<?php echo $i;?>,
       url: '<?php echo url("/admin/orderStatusUpdate");?>',
       success: function(response){
         console.log(response);
         $('#successMsg<?php echo $i;?>').html(response);
       }
      });
  });
  <?php }?>
});
</script>
                  @endsection
         
