@extends('layout.adminlayout.design')

@section('content')
<div id="right-panel" class="right-panel">
@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">
   
            <div class="animated fadeIn">


                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Utilisateur</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  {!! Form::open(['route'=>'ajout.user', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="name" class="control-label mb-1">name de l'Utilisateur</label>
                                          <input id="name" name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="email" class="control-label mb-1">email de l'Utilisateur</label>
                                          <input id="email" name="email" type="email" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="phone" class="control-label mb-1">Téléphone de l'Utilisateur</label>
                                          <input id="phone" name="phone" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                    
                                      <div class="form-group">
                                          <label for="password" class="control-label mb-1">Password de l'Utilisateur</label>
                                          <input id="password" name="password" type="password" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="password_confirmation" class="control-label mb-1">Confirmer Password de l'Utilisateur</label>
                                          <input id="password_confirmation" name="password_confirmation" type="password" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="condiction" class="control-label mb-1">Condiction</label>
                                          <input id="condiction" name="condiction" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                      <div>
                                          <button id="submit" name="submit" style="margin-top:20px; background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection