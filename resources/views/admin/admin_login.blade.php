<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>JALÔ</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="{{asset('css/backend_css/bootstrap.min.css')}}" />
		<link rel="stylesheet" href="{{asset('css/backend_css/bootstrap-responsive.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/backend_css/matrix-login.css')}}" />
        <link href="{{asset('fonts/backend_fonts/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
    <center><a href="/" align="center" style=color:yellow;><b>HOME</b></a></center>
        <div id="loginbox"> 
        @if(Session::has('flash_message_error'))
        <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert"></button>
       <strong>
      {!! session('flash_message_error') !!}
      </strong>
      </div>

        @endif   

        @if(Session::has('flash_message_success'))
        <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert"></button>
       <strong>
      {!! session('flash_message_error') !!}
      </strong>
      </div>

        @endif          
            <form id="loginform" class="form-vertical" method="post" action="{{ url('admin')}}">
            {{ csrf_field() }}
				 <a href="/"><div class="control-group normal_text"> <h3><img src="{{asset('images/jalo/logo.png')}}" alt="Logo" /></h3></div></a>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lg"  style="background-color:yellow;"><i class="icon-user"> </i></span><input type="text" name="phone" placeholder="téléphone" required/>
                        </div>
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_ly" style="background-color:yellow;"><i class="icon-lock"></i></span><input type="password" name="password" placeholder="Password" required/>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn" id="to-recover" style="background-color:black; color:yellow;">Oublier Mot de Pass?</a></span>
                    <span class="pull-right"><input type="submit" value="Login" class="btn btn" style="background-color:black; color:yellow;"/></span>
                </div>
            </form>
            <form id="recoverform" action="#" class="form-vertical">
				<p class="normal_text">Mettez votre addresse mail et on va vous envoyer un message .</p>
				
                    <div class="controls">
                        <div class="main_input_box">
                            <span class="add-on bg_lo"><i class="icon-envelope"></i></span><input type="text" placeholder="E-mail address" />
                        </div>
                    </div>
               
                <div class="form-actions">
                    <span class="pull-left"><a href="#" class="flip-link btn btn-warning" id="to-login" style="background-color:yellow; color:black;">&laquo; Retour to login</a></span>
                    <span class="pull-right"><a class="btn btn-info"/>Recover</a></span>
                </div>
            </form>
        </div>
        
        <script src="{{asset('js/backend_js/jquery.min.js')}}"></script>  
        <script src="{{asset('js//backend_js/matrix.login.js')}}"></script>
        <script src="{{asset('js/backend_js//bootstrap.min.js')}}"></script> 

    </body>

</html>
