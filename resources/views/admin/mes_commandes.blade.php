@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header"> 
                            <center><strong class="card-title">Produits</strong></center>
                        </div>
                        <div class="card-body">
            
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">  
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>Image</th>
                        <th>Nom du Produit</th>
                        <th>Description du Produit</th>
                        <th>Prix de la commande</th>
                        <th>Quantité de la commande</th>
                        <th>Total de la commande</th>
                        <th>Date de la commande</th>


                        </tr>
                        </thead>

                        <tbody>


                        @foreach($commandes as $product)
                        <tr>
                        <td><img src="{{ $product->image }}" style="width:50px;"></td>
                       <td>{{ $product->name }}</td>
                         <td>{{ $product->description }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->qty }}</td>
                        <td>{{ $product->total }}</td>
                        <td>{{ date('F d, Y', strtotime($product->created_at))}} at {{ date('g:ia', strtotime($product->created_at))}}</td>


                        
                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                 {{ $commandes->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
         
