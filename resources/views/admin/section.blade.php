@extends('layouts.adminClient.admin_design')

@section('content')
<!-- SECTION -->
<div class="section">
			<!-- container -->
			<div class="container">
				<!-- row -->
				<div class="row">

					<!-- section title -->
					<div class="col-md-12">
						<div class="section-title">
							<h3 class="title">Nouveaux produits</h3>
							<div class="section-nav">
								<ul class="section-tab-nav tab-nav">
									
								</ul>
							</div>
						</div>
					</div>
					<!-- /section title -->



					<!-- Products tab & slick -->

					<div class="col-md-12">

						<div class="row">

							<div class="products-tabs">

								<!-- tab -->
								<div id="tab1" class="tab-pane active">

									<div class="products-slick" data-nav="#slick-nav-1">
										<!-- product -->

										<div class="product">

											<div class="product-img">
												<div class="product-label">
													<span class="sale">-30%</span>
													<span class="new">NEW</span>
												</div>
											</div>
											<div class="product-body">
												<p class="product-category">Category</p>
												<h3 class="product-name"><a href="#">product name goes here</a></h3>
												
												</div>
												<div class="product-btns">

													<a href="#"><button class="quick-view"><span class="glyphicon glyphicon-eye-open"></span><span class="tooltipp">Voir les details</span></button></a>
												</div>
											</div>
											<div class="add-to-cart">
											<a href="#"><button class="add-to-cart-btn"><span class="glyphicon glyphicon-shopping-cart"> ajouter au panier</button></a>

											</div>
										</div>
										

										<!-- /product -->

									</div>
									<div id="slick-nav-1" class="products-slick-nav"></div>
								</div>
								<!-- /tab -->
							</div>
						</div>
					</div>
					<!-- Products tab & slick -->
				</div>
				<!-- /row -->
			</div>

			<!-- /container -->
		</div>

		<!-- /SECTION -->


@endsection