@extends('layout.adminlayout.design')

@section('content')
<div id="right-panel" class="right-panel">
@include('layout.adminlayout.header')


@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Categories</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table style="width:100%" class="table table-hover table-striped" >

                                <tr>
                                  <th>Name</th>
                                  <th>Email</th>
                                  <th>Téléphone</th>
                                  <th>En Ligne</th>
                                  <th>Date</th>
                                  <th>Status</th>
                                  <th>Login Status</th>
                                  <th>Action</th>
                                  <th>Options</th>
                                </tr>
                                @foreach($data as $user)
                                <tr>
                                  <td>{{$user->name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>{{$user->phone}}</td>
                                    <td>
                                      @if($user->isOnline())
                                      <li class="text-success">En Ligne</li>
                                      @else
                                      <li class="text-danger">Deconnecter</li>
                                      @endif
                                    </td>
                                    <td>{{ date('F d, Y', strtotime($user->created_at))}} at {{ date('g:ia', strtotime($user->created_at))}}</td>
                                    <td>@if($user->status==0)
                                      <b style="color:green"> enable</b>
                                      <td>@elseif($user->status==1)
                                      <b style="color:red"> Disabled</b>
                                      <td>@elseif($user->status==2)
                                      <b style="color:yellow"> admin</b>
                                      <td>@elseif($user->status==3)
                                      <b style="color:yellow"> resCommerce</b>
                                      <td>@elseif($user->status==4)
                                      <b style="color:yellow"> tmarke</b>
                                      <td>@elseif($user->status==5)
                                      <b style="color:yellow"> resfourni</b>
                                      <td>@elseif($user->status==6)
                                      <b style="color:yellow"> finance</b>
                                      <td>@elseif($user->status==7)
                                      <b style="color:yellow"> commercesup</b>
                                      <td>@elseif($user->status==8)
                                      <b style="color:yellow">  Boutiquier</b>
                                      @else
                                    <b style="color:yellow"> Freelance</b>
                                      @endif
                                      <br>
                                      <button id="showSelectDiv{{$user->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>
                                      <div id="selectDiv{{$user->id}}">
                                      <input type="hidden" id="userID{{$user->id}}" value="{{$user->id}}">
                                      <select id="loginStatus{{$user->id}}">
                                        <option value="">select a option</option>
                                        <option value="0">enable</option>
                                        <option value="1">Disabled</option>
                                        <option value="2">admin</option>
                                        <option value="3">resCommerce</option>
                                        <option value="4">tmarke</option>
                                        <option value="5">resfourni</option>
                                        <option value="6">finance</option>
                                        <option value="7">commercesup</option>
                                        <option value="8">Boutiquier</option>
                                        <option value="9">Freelance</option>


                                      </select>
                                      </div>   
                                    </td>
                                   <td><a href="" class="btn btn-fill btn-warning">Actions</a></td>
                                  <td>
                                  {!! Form::open(['method'=>'delete', 'route'=>['user.destroy', $user->id]]) !!}
                                  <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                          </button>
                                  {!! Form::close() !!}
                                  </td>

                                </tr>
                                  @endforeach
                              </table>
                               {{$data->links()}}
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

                  <script>
$(document).ready(function(){

  @foreach($data as $user)
  $("#selectDiv{{$user->id}}").hide();
  $("#showSelectDiv{{$user->id}}").click(function(){
      $("#selectDiv{{$user->id}}").show();    
  });
$("#loginStatus{{$user->id}}").change(function(){  
  var status = $("#loginStatus{{$user->id}}").val();
  var userID = $("#userID{{$user->id}}").val()
  if(status==""){
    alert("please select an option");
  }else{
   $.ajax({
     url: '{{url("/admin/banUser")}}',
     data: 'status=' + status + '&userID=' + userID,
     type: 'get',
     success:function(response){
       console.log(response);
     }
   });
  }

});
  @endforeach
});
</script>
                  @endsection
         
