@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Mes Commandes</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>


                                <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                                <thead>
                                <tr>
                                <th>numéro de commande</th>
                                <th>Date</th>
                                <th>Identifiant du Client</th>
                                <th>Total de la commande</th>
                                <th>status</th>  
                                <th>Détails</th>
                                </tr>
                                </thead>

                                <tbody>

                    @foreach($orders as $order)

                       <td>{{  $order->id }}</td>
                       <td>{{ date('F d, Y', strtotime($order->created_at))}} at {{ date('g:ia', strtotime($order->created_at))}}</td>

                        <td>{{$order->username}}(#{{$order->userId}})<br>{{$order->telephone}}</td>

                        <td>{{$order->total}}</td>
                        
                        <td>@if($order->status=="")
                                      <b style="color:yellow">Commande en cours</b>
                                      @elseif($order->status==0)
                                      <b style="color:yellow">Commande en cours</b>
                                      @elseif($order->status==1)
                                      <b style="color:green">Livraison terminée</b>
                                      @elseif($order->status==2)
                                      <b style="color:red">Commande Annulée</b>
                                      @elseif($order->status==3)
                                      <b style="color:green">Paiement reçu</b>
                                      @elseif($order->status==4)
                                      <b style="color:yellow">Livraison en cours</b>
                                      @elseif($order->status==5)
                                      <b style="color:green">Paiement en attente</b>
          
                                      @else
                                    <b style="color:green">Prêt pour expédition</b>
                                      @endif
                                      <br>
                                      <button id="showSelectDiv{{$order->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>
                                      <div id="selectDiv{{$order->id}}">
                                      <input type="hidden" id="userID{{$order->id}}" value="{{$order->id}}">
                                      <select id="orderStatus{{$order->id}}">
                                      <option value="">En cours</option>
                                        <option value="0">Commande en cours</option>
                                        <option value="1">Livraison terminée</option>
                                        <option value="2">Commande Annulée</option>
                                        <option value="3">Paiement reçu</option>
                                        <option value="4">Livraison en cours</option>
                                        <option value="5">Paiement en attente</option>
                                        <option value="6">Prêt pour expédition</option>

                                      </select>
                                      </div>   
                                    </td>
                                    <td><a href=""><button class="btn btn-fill btn-warning">Actions</button></a></td>

                          </div>
                          </tr>
                        @endforeach

                      </tbody>
                      </table>
                  {{$orders->links()}}

                       <div class="card-header">
                            <center><strong class="card-title">Adresse du Client</strong></center>
                        </div>
                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

<thead>
<tr>
<th>numéro de commande</th>
<th>Date</th>
<th>Identifiant du Client</th>
<th>Adresse du client</th>
</tr>
</thead>  

<tbody>

@foreach($addresse_livraisons as $adresse)

<td>{{  $adresse->id }}</td>
<td>{{date('d M Y', strtotime($adresse->created_at))}}
<span>{{date('H:i A', strtotime($adresse->created_at))}}</span></td>

<td>{{$adresse->username}}(#{{$adresse->userId}})<br>{{$adresse->telephone}}</td>

<td>{{$adresse->adresse}}</td>


</div>
</tr>
@endforeach

</tbody>
</table>
{{$addresse_livraisons->links()}}


                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
        
                  

<script>
$(document).ready(function(){

@foreach($orders as $order)
$("#selectDiv{{$order->id}}").hide();
$("#showSelectDiv{{$order->id}}").click(function(){  
$("#selectDiv{{$order->id}}").show();
});
$("#orderStatus{{$order->id}}").change(function(){
var status = $("#orderStatus{{$order->id}}").val();
var userID = $("#userID{{$order->id}}").val()
if(status==""){
alert("please select an option");
}else{
$.ajax({
url: '{{url("/order/banOrder")}}',
data: 'status=' + status + '&userID=' + userID,
type: 'get',
success:function(response){
console.log(response);
}
});
}

});
@endforeach
});
</script>
                  @endsection
         
