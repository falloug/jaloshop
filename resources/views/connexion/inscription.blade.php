<!doctype html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>JALO ECOMMERCE</title>
		<!-- Google font -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick-theme.css')}}"/>
		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/nouislider.min.css')}}"/>
		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}"/>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    </head>
  <body>
<!-- HEADER -->
<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><span class="glyphicon glyphicon-earphone"></span> 77-225-94-32 ou 78-384-01-04</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-envelope"></span> info@jaloshops.com</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-home"></span> Résidence Fatou Kasse, Sacré Coeur III</a></li>
					</ul>
					<ul class="header-links pull-right">
						<li><a href="/inscription"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>
						<li><a href="/connexion"><span class="glyphicon glyphicon-user"></span> Connexion</a></li>
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img style="width:100px;" src="./img/jalo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									
									<!-- <input class="input" placeholder="Search here">
									<button class="search-btn"><span class="glyphicon glyphicon-search"></span> Search
                   </button> -->
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
						<div class="col-md-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								<div>
									
								</div>
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="collaps navbar-collaps" id="bs-example-navbar-collaps-1">
									<ul class"navbar-nav navbar-right">
									<!-- <li>
									
									<a href="{{ route('produit.shoppingCart')}}" style="color:white">
									<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"> Shopping Cart
										<span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
										</a>
										</li> -->
									</ul>
									
										
										
									
								</div>
								<!-- /Cart -->

								
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->
  <div class="container" style="width:600px; margin-left:500px;">
  <h3 style="margin-top:50px;">Veuillez-vous inscrir?</h3>

        <form action="/inscription" method="post" class="section">
        {{ csrf_field() }}
        <div class="field">
            <label class="label">Nom Client</label>
                <div class="control">
                  <input type="text" name="nomClient" placeholder="Votre nom">
                </div>
                @if($errors->has('nomClient'))
                        <p style="color:red" class="help is-danger">{{ $errors->first('nomClient') }}</p>
                    @endif
                    </div>

                    <div class="field">
                       <label class="label">Adresse</label>
                         <div class="control">
                           <input type="text" name="adresse" placeholder="Votre adresse">
                           </div>
                @if($errors->has('adresse'))
                        <p style="color:red" class="help is-danger">{{ $errors->first('adresse') }}</p>
                    @endif
                    </div>

                    <div class="field">
                       <label class="label">Telephone</label>
                         <div class="control">
                          <input type="text" name="telephone" placeholder="votre numero de telephone">
                          </div>
                @if($errors->has('telephone'))
                        <p style="color:red" class="help is-danger">{{ $errors->first('telephone') }}</p>
                    @endif
                    </div>

                    <div class="field">
                       <label class="label">Email</label>
                         <div class="control">
                           <input type="email" name="email" placeholder="Email" value="{{ old('email') }}">
                           </div>
                @if($errors->has('email'))
                        <p style="color:red" class="help is-danger">{{ $errors->first('email') }}</p>
                    @endif
                    </div>

                    <div class="field">
                       <label class="label">Password</label>
                         <div class="control">
                           <input type="password" name="password" placeholder="Mot de passe">
                           </div>
                @if($errors->has('password'))
                        <p style="color:red" class="help is-danger">{{ $errors->first('password') }}</p>
                    @endif
                    </div>

                    <div class="field">
                       <label class="label">Password_confirmation</label>
                         <div class="control">
                           <input type="password" name="password_confirmation" placeholder="Mot de passe (confirmation)">
                           </div>
                @if($errors->has('password_confirmation'))
                        <p style="color:red" class="help is-danger">{{ $errors->first('password_confirmation') }}</p>
                    @endif
                    </div>

                     <div class="field">
                       <div class="control">
                <button class="button is-link btn btn-warning" type="submit" style="margin-top:10px;">"M'inscrire"</button>
                      </div>
                    </div>
            </form>
    </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.2/js/bootstrap.min.js" integrity="sha384-o+RDsa0aLu++PJvFqy8fFScvbHFLtbvScb8AjopnFD+iEQ7wo/CG0xlczd+2O/em" crossorigin="anonymous"></script>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171493377-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171493377-1');
</script>

  </body>
</html>
