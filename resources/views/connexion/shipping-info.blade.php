<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Jalo</title>
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="../css/app.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="/resources/demos/style.css">

  
<style>
0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}


/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}

.container1{

  background-color: white;
}

legend {
    background-color: white;
    color: lightgray;
    padding: 3px 6px;
}

.output {
    font: 1rem 'Fira Sans', sans-serif;
}

input {
    margin: .4rem;
}

.ui-controlgroup-vertical {
    width: 150px;
    }
.ui-controlgroup.ui-controlgroup-vertical > button.ui-button,
.ui-controlgroup.ui-controlgroup-vertical > .ui-controlgroup-label {
    text-align: center;
    }
#car-type-button {
    width: 120px;
    }
.ui-controlgroup-horizontal .ui-spinner-input {
    width: 20px;
    }
    .important {
    font-weight: bold;
    font-size: xx-large;
}
    .blue{
        background-color: blue;
    }

.ui-menu { width: 500px; }

</style>
  
  
</head>
<body>

<header class="header">

            <div class="header-top">
                <div class="row">
                    <div class="small-12 medium-4 large-2">
                    <a href="/"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
                    </div>
                        
                </div>
            </div>

        <div class="row">

            <div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">

                <div class="top-bar-right">
                    <ul class="menu">
                    <li><a href="{{route('cart.index')}}" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span>{{Cart::count()}}</span></a></li>
                    </ul>
                </div>
            </div>
        </div>

</header>

<main class="main">


          <div class="container1">
          <div class="row" style="margin-left:50px; margin-top:50px;">
  <div class="col-lg-3">

  </div>

<div class="col-lg-7" style="margin-left:100px;">




<div class="form-row">

    <div class="form-group col-mg-4" style="width: 340px;">

            <fieldset>
            <center><legend><h2>Livraison</h2></legend></center>

                        <fieldset>

                            {!! Form::open(['route'=>'address.store', 'method' => 'post']) !!}


                            <div>

                                <input type="radio" id="huey" 
                                    name="livraison" class="lop" onClick="livr()" value="Étre livrer à domicile" />
                                <label for="huey">Étre livré à domicile</label>
                                <div class="form-group" id="quar">

                                    <div class="col-md-10">
                                    <select name="quartier_id" required>

                                    <option value="" disabled selected>Sélectionner le quartier</option>
                                    @foreach($quartiers as $quartier)
                                        <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                                        @endforeach
                                        <span style="color: red">@if($errors->has('quartier')) {{ $errors->first('quartier') }}@endif</span>
                                    </select>
                                    </div>
                                    <label for="usr">Adresse du récipiendairess*</label>
                                    <input type="text" name="adresse" class="validate" class="form-control" id="usr" placeholder="Adresse">
                                    <label for="usr">Téléphone du récipiendaire*</label>
                                    <input type="text" name="phone" class="validate" class="form-control" id="usr" placeholder="Téléphone">
                                    <label for="usr">Nom et Prénom du récipiendaire*</label>
                                    <input type="text" name="name" class="validate" class="form-control" id="usr" placeholder="Nom&Prénom">
                                    <h6>En cliquant sur terminer, vous permettez à JALO d'utiliser le Nom, le(s) Prénoms et le téléphone du récipiendaire pour la livraison de votre commande</h6>

                                    <div class="form-group">
                                     <button type="submit" value="submit" class="add-bucket-btn waves-effect waves-black add_to_cart" style="margin-left:80px;">Terminer</button>
                                    </div>
                                </div>


                            </div>
                            {!! form::close() !!}

                        </fieldset>

 

                        <fieldset>
                        {!! Form::open(['route'=>'address.store', 'method' => 'post']) !!}

                            <div>


                                <input type="radio" id="dewey" 
                                    name="livraison" onClick="livrer()" value="Étre livrer au bureau" />
                                <label for="dewey">Étre livré au bureau</label>
                                <div class="form-group" id="quart">

                                    <div class="col-md-10">
                                    <select name="quartier_id" required>

                                    <option value="" disabled selected>Sélectionner le quartier</option>
                                    @foreach($quartiers as $quartier)
                                        <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                                        @endforeach
                                        <span style="color: red">@if($errors->has('quartier')) {{ $errors->first('quartier') }}@endif</span>
                                    </select>

                                    </div>
                                    <label for="usr">Adresse du récipiendaire*</label>
                                    <input type="text" name="adresse" class="validate" class="form-control" id="usr" placeholder="Adresse">
                                    <label for="usr">Téléphone du récipiendaire*</label>
                                    <input type="text" name="phone" class="validate" class="form-control" id="usr" placeholder="Téléphone">
                                    <label for="usr">Nom et Prénom du récipiendaire*</label>
                                    <input type="text" name="name" class="validate" class="form-control" id="usr" placeholder="Nom&Prénom">
                                    <h6>En cliquant sur terminer, vous permettez à JALO d'utiliser le Nom, le(s) Prénoms et le téléphone du récipiendaire pour la livraison de votre commande</h6>

                                    <div class="form-group">
                                    <button type="submit" value="submit" class="add-bucket-btn waves-effect waves-black add_to_cart" style="margin-left:80px;">Terminer</button>
                                    </div>
                                </div>

                            </div>
                            {!! form::close() !!}


                        </fieldset>


                        <fieldset>

                            {!! Form::open(['route'=>'address.store', 'method' => 'post']) !!}

                            <div>

                                <input type="radio" id="louie" 
                                    name="livraison" onClick="livrai()" value="Offrire cette livraison à une connaissance" />
                                    <label for="louie">Offrir cette livraison à une connaissance</label>
                                <div class="form-group" id="quartie">

                                    <div class="col-md-10">
                                   <select name="quartier_id" required>

                                    <option value="" disabled selected>Sélectionner le quartier</option>
                                    @foreach($quartiers as $quartier)
                                        <option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                                        @endforeach
                                        <span style="color: red">@if($errors->has('quartier')) {{ $errors->first('quartier') }}@endif</span>
                                    </select>
                                    </div>
                                    <label for="usr">Adresse du récipiendaire*</label>
                                    <input type="text" name="adresse" class="validate" class="form-control" id="usr" placeholder="Adresse">
                                    <label for="usr">Téléphone du récipiendaire*</label>
                                    <input type="text" name="phone" class="validate" class="form-control" id="usr" placeholder="Téléphone">
                                    <label for="usr">Nom et Prénom du récipiendaire*</label>
                                    <input type="text" name="name" class="validate" class="form-control" id="usr" placeholder="Nom&Prénom">
                                    <h6>En cliquant sur terminer, vous permettez à JALO d'utiliser le Nom, le(s) Prénoms et le téléphone du récipiendaire pour la livraison de votre commande</h6>
                                    <div class="form-group">
                                    <button type="submit" value="submit" class="add-bucket-btn waves-effect waves-black add_to_cart" style="margin-left:80px;">Terminer</button>
                                    </div>
                                </div>


                            </div>
                            {!! form::close() !!}

                        </fieldset>

                        <fieldset>

{!! Form::open(['route'=>'address.store', 'method' => 'post']) !!}

<div>

    <input type="radio" id="louis" 
        name="livraison" onClick="livrais()" value="livraison au point relais" />
        <label for="louis">Livraison dans un point relais</label>
    <div class="form-group" id="quartiers">

        <div class="col-md-10">
       <select name="quartier_id" required>

        <option value="" disabled selected>Sélectionner le quartier</option>
        @foreach($quartiers as $quartier)

            <ul id="menu">
                <li><option value="{{$quartier->id}}">{{$quartier->nom}}</option>
                    <ul>
                    <li class="ui-state-disabled"><div>Home Entertainment</div></li>
                    <li><div>Car Hifi</div></li>
                    <li><div>Utilities</div></li>
                </ul>
                </li>
            </ul>

            @endforeach
            <span style="color: red">@if($errors->has('quartier')) {{ $errors->first('quartier') }}@endif</span>
        </select>
        </div>
        <label for="usr">Adresse du récipiendaire*</label>
        <input type="text" name="adresse" class="validate" class="form-control" id="usr" placeholder="Adresse">
        <label for="usr">Téléphone du récipiendaire*</label>
        <input type="text" name="phone" class="validate" class="form-control" id="usr" placeholder="Téléphone">
        <label for="usr">Nom et Prénom du récipiendaire*</label>
        <input type="text" name="name" class="validate" class="form-control" id="usr" placeholder="Nom&Prénom">
        <h6>En cliquant sur terminer, vous permettez à JALO d'utiliser le Nom, le(s) Prénoms et le téléphone du récipiendaire pour la livraison de votre commande</h6>
        <div class="form-group">
        <button type="submit" value="submit" class="add-bucket-btn waves-effect waves-black add_to_cart" style="margin-left:80px;">Terminer</button>
        </div>
    </div>


</div>
{!! form::close() !!}

</fieldset>


            </fieldset>


    </div>
   

</div>

<!-- <div class="articles">
    <form method="GET" action=_output.html>
        <table align="center">
        <fieldset>

                <tr>

                    <th>
                    <input type="radio" id="dewey" 
                    name="livraison" onClick="col()" value="Étre livrer au Bureau" />
                    <label for="id_application_method" onClick="col()">Application method:</label>
                    </th>
                    <td>

                   
                    <div class="form-group col-mg-4" style="width:340px; margin-top: 10px;">
            <label for="sel1" id="quaritier">Quartier*</label>
            <select name="quartier_id" id="id_application_method" required>
              <option value="" disabled selected>quartier</option>
              @foreach($quartiers as $quartier)
              <option value="A">{{$quartier->nom}}</option>
              @endforeach
              <span style="color: red">@if($errors->has('quartier_id')) {{ $errors->first('quartier_id') }}@endif</span>
              </select>
          </div>

    </div>
                    </td>

                </tr>

            <tr>

                <th>
                <label for="id_A">Adresse*</label>
                </th>

                <td>

                 <div class="form-group">
                    <div name="aerial_size_dist" id="id_A">
                    
                        <input type="text" name="adresse" class="validate" class="form-control" id="usr" placeholder="Adresse">
                        <input type="hidden" name="phone" class="validate" class="form-control" id="usr" placeholder="Téléphone">

                    </div>
                </div>  

                </td>

            </tr>
    </fieldset>

                 <tr>

                            <th>

                            <label for="id_B">Ground spray type:</label>

                            </th>

                        <td>
                        <select name="ground_spray_type" id="id_B">
                        <option value="B1" selected="selected">B1</option>
                        <option value="B2">B2</option>
                        </select>

                        </td>
                </tr> 

        </table>                  
    </form>
</div>
 -->

                
                
    </div>
   </div>
</div>
</main>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( "#menu" ).menu();
  } );
  </script>
  <script>
  /* $( function() {
    $( ".controlgroup" ).controlgroup()
    $( ".controlgroup-horizontal" ).controlgroup({
      "direction": "vertical"
    });
    $( ".control-group" ).control-group()
    $( ".control-group-vertical" ).control-group({
      "direction": "vertical"
    });

  } ); */
  </script>

  <script type="text/javascript" src=" https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.js"></script>
  <script>
/* $('#id_application_method').hide();
  function col(){
    $('#id_application_method').show();
   
 } */

 $('#quar').hide();
  function livr(){
    $('#quar').show();
    $('#quart').hide();
    $('#quartie').hide();

   
 }

 $('#quart').hide();
  function livrer(){
    $('#quart').show();
    $('#quar').hide();
    $('#quartie').hide();
    $(".lop").addClass("important blue");


}

$('#quartie').hide();
  function livrai(){
    $('#quartie').show();
    $('#quar').hide();
    $('#quart').hide();


   
 }

 $('#quartiers').hide();
  function livrais(){
    $('#quartiers').show();
    $('#quar').hide();
    $('#quart').hide();
    $('#quartie').hide();

   
 }
 $( "#menu" ).menu();
  /* $(document).ready(function() {
    var $aerialTr = $('#id_A').closest('tr').hide();
    var $groundSprayTr = $('#id_B').closest('tr').hide();

    $('#id_application_method').change(function() {
        var selectedValue = $(this).val();

        if(selectedValue  === 'A') {
            $aerialTr.show();
            $groundSprayTr.hide();
        } else if (selectedValue === 'B') {
            $aerialTr.hide();
            $groundSprayTr.show();

        } else {
            $aerialTr.hide();
            $groundSprayTr.hide();
        }
    });

}); */
  </script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
 
</body>
</html>


