<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Jalo</title>
<link rel="stylesheet" href="../css/app.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
</head>
<style>
0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}

.container1{

  background-color: lightyellow;
}
</style>

<body>

<header class="header">

            <div class="header-top">
                <div class="row">
                    <div class="small-12 medium-4 large-2">
                    <a href="/"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
                    </div>
                        
                </div>
            </div>

        <div class="row">

            <div class="top-bar" id="example-animated-menu" data-animate="hinge-in-from-top spin-out">

                <div class="top-bar-right">
                    <ul class="menu">
                    <li><a href="{{route('cart.index')}}" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span>{{Cart::count()}}</span></a></li>
                    </ul>
                </div>
            </div>
        </div>

</header>

<main class="main">



          <div class="container1">

<div class="row" style="margin-left:50px; margin-top:50px;">
  <div class="col-lg-3">

  </div>

  <div class="col-lg-7" style="margin-left:100px;">
<form action="your-server-side-code" method="POST">
  <script
    src="https://checkout.stripe.com/checkout.js" class="stripe-button"
    data-key="pk_test_TYooMQauvdEDq54NiTphI7jx"
    data-amount="999"
    data-name="Stripe.com"
    data-description="Example charge"
    data-image="https://stripe.com/img/documentation/checkout/marketplace.png"
    data-locale="auto"
    data-zip-code="true">
  </script>
</form>
  </div>
</div>
</div>
  

        </main>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171493377-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171493377-1');
</script>
        
</body>
</html>


