<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
<style>
body {font-family: Arial, Helvetica, sans-serif;}
form {border: 3px solid #f1f1f1;}

input[type=text], input[type=password] {
    width: 100%;
    padding: 12px 20px;  
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    box-sizing: border-box;
}

button {
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    cursor: pointer;
    width: 100%;
}

button:hover {
    opacity: 0.8;
}

.cancelbtn {
    width: auto;
    padding: 10px 18px;
    background-color: #f44336;
}

.imgcontainer {
    text-align: center;
    margin: 24px 0 12px 0;
}

img.avatar {
    width: 40%;
    border-radius: 50%;
}

.container {
    padding: 16px;
}

span.psw {
    float: right;
    padding-top: 16px;
}

/* Change styles for span and cancel button on extra small screens */
@media screen and (max-width: 300px) {
    span.psw {
       display: block;
       float: none;
    }
    .cancelbtn {
       width: 100%;
    }
}

.container1{

  background-color: lightyellow;
}
</style>
</head>
<body>

  <div class="container1">

    <nav class="navbar navbar-expand-sm bg-warning navbar-warning">
      <!-- Links -->

        <a class="navbar-brand" href="#">
      <img src="../images/logo-black.jpeg" alt="Logo" style="width:80px; margin-left: 600px;">
        </a>
        <!-- Links -->
  
    </nav>

    <div class="row" style="margin-left:50px; margin-top:50px;">
      <div class="col-lg-3">


          <form action="/login" method="post">
                        {{ csrf_field() }}
              <span style="color: red">@if (session('message')) {{ session('message') }} @endif</span>

              <h2 style="font-size:20px;">Déjà client, connectez-vous</h2>

            <div class="imgcontainer">
              <img src="../images/avatar-placeholder.png" alt="Avatar" class="avatar">
            </div>

            <div class="container">

            <div class="input-field">

              <label for="uname" data-error="votre adresse  email est invalide" ><b>Adresse e-mail*</b></label>
              <input type="text" placeholder="votre adresse e-mail" name="email" required>
              @if (session('message')) {{ session('message') }} @endif

  </div>


<div class="input-field">
              <label for="psw" data-error="Votre mot de passe est incorect"><b>Password</b></label>
              <input type="password" placeholder="Entrer votre mot de passe" name="password" required>
              @if (session('message')) {{ session('message') }} @endif
  </div>
              <button type="submit" value="submit" class="btn btn-warning">Login</button>
              <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Souvient moi') }}
                                    </label>
                                </div>
                            </div>
                        </div>
            </div>

            <div class="container" style="background-color:#f1f1f1">
            <a class="btn btn-link" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
            </div>
          </form>


      </div>

      <div class="col-lg-7" style="margin-left:100px;">


          <form action="/register" method="post">
                        {{ csrf_field() }}

            <h2  style="font-size:20px;">Créer votre compte pour continuer</h2>

            <div class="imgcontainer">
              <img src="img_avatar2.jpg" alt="" class="avatar">
            </div>



<div class="form-row">

 <div class="form-group col-mg-4" style="width: 340px;">

<label for="usr">Nom*</label>
<input type="text" name="name" class="validate" class="form-control" id="usr" placeholder="Nom" required>

<span style="color: red">@if($errors->has('name')) {{ $errors->first('name') }}@endif</span>
</div>
  <div class="form-group col-mg-4" style="width: 340px;">
  <label for="usr"  data-error="votre adresse eamil est invalide">Email*</label>
  <input type="text" name="email" class="validate" class="form-control" id="usr"
  placeholder="exemple@gmail.com" required>
  </div>
                  

</div>

  <div class="form-row">

          <div class="form-group col-mg-4" style="width: 340px;">
        <label for="usr" data-error="Votre mot de passe est incorect">Mot de passe</label>
          <input type="password" class="validate" name="password" class="form-control" id="usr" required>
          <span style="color: red">@if($errors->has('password')) {{ $errors->first('password') }}@endif</span>
          </div>

            <div class="form-group col-mg-4" style="width: 340px;">

          <label for="usr" data-error="Votre mot de passe est incorect">Confirmer mdp</label>
          <input type="password" name="password_confirmation" class="validate" class="form-control" id="usr" required>


          </div>

  </div>

            
              <button type="submit" value="submit" class="btn btn-warning">S'inscrire</button>
          </form>


      </div>
    </div>
  </div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171493377-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171493377-1');
</script>

</body>
</html>
