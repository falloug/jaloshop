
<!DOCTYPE html>
<html>
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>JALO ECOMMERCE</title>
		<!-- Google font -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick-theme.css')}}"/>
		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/nouislider.min.css')}}"/>
		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}"/>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    </head>
    <body>

    <!-- HEADER -->
		<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><span class="glyphicon glyphicon-earphone"></span> 77-225-94-32 ou 78-384-01-04</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-envelope"></span> info@jaloshops.com</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-home"></span> Résidence Fatou Kasse, Sacré Coeur III</a></li>
					</ul>
					<ul class="header-links pull-right">
						<li><a href="/inscription"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>
						<li><a href="/connexion"><span class="glyphicon glyphicon-user"></span> Connexion</a></li>
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img style="width:100px;" src="./img/jalo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									
									<!-- <input class="input" placeholder="Search here">
									<button class="search-btn"><span class="glyphicon glyphicon-search"></span> Search
                                        </button> -->
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
						<div class="col-md-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								<div>
									
								</div>
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="collaps navbar-collaps" id="bs-example-navbar-collaps-1">
									<ul class"navbar-nav navbar-right">
									<!-- <li>
									
									<a href="{{ route('produit.shoppingCart')}}" style="color:white">
									<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"> Shopping Cart
										<span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
										</a>
										</li> -->
									</ul>
									
										
										
									
								</div>
								<!-- /Cart -->

								
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->
    <div class="container" style="width:600px;">
<h3 style="margin-top:50px;">Veuillez-vous connecter?</h3>
    @if(Session::has('flash_message_error'))
        <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert"></button>
       <strong>
      {!! session('flash_message_error') !!}
      </strong>
      </div>

        @endif   

        @if(Session::has('flash_message_success'))
        <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert"></button>
       <strong>
      {!! session('flash_message_error') !!}
      </strong>
      </div>

        @endif      

<form action="/connexion" method="post" class="section">
        {{ csrf_field() }}

        <div class="field">
            <label class="label">Adresse e-mail</label>
            <div class="control">
                <input class="input" type="email" name="email" value="{{ old('email') }}">
            </div>
            @if($errors->has('email'))
                <p style="color:red" class="help is-danger">{{ $errors->first('email') }}</p>
            @endif
        </div>

        <div class="field">
            <label class="label">Mot de passe</label>
            <div class="control">
                <input class="input" type="password" name="password">
            </div>
            @if($errors->has('password'))
                <p style="color:red" class="help is-danger">{{ $errors->first('password') }}</p>
            @endif
        </div>

        <div class="field">
            <div class="control" style="margin-top:10px;">
               <button class="button is-link btn btn-warning" type="submit">Se connecter</button>
                <a href="/inscription" style="margin-left:30px; background-color:yellow;"><strong>Inscription ?</strong></a>

            </div>
        </div>

    </form>
    </div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171493377-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171493377-1');
</script>

   </body>
</html>
 
