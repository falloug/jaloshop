<!-- <div id="coin-slider">

@foreach($products as $slider)

<a href="{{ $slider->name }}"><img id="myimage" src="{{url('images', $slider->image)}}" style="width:960; height:320">
<span>{{ $slider->name }}</span>
</a>
@endforeach
</div> -->


<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>

<div class="container-flix">
<div class="row" style="width:980px;">
<div class="col-lg-12">
  <div id="myCarousel" class="carousel slide" data-ride="carousel" > 
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>

    </ol>

                        <!-- Wrapper for slides -->
                        <div class="carousel-inner">
                        <div class="item active">
                            <img src="./images/jumbotron-background.jpg" alt="Los Angeles" style="width:100%; height:310px">

                            <div class="row align-middle">
                                    <div class="small-12 medium-8 large-5" style="margin-top:-350px; margin-left:20px;">
                                        <h2 class="jumbotron-title" style="color:white;">JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines</h2>
                                        <div class="jumbotron-btn" style="display:flex;">
                                            <a href="/boutique" class="btn yellow waves-effect waves-black"  style="font-size:10px; color:black;">Voir nos boutiquiers</a>
                                            <a href="/catalogues" class="btn white waves-effect waves-black"  style="font-size:10px; color:black; margin-left:10px;">Voir notre catalogue </a>
                                        </div>
                                    </div>
                            </div>
                              <img class="preview-mobile" style="width:400px; margin-left:580px; margin-top:-200px;" src="./images/nexus5x.png" alt="">

                        </div>

                        <div class="item">
                            <img src="./images/tablet-home.jpg" alt="Chicago" style="width:100%; height:300px">

                             <div class="row align-middle">
                                    <div class="small-12 medium-8 large-5" style="margin-top:-350px; margin-left:20px;">
                                        <h2 class="jumbotron-title" style="color:white;">JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines</h2>
                                        <div class="jumbotron-btn" style="display:flex;">
                                            <a href="/boutique" class="btn yellow waves-effect waves-black"  style="font-size:12px; color:black;">Voir nos boutiquiers</a>
                                            <a href="/catalogues" class="btn white waves-effect waves-black"  style="font-size:12px; color:black; margin-left:10px;">Voir notre catalogue </a>
                                        </div>
                                    </div>
                            </div>
                              <img class="preview-mobile" style="width:400px; margin-left:850px; margin-top:-200px;" src="./images/nexus5x.png" alt="">

                        </div>
                        
                        <div class="item">
                            <img src="./images/promo.png" alt="New york" style="width:100%; height:300px">

                             <div class="row align-middle">
                                    <div class="small-12 medium-8 large-5" style="margin-top:-350px; margin-left:20px;">
                                        <h2 class="jumbotron-title" style="color:white;">JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines</h2>
                                        <div class="jumbotron-btn" style="display:flex;">
                                            <a href="/boutique" class="btn yellow waves-effect waves-black"  style="font-size:12px; color:black;">Voir nos boutiquiers</a>
                                            <a href="/catalogues" class="btn white waves-effect waves-black"  style="font-size:12px; color:black; margin-left:10px;">Voir notre catalogue </a>
                                        </div>
                                    </div>
                            </div>
                              <img class="preview-mobile" style="width:400px; margin-left:850px; margin-top:-200px;" src="./images/nexus5x.png" alt="">

                        </div>
                        <div class="item">
                            <img src="./images/background-slider.png" alt="New york" style="width:100%; height:300px">

                             <div class="row align-middle">
                                    <div class="small-12 medium-8 large-5" style="margin-top:-350px; margin-left:20px;">
                                        <h2 class="jumbotron-title" style="color:white;">JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines</h2>
                                        <div class="jumbotron-btn" style="display:flex;">
                                            <a href="/boutique" class="btn yellow waves-effect waves-black"  style="font-size:12px; color:black;">Voir nos boutiquiers</a>
                                            <a href="/catalogues" class="btn white waves-effect waves-black"  style="font-size:12px; color:black; margin-left:10px;">Voir notre catalogue </a>
                                        </div>
                                    </div>
                            </div>
                              <img class="preview-mobile" style="width:400px; margin-left:850px; margin-top:-200px;" src="./images/nexus5x.png" alt="">

                        </div>
                        </div>
                       
                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="glyphicon glyphicon-chevron-left"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="glyphicon glyphicon-chevron-right"></span>
                    <span class="sr-only">Next</span>
                    </a>
  </div>
</div>
</div>
</div>

</body>
</html>
