@extends('layout.deco1')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card" style="margin-top:50px;">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif


                           <strong>MERCI <p>{{ Auth::user()->name }} ! Votre compte a bien été créé</p></strong>
                           <b><a href="/cart" class="button" style="background-color:black; color:yellow; font-size:18px;">Poursuivre votre commande</a></b>
   </div>
            </div>
        </div>
    </div>
</div>
@endsection
