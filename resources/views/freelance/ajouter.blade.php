@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">
   
            <div class="animated fadeIn">
  

                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Freelance</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                    
                                  <hr>
                                  {!! Form::open(['route'=>'freelance.store', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="nom" class="control-label mb-1">Nom du Freelance</label>
                                          <input id="nom" name="nom" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="adresse" class="control-label mb-1">Adresse du Freelance</label>
                                          <input id="adresse" name="adresse" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="phone" class="control-label mb-1">Téléphone du Freelance</label>
                                          <input id="phone" name="phone" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                    
                                      <div class="form-group">
                                      <label for="quartier_id" class="control-label mb-1">Quartier du Freelance</label>
                                          <div class="col-md-10">
                                          {!! Form::select('quartier_id',$quartiers, null, ['class'=>'form-controll', 'placeholder'=> 'select quartier du Freelance']) !!}
                                          {!! $errors->has('quartier_id')?$errors->first('quartier_id'):'' !!}
                                          </div>
                                      </div>

                                      <div class="form-group">
                                      <label for="parametre_id" class="control-label mb-1">parametre</label>
                                          <div class="col-md-10">
                                          {!! Form::select('parametre_id',$parametres, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner parametre']) !!}
                                          {!! $errors->has('parametre_id')?$errors->first('parametre_id'):'' !!}
                                          </div>
                                        </div>

                                        <div class="form-group">
                                        <label for="usr" data-error="Votre mot de passe est incorect">Mot de passe(min:6 caractères)*</label>
                                        <input type="password" class="validate" name="password" placeholder="mot de passe" class="form-control" id="usr" required>
                                        <span style="color: red">@if($errors->has('password')) {{ $errors->first('password') }}@endif</span>
                                        </div>

                                        <div class="form-group">
                                        <label for="usr" class="control-label mb-1">Confirmer mot de passe*</label><br>
                                        <input type="password" name="password_confirmation" placeholder="confirmer mot de passe" class="validate" class="form-control" id="usr" required>
                                        <span style="color: red">@if($errors->has('password_confirmation')) {{ $errors->first('password_confirmation') }}@endif</span>
                                        </div>

                                <div class="form-group">
                                        <label for="usr" data-error="Votre mot de passe est incorect">Email*</label><br>
                                        <input type="email" class="validate" name="email" placeholder="example@gmail.com" class="form-control" id="usr">
                                        <span style="color: red">@if($errors->has('email')) {{ $errors->first('email') }}@endif</span>
                                </div>

                                 <div class="form-group">
                                      <label for="role_id" class="control-label mb-1">Role</label><br>
                                          <div class="col-md-10">
                                          {!! Form::select('role_id',$roles, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner le role']) !!}
                                          {!! $errors->has('role_id')?$errors->first('role_id'):'' !!}
                                          </div>
                                        </div>
   
                                <div class="form-group">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="condiction" value="J'accepte les conditions générales de vente" id="remember" required>
                                                <label class="form-check-label" for="remember">
                                                    J'accepte les conditions générales de vente
                                                </label>
                                            </div>
                                </div>
                          

                                      <div>
                                          <button id="submit" name="submit" style="margin-top:20px; background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection