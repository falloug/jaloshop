@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Freelances</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom du freelance</th>
                        <th>Téléphone du freelance</th>
                        <th>Adresse du freelance</th>
                        <th>Quartier du freelance</th>
                        <th>Profile du freelance</th>
                        <th>Pourcentage freelance</th>
                        <th>Date de Création</th>
                        <th>Supprimer</th>

                        </tr>
                        </thead>

                        <tbody>


                        @foreach($freelances as $freelance)
                        <tr>
                        <td>{{ $freelance->id }}</td>
                        <td>{{ $freelance->nom}}</td>
                        <td>{{ $freelance->phone}}</td>
                        <td>{{ $freelance->adresse}}</td>
                        <td>{{ $freelance->quartier->nom}}</td>
                        <td>{{ $freelance->parametre->nom}}</td>
                        <td>{{ $freelance->parametre->pourcentage}}%</td>
                        <td>{{ $freelance->created_at }}</td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['freelance.destroy', $freelance->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>
                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      {{ $freelances->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
         
