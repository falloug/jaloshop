@extends('layout.produit')

@section('header')
<h2>Mes produits</h2>
@stop

@section('content')
<div class="command">


@foreach($commands as $command)
<div class="card  hoverable" style="display: flex; padding-right:-2px; margin: 15px; margin-left: 120px; width:250px;">
    
    <div id="hide" class="card-reveal">
    <p><span><B>Adresse:</B></span>{{ $command->address }}</p>
    <p><span><B>prixBien:</B></span>{{ $command.$produit->prixBien }}</p>


        {!! Form::open(['url'=>'command','class'=>'form-horizontal']) !!}

        {!! Form::number('quantity',null, ['class'=>'form-controll']) !!}
        {!! Form::submit('Ajouter',['class'=>'btn btn-primary']) !!}

        {!! Form::close() !!}

    </div>
    <div class="button" style="margin-left:150px; margin-bottom:20px;">
  </div>
  </div>
  @endforeach
  </div>
@stop


<div class="articles">
        <table align="center">
        <fieldset>

                <tr>

                    <th>
                    <input type="radio" id="dewey" 
                    name="livraison" onClick="col()" value="offrir la livraison à une connaisance" />
                    <label for="id_application_method" onClick="col()">Offrir la livraison à une connaisance</label>
                    </th>
                    <td>

                    <!-- <select  name="application_method" id="id_application_method">
                    <option value="">Pick first</option>
                    <option value="A">Aerial</option>
                    <option value="B">Ground</option>

                    
                    </select> -->
                    <div class="form-group col-mg-4" style="width:340px; margin-top: 10px;">
            <label for="sel1" id="quaritier">Quartier*</label>
            <select name="quartier_id" id="id_application_method" required>
              <option value="" disabled selected>quartier</option>
              @foreach($quartiers as $quartier)
              <option value="A">{{$quartier->nom}}</option>
              @endforeach
              <span style="color: red">@if($errors->has('quartier_id')) {{ $errors->first('quartier_id') }}@endif</span>
              </select>
          </div>

    </div>
                    </td>

                </tr>

            <tr>

                <th>
                <label for="id_A">Adresse*</label>
                </th>

                <td>

                 <div class="form-group">
                    <div name="aerial_size_dist" id="id_A">
                    
                        <input type="text" name="adresse" class="validate" class="form-control" id="usr" placeholder="Adresse">
                        <input type="text" name="phone" class="validate" class="form-control" id="usr" placeholder="Téléphone">
                        <input type="text" name="name" class="validate" class="form-control" id="usr" placeholder="Nom&Prénom">


                    </div>
                </div>  

                </td>

            </tr>

                </fieldset>

        </table>                  
</div>
