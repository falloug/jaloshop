

<div class="container2" style="width:500px; margin-left:500px;">  
  <div class="panel panel-default col-lg-12">
                                
        <div class="panel-body">
          <div class="form"> 
{!! Form::open(['route'=>'product.store', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}

<div class="form-group">
{!! Form::label('name','name',['class'=>'control-label col-md-2']) !!}

<div class="col-md-10">
{!! Form::text('name',null, ['class'=>'form-controll']) !!}
{!! $errors->has('name')?$errors->first('name'):'' !!}
</div>

</div>

<div class="form-group">
{!! Form::label('description','Description', ['class'=>'control-label col-md-2']) !!}

<div class="col-md-10">
{!! Form::textarea('description',null, ['class'=>'form-controll']) !!}
{!! $errors->has('description')?$errors->first('description'):'' !!}

</div>

</div>

<div class="form-group">
{!! Form::label('price','PrixBien',['class'=>'control-label col-md-2']) !!}

<div class="col-md-10">
{!! Form::text('price',null, ['class'=>'form-controll']) !!}
{!! $errors->has('price')?$errors->first('price'):'' !!}

</div>

</div>



<div class="form-group">
{!! Form::label('tarif','Tarif',['class'=>'control-label col-md-2']) !!}

<div class="col-md-10">
{!! Form::text('tarif',null, ['class'=>'form-controll']) !!}
{!! $errors->has('tarif')?$errors->first('tarif'):'' !!}

</div>

</div>

<div class="form-group">
{!! Form::label('image','Image',['class'=>'control-label col-md-2']) !!}

<div class="col-md-10">
{!! Form::file('image',null, ['class'=>'form-controll']) !!}
{!! $errors->has('image')?$errors->first('image'):'' !!}

</div>

</div>

<div class="form-group">
{!! Form::label('category_id','Category',['class'=>'control-label col-md-2']) !!}

<div class="col-md-10">
{!! Form::select('category_id',$categories, null, ['class'=>'form-controll', 'placeholder'=> 'select']) !!}
{!! $errors->has('category_id')?$errors->first('category_id'):'' !!}

</div>

</div>

<div class="form-group">
<div class="col-md-offset-2 col-md-10">
{!! Form::submit('Ajouter',['class'=>'btn btn-primary']) !!}
</div>

</div>
{!! Form::close() !!} 
</div>                              
</div>
</div>                              
</div>
</div>
