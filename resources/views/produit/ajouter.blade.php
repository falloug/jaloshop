@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Produits</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                 {!! Form::open(['route'=>'product.store', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal', 'enctype' =>'multipart/form-data']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="name" class="control-label mb-1">Nom du Produit</label>
                                          <input id="name" name="name" type="text" class="form-control" aria-required="" aria-invalid="" required>
                                      </div>
                                      <div class="form-group has-success">
                                          <label for="description" class="control-label mb-1">Description du Produit</label>
                                          <input id="description" name="description" type="textareat" class="form-control cc-name valid" data-val="" required>
                                          <span class="help-block field-validation-valid" data-valmsg-for="description" data-valmsg-replace="true"></span>
                                      </div>
                                      <div class="form-group">
                                          <label for="price" class="control-label mb-1">Prix de Vente</label>
                                          <input id="cc-number" name="price" type="tel" class="form-control cc-number identified visa" value="" data-val="" required>
                                          <span class="help-block" data-valmsg-for="cc-number" data-valmsg-replace="true"></span>
                                      </div>
                                      <div class="form-group">
                                          <label for="prix_achat" class="control-label mb-1">Prix d'Achat</label>
                                          <input id="prix_achat" name="prix_achat" type="tel" class="form-control cc-number identified visa" value="" data-val="">
                                          <span class="help-block" data-valmsg-for="prix_achat" data-valmsg-replace="true"></span>
                                      </div>
                                      <div class="form-group">
                                          <label for="prix_concu" class="control-label mb-1">Prix du Concurrent</label>
                                          <input id="prix_concu" name="prix_concu" type="tel" class="form-control cc-number identified visa" value="" data-val="">
                                          <span class="help-block" data-valmsg-for="prix_concu" data-valmsg-replace="true"></span>
                                      </div>
 <div class="form-group">
                                          <label for="marge" class="control-label mb-1">Marge</label>
                                          <input id="marge" name="marge" type="number" class="form-control cc-number identified visa" value="" data-val="">
                                          <span class="help-block" data-valmsg-for="marge" data-valmsg-replace="true"></span>
                                      </div>
                                      <div class="form-group">
                                          <label for="stock" class="control-label mb-1">Stock</label>
                                          <input id="stock" name="stock" type="number" class="form-control cc-number identified visa" value="" data-val="">
                                          <span class="help-block" data-valmsg-for="stock" data-valmsg-replace="true"></span>
                                      </div>
                                      <div class="form-group">
                                          <label for="pourcentage_om" class="control-label mb-1">Pourcentage Orange Money</label>
                                          <input id="pourcentage_om" name="pourcentage_om" type="number" class="form-control cc-number identified visa" value="" data-val="">
                                          <span class="help-block" data-valmsg-for="pourcentage_om" data-valmsg-replace="true"></span>
                                      </div>
                                      <div class="form-group">
                                          <label for="type_produit" class="control-label mb-1">Type de produit</label>
                                            <select name="type_produit" required>  
                                                                    <option value="" disabled selected>Sélectionner le type de produit</option>
                                                                   
                                                                        <option value="alimentaire">Alimentaire</option>
                                                                        <option value="ecommerce">E-commerce</option>
                                                                        <span class="help-block" data-valmsg-for="type_produit" data-valmsg-replace="true"></span>
             
                                            </select>   
                                      </div>

                                      <div class="form-group">
                                          <label for="tarif" class="control-label mb-1">livraison</label>
                                          <input id="tarif" name="tarif" type="number" class="form-control cc-number identified visa" value="" data-val="">
                                          <span class="help-block" data-valmsg-for="tarif" data-valmsg-replace="true"></span>
                                      </div>

                                      <div class="form-group">
                                          <label for="promo_prix" class="control-label mb-1">Promotion(Mettez 0 si ce n'est pas en promotion)</label>
                                          <input id="promo_prix" name="promo_prix" type="number" class="form-control cc-number identified visa" value="" data-val="" required>
                                          <span class="help-block" data-valmsg-for="tarif" data-valmsg-replace="true"></span>
                                      </div>

                                      <div class="form-group">
                                          <label for="pourcentage_jalo" class="control-label mb-1">Pourcentage Jalo</label>
                                          <input id="pourcentage_jalo" name="pourcentage_jalo" type="number" class="form-control cc-number identified visa" value="" data-val="">
                                          <span class="help-block" data-valmsg-for="tarif" data-valmsg-replace="true"></span>
                                      </div>

                                      <div class="form-group">
                                          <label for="pourcentage_boutiquier" class="control-label mb-1">Pourcentage Boutiquier</label>
                                          <input id="pourcentage_boutiquier" name="pourcentage_boutiquier" type="number" class="form-control cc-number identified visa" value="" data-val="">
                                          <span class="help-block" data-valmsg-for="tarif" data-valmsg-replace="true"></span>
                                      </div>

                                      <div class="form-group">
                                          <label for="image" class="control-label mb-1">Image du Produit</label>
                                          <input id="image" name="image" type="file" class="form-control cc-number identified visa" value="" data-val="" required>
                                          <span class="help-block" data-valmsg-for="image" data-valmsg-replace="true"></span>
                                      </div>

                                      <div class="form-group">
                                      <label for="category_id" class="control-label mb-1">Categorie</label>
                                          <div class="col-md-10">
                                          {!! Form::select('category_id',$categories, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner categorie']) !!}
                                          {!! $errors->has('category_id')?$errors->first('category_id'):'' !!}
                                          </div>
                                      </div>  

                                        <div class="form-group">
                                      <label for="fournisseur_id" class="control-label mb-1">Fournisseur</label>
                                          <div class="col-md-10">
                                          {!! Form::select('fournisseur_id',$fournisseurs, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner fournisseur']) !!}
                                          {!! $errors->has('fournisseur_id')?$errors->first('fournisseur_id'):'' !!}
                                          </div>
                                        </div>
                                      <div>
                                          <button id="submit" name="submit" style="margin-top:20px; background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
