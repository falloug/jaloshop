@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">   

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">  
                        <div class="card-header">
                            <center><strong class="card-title">Produits</strong></center>
                        </div>
                        <div class="card-body">
                        <form method="GET" action="{{ url('listerProduct') }}">
                            <div class="row">
                                <div class="col-md-6">
                                    <input type="text" name="search" class="form-control" placeholder="Rechercher un produit" value="{{ old('search') }}">
                                </div>
                                <div class="col-md-6">
                                <button style="background-color:yellow; color:black" class="btn">Rechercher</button>
                                </div>
                            </div>
                        </form>
                          <!-- Credit Card -->
                          <button type="button" style="margin-top:20px; background:yellow;" class="button"><a href="/export/xls">Exporter les produits</a></button>

                          <div id="pay-invoice">
                              <div class="card-body">  
                              

                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">
                                 

                        <thead>
                       

                        <th>Nom du Produit</th>
                        <th>Description du Produit</th>
                        <th>Prix de Vente</th>
                        <th>Prix d'Achat</th>
                        <th>Prix du Concurrent</th>
                        <th>promo_prix</th>
                       
                            <th>Image</th>
<!--                         <th>Date de Création</th>
 -->                        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($products as $product)
                        
                        <tr>
<!--                         <td>{{ $product->id }}</td>
 -->       
                               
                        <td>{{ $product->name }}</td>
                        <td>{{ $product->description }}</td>
                        <td>{{ $product->price }}</td>
                        <td>{{ $product->prix_achat }}</td>
                        <td>{{ $product->prix_concu }}</td>
                        <td>{{ $product->promo_prix }}</td>

                          

                        <td><img src="{{ $product->image }}" style="width:50px;"></td>

<!--                         <td>{{ $product->created_at }}</td>
 -->
                        <td>
                        <a href="{{route('product.edit', $product->id)}}" class="btn btn-warning"><i class="fa fa-pencil-square-o" style="font-size:24px"></i>

                         </a>                  </td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['product.destroy', $product->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>

                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                 {{ $products->links() }}

                              </div>
                          </div>

                        </div>
                    </div> 

                  </div>
                  </div>

                  </div>
                  </div>
                  </div>

                  

                  @endsection
         
