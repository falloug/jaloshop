@extends('layouts.app')

@section('header')

@endsection

@section('content')
<div class="container">

        if(Session::has('cart')){
        <div class="row">
          <div class="col-lg-8">
            <ul class="list-group">

            @foreach($produit as $produit)
                <p><li class="list-group-item">
                <span class="badge">{{ $produit['qty'] }}</span>
                <strong>{{ $produit['item']['nomProduit'] }}</strong>
                <span class="label label-success">{{ $produit->price }}</span>

                </li></p>
            @endforeach
                </ul>
                </div>
                </div>

                <div class="row">
                <div class="col-lg-4">
                <strong>Total: {{ $totalPrice }}</strong>
                </div>
                </div>
                <hr>
                <div class="row">
                <div class="col-lg-4">
                    <button class="btn btn-warning">Checkout</button>
                </div>
            </div>

        else{
            <div class="row">
            <div class="col-lg-4">
            <h2>No Items in Cart!</h2>
            </div>
            </div>
            }
        }
      
            </div>

            @endsection