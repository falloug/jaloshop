<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Raleway', sans-serif;
                font-weight: 100;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 12px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>
                        <a href="{{ route('register') }}">Register</a>
                    @endauth
                </div>
            @endif

            <div class="content">
                <div class="title m-b-md">
                    Laravel
                </div>
                <div id="container-floating">

<a href="{{route('cart.index')}}" id="floating-button" type="button" class="navbar-toggle collapsed shopping-cart-notif modal-trigger" data-toggle="collapse" data-target="#floatingnav"  data-placement="left">
  <i id="legend" class="fa fa-tasks fa-stack-1x fa-inverse fa-2x tiny material-icons" style="top:12px" role="button" tabindex="0">shopping_cart</i><span>{{Cart::count()}}</span>
</a>
<div id="floatingnav" class="collapse">
<div class="nd1 nds" data-toggle="tooltip" data-placement="left">
<i id="locateplace" class="fa fa-search fa-stack-1x fa-inverse fa-2x" style="top:12px" ng-click="navMenuController($event);" role="button" tabindex="0"></i>
</div>
<div class="nd3 nds" data-toggle="tooltip" data-placement="left">
<i id="locateplace" class="fa fa-search fa-stack-1x fa-inverse fa-2x" style="top:12px" ng-click="navMenuController($event);" role="button" tabindex="0"></i>
</div>
<div class="nd4 nds" data-toggle="tooltip" data-placement="left">
<i id="locateplace" class="fa fa-search fa-stack-1x fa-inverse fa-2x" style="top:12px" ng-click="navMenuController($event);" role="button" tabindex="0"></i>
</div>
</div>

</div>
                <div class="links">
                    <a href="https://laravel.com/docs">Documentation</a>
                    <a href="https://laracasts.com">Laracasts</a>
                    <a href="https://laravel-news.com">News</a>
                    <a href="https://forge.laravel.com">Forge</a>
                    <a href="https://github.com/laravel/laravel">GitHub</a>
                </div>
            </div>
        </div>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-171493377-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-171493377-1');
</script>

    </body>
</html>
