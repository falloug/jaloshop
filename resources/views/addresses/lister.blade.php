@extends('layout.adminlayout.design')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Commandes</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom et Prenom</th>
                        <th>Nom du Quartier</th>
                        <th>adresse</th>
                        <th>Téléphone</th>
                        <th>livraison</th>
                        <th>Nom du Récipiendaire</th>
                        <th>Date de Création</th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($commandes as $address)
                        <tr>
                        <td>{{ $address->id }}</td>
                        <td>{{ $address->user_id }}</td>
                        <td>{{ $address->quartier_id }}</td>
                        <td>{{ $address->adresse }}</td>
                        <td>{{ $address->phone}}</td>
                        <td>{{ $address->livraison }}</td>
                        <td>{{ $address->name }}</td>
                        <td>{{ $address->created_at }}</td>

                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      {{ $commandes->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
         
