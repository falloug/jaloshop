@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">

  
                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Categories</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                      
                                  {!! Form::model($category, ['route'=>['category.update',$category->id],'method'=>'PATCH','class'=>'form-horizontal'] ) !!}                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">Nom Categorie</label>
                                          <input id="name" name="name" type="text" class="form-control" value="{{$category->name}}" aria-required="true" aria-invalid="false" required>
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">Null</label>
                                          <input id="name" name="slug" type="text" class="form-control" placeholder="Mettez zéro" aria-required="true" aria-invalid="false" required>
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">Image Categorie</label>
                                          <input id="name" name="image" type="file" class="form-control" value="{{$category->image}}" aria-required="true" aria-invalid="false" required>
                                      </div>
                            
                                      <div class="form-group">
                                          <label for="product_id" class="control-label mb-1">Product_Id</label>
                                          <input id="product_id" name="product_id" type="number" class="form-control" value="{{$category->product_id}}" aria-required="true" aria-invalid="false" required>
                                      </div>
                                    
                                      
                                      <div>
                                          <button id="submit" name="submit" style="background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection