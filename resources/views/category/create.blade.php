@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="margin-left:250px;">
                    <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Categories</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <p class="alert-success">
                                   <?php 
                                   $message = Session::get('message');
                                   if($message){
                                       echo $message;
                                       Session::put('message', null);
                                   }
                                   
                                   ?>
                                  
                                  </p>
                                  {!! Form::open(['route'=>'category.store', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">Nom Categorie</label>
                                          <input id="name" name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
 

                                    <!-- <div class="form-group">
                                          <label for="image" class="control-label mb-1">Image Categorie</label>
                                          <input id="image" name="image" type="file" class="form-control cc-number identified visa" value="" data-val="" required>
                                      </div>  -->

                                      <div class="form-group">
                                          <label for="product_id" class="control-label mb-1">Product_Id</label>
                                          <input id="product_id" name="product_id" type="number" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                    
                                      
                                      <div>
                                          <button id="submit" name="submit" style="background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection