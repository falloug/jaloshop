@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="margin-left:250px;">
                    <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Sous Categories</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <p class="alert-success">
                                   <?php 
                                   $message = Session::get('message');
                                   if($message){
                                       echo $message;
                                       Session::put('message', null);
                                   }
                                   
                                   ?>
                                  
                                  </p>
                                  {!! Form::open(['route'=>'ajout.SousCate', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">Nom Sous Categorie</label>
                                          <input id="name" name="name" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="image" class="control-label mb-1">Nom Categorie</label>
                                          {!! Form::select('category_id',$categories, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner categorie']) !!}
                                          {!! $errors->has('category_id')?$errors->first('category_id'):'' !!}                                      </div>
                                    
                                      
                                      <div>
                                          <button id="submit" name="submit" style="background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection