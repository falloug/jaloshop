@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">
   

                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Sous Categories</strong></center>
                        </div>
                          <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom Categorie</th>
                        <th>Nom Sous Categorie</th>
                        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>

                        
                        </tr>
                        </thead>

                        <tbody>

  
                        @foreach($souscats as $category)
                        <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->category->name}}</td>
                        <td>{{ $category->name}}</td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['souscat.destroy', $category->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>

                        </tr>
                        @endforeach

                      </tbody>
                      </table>

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
         
