@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">   


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Categories</strong></center>
                        </div>
                          <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                       <th>Nom du Categorie</th>
                        <th>Product Id</th>
                        <th>Date de Création</th>
                        <th>status</th>
                        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($categories as $category)
                        <tr>
                        <td>{{ $category->id }}</td>
                        <td>{{ $category->name}}</td>
                      <td><img src="{{ $category->image }}" style="width:50px;"></td>
                        <td>{{ $category->product_id}}</td>
                        
                        <td>{{ $category->created_at }}</td>
                        <td>@if($category->status==0)
                                      <b style="color:green"> Activer</b>
                                      @else
                                    <b style="color:red">  Desactiver</b>
                                      @endif
                                      <br>  
                                      <button id="showSelectDiv{{$category->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>
                                      <div id="selectDiv{{$category->id}}">
                                      <input type="hidden" id="userID{{$category->id}}" value="{{$category->id}}">
                                      <select id="categoryStatus{{$category->id}}">
                                        <option value="">select a option</option>
                                        <option value="0">Activer</option>
                                        <option value="1">Desactiver</option>
                                      </select>
                                      </div>   
                                    </td>
                                   <td><a href="" class="btn btn-fill btn-warning">Actions</a></td>

                        
   
                        <td>
                        <a href="{{route('category.edit', $category->id)}}" class="btn btn-warning"><i class="fa fa-pencil-square-o" style="font-size:24px"></i>

                         </a>                  </td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['category.destroy', $category->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>

                        </tr>
                        @endforeach

                      </tbody>
                      </table>

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                   <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

                        <script>
                        $(document).ready(function(){

                        @foreach($categories as $category)
                        $("#selectDiv{{$category->id}}").hide();
                        $("#showSelectDiv{{$category->id}}").click(function(){
                        $("#selectDiv{{$category->id}}").show();    
                        });
                         $("#categoryStatus{{$category->id}}").change(function(){
                        var status = $("#categoryStatus{{$category->id}}").val();
                        var userID = $("#userID{{$category->id}}").val()
                        if(status==""){
                        alert("please select an option");
                        }else{
                        $.ajax({
                        url: '{{url("/admin/banCategory")}}',
                        data: 'status=' + status + '&userID=' + userID,
                        type: 'get',  
                        success:function(response){
                        console.log(response);
                        }
                        });
                        }

                        });
                        @endforeach
                        });
                        </script>

                  @endsection
         
