<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>

  <div class="main">
<div class="row">    
        <header class="header-main-content">
            <div class="top-bar" style="display:flex;">
                <div class="top-bar-left" >
                    <ul class="menu">
                        <li><a href="catalogues?category=all" class="button">Toutes les catégories</a></li>
                        @if($categories != [])
                            @foreach($categories as $categorie)
                                <li><a href="{{url('catalogues')}}/{{$categorie->name}}"
                                       class="button">{{$categorie->name}}</a></li>
                            @endforeach
                        @endif  
                    </ul>
                </div>   
                <div class="top-bar-right pos-rltv">
                    <div class="fixed-action-btn horizontal">
                        <a class="waves-effect waves-yellow btn-floating btn-large black">
                            <i class="material-icons">add_shopping_cart</i>
                        </a>
                        <!-- Modal Trigger -->
                        <ul>
                            <li>
                                <a class="waves-effect waves-black btn-floating green darken-1 modal-trigger"
                                   href="#create-shop" data-position="bottom" data-delay="50"
                                   data-tooltip="Faire une commande">
                                    <i class="material-icons">add_shopping_cart</i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </header>
    </div>

    <div class="row">
        @if($products != [])
            @foreach($catalogues as $catalogue)
                <div class="small-12 medium-6 large-3 p-10">
                    <div class="card card-center">
                        <img src="https://source.unsplash.com/collection/190727" alt="">
                        <div class="card-section">
                            <h4 class="card-title">{{ $catalogue->name}}</h4>
                            <strong class="card-price">{{ $catalogue->price}} <span>F cfa</span></strong>
                            
                                <button class="add-bucket-btn waves-effect waves-black" type="submit" value="Submit"
                                        id="add_panier">
                                    Ajouter au panierF
                                    <span class="btn-floating btn-tiny waves-effect waves-yellow white">
                  <i class="material-icons">add</i>
                  </span>
                                </button>
                        </div>
                    </div>
                </div>
            @endforeach
        @endif
    </div>

    </div>

<script src="{{asset('js/jalo/jquery.min.js')}}"></script>
 <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
 <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
 <script src="{{asset('js/jalo/slick.min.js')}}"></script>
 <script src="{{asset('js/jalo/flickity.pkgd.min.js')}}"></script>
 <script src="{{asset('js/jalo/app.js')}}"></script>

 
</body>
</html>