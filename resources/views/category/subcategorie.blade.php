<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="icon" type="{{asset('image/jalo/png')}}" href="images/jalo/logo.png">
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  </head>
  <body>

  <div class="main">
    <div class="row">
        <div class="small-12 medium-12 xmedium-4 large-3">
          <ul class="multilevel-accordion-menu vertical menu" data-accordion-menu>
          @foreach($categories as $category)

                        <li>
                          <a href="#">{{$category->name}}</a>
                              <ul class="menu vertical sublevel-1">
                              @foreach($subcategories as $subcategory)

                                      <li>
                                        <a href=""><button class="search-submit">{{$subcategory->name}}</button></a> 
                                       </li>
                                       @endforeach

                                </ul>

                        </li>
                        @endforeach

          </ul>


        </div>
        </div>

  
  </div>

   <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/flickity.pkgd.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>

    
  </body>
</html>