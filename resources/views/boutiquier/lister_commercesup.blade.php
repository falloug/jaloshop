@extends('layout.adminlayout.design1')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Boutiquiers</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom du Boutiquier</th>
                        <th>Adresse du Boutiquier</th>
                        <th>Téléphone du Boutiquier</th>
                        <th>Date de Création</th>
                        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($boutiquiers as $boutique)
                        <tr>
                        <td>{{ $boutique->id }}</td>
                        <td>{{ $boutique->nom }}</td>
                        <td>{{ $boutique->adresse }}</td>
                        <td>{{ $boutique->phone}}</td>
                        <td>{{ $boutique->created_at }}</td>

                        <td>
                        <a href="{{route('boutique.edit', $boutique->id)}}" class="btn btn-warning"><i class="fa fa-pencil-square-o" style="font-size:24px"></i>

                         </a>                  </td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['boutique.destroy', $boutique->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>

                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      {{ $boutiquiers->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
         
