@extends('layout.adminlayout.design1')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">
   
            <div class="animated fadeIn">


                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Boutiquier</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  {!! Form::open(['route'=>'boutique_commercesup.ajout', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="nom" class="control-label mb-1">Nom du Boutiquier</label>
                                          <input id="nom" name="nom" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="adresse" class="control-label mb-1">Adresse du Boutiquier</label>
                                          <input id="adresse" name="adresse" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="phone" class="control-label mb-1">Téléphone du Boutiquier</label>
                                          <input id="phone" name="phone" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                    
                                      <div class="form-group">
                                      <label for="quartier_id" class="control-label mb-1">Quartier du Boutiquier</label>
                                          <div class="col-md-10">
                                          {!! Form::select('quartier_id',$quartiers, null, ['class'=>'form-controll', 'placeholder'=> 'select quartier du boutiquier']) !!}
                                          {!! $errors->has('quartier_id')?$errors->first('quartier_id'):'' !!}
                                          </div>
                                      </div>

                                      <div class="form-group">
                                      <label for="commercial_id" class="control-label mb-1">Commercial</label>
                                          <div class="col-md-10">
                                          {!! Form::select('commercial_id',$commercials, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner commercial']) !!}
                                          {!! $errors->has('commercial_id')?$errors->first('commercial_id'):'' !!}
                                          </div>
                                        </div>

                                      <div>
                                          <button id="submit" name="submit" style="margin-top:20px; background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection