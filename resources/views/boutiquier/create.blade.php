@extends('layout.adminlayout.design2')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">
   
            <div class="animated fadeIn">


                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Boutiquier</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                    
                                  <hr>
                                  {!! Form::open(['route'=>'boutique.store', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="nom" class="control-label mb-1">Nom du Boutiquier</label>
                                          <input id="nom" name="nom" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                     </div>

                                       <div class="form-group">
                                          <label for="adresse" class="control-label mb-1">Adresse du Boutiquier</label>
                                          <input id="adresse" name="adresse" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
 <div class="form-group">
                                          <label for="adresse" class="control-label mb-1">Adresse email</label>
                                          <input id="adresse" name="email" type="text" class="form-control" aria-required="true" aria-invalid="false">
                                      </div>

                                       <div class="form-group">
                                          <label for="phone" class="control-label mb-1">Téléphone du Boutiquier</label>
                                          <input id="phone" name="phone" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
 <div class="form-group" hidden>
                                          <label for="phone" class="control-label mb-1">Téléphone du Boutiquier</label>
                                          <input id="phone" name="condiction" type="text" class="form-control" value="yes" aria-required="true" aria-invalid="false" required>
                                      </div>
                                    
                                      <div class="form-group">
                                      <label for="quartier_id" class="control-label mb-1">Quartier du Boutiquier</label>
                                          <div class="col-md-10">
                                          {!! Form::select('quartier_id',$quartiers, null, ['class'=>'form-controll', 'placeholder'=> 'select quartier du boutiquier']) !!}
                                          {!! $errors->has('quartier_id')?$errors->first('quartier_id'):'' !!}
                                          </div>
                                      </div>

 <div class="form-group">
                                          <label for="type_boutiquier" class="control-label mb-1">Type de boutiquier</label>
                                            <select name="type_boutiquier">  
                                                                    <option value="" disabled selected>Sélectionner le type deboutiquier</option>
                                                                   
                                                                        <option value="alimentaire">Alimentaire</option>
                                                                        <option value="ecommerce">E-commerce</option>
                                                                        <span class="help-block" data-valmsg-for="type_boutiquier" data-valmsg-replace="true"></span>
             
                                            </select>   
                                      </div>
 <div class="form-group">
                                      <label for="role_id" class="control-label mb-1">role</label>
                                          <div class="col-md-10">
                                          {!! Form::select('role_id',$roles, null, ['class'=>'form-controll', 'placeholder'=> 'select role']) !!}
                                          {!! $errors->has('role_id')?$errors->first('role_id'):'' !!}
                                          </div>
                                      </div>

                                      <div class="form-group">
                                      <label for="commercial_id" class="control-label mb-1">Commercial</label>
                                          <div class="col-md-10">
                                          {!! Form::select('commercial_id',$commercials, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner commercial']) !!}
                                          {!! $errors->has('commercial_id')?$errors->first('commercial_id'):'' !!}
                                          </div>
                                        </div>
 <div class="form-group" hidden>

                                                                        <div class="large-6 p-0-15">
                                                                        <label for="usr" data-error="Votre mot de passe est incorect">Mot de passe(min:6 caractères)*</label>
                                                                        <input type="password" class="validate" name="password" placeholder="mot de passe" class="form-control" value="jalo2020" id="usr" required>
                                                                        <span style="color: red">@if($errors->has('password')) {{ $errors->first('password') }}@endif</span>
                                                                        </div>

                                                                                <div class="large-6 p-0-15">

                                                                            <label for="usr" data-error="Votre mot de passe est incorect">Confirmer mot de passe*</label>
                                                                            <input type="password" name="password_confirmation" placeholder="confirmer mot de passe" value="jalo2020" class="validate" class="form-control" id="usr" required>
                                                                            <span style="color: red">@if($errors->has('password_confirmation')) {{ $errors->first('password_confirmation') }}@endif</span>



                                                                            </div>

                                                </div>

                                      <div>
                                          <button id="submit" name="submit" style="margin-top:20px; background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
