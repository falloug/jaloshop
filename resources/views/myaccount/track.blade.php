
@extends('layout.deco2')

@section('content')

<div class="greyBg">
    <div class="container">
		<div class="wrapper">
      <div class="row">
				<div class="col-sm-12" style="margin-top:50px;">
				 <div class="breadcrumbs" style="margin-left:450px;">
			       <ul >
			          <li><a href="{{url('/')}}" style="color:black;">Accueil </a></li>
                 <li><span class="dot">/</span>
			          <a href="{{url('/myaccount')}}" style="color:black;"> {{Auth::user()->name}}</a></li>
                <li><span class="dot">/</span>
                  <a href="" style="color:black;">Suivie de commande</a>
			        </ul>
                        </div>
                    </div>  
	     </div>

          <div class="row top25 inboxMain" style="margin-left:300px;">
             <div class="row text-center alert" style="background-color:yellow;">
             <div class="col-md-4"><h3>Commande No:  {{$data[0]->id}}</h3> </div>
             <div class="col-md-4"><h3>Total: {{$data[0]->total}}</h3> </div>
                    @if($data[0]->status=="")
             <div class="col-md-4"><h4> Commande: <mark style="background-color:black; color:yellow;">Commande en cours</h4></mark></div>
                    @elseif($data[0]->status==0)
                    <div class="col-md-4"><h3> Commande: <mark style="background-color:black; color:yellow;">Commande en cours</h3></mark></div>
                    @elseif($data[0]->status==1)
                    <div class="col-md-4"><h3> Commande: <mark style="background-color:black; color:green;">Livraison terminée</h3></mark></div>
                    @elseif($data[0]->status==2)
                    <div class="col-md-4"><h3> Commande: <mark style="background-color:black; color:red;">Commande Annuler</h3></mark></div>
                    @elseif($data[0]->status==3)
                    <div class="col-md-4"><h3> Commande: <mark style="background-color:black; color:green;">Paiement reçu</h3></mark></div>
                    @elseif($data[0]->status==4)
                    <div class="col-md-4"><h3> Commande: <mark style="background-color:black; color:yellow;">Livraison en cours</h3></mark></div>
                    @elseif($data[0]->status==5)
                    <div class="col-md-4"><h3> Commande: <mark style="background-color:black; color:green;">Paiement en attente</h3></mark></div>
                               @else
                      <div class="col-md-4"><h3> Commande: <mark style="background-color:black; color:green;">Prêt pour expédition</h3></mark></div>
                            @endif
            </div>
            </div>
            
          <div class="myTrack" style="display:flex;">
          
               @if($data[0]->status=="")
               @include('myaccount.steps.cours')

               @elseif($data[0]->status=="0")
                @include('myaccount.steps.en_cour')

                 @elseif($data[0]->status=="1")
                @include('myaccount.steps.livrer')
                @elseif($data[0]->status=="2")
                @include('myaccount.steps.anuler')
                @elseif($data[0]->status=="3")
                @include('myaccount.steps.Paiement_reçu')
                @elseif($data[0]->status=="4")
                @include('myaccount.steps.traitement')
                @elseif($data[0]->status=="5")
                @include('myaccount.steps.Paiement_attente')
                @else
                @include('myaccount.steps.prêt_expédition')

               @endif
               </div>

       

        </div>
    </div>
  </div>
</div>
@endsection
