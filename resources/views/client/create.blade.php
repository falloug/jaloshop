<!doctype html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>JALO ECOMMERCE</title>
		<!-- Google font -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick-theme.css')}}"/>
		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/nouislider.min.css')}}"/>
		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}"/>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    </head>
  <body>
  <div class="container">
  <div class="row">
    <div class="col-lg-6 bg-primary"></div>
    <div class="col-lg-6 bg-success"></div>
  </div> 
  </div> 
<div class="container">

<section class="row">
 <div class="col-lg-12">

  <div class="row">
    <div class="col-lg-4 bg-primary">
        @if(Session::has('flash_message_error'))
        <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert"></button>
       <strong>
      {!! session('flash_message_error') !!}
      </strong>
      </div>

        @endif   

        @if(Session::has('flash_message_success'))
        <div class="alert alert-error alert-block">
        <button type="button" class="close" data-dismiss="alert"></button>
       <strong>
      {!! session('flash_message_error') !!}
      </strong>
      </div>

        @endif          
            <form id="loginform" class="form-vertical" method="post" action="/client/create">
            {{ csrf_field() }}
				 <div class="control-group normal_text"> <h3><img src="{{asset('image/backend_images/logo.png')}}" alt="Logo" /></h3></div>
                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                        <label for="email">Email</label>
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="email" name="email" placeholder="Username"  required/>
                        </div>
                    </div>
                </div>

                <div class="control-group">
                    <div class="controls">
                        <div class="main_input_box">
                        <label for="password">Password</label>
                            <span class="add-on bg_ly"><i class="icon-lock"></i></span><input type="password" name="password" placeholder="Password" required/>
                        </div>
                    </div>
                </div>

                <div class="form-actions">
                    <span class="pull-right"><input type="submit" value="Envoyer" class="btn btn-warning" /></span>
                </div>
            </form>
        </div>

    </div>
  </div>

        <div class="row">
            <div class="col-lg-4">
                {!! Form::open(['url'=>'client','class'=>'form-horizontal']) !!}

                <div class="form-group">
                {!! Form::label('nomClient','nomClient',['class'=>'control-label col-md-2']) !!}

                <div class="col-md-10">
                {!! Form::text('nomClient',null, ['class'=>'form-controll']) !!}
                {!! $errors->has('nomClient')?$errors->first('nomClient'):'' !!}
                </div>

                </div>

                <div class="form-group">
                {!! Form::label('adresse','adresse',['class'=>'control-label col-md-2']) !!}

                <div class="col-md-10">
                {!! Form::text('adresse',null, ['class'=>'form-controll']) !!}
                {!! $errors->has('adresse')?$errors->first('prixBien'):'' !!}

                </div>

                </div>

                <div class="form-group">
                {!! Form::label('telephone','telephone',['class'=>'control-label col-md-2']) !!}

                <div class="col-md-10">
                {!! Form::text('telephone',null, ['class'=>'form-controll']) !!}
                {!! $errors->has('telephone')?$errors->first('telephone'):'' !!}

                </div>

                </div>

                    <div class="form-group">
                {!! Form::label('email','email',['class'=>'control-label col-md-2']) !!}

                <div class="col-md-10">
                {!! Form::email('email',null, ['class'=>'form-controll']) !!}
                {!! $errors->has('email')?$errors->first('email'):'' !!}

                </div>

                </div>

                <div class="form-group">
                    {!! Form::label('password','password',['class'=>'control-label col-md-2']) !!}

                    <div class="col-md-10">
                    {!! Form::password('password',null, ['class'=>'form-controll']) !!}
                    {!! $errors->has('password')?$errors->first('password'):'' !!}

                    </div>

                </div>


                <div class="form-group">
                    <div class="col-md-offset-2 col-md-10">
                {!! Form::submit('Ajouter',['class'=>'btn btn-primary']) !!}
                    </div>

                </div>

                {!! Form::close() !!} 

        </div>
            </div>
  </div>
 </section>
</div>


        <script src="{{asset('js/jquery.min.js')}}"></script>
		<script src="{{asset('js/bootstrap.min.js')}}"></script>
		<script src="{{asset('js/slick.min.js')}}"></script>
		<script src="{{asset('js/nouislider.min.js')}}"></script>
		<script src="{{asset('js/jquery.zoom.min.js')}}"></script>
		<script src="{{asset('js/main.js')}}"></script>

  </body>
</html>