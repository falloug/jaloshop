<!doctype html>
<html lang="en">
<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
		 <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
		<title>JALO ECOMMERCE</title>
		<!-- Google font -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}"/>
		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/slick-theme.css')}}"/>
		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/nouislider.min.css')}}"/>
		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/style.css')}}"/>
		<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
		  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
		  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
    </head>
  <body>
  <!-- HEADER -->
  <header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><span class="glyphicon glyphicon-earphone"></span> 77-225-94-32 ou 78-384-01-04</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-envelope"></span> info@jaloshops.com</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-home"></span> Résidence Fatou Kasse, Sacré Coeur III</a></li>
					</ul>
					<ul class="header-links pull-right">
						<li><a href="/inscription"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>
						<li><a href="/connexion"><span class="glyphicon glyphicon-user"></span> Connexion</a></li>
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img style="width:100px;" src="./img/jalo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									
									<input class="input" placeholder="Search here">
									<button class="search-btn"><span class="glyphicon glyphicon-search"></span> Search
</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
						<div class="col-md-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								<div>
									
								</div>
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="collaps navbar-collaps" id="bs-example-navbar-collaps-1">
									<ul class"navbar-nav navbar-right">
									<li>
									
									<a href="{{ route('produit.shoppingCart')}}" style="color:white">
									<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"> Shopping Cart
										<span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
										</a>
										</li>
									</ul>
									
										
										
									
								</div>
								<!-- /Cart -->

								
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->
  <div class="container" style="margin-top:50px; width:550px; background-color:white;">

@if(Session::has('cart'))
<div class="row">
  <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
    <ul class="list-group">
    @foreach($produits as $produit)
        <li class="list-group-item">
        <img src="{{url('image', $produit['item']['image']) }}" alt="image" style="width:50px;">
        <span class="badge">{{ $produit['qty'] }}</span>
        <strong>{{ $produit['item']['nomProduit'] }}</strong>
        <span class="label label-success">{{ $produit['price'] }}</span>
        <div class="btn-group"> 
       
        <!-- <ul class="dropdown-menu">
        <li><a href="#">Reduce by1</a></li>
        <li><a href="#">Reduce All</a></li>

        </ul> -->
        </div>
        </li>
    @endforeach
        </ul>
        </div>
        </div>

        <div class="row">
        <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
        <strong>Total: {{ $totalPrice }}</strong>
        </div>
        </div>
        <hr>
        <div class="row">
        <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
            <a href="/connexion" class="btn btn-warning">Finaliser votre demande</a>
        </div>
        <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
            <a href="/lister" class="btn btn-warning" style="margin-top:10px;">Continuez votre shopping!</a>
        </div>
    </div>

@else
    <div class="row">
    <div class="col-lg-4">
    <h2>No Items in Cart!</h2>
    </div>
    </div>
@endif

    </div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{asset('js/backend_js//excanvas.min.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.min.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.ui.custom.js')}}"></script> 
<script src="{{asset('js/backend_js//bootstrap.min.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.flot.min.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.flot.resize.min.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.peity.min.js')}}"></script> 
<script src="{{asset('js/backend_js//fullcalendar.min.js')}}"></script> 
<script src="{{asset('js/backend_js//matrix.js')}}"></script> 
<script src="{{asset('js/backend_js//matrix.dashboard.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.gritter.min.js')}}"></script> 
<script src="{{asset('js/backend_js//matrix.interface.js')}}"></script> 
<script src="{{asset('js/backend_js//matrix.chat.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.validate.js')}}"></script> 
<script src="{{asset('js/backend_js//matrix.form_validation.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.wizard.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.uniform.js')}}"></script> 
<script src="{{asset('js/backend_js//select2.min.js')}}"></script> 
<script src="{{asset('js/backend_js//matrix.popover.js')}}"></script> 
<script src="{{asset('js/backend_js//jquery.dataTables.min.js')}}"></script> 
<script src="{{asset('js/backend_js//matrix.tables.js')}}"></script> 
  </body>
</html>