<!DOCTYPE html>
<html lang="en">
    
<head>
        <title>Matrix Admin</title><meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="{{asset('css/backend_css/bootstrap.min.css')}}" />
		<link rel="stylesheet" href="{{asset('css/backend_css/bootstrap-responsive.min.css')}}" />
        <link rel="stylesheet" href="{{asset('css/backend_css/matrix-login.css')}}" />
        <link href="{{asset('fonts/backend_fonts/font-awesome/css/font-awesome.css')}}" rel="stylesheet" />
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700,800' rel='stylesheet' type='text/css'>

    </head>
    <body>
    <div id="loginbox"> 
<h4>Votre Totale: ${{ $total }}</h4>
                    <form class="form-vertical" method="POST" action="/checkout" aria-label="{{ __('Register') }}">
                        @csrf
                        <div class="control-group normal_text"> <h3><img src="{{asset('image/backend_images/logo.png')}}" alt="Logo" /></h3></div>

                         <div class="control-group">
                            <div class="controls">
                                <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" name="nom" placeholder="Mettez votre nom"  required/>
                                </div>
                                @if ($errors->has('nom'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nom') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                            

                        <div class="control-group">
                            <div class="controls">
                                <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" name="adresse" placeholder="Mettez votre addresse"  required/>
                                </div>
                                @if ($errors->has('adresse'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('adresse') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="text" name="telephone" placeholder="Mettez votre téléphone"  required/>
                                </div>
                                @if ($errors->has('telephone'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('telephone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="email" name="email" placeholder="exemple@gmail.com"  required/>
                                </div>
                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('login') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="control-group">
                            <div class="controls">
                                <div class="main_input_box">
                            <span class="add-on bg_lg"><i class="icon-user"> </i></span><input type="password" name="password" placeholder="Mettez votre mot de passe"  required/>
                                </div>
                                @if ($errors->has('password'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                  <div class="row">
                     <div class="col-sm-6 col-md-6 col-md-offset-3 col-sm-offset-3">
                     <a href="{{ route('produit.templ')}}" class="btn btn-warning">Retour</a>
                    </div>
                  </div>  

                    <div class="form-actions">
                       <a href="/templ"><span class="pull-right"><input type="submit" value="Valider" class="btn btn-warning" /></span></a>
                    </div>
               </form>
               
            </div>
<script src="{{asset('js/backend_js/jquery.min.js')}}"></script>  
        <script src="{{asset('js//backend_js/matrix.login.js')}}"></script>
        <script src="{{asset('js/backend_js//bootstrap.min.js')}}"></script> 

    </body>

</html>
