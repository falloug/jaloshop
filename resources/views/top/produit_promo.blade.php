<!doctype html>
<html lang="en">
  <head>    
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/app.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    

  </head>
  <body>
<h2>Produit en promo</h2>
     <div class="row" style="margin-left:50px;">
@foreach($promo as $top_promo)
                    <div class="small-12 medium-6 large-3 p-10">
                        <div class="card card-center">

            <div class="img-magnifier-container" id="main">
            <img id="myimage" onClick="myImag()" src="{{url('images', $top_promo->image)}}" onmouseover="this.src='{{url('images', $top_promo->image)}}'" onmouseout="this.src='{{url('images', $top_promo->image)}}'" style="width:35; height:500">
</div>
            
                                      <div class="card-section">
                                <a href="#"><h4 class="card-title">{{ $top_promo->name}}</h4></a>
                                <div class="card-wrap-price">
<!--                                         <strong class="card-price-old"><span>{{ $top_promo->price}} F CFA</span></strong>
 -->                                        <strong class="card-price"><span>Prix:{{ $top_promo->price}}F CFA</span></strong>
                                        <strong class="card-price"><span>Promotion:{{ $top_promo->promo_prix}}%</span></strong>
                                </div>
                            </div>

                            <div class="add-to-cart">
							<a href="{{route('cart.addItem', $top_promo->id)}}"style="color:black;"><button class="add-to-cart-btn" onClick="showMsg()"><i class="fa fa-shopping-cart" style="font-size:20px; margin-left:120px;" id="imagesss" onmouseover="shopi()" onmouseout="shopin()"></i></button></a>
							</div>
                           

                                <div class="top_promo-btns">
				<a href="#" class="add-bucket-btn waves-effect waves-black" style="margin-left:70px;">
                                    <button class="quick-view">
                                    <span class="glyphicon glyphicon-eye-open">
                                    </span><span class="tooltipp">Voir les details</span>
                                    </button>
                                    </span>
                                    </a>
				</div>

                        </div>
                    </div>
@endforeach
            </div> 

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>