<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>Jalo</title>
<link rel="stylesheet" href="../css/app.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
<link href="{{asset('starter-template.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />

</head>
<style>

#presentation {
  width: 480px;
  height: 120px;
  padding: 20px;
  margin: auto;
  background: #FFF;
  margin-top: 10px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  transition: all 0.3s;
  border-radius: 3px;
}

#presentation:hover {
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  transition: all 0.3s;
  transform: translateZ(10px);
}

#floating-button {
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background:black;
  position: fixed;
  bottom: 30px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px yellow;
}

.plus {
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}

#container-floating {
  position: fixed;
  width: 70px;
  height: 70px;
  bottom: 30px;
  right: 30px;
  z-index: 50px;
}

#container-floating:hover {
  height: 400px;
  width: 90px;
  padding: 30px;
}

#container-floating:hover .plus {
  animation: plus-in 0.15s linear;
  animation-fill-mode: forwards;
}

.edit {
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  display: block;
  right: 0;
  padding: 0;
  opacity: 0;
  margin: auto;
  line-height: 65px;
  transform: rotateZ(-70deg);
  transition: all 0.3s;
  animation: edit-out 0.3s;
}

#container-floating:hover .edit {
  animation: edit-in 0.2s;
  animation-delay: 0.1s;
  animation-fill-mode: forwards;
}

@keyframes edit-in {
  from {
    opacity: 0;
    transform: rotateZ(-70deg);
  }
  to {
    opacity: 1;
    transform: rotateZ(0deg);
  }
}

@keyframes edit-out {
  from {
    opacity: 1;
    transform: rotateZ(0deg);
  }
  to {
    opacity: 0;
    transform: rotateZ(-70deg);
  }
}

@keyframes plus-in {
  from {
    opacity: 1;
    transform: rotateZ(0deg);
  }
  to {
    opacity: 0;
    transform: rotateZ(180deg);
  }
}

@keyframes plus-out {
  from {
    opacity: 0;
    transform: rotateZ(180deg);
  }
  to {
    opacity: 1;
    transform: rotateZ(0deg);
  }
}

.nds {
  width: 55px;
  height: 55px;
  border-radius: 50%;
  position: fixed;
  z-index: 300;
  transform: scale(0);
  cursor: pointer;
}



.nd5 {
  background-image: url('https://lh3.googleusercontent.com/-X-aQXHatDQY/Uy86XLOyEdI/AAAAAAAAAF0/TBEZvkCnLVE/w140-h140-p/fb3a11ae-1fb4-4c31-b2b9-bf0CFA835c27');
  background-size: 100%;
  right: 40px;
  bottom: 300px;
  animation-delay: 0.08s;
  animation: bounce-out-nds 0.1s linear;
  animation-fill-mode: forwards;
}

@keyframes bounce-nds {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
    transform: scale(1);
  }
}

@keyframes bounce-out-nds {
  from {
    opacity: 1;
    transform: scale(1);
  }
  to {
    opacity: 0;
    transform: scale(0);
  }
}

#container-floating:checked {  
  margin-top:0;
  opacity:1;
}

#container-floating:hover .nds {
  animation: bounce-nds 0.1s linear;
  animation-fill-mode: forwards;
}

#container-floating:hover .nd3 {
  animation-delay: 0.08s;
}

#container-floating:hover .nd4 {
  animation-delay: 0.15s;
}

#container-floating:hover .nd5 {
  animation-delay: 0.2s;
}

.letter {
  font-size: 23px;
  font-family: 'Roboto';
  color: white;
  position: absolute;
  left: 0;
  right: 0;
  margin: 0;
  top: 0;
  bottom: 0;
  text-align: center;
  line-height: 40px;
}

.reminder {
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  top: 0;
  bottom: 0;
  line-height: 40px;
}

.profile {
  border-radius: 50%;
  width: 40px;
  position: absolute;
  top: 0;
  bottom: 0;
  margin: auto;
  right: 20px;
}


}
.modal-trigger :hover{
    color:black;
}

#compte :hover{
    color:black;
}
</style>
<body>
<div class="preview">
	<header class="header">

			<div class="header-top">
				<div class="row" style="margin-left:100px;">
					<div class="small-12 medium-4 large-2">
						<a href="/catalogues"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
					</div>

                   <div class="small-12 medium-8 large-6">
                        <div class="search">
                         <form action="">
                         <input class="typeahead form-control" type="text">
                           <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                         </form>
                        </div>
                    </div>
              
                    <div class="small-12 medium-8 large-4" style="display:flex;">
                    <div class="top-bar-right" style="margin-left:200px; margin-top:5px;">

                            <ul class="menu">
                            <li><a href="#" style="font-size:15px; color:black;" id="compte" data-toggle="modal" data-target="#myModal"><i style="height:20px;" class="tiny material-icons">person</i>@include('layouts.pupup')</a></li>
                            </ul>

					</div>

                    <div class="top-bar-right" style="margin-left:20px; margin-top:5px;">
							<ul class="menu">
								<li><a href="{{route('cart.index')}}" style="color:black;" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span>{{Cart::count()}}</span></a></li>
							</ul>
					</div>


				</div>
			</div>
<!--             @include('layout.menu1')
 -->
	</header>


<main class="main">


        <div class="row">

                <div class="small-12 medium-12 large-8 large-offset-2"oo style="margin-top:80px; margin-left:200px;">
                    <div class="card card-center">
                    
                            <div class="card-section">
                            <div class="card-section">
                          
                            <strong class="card-price"><span></span></strong>

                            <div class="row">
                            <div class="small-12 medium-6 columns">

                            <div class="main-carousel-product">
                                <div class="carousel-cell">

                                    <div class="img-zoom-container"  style="display:flex;">
                                      <img id="myimage" src="{{url('images', $product->image)}}" style="width:950; height:500; margin-left:-20px;">
                                      <div id="myresult" class="img-zoom-result"></div>
                                    </div> 

                                </div>

                            </div>
                            </div>
                                    <div class="small-12 medium-6 columns">
                                       <h4 class="separator-left">
                                       {{ucfirst($product->name)}}
                                       </h4>
                                       <p>{{ $product->description}}</p>
                                        <div class="card-wrap-price">

<!--                                         <strong class="card-price-old"><span>{{ $product->price}} F CFA</span></strong>
 -->                                        <strong class="card-price"><span>Prix: {{ $product->price}}F CFA</span></strong>
                                        <strong class="card-price"><span>Promotion: {{ $product->promo_prix}}%</span></strong>

                                        </div>
                                   </div>
                                </div>
                            

                                    
                                    <center><a href="{{route('cart.addItem', $product->id)}}" style="margin-top:50px; width:200px;" class="button expanded add-to-cart" id="addtocart" onClick="showMsg()">
							Ajouter au Panier
							</a></center>
                            </div>
<div id="container-floating">

    <div class="top-bar-right">
        <ul class="menu">
            <li><a href="{{route('cart.index')}}" id="floating-button" class="shopping-cart-notif modal-trigger" style="color:yellow;"><i class="tiny material-icons">shopping_cart</i><span style="color:white; background:red; border-radius:60%;">{{Cart::count()}}</span></a></li>
            </ul>
    </div>

</div>


                    </div>
                </div>

        </div>
        <style>
        .twitter-typeahead,
        .tt-hint,
        .tt-input,
        .tt-menu{
            width: auto ! important;
            font-weight: normal;
        
        }
     </style>
        <style>
* {box-sizing: border-box;}

.img-zoom-container {
  position: relative;
}

.img-zoom-lens {
  position: absolute;
  border: 1px solid #d4d4d4;
  /*set the size of the lens:*/
  width: 40px;
  height: 40px;
}

.img-zoom-result {
  border: 1px solid #d4d4d4;
  /*set the size of the result div:*/
  width: 300px;
  height: 300px;
}
</style>


        <script src="{{asset('js/js/jquery.min.js')}}"></script>
        <script src="{{asset('js/js/app.js')}}"></script>
        <script src="{{asset('js/js/jquery3.js')}}"></script>
	      <script src="{{asset('js/js/bootstrap.min.js')}}"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <!-- Import typeahead.js -->
        <script src="https://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

		
    <script type="text/javascript">

var path = "{{ route('autocomplete') }}";

$('input.typeahead').typeahead({

    source:  function (query, process) {

    return $.get(path, { query: query }, function (data) {

            return process(data);

        });

    }

});

</script>
<script type="text/javascript">

function showMsg(){
    alert('produit ajouté');
    stop();
   
 }

 </script> 

<script>
function imageZoom(imgID, resultID) {
  var img, lens, result, cx, cy;
  img = document.getElementById(imgID);
  result = document.getElementById(resultID);
  /*create lens:*/
  lens = document.createElement("DIV");
  lens.setAttribute("class", "img-zoom-lens");
  /*insert lens:*/
  img.parentElement.insertBefore(lens, img);
  /*calculate the ratio between result DIV and lens:*/
  cx = result.offsetWidth / lens.offsetWidth;
  cy = result.offsetHeight / lens.offsetHeight;
  /*set background properties for the result DIV:*/
  result.style.backgroundImage = "url('" + img.src + "')";
  result.style.backgroundSize = (img.width * cx) + "px " + (img.height * cy) + "px";
  /*execute a function when someone moves the cursor over the image, or the lens:*/
  lens.addEventListener("mousemove", moveLens);
  img.addEventListener("mousemove", moveLens);
  /*and also for touch screens:*/
  lens.addEventListener("touchmove", moveLens);
  img.addEventListener("touchmove", moveLens);
  function moveLens(e) {
    var pos, x, y;
    /*prevent any other actions that may occur when moving over the image:*/
    e.preventDefault();
    /*get the cursor's x and y positions:*/
    pos = getCursorPos(e);
    /*calculate the position of the lens:*/
    x = pos.x - (lens.offsetWidth / 2);
    y = pos.y - (lens.offsetHeight / 2);
    /*prevent the lens from being positioned outside the image:*/
    if (x > img.width - lens.offsetWidth) {x = img.width - lens.offsetWidth;}
    if (x < 0) {x = 0;}
    if (y > img.height - lens.offsetHeight) {y = img.height - lens.offsetHeight;}
    if (y < 0) {y = 0;}
    /*set the position of the lens:*/
    lens.style.left = x + "px";
    lens.style.top = y + "px";
    /*display what the lens "sees":*/
    result.style.backgroundPosition = "-" + (x * cx) + "px -" + (y * cy) + "px";
  }
  function getCursorPos(e) {
    var a, x = 0, y = 0;
    e = e || window.event;
    /*get the x and y positions of the image:*/
    a = img.getBoundingClientRect();
    /*calculate the cursor's x and y coordinates, relative to the image:*/
    x = e.pageX - a.left;
    y = e.pageY - a.top;
    /*consider any page scrolling:*/
    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return {x : x, y : y};
  }
}
</script>
<script>
// Initiate zoom effect:
imageZoom("myimage", "myresult");
</script>
   <script>
        $(document).ready(function() {
            var bloodhound = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '/user/find?q=%QUERY%',
                    wildcard: '%QUERY%'
                },
            });
            
            $('#search').typeahead({
                hint: true,
                highlight: true,
                minLength: 1
            }, {
                name: 'categories',
                source: bloodhound,
                display: function(data) {
                    return data.name  //Input value to be set when you select a suggestion. 
                },
                templates: {
                    empty: [
                        '<div class="list-group search-results-dropdown"><div class="list-group-item">Nothing found.</div></div>'
                    ],
                    header: [
                        '<div class="list-group search-results-dropdown">'
                    ],
                    suggestion: function(data) {
                    return '<div style="font-weight:normal; margin-top:-10px ! important;" class="list-group-item">' + data.name + '</div></div>'
                    }
                }
            });
        });
    </script>

   
</body>
</html>


