<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>JALÔ</title>
<link rel="stylesheet" href="../css/app.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
<link href="{{asset('starter-template.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>

<style>

#presentation {
  width: 480px;
  height: 120px;
  padding: 20px;
  margin: auto;
  background: #FFF;
  margin-top: 10px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  transition: all 0.3s;
  border-radius: 3px;
}

#presentation:hover {
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  transition: all 0.3s;
  transform: translateZ(10px);
}

#floating-button {
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background:black;
  position: fixed;
  bottom: 30px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px yellow;
}

.plus {
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}

#container-floating {
  position: fixed;
  width: 70px;
  height: 70px;
  bottom: 30px;
  right: 30px;
  z-index: 50px;
}

#container-floating:hover {
  height: 400px;
  width: 90px;
  padding: 30px;
}

#container-floating:hover .plus {
  animation: plus-in 0.15s linear;
  animation-fill-mode: forwards;
}

.edit {
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  display: block;
  right: 0;
  padding: 0;
  opacity: 0;
  margin: auto;
  line-height: 65px;
  transform: rotateZ(-70deg);
  transition: all 0.3s;
  animation: edit-out 0.3s;
}

#container-floating:hover .edit {
  animation: edit-in 0.2s;
  animation-delay: 0.1s;
  animation-fill-mode: forwards;
}

@keyframes edit-in {
  from {
    opacity: 0;
    transform: rotateZ(-70deg);
  }
  to {
    opacity: 1;
    transform: rotateZ(0deg);
  }
}

@keyframes edit-out {
  from {
    opacity: 1;
    transform: rotateZ(0deg);
  }
  to {
    opacity: 0;
    transform: rotateZ(-70deg);
  }
}

@keyframes plus-in {
  from {
    opacity: 1;
    transform: rotateZ(0deg);
  }
  to {
    opacity: 0;
    transform: rotateZ(180deg);
  }
}

@keyframes plus-out {
  from {
    opacity: 0;
    transform: rotateZ(180deg);
  }
  to {
    opacity: 1;
    transform: rotateZ(0deg);
  }
}

.nds {
  width: 55px;
  height: 55px;
  border-radius: 50%;
  position: fixed;
  z-index: 300;
  transform: scale(0);
  cursor: pointer;
}



.nd5 {
  background-image: url('https://lh3.googleusercontent.com/-X-aQXHatDQY/Uy86XLOyEdI/AAAAAAAAAF0/TBEZvkCnLVE/w140-h140-p/fb3a11ae-1fb4-4c31-b2b9-bf0CFA835c27');
  background-size: 100%;
  right: 40px;
  bottom: 300px;
  animation-delay: 0.08s;
  animation: bounce-out-nds 0.1s linear;
  animation-fill-mode: forwards;
}

@keyframes bounce-nds {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
    transform: scale(1);
  }
}

@keyframes bounce-out-nds {
  from {
    opacity: 1;
    transform: scale(1);
  }
  to {
    opacity: 0;
    transform: scale(0);
  }
}

#container-floating:checked {  
  margin-top:0;
  opacity:1;
}

#container-floating:hover .nds {
  animation: bounce-nds 0.1s linear;
  animation-fill-mode: forwards;
}

#container-floating:hover .nd3 {
  animation-delay: 0.08s;
}

#container-floating:hover .nd4 {
  animation-delay: 0.15s;
}

#container-floating:hover .nd5 {
  animation-delay: 0.2s;
}

.letter {
  font-size: 23px;
  font-family: 'Roboto';
  color: white;
  position: absolute;
  left: 0;
  right: 0;
  margin: 0;
  top: 0;
  bottom: 0;
  text-align: center;
  line-height: 40px;
}

.reminder {
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  top: 0;
  bottom: 0;
  line-height: 40px;
}

.profile {
  border-radius: 50%;
  width: 40px;
  position: absolute;
  top: 0;
  bottom: 0;
  margin: auto;
  right: 20px;
}


}
.modal-trigger :hover{
    color:black;
}

#compte :hover{
    color:black;
}
</style>
<body>
<div class="preview">
	<header class="header">

			<div class="header-top">
				<div class="row">
					<div class="small-12 medium-4 large-2" style="margin-left:100px;">
						<a href="/"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
					</div>

                    <div class="small-12 medium-8 large-6">
                        <div class="search">
                         <form action="">
                         <input class="typeahead form-control" name="search" type="text" value="{{ old('search') }}">
                           <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                         </form>
                        </div>
                    </div>
              
                    <div class="small-12 medium-8 large-4" style="display:flex;">
                    <div class="top-bar-right" style="margin-left:980px; margin-top:-45px;">

                            <ul class="menu">
                            <li><a href="#" style="font-size:15px; color:black;" id="compte" data-toggle="modal" data-target="#myModal"><i style="height:20px;" class="tiny material-icons">person</i>@include('layouts.pupup')</a></li>
                            </ul>

					</div>

                    <div class="top-bar-right" style="margin-left:20px; margin-top:-35px;">
							<ul class="menu">
								<li><a href="{{route('cart.index')}}" style="color:black;" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span>{{Cart::count()}}</span></a></li>
							</ul>
					</div>


				</div>
			</div>
            <!-- @include('layout.menu1') -->

	</header>


<main class="main">
<div class="row">
<header class="header-main-content">
<div class="top-bar">
<div class="top-bar-left">
<!-- <ul class="menu">
<li> <a href="" class="button">Toutes les catégories</a> </li>
<li> <a href="" class="hollow button secondary">Matériels électorinique</a> </li>
<li> <a href="" class="hollow button secondary">Vétements</a> </li>
<li> <a href="" class="hollow button secondary">Consomation courante</a> </li>
</ul> -->
</div>

</div>
</header>
</div>

        <div class="row" style="margin-left:50px;">
@foreach($products as $product)
                    <div class="small-12 medium-6 large-3 p-10">
                        <div class="card card-center">

            <div class="img-magnifier-container" id="main">
            <img id="myimage" onClick="myImag()" src="{{url('images', $product->image)}}" onmouseover="this.src='{{url('images', $product->category->image)}}'" onmouseout="this.src='{{url('images', $product->image)}}'" style="width:35; height:500">
</div>
            
                                      <div class="card-section">
                                <a href="#"><h4 class="card-title">{{ $product->name}}</h4></a>
                                <div class="card-wrap-price">
<!--                                         <strong class="card-price-old"><span>{{ $product->price}} F CFA</span></strong>
 -->                                        <strong class="card-price"><span>Prix:{{ $product->price}}F CFA</span></strong>
                                        <strong class="card-price"><span>Promotion:{{ $product->promo_prix}}%</span></strong>
                                </div>
                            </div>

                            <div class="add-to-cart">
							<a href="{{route('cart.addItem', $product->id)}}"style="color:black;"><button class="add-to-cart-btn" onClick="showMsg()"><i class="fa fa-shopping-cart" style="font-size:20px; margin-left:120px;" id="imagesss" onmouseover="shopi()" onmouseout="shopin()"></i></button></a>
							</div>
                           

                                <div class="product-btns">
				<a href="{{route('product.show', $product->id)}}" class="add-bucket-btn waves-effect waves-black" style="margin-left:70px;">
                                    <button class="quick-view">
                                    <span class="glyphicon glyphicon-eye-open">
                                    </span><span class="tooltipp">Voir les details</span>
                                    </button>
                                    </span>
                                    </a>
				</div>

                        </div>
                    </div>
@endforeach
            </div>
<div id="container-floating">

<div class="top-bar-right">
	<ul class="menu">
		<li><a href="{{route('cart.index')}}" id="floating-button" class="shopping-cart-notif modal-trigger" style="color:yellow;"><i class="tiny material-icons">shopping_cart</i><span style="color:white; background:red; border-radius:60%;">{{Cart::count()}}</span></a></li>
		</ul>
</div>

</div>

<footer class="footer">
            <div class="row">

                <div class="small-12 medium-6 large-4">
                    <div class="footer-brand">
                        <img src="images/logo-yellow.jpeg" alt="">
                        <p>JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines.</p>
                        {{--<a href="https://play.google.com/store?hl=en" target="_blank" class="mobile"><img src="./images/android-download.svg" alt=""></a>--}}
                    </div>
                </div>

                <div class="small-12 medium-6 large-3 large-offset-2">
                    {{--<h4 class="section-title">A propos de JALO</h4>--}}
                    {{--<ul class="menu vertical">--}}
                        {{--<li><a href="">Historique</a></li>--}}
                        {{--<li><a href="">Devenir Agent Jalo</a></li>--}}
                        {{--<li><a href="">Devenir Boutiquier</a></li>--}}
                        {{--<li><a href="">Recrutement</a></li>--}}
                    {{--</ul>--}}
                </div>
                <div class="small-12 medium-6 large-3">
                    <h4 class="section-title">Nous contacter</h4>
                    <address>
                        <strong>85B, Sacré Cœur III, Résidence Fatou Kasse, Dakar Sénégal</strong>
                        <br>
                        <span>+221 33 824 81 95  <i>&nbsp;</i> | <i>&nbsp;</i> 77 225 94 32 <i>&nbsp;</i> | <i>&nbsp;</i> 78 384 01 04 </span>
                        <br>
                        <span>info@jaloshops.com</span>
                    </address>
                    <ul class="menu horizontal">
                        <li><a href="https://www.facebook.com/jalo.jalo.1297943" target="_blank"><img src="images/social-network/facebook.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </footer>

       <style>
* {box-sizing: border-box;}

.img-magnifier-container {
  position:relative;
}

.img-magnifier-glass {
  position: absolute;
  border: 3px solid #000;
  border-radius: 50%;
  cursor: none;
  /*Set the size of the magnifier glass:*/
  width: 100px;
  height: 100px;
}
</style>  

    <script src="{{asset('js/js/jquery.min.js')}}"></script>
    <script src="{{asset('js/js/app.js')}}"></script>
    <script src="{{asset('js/js/jquery3.js')}}"></script>
	  <script src="{{asset('js/js/bootstrap.min.js')}}"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

		<script type="text/javascript">

var path = "{{ route('autocomplete') }}";

$('input.typeahead').typeahead({

    source:  function (query, process) {

    return $.get(path, { query: query }, function (data) {

            return process(data);

        });

    }

});

</script>

<script type="text/javascript">


function showMsg(){
    alert('produit ajouté');
    window.stop();
    return back();
 }


</script>      
     <!--  <script>
function magnify(imgID, zoom) {
  var img, glass, w, h, bw;
  img = document.getElementById(imgID);
  /*create magnifier glass:*/
  glass = document.createElement("DIV");
  glass.setAttribute("class", "img-magnifier-glass");
  /*insert magnifier glass:*/
  img.parentElement.insertBefore(glass, img);
  /*set background properties for the magnifier glass:*/
  glass.style.backgroundImage = "url('" + img.src + "')";
  glass.style.backgroundRepeat = "no-repeat";
  glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
  bw = 3;
  w = glass.offsetWidth / 2;
  h = glass.offsetHeight / 2;
  /*execute a function when someone moves the magnifier glass over the image:*/
  glass.addEventListener("mousemove", moveMagnifier);
  img.addEventListener("mousemove", moveMagnifier);
  /*and also for touch screens:*/
  glass.addEventListener("touchmove", moveMagnifier);
  img.addEventListener("touchmove", moveMagnifier);
  function moveMagnifier(e) {
    var pos, x, y;
    /*prevent any other actions that may occur when moving over the image*/
    e.preventDefault();
    /*get the cursor's x and y positions:*/
    pos = getCursorPos(e);
    x = pos.x;
    y = pos.y;
    /*prevent the magnifier glass from being positioned outside the image:*/
    if (x > img.width - (w / zoom)) {x = img.width - (w / zoom);}
    if (x < w / zoom) {x = w / zoom;}
    if (y > img.height - (h / zoom)) {y = img.height - (h / zoom);}
    if (y < h / zoom) {y = h / zoom;}
    /*set the position of the magnifier glass:*/
    glass.style.left = (x - w) + "px";
    glass.style.top = (y - h) + "px";
    /*display what the magnifier glass "sees":*/
    glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
  }
  function getCursorPos(e) {
    var a, x = 0, y = 0;
    e = e || window.event;
    /*get the x and y positions of the image:*/
    a = img.getBoundingClientRect();
    /*calculate the cursor's x and y coordinates, relative to the image:*/
    x = e.pageX - a.left;
    y = e.pageY - a.top;
    /*consider any page scrolling:*/
    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return {x : x, y : y};
  }
}
</script>

<script>
/* Initiate Magnify Function
with the id of the image, and the strength of the magnifier glass:*/
magnify("myimage", 3);
</script> -->

<script>
     /*  $('#myimage').mouseover(function() {
      $(this).animate({width : '100px'});
      });
      $('#myimage').mouseout(function() {
      $(this).animate({width : '900px'});
      }); */
      </script>
</body>
</html>


