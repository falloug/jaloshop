<!--sidebar-menu-->
<div id="sidebar"><a href="#" class="visible-phone"><i class="icon icon-home"></i> Dashboard</a>
  <ul>
    <li class="active"><a href="{{url('/admin/dashboard')}}"><i class="icon icon-home"></i> <span>Dashboard</span></a> </li>
    <li> <a href="#"><i class="icon icon-signal"></i> <span>Ajouter Produit</span></a> </li>
    <li> <a href="#"><i class="icon icon-inbox"></i> <span>Lister Produit</span></a> </li>
    <li> <a href="#"><i class="icon icon-inbox"></i> <span>Lister Category</span></a> </li>
    <li> <a href="#"><i class="icon icon-inbox"></i> <span>Ajout Category</span></a> </li>
    <li> <a href="#"><i class="icon icon-inbox"></i> <span>Voir Les Commandes</span></a> </li>

    </li>

    </li>
   
    </li>
    <li class="content"> <span>Monthly Bandwidth Transfer</span>
      <div class="progress progress-mini progress-danger active progress-striped">
        <div style="width: 77%;" class="bar"></div>
      </div>
      <span class="percent">77%</span>
      <div class="stat">21419.94 / 14000 MB</div>
    </li>
    <li class="content"> <span>Disk Space Usage</span>
      <div class="progress progress-mini active progress-striped">
        <div style="width: 87%;" class="bar"></div>
      </div>
      <span class="percent">87%</span>
      <div class="stat">604.44 / 4000 MB</div>
    </li>
  </ul>
</div>
<!--sidebar-menu-->