<!doctype html>
<html class="no-js" lang="fr">
<head>
<meta charset="utf-8">
<meta http-equiv="x-ua-compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="csrf-token" content="{{ csrf_token() }}">
<title>JALOSHOPS TOUT PRÉS DE CHEZ VOUS</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link type="text/css" rel="stylesheet" href="{{asset('css/cs/bootstrap.min.css')}}"/>
		<!-- Slick -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/cs/slick.css')}}"/>
		<link type="text/css" rel="stylesheet" href="{{asset('css/cs/slick-theme.css')}}"/>
		<!-- nouislider -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/cs/nouislider.min.css')}}"/>
		<!-- Font Awesome Icon -->
		<link rel="stylesheet" href="{{asset('css/cs/font-awesome.min.css')}}">
		<!-- Custom stlylesheet -->
		<link type="text/css" rel="stylesheet" href="{{asset('css/cs/style.css')}}"/>
<link rel="stylesheet" href="../css/app.css">
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet">
<link href="{{asset('starter-template.css')}}" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />

</head>

<style>
#presentation {
  width: 480px; 
  height: 120px;
  padding: 20px;
  margin: auto;
  background: #FFF;
  margin-top: 10px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.16), 0 3px 6px rgba(0, 0, 0, 0.23);
  transition: all 0.3s;
  border-radius: 3px;
}

#presentation:hover {
  box-shadow: 0 14px 28px rgba(0, 0, 0, 0.25), 0 10px 10px rgba(0, 0, 0, 0.22);
  transition: all 0.3s;
  transform: translateZ(10px);
}
  
#floating-button {
  width: 50px;
  height: 50px;
  border-radius: 50%;
  background:black;
  position: fixed;
  bottom: 30px;
  right: 30px;
  cursor: pointer;
  box-shadow: 0px 2px 5px yellow;
}

.plus {
  color: white;
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  right: 0;
  text-align: center;
  padding: 0;
  margin: 0;
  line-height: 55px;
  font-size: 38px;
  font-family: 'Roboto';
  font-weight: 300;
  animation: plus-out 0.3s;
  transition: all 0.3s;
}

#container-floating {
  position: fixed;
  width: 70px;
  height: 70px;
  bottom: 30px;
  right: 30px;
  z-index: 50px;
}

#container-floating:hover {
  height: 400px;
  width: 90px;
  padding: 30px;
}

#container-floating:hover .plus {
  animation: plus-in 0.15s linear;
  animation-fill-mode: forwards;
}

.edit {
  position: absolute;
  top: 0;
  display: block;
  bottom: 0;
  left: 0;
  display: block;
  right: 0;
  padding: 0;
  opacity: 0;
  margin: auto;
  line-height: 65px;
  transform: rotateZ(-70deg);
  transition: all 0.3s;
  animation: edit-out 0.3s;
}

#container-floating:hover .edit {
  animation: edit-in 0.2s;
  animation-delay: 0.1s;
  animation-fill-mode: forwards;
}

@keyframes edit-in {
  from {
    opacity: 0;
    transform: rotateZ(-70deg);
  }
  to {
    opacity: 1;
    transform: rotateZ(0deg);
  }
}

@keyframes edit-out {
  from {
    opacity: 1;
    transform: rotateZ(0deg);
  }
  to {
    opacity: 0;
    transform: rotateZ(-70deg);
  }
}

@keyframes plus-in {
  from {
    opacity: 1;
    transform: rotateZ(0deg);
  }
  to {
    opacity: 0;
    transform: rotateZ(180deg);
  }
}

@keyframes plus-out {
  from {
    opacity: 0;
    transform: rotateZ(180deg);
  }
  to {
    opacity: 1;
    transform: rotateZ(0deg);
  }
}

.nds {
  width: 55px;
  height: 55px;
  border-radius: 50%;
  position: fixed;
  z-index: 300;
  transform: scale(0);
  cursor: pointer;
}



.nd5 {
  background-image: url('https://lh3.googleusercontent.com/-X-aQXHatDQY/Uy86XLOyEdI/AAAAAAAAAF0/TBEZvkCnLVE/w140-h140-p/fb3a11ae-1fb4-4c31-b2b9-bf0CFA835c27');
  background-size: 100%;
  right: 40px;
  bottom: 300px;
  animation-delay: 0.08s;
  animation: bounce-out-nds 0.1s linear;
  animation-fill-mode: forwards;
}

@keyframes bounce-nds {
  from {
    opacity: 0;
  }
  to {
    opacity: 1;
    transform: scale(1);
  }
}

@keyframes bounce-out-nds {
  from {
    opacity: 1;
    transform: scale(1);
  }
  to {
    opacity: 0;
    transform: scale(0);
  }
}

#container-floating:checked {  
  margin-top:0;
  opacity:1;
}

#container-floating:hover .nds {
  animation: bounce-nds 0.1s linear;
  animation-fill-mode: forwards;
}

#container-floating:hover .nd3 {
  animation-delay: 0.08s;
}

#container-floating:hover .nd4 {
  animation-delay: 0.15s;
}

#container-floating:hover .nd5 {
  animation-delay: 0.2s;
}

.letter {
  font-size: 23px;
  font-family: 'Roboto';
  color: white;
  position: absolute;
  left: 0;
  right: 0;
  margin: 0;
  top: 0;
  bottom: 0;
  text-align: center;
  line-height: 40px;
}

.reminder {
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  top: 0;
  bottom: 0;
  line-height: 40px;
}

.profile {
  border-radius: 50%;
  width: 40px;
  position: absolute;
  top: 0;
  bottom: 0;
  margin: auto;
  right: 20px;
}


}

#successMsg{

    animation-duration: 6s;
}
.modal-trigger :hover{
    color:black;
}
#compte :hover{
    color:black;
}
</style>
<body>
<div class="container-flix;">
<header class="header">

	<div class="header-top">
				<div class="row">
					<div class="small-12 medium-4 large-2" style="margin-left:100px;">
						<a href="/"><img src="../images/logo-yellow.jpeg" class="logo" alt=""></a>
					</div>

                    <div class="small-12 medium-8 large-6" style="margin-left:100px; margin-top:10px;">
                        <div class="search">
                         <form action="">
                         <input class="typeahead form-control" name="search" type="text" value="{{ old('search') }}">
                           <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                         </form>
                        </div>
                    </div>
                    <div class="small-12 medium-8 large-4" style="display:flex;">
                    <div class="top-bar-right" style="margin-left:980px; margin-top:-45px;">

                            <ul class="menu">
                            <li><a href="#" style="font-size:15px; color:black;" id="compte" data-toggle="modal" data-target="#myModal"><i style="height:20px;" class="tiny material-icons">person</i>@include('layouts.pupup')</a></li>
                            </ul>

					</div>

                    <div class="top-bar-right" style="margin-left:20px; margin-top:-35px;">
							<ul class="menu">
								<li><a href="{{route('cart.index')}}" style="color:black;" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span>{{Cart::count()}}</span></a></li>
							</ul>
					</div>


				</div>
			</div>
<!-- @include('layout.menu1') -->

</header>

        <!-- <div class="jumbotron">
            <div class="row align-middle">
                <div class="small-12 medium-8 large-5">
                    <h2 class="jumbotron-title">JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines</h2>
                    <div class="jumbotron-btn">
                        <a href="/boutique" class="btn yellow waves-effect waves-black"  style="font-size:12px;">Voir nos boutiquiers</a>
                        <a href="/catalogues" class="btn white waves-effect waves-black"  style="font-size:12px;">Voir notre catalogue </a>
                    </div>
                </div>
            </div>
            <img class="preview-mobile" src="./images/nexus5x.png" alt="">
        </div> -->
        <div class="row">
        <div class="col-lg-12" style="background-color:black;">
        <div class="col-lg-4">
        @include('layout.menu')
   
        </div>

            <div class="col-lg-8">
            @include('slider.slider', array('products' => DB::table('categories')->get()))

            </div>
            </div>
        </div>

       <!--  <section class="section section-services" style="margin-left:110px; margin-top:75px; display:flex;">

            <div class="row">
                <div class="small-12 medium-6 large-4">
                    <div class="service" style="margin-left:-30px;">
                        <a href="#"><img src="images/ventes-priv_es-kaytek.png" alt=""></a>
                        
                    </div>
                </div>
                <div class="small-12 medium-6 large-4">
                    <div class="service">
                        <a href="#"><img src="images/Vente-flash-kaytek.png" alt="" class="mb"></a>
                    </div>
                </div>
                <div class="small-12 medium-6 large-4">
                    <div class="service" style="margin-left:20px;">
                        <a href="#"><img src="images/Nouvell-arrivage-kaytek.png" alt="" class="mb10"></a>
                    </div>
                </div>
            </div>

             <div class="small-12 medium-6 large-4">
                    <div class="service" style="margin-left:20px;">
                        <a href="#"><img src="images/Promo-kaytek.png" alt="" class="mb10"></a>
                    </div>
                </div>
            </div>
        </section> -->
    
        <!-- <section class="section section-services" style="margin-left:250px; margin-top:20px;">

<div class="row">
    <div class="small-12 medium-6 large-4">
        <div class="service" style="margin-left:-30px;">
            <img src="images/services/commande.svg" alt="">
            <h4 class="service-title">Commandez chez votre boutiquier</h4>
            <p class="service-sumary">Votre boutiquier dispose maintenant d’une tablette JALÔ qui vous donne accès à plus de 2.000 produits à des tarifs négociés auprès de notre réseau de fournisseurs. Si nécessaire, notre service de télémarketing vous appelle instantanément.</p>
        </div>
    </div>
    <div class="small-12 medium-6 large-4">
        <div class="service">
            <img src="images/services/devices.svg" alt="" class="mb">
            <h4 class="service-title">Payez instantanément, en cash ou en paiement mobile</h4>
            <p class="service-sumary">Vous achetez en ligne, mais vous payez hors ligne, par cash, ou via Orange Money. Vous n’avez pas tout le montant requis ? Pas d’inquiétude, contactez votre boutiquier du quartier pour des facilités de paiement.</p>
        </div>
    </div>
    <div class="small-12 medium-6 large-4">
        <div class="service" style="margin-left:20px;">
            <img src="images/services/commercial.svg" alt="" class="mb10">
            <h4 class="service-title">Faites-vous livrer chez votre boutiquier</h4>
            <p class="service-sumary">Vous avez confiance en votre boutiquier du quartier, nous aussi. Nous assurons la livraison de vos dans un délai maximum de 48H, à la boutique de quartier. Vous êtes alertés dès que la livraison est effective.</p>
        </div>
    </div>
</div>
</section> -->

        <section class="section" style="margin-top:-50px;">
            <div class="row">
                <center><h4 style="margin-left:30px;" class="section-title p-10">Les produits du jour</h4></center>
            </div>

            <div class="row" style="margin-left:200px;">    
        @foreach($products as $product)

                    <div class="small-12 medium-6 large-3 p-10">
                        <div class="card card-center">
                        
                            <div class="img-magnifier-container" id="imag">
                            <span class="images hover">
                                <img id="myimage" onClick="myImag()" src="{{url('images', $product->image)}}" onmouseover="this.src='{{url('images', $product->category->image)}}'" onmouseout="this.src='{{url('images', $product->image)}}'" style="width:35; height:500">
                            </span>
                            </div>                                    
                            
                            <div class="card-section">
                                <a href="#"><h4 class="card-title">{{ $product->category->name}}</h4></a>
                                
                                <div class="card-wrap-price">
<!--                                         <strong class="card-price-old"><span>{{ $product->price}} F CFA</span></strong>
 -->                                        <strong class="card-price"><span>Prix:{{ $product->price}}F CFA</span></strong>
<!--                                         <strong class="card-price"><span>Promotion:{{ $product->promo_prix}}%</span></strong>
 -->                                </div>
                            </div>

                            <!-- <a href="{{route('cart.addItem', $product->id)}}" type="submit" name="submit" value="submit">
                            <button type="submit" name="submit" value="submit" class="button expanded add-to-cart" id="addtocart" onClick="showMsg()">
							<i class="tiny material-icons">shopping_cart</i>Ajouter au Panier
                            </button>
							</a>  --> 
                            <div class="add-to-cart">
							<a href="{{route('cart.addItem', $product->id)}}"style="color:black;"><button class="add-to-cart-btn" onClick="showMsg()" id="show"><span class="glyphicon glyphicon-shopping-cart" id="carts" style="width:200px; font-size:20px; margin-top:-30px;"></button></a>
							</div>
                            <div class="product-btns">
								    <a href="{{route('product.show', $product->id)}}" class="add-bucket-btn waves-effect waves-black" style="margin-left:25px;">
                                    <button class="quick-view">
                                    <span class="glyphicon glyphicon-eye-open">
                                    </span><span class="tooltipp">Voir les details</span>
                                    </button>
                                    </span>
                                    </a>
							</div>

                            {{ csrf_field()}}
                        </div>
                    </div>
@endforeach
{{ $products->links() }}
            </div>
            <p>&nbsp;</p>
            
<div id="container-floating">

<div class="top-bar-right">
	<ul class="menu">
		<li><a href="{{route('cart.index')}}" id="floating-button" class="shopping-cart-notif modal-trigger" style="color:yellow;"><i class="tiny material-icons">shopping_cart</i><span style="color:white; background:red; border-radius:60%;">{{Cart::count()}}</span></a></li>
		</ul>
</div>

</div>
        </section>


        <div class="featured-testimonials-section" style="margin-top:-50px;">
            <div class="featured-testimonials row">
                <div class="small-12 medium-12 large-12 columns">
                    <div class="testimonial">
                        <div style="margin-left:400px;">
                            <h3>LES BONNES AFFAIRES DU MOIS</h3>
                            <br>
                            <a href="/catalogues" class="btn yellow waves-effect waves-black">Voir notre catalogue</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <section class="section" style="margin-top:-50px;">
            <div class="row">
                <center><h4 style="margin-left:30px;" class="section-title p-10">Nos points de relais</h4></center>
            </div>

            <div class="row" style="margin-left:200px;">    
            @foreach($boutiquiers as $boutiquier)
            <div class="small-12 medium-6 large-4 p-10">
                <div class="card card-profil">
                    <div class="card-section">
                        <div class="seller">
                            <div class="seller-details">
                                <h4>{{$boutiquier->nom}}</h4>
                                <ul>
                                    <li><p><i class="material-icons"></i><span>Adresse : {{ str_limit(ucfirst($boutiquier->adresse), 40)}}</span></p></li>
                                    <li><p><i class="material-icons"></i><span>Téléphone : {{$boutiquier->phone}}</span></p></li>
                                    <li></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <p>&nbsp;</p>
            </div>

            @endforeach
            {{ $boutiquiers->links() }}

            </div>
            <p>&nbsp;</p>
            
<div id="container-floating">

<div class="top-bar-right">
	<ul class="menu">
		<li><a href="{{route('cart.index')}}" id="floating-button" class="shopping-cart-notif modal-trigger" style="color:yellow;"><i class="tiny material-icons">shopping_cart</i><span style="color:white; background:red; border-radius:60%;">{{Cart::count()}}</span></a></li>
		</ul>
</div>

</div>
        </section>

        <footer class="footer">
            <div class="row">

                <div class="small-12 medium-6 large-4">
                    <div class="footer-brand">
                        <img src="images/logo-yellow.jpeg" alt="">
                        <p>JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines.</p>
                        {{--<a href="https://play.google.com/store?hl=en" target="_blank" class="mobile"><img src="./images/android-download.svg" alt=""></a>--}}
                    </div>
                </div>

                <div class="small-12 medium-6 large-3 large-offset-2">
                    {{--<h4 class="section-title">A propos de JALO</h4>--}}
                    {{--<ul class="menu vertical">--}}
                        {{--<li><a href="">Historique</a></li>--}}
                        {{--<li><a href="">Devenir Agent Jalo</a></li>--}}
                        {{--<li><a href="">Devenir Boutiquier</a></li>--}}
                        {{--<li><a href="">Recrutement</a></li>--}}
                    {{--</ul>--}}
                </div>
                <div class="small-12 medium-6 large-3">
                    <h4 class="section-title">Nous contacter</h4>
                    <address>
                        <strong>85B, Sacré Cœur III, Résidence Fatou Kasse, Dakar Sénégal</strong>
                        <br>
                        <span>+221 33 824 81 95  <i>&nbsp;</i> | <i>&nbsp;</i> 77 225 94 32 <i>&nbsp;</i> | <i>&nbsp;</i> 78 384 01 04 </span>
                        <br>
                        <span>info@jaloshops.com</span>
                    </address>
                    <ul class="menu horizontal">
                        <li><a href="https://www.facebook.com/jalo.jalo.1297943" target="_blank"><img src="images/social-network/facebook.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </footer>
    </div>
    <style>
        .twitter-typeahead,
        .tt-hint,
        .tt-input,
        .tt-menu{
            width: auto ! important;
            font-weight: normal;
        
        }
     </style>
        <script src="{{asset('js/jss/jquery.min.js')}}"></script>
		<script src="{{asset('js/jss/bootstrap.min.js')}}"></script>
		<script src="{{asset('js/jss/slick.min.js')}}"></script>
		<script src="{{asset('js/jss/nouislider.min.js')}}"></script>
		<script src="{{asset('js/jss/jquery.zoom.min.js')}}"></script>
		<script src="{{asset('js/jss/main.js')}}"></script>
        <script src="{{asset('js/js/jquery.min.js')}}"></script>
        <script src="{{asset('js/js/app.js')}}"></script>
        <script src="{{asset('js/js/jquery3.js')}}"></script>
        <script src="{{asset('js/js/bootstrap.min.js')}}"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>
            
    <script type="text/javascript">

var path = "{{ route('autocomplete') }}";

$('input.typeahead').typeahead({

    source:  function (query, process) {

    return $.get(path, { query: query }, function (data) {

            return process(data);

        });

    }

});

</script>
<script type="text/javascript">

/* $(document).ready(function(){
$('#successMsg').hide();

$('#addtocart').click(function(){
alert('produit ajouté');
    $('#addtocart').hide();
    $('#successMsg').show();
    $('#successMsg').append('produit ajouté');

    
console.log('addtocart');

});

var son = document.getElementById("mySon"); 

function playSon() { 
    son.play(); 
} 

 
});
 */
 function showMsg(){

    alert('produit ajouté');
   
 }
/* $("#shop").hide();
 $("#myimage").mouseover(function(){
        $("#shop").show();
        $("#myimage").hide();

    });

    $("#shop").mouseover(function(){
        $("#shop").hide();
        $("#myimage").show();

    }); */

</script>

 <script>
 /* $('#carts').onmouseover(function(){
     $('#carts').val('ajouter');
 }) */
 /* $('#addtocart').hide();
 $(".glyphicon-shopping-cart").mouseover(function(){
        $('.glyphicon-shopping-cart').css('background-color: red'); */
/* $(document).ready(function(){
    $('#shop').hide();
    $("p").mouseover(function(){
        $("#shop").show();
        $('p').hide();

    });
    $("p").mouseout(function(){
        $("#shop").show();
        $('p').hide();
    });
}); */
</script>

	</body>
</html>
