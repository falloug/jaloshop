<!-- HEADER -->
<header>
			<!-- TOP HEADER -->
			<div id="top-header">
				<div class="container">
					<ul class="header-links pull-left">
						<li><a href="#"><span class="glyphicon glyphicon-earphone"></span> 77-225-94-32 ou 78-384-01-04</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-envelope"></span> info@jaloshops.com</a></li>
						<li><a href="#"><span class="glyphicon glyphicon-home"></span> Résidence Fatou Kasse, Sacré Coeur III</a></li>
					</ul>
					<ul class="header-links pull-right">
						<li><a href="/register"><span class="glyphicon glyphicon-user"></span> Inscription</a></li>
						<li><a href="/login"><span class="glyphicon glyphicon-user"></span> Connexion</a></li>
					</ul>
				</div>
			</div>
			<!-- /TOP HEADER -->

			<!-- MAIN HEADER -->
			<div id="header">
				<!-- container -->
				<div class="container">
					<!-- row -->
					<div class="row">
						<!-- LOGO -->
						<div class="col-md-3">
							<div class="header-logo">
								<a href="#" class="logo">
									<img style="width:100px;" src="./img/jalo.png" alt="">
								</a>
							</div>
						</div>
						<!-- /LOGO -->

						<!-- SEARCH BAR -->
						<div class="col-md-6">
							<div class="header-search">
								<form>
									
									<input class="input" placeholder="Search here">
									<button class="search-btn"><span class="glyphicon glyphicon-search"></span> Search
</button>
								</form>
							</div>
						</div>
						<!-- /SEARCH BAR -->

						<!-- ACCOUNT -->
						<div class="col-md-3 clearfix">
							<div class="header-ctn">
								<!-- Wishlist -->
								<div>
									
								</div>
								<!-- /Wishlist -->

								<!-- Cart -->
								<div class="collaps navbar-collaps" id="bs-example-navbar-collaps-1">
									<ul class"navbar-nav navbar-right">
									<li>
									
									<a href="#" style="color:white">
									<span class="glyphicon glyphicon-shopping-cart" aria-hidden="true"> Shopping Cart
										<span class="badge">{{ Session::has('cart') ? Session::get('cart')->totalQty : '' }}</span>
										</a>
										</li>
									</ul>
									
									
								</div>
								<!-- /Cart -->

								<!-- Menu Toogle -->
								<div class="menu-toggle">
									<a href="#">
										<i class="fa fa-bars"></i>
										<span>Menu</span>
									</a>
								</div>
								<!-- /Menu Toogle -->
							</div>
						</div>
						<!-- /ACCOUNT -->
					</div>
					<!-- row -->
				</div>
				<!-- container -->
			</div>
			<!-- /MAIN HEADER -->
		</header>
		<!-- /HEADER -->