<body>
<div class="preview">
	<header class="header">
   {{ csrf_field() }}
			<div class="header-top">
				<div class="row">
					<div class="small-12 medium-4 large-2" style="margin-left:100px;">
					</div>

                    <div class="small-12 medium-8 large-6">
                        <div class="search">
                         <form action="">
                         <input class="typeahead form-control" name="search" type="text" value="{{ old('search') }}">
                           <button class="button button-search"><img src="../images/search-icon.png" alt="Rechercher icon"></button>
                         </form>
                        </div>
                    </div>
              
                    <div class="small-12 medium-8 large-4" style="display:flex;">
                    <div class="top-bar-right" style="margin-left:980px; margin-top:-45px;">

                            <ul class="menu">
                            <li><a href="#" style="font-size:15px; color:black;" id="compte" data-toggle="modal" data-target="#myModal"><i style="height:20px;" class="tiny material-icons">person</i>')</a></li>
                            </ul>

					</div>

                    <div class="top-bar-right" style="margin-left:20px; margin-top:-35px;">
							<ul class="menu">
								<li><a href="" style="color:black;" class="shopping-cart-notif modal-trigger"><i class="tiny material-icons">shopping_cart</i><span></span></a></li>
							</ul>
					</div>


				</div>
			</div>
            <!-- @include('layout.menu1') -->

	</header>


<main class="main">
<div class="row">
<header class="header-main-content">
<div class="top-bar">
<div class="top-bar-left">
<!-- <ul class="menu">
<li> <a href="" class="button">Toutes les catégories</a> </li>
<li> <a href="" class="hollow button secondary">Matériels électorinique</a> </li>
<li> <a href="" class="hollow button secondary">Vétements</a> </li>
<li> <a href="" class="hollow button secondary">Consomation courante</a> </li>
</ul> -->
</div>

</div>
</header>
</div>

        <div class="row" style="margin-left:50px;">
                    <div class="small-12 medium-6 large-3 p-10">
                        <div class="card card-center">

            <div class="img-magnifier-container" id="main">
            <img id="myimage" onClick="myImag()" src="" onmouseover="this.src="" onmouseout="this.src="" style="width:35; height:100">
</div>
            
                                      <div class="card-section">
                                <a href="#"><h4 class="card-title"></h4></a>
                                <div class="card-wrap-price">
 -->                                        <strong class="card-price"><span></span></strong>
                                        <strong class="card-price"><span></span></strong>
                                </div>
                            </div>

                            <div class="add-to-cart">
							<a href=""style="color:black;"><button class="add-to-cart-btn" onClick="showMsg()"><i class="fa fa-shopping-cart" style="font-size:20px; margin-left:100px;" id="imagesss" onmouseover="shopi()" onmouseout="shopin()"></i></button></a>
							</div>
                           

                 
                        </div>
                    </div>
            </div>
<!-- <div id="container-floating">

<div class="top-bar-right">
	<ul class="menu">
		<li><a href="" id="floating-button" class="shopping-cart-notif modal-trigger" style="color:yellow;"><i class="tiny material-icons">shopping_cart</i><span style="color:white; background:red; border-radius:60%;"></span></a></li>
		</ul>
</div>

</div> -->
 
<!-- <footer class="footer">
            <div class="row">

                <div class="small-12 medium-6 large-4">
                    <div class="footer-brand">
                        <p>JALÔ est un réseau de distribution de proximité innovant, qui rend hommage aux valeureux boutiquiers de quartier dans les villes africaines.</p>
                        {{--<a href="https://play.google.com/store?hl=en" target="_blank" class="mobile"><img src="./images/android-download.svg" alt=""></a>--}}
                    </div>
                </div>

                <div class="small-12 medium-6 large-3 large-offset-2">
                    {{--<h4 class="section-title">A propos de JALO</h4>--}}
                    {{--<ul class="menu vertical">--}}
                        {{--<li><a href="">Historique</a></li>--}}
                        {{--<li><a href="">Devenir Agent Jalo</a></li>--}}
                        {{--<li><a href="">Devenir Boutiquier</a></li>--}}
                        {{--<li><a href="">Recrutement</a></li>--}}
                    {{--</ul>--}}
                </div> -->
                <!-- <div class="small-12 medium-6 large-3">
                    <h4 class="section-title">Nous contacter</h4>
                    <address>
                        <strong>85B, Sacré Cœur III, Résidence Fatou Kasse, Dakar Sénégal</strong>
                        <br>
                        <span>+221 33 824 81 95  <i>&nbsp;</i> | <i>&nbsp;</i> 77 225 94 32 <i>&nbsp;</i> | <i>&nbsp;</i> 78 384 01 04 </span>
                        <br>
                        <span>info@jaloshops.com</span>
                    </address>
                    <ul class="menu horizontal">
                        <li><a href="https://www.facebook.com/jalo.jalo.1297943" target="_blank"><img src="images/social-network/facebook.svg" alt=""></a></li>
                    </ul>
                </div>
            </div>
        </footer> -->

       <style>
* {box-sizing: border-box;}

.img-magnifier-container {
  position:relative;
}

.img-magnifier-glass {
  position: absolute;
  border: 3px solid #000;
  border-radius: 50%;
  cursor: none;
  /*Set the size of the magnifier glass:*/
  width: 100px;
  height: 100px;
}
</style>  

    <script src="{{asset('js/js/jquery.min.js')}}"></script>
    <script src="{{asset('js/js/app.js')}}"></script>
    <script src="{{asset('js/js/jquery3.js')}}"></script>
	  <script src="{{asset('js/js/bootstrap.min.js')}}"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.1/bootstrap3-typeahead.min.js"></script>

		<script type="text/javascript">

var path = "{{ route('autocomplete') }}";

$('input.typeahead').typeahead({

    source:  function (query, process) {

    return $.get(path, { query: query }, function (data) {

            return process(data);

        });

    }

});

</script>

<script type="text/javascript">


function showMsg(){
    alert('produit ajouté');
    window.stop();
    return back();
 }


</script>  */     
     <!--  <script>
function magnify(imgID, zoom) {
  var img, glass, w, h, bw;
  img = document.getElementById(imgID);
  /*create magnifier glass:*/
  glass = document.createElement("DIV");
  glass.setAttribute("class", "img-magnifier-glass");
  /*insert magnifier glass:*/
  img.parentElement.insertBefore(glass, img);
  /*set background properties for the magnifier glass:*/
  glass.style.backgroundImage = "url('" + img.src + "')";
  glass.style.backgroundRepeat = "no-repeat";
  glass.style.backgroundSize = (img.width * zoom) + "px " + (img.height * zoom) + "px";
  bw = 3;
  w = glass.offsetWidth / 2;
  h = glass.offsetHeight / 2;
  /*execute a function when someone moves the magnifier glass over the image:*/
  glass.addEventListener("mousemove", moveMagnifier);
  img.addEventListener("mousemove", moveMagnifier);
  /*and also for touch screens:*/
  glass.addEventListener("touchmove", moveMagnifier);
  img.addEventListener("touchmove", moveMagnifier);
  function moveMagnifier(e) {
    var pos, x, y;
    /*prevent any other actions that may occur when moving over the image*/
    e.preventDefault();
    /*get the cursor's x and y positions:*/
    pos = getCursorPos(e);
    x = pos.x;
    y = pos.y;
    /*prevent the magnifier glass from being positioned outside the image:*/
    if (x > img.width - (w / zoom)) {x = img.width - (w / zoom);}
    if (x < w / zoom) {x = w / zoom;}
    if (y > img.height - (h / zoom)) {y = img.height - (h / zoom);}
    if (y < h / zoom) {y = h / zoom;}
    /*set the position of the magnifier glass:*/
    glass.style.left = (x - w) + "px";
    glass.style.top = (y - h) + "px";
    /*display what the magnifier glass "sees":*/
    glass.style.backgroundPosition = "-" + ((x * zoom) - w + bw) + "px -" + ((y * zoom) - h + bw) + "px";
  }
  function getCursorPos(e) {
    var a, x = 0, y = 0;
    e = e || window.event;
    /*get the x and y positions of the image:*/
    a = img.getBoundingClientRect();
    /*calculate the cursor's x and y coordinates, relative to the image:*/
    x = e.pageX - a.left;
    y = e.pageY - a.top;
    /*consider any page scrolling:*/
    x = x - window.pageXOffset;
    y = y - window.pageYOffset;
    return {x : x, y : y};
  }
}
</script>

<script>
/* Initiate Magnify Function
with the id of the image, and the strength of the magnifier glass:*/
magnify("myimage", 3);
</script> -->

<script>
     /*  $('#myimage').mouseover(function() {
      $(this).animate({width : '100px'});
      });
      $('#myimage').mouseout(function() {
      $(this).animate({width : '900px'});
      }); */
      </script>
</body>
</html>


