<!doctype html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>JALÔSHOPS-Tout près de chez vous</title>
    <link rel="stylesheet" href="{{asset('css/jalo/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/jalo/brandon/style.css')}}">

  </head>
  <body>

    <!--header-->
@include('version3.header.header1')

<!-- end header--> 

    <main class="main">
    @include('version3.layouts.ourJs')

      <!-- Filter -->
      <form action="">
        <div class="row row-filter">
          <div class="small-12 medium-6 large-4 p-0-15">
            <label for="">
              <strong>Nom produit</strong>
              <input type="text" placeholder="Nom du produit" name="search" value="{{ old('search') }}">
            </label>
          </div>
          <div class="small-12 medium-6 large-4 p-0-15">
            <label for="">
              <strong>Catégorie</strong>
              <select id="catID">
							   <option value="">Selectionner Categorie</option>
                 @foreach(App\Category::all() as $cList)
                 <option class="option" value="{{$cList->id}}">{{$cList->name}}</option>
                 @endforeach
               </select>
            </label>
          </div>
          
          <div class="small-12 medium-6 large-2 p-0-15">
            <label for="">
              <strong>Prix</strong>
              <select id="priceID">
                <option value="0" selected>Croissant / Décroissant</option>
                <option value="0-100000">Décroissant</option>
						    <option value="100000-200000">Croissant</option>
						    
              </select>
            </label>
          </div>

          <div class="small-12 medium-12 large-2 p-0-15">
          <button id="findBtn" onClick="find()" class="filter-button">Filtrer</button>
          </div>

        </div>

      </form>
      <p>&nbsp;</p>
      <!-- End filter -->
      
      <div class="row">

@if(count($data)=="0")
        <div class="col-md-12" align="center">

          <h1 style="font-size:20px;">Aucun data Trouvés</h1>

              
        </div>
        @else 
        <div class="products">
        @foreach($data as $produit)

                <div class="products-item">
                <a href="{{route('product.show', $produit->id)}}"><img src="{{url('images', $produit->image)}}" onmouseover="this.src='{{url('images', $produit->category->image)}}'" onmouseout="this.src='{{url('images', $produit->image)}}'" alt="" class="products-item-img"></a>
                            <div class="products-item-info">
                            <a href="{{route('product.show', $produit->id)}}"><h5 class="products-item-info-title">{{ $produit->name}}</h5></a>
<!--                             <p>Description: {{ $produit->description}}</p>
 -->                                        <div class="products-item-info-footer">
                                            <span class="price">{{ $produit->price}} F CFA</span>
                                            <a href="{{route('product.show', $produit->id)}}">Voir Détails</a>
                                        </div>
                            </div>
                            <div class="add-bucket">
                            <form action="">
                            <a href="{{route('cart.addItem', $produit->id)}}" class="add-bucket-icon"><img src="{{asset('images/jalo/icons/bucket.svg')}}" alt=""></a>
                            </form>
                            </div>
                </div>
                @endforeach

        </div>
@endif
        <div class="see-more">
        
        </div>

      </div>
      
    </main>

   <!-- foooter -->
   @include('version3.footer.footer')

<!-- end footer-->

    <script src="{{asset('js/jalo/jquery.min.js')}}"></script>
    <script src="{{asset('js/jalo/what-input.min.js')}}"></script>
    <script src="{{asset('js/jalo/foundation.min.js')}}"></script>
    <script src="{{asset('js/jalo/slick.min.js')}}"></script>
    <script src="{{asset('js/jalo/app.js')}}"></script>
   @include('front.ourJs')

  </body>
</html>