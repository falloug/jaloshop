@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Livreurs</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Prénom & Nom du livreur</th>
                        <th>Téléphone du livreur</th>
                        <th>Adresse du livreur</th>
                        <th>Quartier du livreur</th>
                        <th>Nom de la société du livreur</th>
                        <th>Date de Création</th>
                        <th>Supprimer</th>

                        </tr>
                        </thead>

                        <tbody>


                        @foreach($livreurs as $livreur)
                        <tr>
                        <td>{{ $livreur->id }}</td>
                        <td>{{ $livreur->prenom}} {{ $livreur->nom}}</td>
                        <td>{{ $livreur->phone}}</td>
                        <td>{{ $livreur->adresse}}</td>
                        <td>{{ $livreur->quartier->nom}}</td>
                        <td>{{ $livreur->nom_societe}}</td>
                        <td>{{ $livreur->date_debut }}</td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['livreur.destroy', $livreur->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>
                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      {{ $livreurs->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
         
