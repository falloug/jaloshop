@extends('layout.adminlayout.design5')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">
   
            <div class="animated fadeIn">
  

                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Livreur</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                    
                                  <hr>
                                  {!! Form::open(['route'=>'livreur.store', 'method' => 'post', 'files' => true, 'class'=>'form-horizontal']) !!}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="prenom" class="control-label mb-1">Prénom du livreur</label>
                                          <input id="prenom" name="prenom" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                      <div class="form-group">
                                          <label for="nom" class="control-label mb-1">Nom du livreur</label>
                                          <input id="nom" name="nom" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                       <div class="form-group">
                                          <label for="adresse" class="control-label mb-1">Adresse du livreur</label>
                                          <input id="adresse" name="adresse" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                      <div class="form-group">
                                          <label for="nom_societe" class="control-label mb-1">Nom de la société du livreur</label>
                                          <input id="nom_societe" name="nom_societe" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                       <div class="form-group">
                                          <label for="phone" class="control-label mb-1">Téléphone du livreur</label>
                                          <input id="phone" name="phone" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                      <div class="form-group">
                                          <label for="date_debut" class="control-label mb-1">Date de début du livreur</label>
                                          <input id="date_debut" name="date_debut" type="date" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                      <div class="form-group">
                                      <label for="quartier_id" class="control-label mb-1">Quartier du livreur</label>
                                          <div class="col-md-10">
                                          {!! Form::select('quartier_id',$quartiers, null, ['class'=>'form-controll', 'placeholder'=> 'select quartier du livreur']) !!}
                                          {!! $errors->has('quartier_id')?$errors->first('quartier_id'):'' !!}
                                          </div>
                                      </div>

                                      
                                        <div class="form-group">
                                        <label for="usr" data-error="Votre mot de passe est incorect">Mot de passe(min:6 caractères)*</label>
                                        <input type="password" class="validate" name="password" placeholder="mot de passe" class="form-control" id="usr" required>
                                        <span style="color: red">@if($errors->has('password')) {{ $errors->first('password') }}@endif</span>
                                        </div>

                                        <div class="form-group">
                                        <label for="usr" class="control-label mb-1">Confirmer mot de passe*</label><br>
                                        <input type="password" name="password_confirmation" placeholder="confirmer mot de passe" class="validate" class="form-control" id="usr" required>
                                        <span style="color: red">@if($errors->has('password_confirmation')) {{ $errors->first('password_confirmation') }}@endif</span>
                                        </div>

                                <div class="form-group">
                                        <label for="usr" data-error="Votre mot de passe est incorect">Email*</label><br>
                                        <input type="email" class="validate" name="email" placeholder="example@gmail.com" class="form-control" id="usr">
                                        <span style="color: red">@if($errors->has('email')) {{ $errors->first('email') }}@endif</span>
                                </div>

                                 <div class="form-group">
                                      <label for="role_id" class="control-label mb-1">Role</label><br>
                                          <div class="col-md-10">
                                          {!! Form::select('role_id',$roles, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner le role']) !!}
                                          {!! $errors->has('role_id')?$errors->first('role_id'):'' !!}
                                          </div>
                                        </div>
   
                                <div class="form-group">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" name="condiction" value="J'accepte les conditions générales de vente" id="remember" required>
                                                <label class="form-check-label" for="remember">
                                                    J'accepte les conditions générales de vente
                                                </label>
                                            </div>
                                </div>
                          

                                      <div>
                                          <button id="submit" name="submit" style="margin-top:20px; background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection