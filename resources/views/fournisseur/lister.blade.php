@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">   
                  <div class="col-lg-12">   
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Fournisseurs</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom du fournisseur</th>
                        <th>Téléphone du fournisseur</th>
                        <th>Adresse du fournisseur</th>
                        <th>Image</th>
                        <th>Date de Création</th>
                        <th>Type Fournisseur</th>
                        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($fournisseurs as $fournisseur)
                        <tr>
                        <td>{{ $fournisseur->id }}</td>
                        <td>{{ $fournisseur->nom}}</td>
                        <td>{{ $fournisseur->phone}}</td>
                        <td>{{ $fournisseur->adresse}}</td>
                        <td><img src="{{url('jaloimage', $fournisseur->image) }}" style="width:50px;"></td>
                        <td>{{ $fournisseur->created_at }}</td>
                        <td>@if($fournisseur->status==0)
                                      <b style="color:yellow">Petit Fourniseur</b>
                                      @else
                                    <b style="color:green">Grand Fournisseur</b>
                                      @endif
                                      <br>
                                      <button id="showSelectDiv{{$fournisseur->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>
                                      <div id="selectDiv{{$fournisseur->id}}">
                                      <input type="hidden" id="userID{{$fournisseur->id}}" value="{{$fournisseur->id}}">
                                      <select id="fournisseurStatus{{$fournisseur->id}}">
                                        <option value="">select a option</option>
                                        <option value="0">Petit Fourniseur</option>
                                        <option value="1">Grand Fournisseur</option>
                                      </select>
                                      </div>   
                                    </td>
                                   <td><a href="" class="btn btn-fill btn-warning">Actions</a></td>

                        
   
                        <td>

                        

                        <td>
                        <a href="{{route('fournisseur.edit', $fournisseur->id)}}" class="btn btn-warning"><i class="fa fa-pencil-square-o" style="font-size:24px"></i>

                         </a>                  </td>
                        <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['fournisseur.destroy', $fournisseur->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>

                        </tr>
                        @endforeach

                      </tbody>
                      </table>
                      {{ $fournisseurs->links() }}

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>
                  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

                    <script>
                    $(document).ready(function(){

                    @foreach($fournisseurs as $fournisseur)
                    $("#selectDiv{{$fournisseur->id}}").hide();
                    $("#showSelectDiv{{$fournisseur->id}}").click(function(){
                    $("#selectDiv{{$fournisseur->id}}").show();    
                    });
                    $("#fournisseurStatus{{$fournisseur->id}}").change(function(){
                    var status = $("#fournisseurStatus{{$fournisseur->id}}").val();
                    var userID = $("#userID{{$fournisseur->id}}").val()
                    if(status==""){
                    alert("please select an option");
                    }else{
                    $.ajax({
                    url: '{{url("/admin/banFournisseur")}}',
                    data: 'status=' + status + '&userID=' + userID,
                    type: 'get',  
                    success:function(response){
                    console.log(response);
                    }
                    });
                    }

                    });
                    @endforeach
                    });
                    </script>

                  @endsection
         
