@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">

  
                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Quartiers</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                      
                                  {!! Form::model($quartier, ['route'=>['quartier.update',$quartier->id],'method'=>'PATCH','class'=>'form-horizontal'] ) !!}                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                          <div class="form-group">
                                          <label for="nom" class="control-label mb-1">Nom du Quartier</label>
                                          <input id="nom" name="nom" type="text" value="{{$quartier->nom}}" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>
                                      <div class="row">

                              <div class="large-6 p-0-15">
                                      <label for="zone_id" class="control-label mb-1">Zones*</label>
                                          <div class="col-md-10">
                                          {!! Form::select('zone_id',$zones, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner la zone*']) !!}
                                          {!! $errors->has('zone_id')?$errors->first('zone_id'):'' !!}
                                          </div>
                              </div>
                                    
                                      
                                      <div>
                                          <button id="submit" name="submit" style="background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection