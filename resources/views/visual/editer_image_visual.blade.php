@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Change Image Visual</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card --> 
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>

                                    {!! Form::model($visual, ['route'=>['visual.update',$visual->id],'method'=>'PATCH','class'=>'form-horizontal'] ) !!}                                      <div class="form-group text-center">

                                                    {{csrf_field()}}
                                                                      <div class="form-group text-center">
                                          <ul class="list-inline">
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="nom" class="control-label mb-1">Nom de l'image</label>
                                          <input id="nom" name="nom" type="text" class="form-control" value="{{$visual->nom}}" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="image" class="control-label mb-1">Image Visual</label>
                                          <input id="image" name="image" type="file" class="form-control" value="{{$visual->image}}" required>
                                      </div>

                                       <div class="form-group">
                                          <label for="image" class="control-label mb-1">NULL</label>
                                          <input id="image" name="fournisseur_id" type="number" class="form-control" placeholder="Mettez zéro" required>
                                      </div>
                                    
                                      
                                      <div>
                                          <button id="submit" name="submit" style="background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      {!! Form::close() !!} 
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection