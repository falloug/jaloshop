<!doctype html>
<html lang="en">
  <head>
    <title>Title</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="../css/app.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.min.css" />
    

  </head>
  <body>
<h2>Visuals</h2>

<div class="container">
  <h2>Carousel Example</h2> 
 
  <div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>

    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner">

      @foreach( $visuals as $visual )
   <div class="carousel-item {{ $loop->first ? 'active' : '' }}">
   <img id="myimage" onClick="myImag()" src="{{url('visualimagess', $visual->image)}}" style="width:700px; height:300px;">
          
   </div>
  @endforeach

    </div>


    `


    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>

  <div class="row" style="margin-left:100px; margin-top:100px;">
@foreach($visuals as $visual)
                    <div class="small-12 medium-6 large-3 p-10">
                        <div class="card card-center">

            <div class="img-magnifier-container" id="main">
            <img id="myimage" onClick="myImag()" src="{{url('visualimagess', $visual->image)}}" style="width:35; height:500">
</div>
            
                                      <div class="card-section">
                                <a href="#"><h4 class="card-title">{{ $visual->nom}}</h4></a>
                                
                            </div>

                        

                        </div>
                    </div>
@endforeach
            </div> 

<!--Carousel Wrapper-->
<div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">

          <!--Controls-->
          <div class="controls-top" style="margin-left:500px;">
            <a class="btn-floating" style="background-color:yellow;" href="#multi-item-example" data-slide="prev"><i class="fa fa-chevron-left"></i></a>
            <a class="btn-floating" style="background-color:yellow;" href="#multi-item-example" data-slide="next"><i class="fa fa-chevron-right"></i></a>
          </div>
          <!--/.Controls-->

              <!--Indicators-->
              <ol class="carousel-indicators">
                <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
                <li data-target="#multi-item-example" data-slide-to="1"></li>
                <li data-target="#multi-item-example" data-slide-to="2"></li>
                <li data-target="#multi-item-example" data-slide-to="3"></li>

              </ol>
              <!--/.Indicators-->

<!--Slides-->
<div class="carousel-inner" role="listbox" style="margin-top:10px;">

                  <!--First slide-->
                  <div class="carousel-item active" style="display:flex;">

                                @foreach($visuals as $visual)
                    <div class="small-12 medium-6 large-3 p-10">
                        <div class="card card-center">

            <div class="img-magnifier-container" id="main">
            <img id="myimage" onClick="myImag()" src="{{url('visualimagess', $visual->image)}}" style="width:35; height:500">
</div>
            
                                      <div class="card-section">
                                <a href="#"><h4 class="card-title">{{ $visual->nom}}</h4></a>
                                
                            </div>

                        

                        </div>
                    </div>
@endforeach
                    
                  </div>
                  <!--/.First slide-->

                    <!--Second slide-->
                    <div class="carousel-item">

                            @foreach($visuals as $visual)
                    <div class="small-12 medium-6 large-3 p-10">
                        <div class="card card-center">

            <div class="img-magnifier-container" id="main">
            <img id="myimage" onClick="myImag()" src="{{url('visualimagess', $visual->image)}}" style="width:35; height:500">
</div>
            
                                      <div class="card-section">
                                <a href="#"><h4 class="card-title">{{ $visual->nom}}</h4></a>
                                
                            </div>

                        

                        </div>
                    </div>
@endforeach

                    </div>
                    <!--/.Second slide-->

                     <!--/.Third slide-->

</div>
<!--/.Slides-->

</div>
<!--/.Carousel Wrapper-->
            </div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
  </body>
</html>