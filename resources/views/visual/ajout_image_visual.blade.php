@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">  


                <div class="row" style="margin-left:250px;">
                  <div class="col-lg-8">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Visuals</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div><br />
                            @endif
                          <div class="message">
                          @if ($success = Session::get('success'))

                            <div class="alert alert-success alert-block">

                                <button type="button" class="close" data-dismiss="alert">×</button>	

                                    <strong>{{ $success }}</strong>

                            </div>

                            @endif
                            </div>
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                        
                                   <form action="/save_visual" method="post" class="dropzone dropzone-custom needsclick add-professors" id="demo1-upload" enctype="multipart/form-data">
                                                    {{ csrf_field() }}
                                      <div class="form-group text-center">
                                          <ul class="list-inline">  
                                             
                                          </ul>
                                      </div>
                                      <div class="form-group">
                                          <label for="cc-payment" class="control-label mb-1">Nom de l'image</label>
                                          <input id="nom" name="nom" type="text" class="form-control" aria-required="true" aria-invalid="false" required>
                                      </div>

                                      <div class="form-group">
                                          <label for="image" class="control-label mb-1">Image Categorie</label>
                                          <input id="image" name="image" type="file" class="form-control cc-number identified visa" value="" data-val="" required>
                                      </div>
                                    
                                      <div class="form-group">
                                      <label for="fournisseur_id" class="control-label mb-1">Fournisseur</label>
                                          <div class="col-md-10">
                                          {!! Form::select('fournisseur_id',$fournisseur, null, ['class'=>'form-controll', 'placeholder'=> 'selectionner fournisseur']) !!}
                                          {!! $errors->has('fournisseur_id')?$errors->first('quartier_id'):'' !!}
                                          </div>
                                      </div>
                                      
                                      <div>
                                          <button id="submit" name="submit" style="background-color:yellow; color:black;" type="submit" class="btn btn-lg btn-block">
                                          Ajouter
                                          </button>
                                      </div>
                                      </form>
                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>

                  @endsection
