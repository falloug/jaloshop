@extends('layout.adminlayout.design4')

@section('content')
<div id="right-panel" class="right-panel">

@include('layout.adminlayout.header')

@include('layout.adminlayout.section')


<div class="content mt-3">

            <div class="animated fadeIn">


                <div class="row" style="">
                  <div class="col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <center><strong class="card-title">Image Visual</strong></center>
                        </div>
                        <div class="card-body">
                          <!-- Credit Card -->
                          <div id="pay-invoice">
                              <div class="card-body">
                                  
                                  <hr>
                                  <table border="3" class="table table-bordered table-responsive" style="margin-top:50px;">

                        <thead>
                        <tr>
                        <th>ID</th>
                        <th>Nom du Visual</th>
                        <th>Image du Visual</th>
                        <th>Date de Création</th>
                        <th>Editer</th>
                        <th colspan="2"><span style="margin-left:40px;">Actions</span></th>
                        <th>Suppression</th>
                        </tr>
                        </thead>

                        <tbody>


                        @foreach($visuals as $visual)
                        <tr>
                        <td>{{ $visual->id }}</td>
                        <td>{{ $visual->nom}}</td>
                        <td><img src="{{url('ima', $visual->image) }}" style="width:50px;"></td>
                        <td>{{ $visual->created_at }}</td>
                        <td>
                        <a href="{{route('visual.edit', $visual->id)}}" class="btn btn-warning"><i class="fa fa-pencil-square-o" style="font-size:24px"></i>
                         </a> 
                        </td> 
                        <td>@if($visual->status==0)
                                      <b style="color:green"> Activer</b>
                                      @else
                                    <b style="color:red">  Desactiver</b>
                                      @endif
                                      <br>
                                      <button id="showSelectDiv{{$visual->id}}"
                                        class="btn btn-primary btn-fill">
                                        Change status
                                      </button>
                                      <div id="selectDiv{{$visual->id}}">
                                      <input type="hidden" id="userID{{$visual->id}}" value="{{$visual->id}}">
                                      <select id="visualStatus{{$visual->id}}">
                                        <option value="">select a option</option>
                                        <option value="0">Activer</option>
                                        <option value="1">Desactiver</option>
                                      </select>
                                      </div>   
                                    </td>
                                   <td><a href="" class="btn btn-fill btn-warning">Actions</a></td>
                                   <td>
                        {!! Form::open(['method'=>'delete', 'route'=>['visual.destroy', $visual->id]]) !!}
                        <button type="submit" class="btn btn-dark"><i style="font-size:24px" class="fa">&#xf014;</i>
                                                </button>
                        {!! Form::close() !!}
                        </td>

                        

                        </tr>
                        @endforeach

                      </tbody>
                      </table>

                              </div>
                          </div>

                        </div>
                    </div> <!-- .card -->

                  </div><!--/.col-->
                  </div>

                  </div>
                  </div>
                  </div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>

<script>
$(document).ready(function(){

@foreach($visuals as $visual)
$("#selectDiv{{$visual->id}}").hide();
$("#showSelectDiv{{$visual->id}}").click(function(){
$("#selectDiv{{$visual->id}}").show();
});
$("#visualStatus{{$visual->id}}").change(function(){
var status = $("#visualStatus{{$visual->id}}").val();
var userID = $("#userID{{$visual->id}}").val()
if(status==""){
alert("please select an option");
}else{
$.ajax({
url: '{{url("/admin/banVisual")}}',
data: 'status=' + status + '&userID=' + userID,
type: 'get',
success:function(response){
console.log(response);
}
});
}

});
@endforeach
});
</script>

                  @endsection
         
