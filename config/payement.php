<?php
return [
   'partenaire' => env('partenaire',''),
   'marchand' => env('marchand',''),
   'secret' => env('OrangeMoney_API_KEY',''),
   'settings' => array(
       'mode' => env('OM_MODE','sandbox'),
       'http.ConnectionTimeOut' => 30,
       'log.LogEnabled' => true,
       'log.FileName' => storage_path() . '/logs/OM.log',
       'log.LogLevel' => 'ERROR'
   ),
];