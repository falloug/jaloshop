<?php
use Illuminate\Support\Facades\Input;
use App\Boutiquier_Vente;

/* v b
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/* Route::get('test', function(){
    return App\User::with('orders')->get();
}); */

//test

//products functions
//Route::view('/produitsss', 'front.index');

//Route::view('productss','front.products',
 //['data'=> App\Product::all(),'catByUser' => 'All Products']);
//Route::get('products/{cat}','TestController@proCat');
//Route::get('searchsss','TestController@search');
//Route::get('productsCat','ProductController@getCatalogue');
//Route::get('details/{id}', 'TestController@details');
// products functions end 
  
// test
Route::resource('evaluation', 'EvaluationController');
Route::get('/export_excel/excel', 'ProductController@excel')->name('export_excel.excel');
Route::get('export/{type}', 'ProductController@export')->name('export/xls');
Route::get('exporter_com/{type}', 'OderController@export_commerciaux')->name('exporter_com/xls'); 
Route::get('exporter_free/{type}', 'OderController@export_freelance')->name('exporter_free/xls');  
Route::get('exporter_bou/{type}', 'OderController@export_boutiquier')->name('exporter_bou/xls');  
Route::get('exporter_cli/{type}', 'OderController@export_client')->name('exporter_cli/xls');  

Route::get('generate-pdf','ProductController@generatePDF');

//orange money
Route::get('/myApi','PaiementController@orange_money');
Route::get('/api','OderController@retour_gestion');
//Route::post('/myApi','PaiementController@myApi');
//Route::post('/myApi','PaiementController@myApi');



Route::get('/testFourni', 'FournisseurController@test');
Route::get('/admin/banFournisseur', 'FournisseurController@banFournisseur');


Route::get('/changePassword','HomeController@showChangePasswordForm');
Route::post('/changePassword','HomeController@changePassword')->name('changePassword');
//Route::get('catalogues/category/{categorie_id}', 'TopController@tests');
Route::get('catalogues/category/{categorie_id}', 'CategoryController@test');
Route::get('/admin/banCategory', 'CategoryController@banCategory');
Route::get('tester', 'CategoryController@test');//->name('test_catalogues');
Route::get('catalogues/{category}', 'CategoryController@proCat');
Route::get('/productsCat','ProductController@productsCat');
Route::get('testez', 'OderController@test');


//made in senegal
Route::get('/made_senegal', 'TopController@made_senegal');
Route::GET('/details_produit/{id}','TopController@details_made')->name('details.produit');


//commercesup
Route::get('lister/c_commercesup', 'CommercialController@lister_commercesup');
Route::get('ajout/c_commercesup', 'CommercialController@ajout_commercesup');
Route::post('save/c_commercesup', 'CommercialController@save_commercesup')->name('commercial_commercesup.ajout');
Route::get('lister/b_commercesup', 'BoutiqueController@lister_commercesup');
Route::get('ajout/b_commercesup', 'BoutiqueController@ajout_commercesup');
Route::post('save/b_commercesup', 'BoutiqueController@save_commercesup')->name('boutique_commercesup.ajout');


//jalo ecommerce route verrsion
Route::get('jalo/', 'TopController@jalo');
Route::get('jalo/{details}', 'DetailsController@details')->name('jalo.details');
Route::get('jalo/{promo}', 'DetailsController@promo')->name('jalo.promo');
Route::get('jalo/{vente}', 'DetailsController@vente')->name('jalo.vente');
Route::view('jalo/produits', 'version3.layouts.index',[
    
    'produits' => App\Product::all()
  ]);
Route::get('actualite', 'TopController@actualite'); 

Route::get('vendeur','TypeController@vendeur');
Route::get('fournisseurss','TypeController@fournisseur');

Route::get("/catalogue/jalo","TopController@mySearchss");
Route::get('mentions', 'TopController@mentions'); 

//Route::get('/','TopController@jalo');
Route::post('/','TopController@store');
//Rejoindre
Route::get('ajout/fourni', 'TypeController@ajout_fournisseur');
Route::post('ajout/fournis', 'TypeController@save_fournisseur')->name('ajout.fournis');
Route::get('ajout/commerci', 'TypeController@ajout_commercial');
Route::post('ajout/commercis', 'TypeController@save_commercial')->name('ajout.commercis');
Route::get('ajout/bouti', 'TypeController@ajout_boutiquier');
Route::post('ajout/boutis', 'TypeController@save_boutiquier')->name('ajout.boutis');

//Sous Categories
Route::get('lister/SousCat', 'CategoryController@lister_SousCat');
Route::get('ajout/SousCategorie', 'CategoryController@ajout_SousCategorie');
Route::post('save/SousCate', 'CategoryController@save_SousCate')->name('ajout.SousCate');
Route::DELETE('soucat/{souscat}', 'CategoryController@supprimer')->name('souscat.destroy');

// Addresse_livraison
Route::get('addresse_livraison', 'AddressController@Addresse_livraison');
Route::get('addresse/livraison', 'AddressController@Addresse_livraisonss');
Route::get('cat_sous', 'CategoryController@cat_sou');


//A propos
Route::get('a-propos','TypeController@a_propos');

//visual image

Route::get('visuals', 'VisualController@visuals');
Route::resource('imagevisuals','ImageVisualController');



//partenaire

Route::get('partenaires', 'PartenaireController@Partenaires');
  
//categorie


//products functions
Route::view('testproducts','front.products',
 ['data'=> App\Product::all(),'catByUser' => 'All Product']);
Route::get('testproducts/{cat}','TestController@proCat');
Route::get('testsearchsss','TestController@search');
Route::get('testproductsCat','TestController@test_productsCat');
Route::get('testdetails/{id}', 'TestController@details');
// products functions end

//Produit top
Route::get('/top_promo', 'TopController@top_promo');
Route::get('/top_vente', 'TopController@top_vente');
Route::get('/produit_jour', 'TopController@produit_jour');


Route::get('checkout', 'CartController@Checkout');

//Route::get('/calculer', 'Boutique_VenteController@calculer');
//Route::post('/calc', 'Boutique_VenteController@calc');
Route::get('/searchs', 'Boutique_VenteController@searchs');
Route::any('/search', 'Boutique_VenteController@search');

Route::get("/my-search","Boutique_VenteController@mySearch");

Route::get("/my-searchs","Fourni_VenteController@mySearch");

Route::get("/catalogue/{category}","ProductController@mySearch");


/* Route::get ( '/search', function () {
    return view('boutiquier.search_boutiquier');
    } );
Route::any('/search', function () {
    $q = Input::get('q');
    $boutiquier = Boutiquier_Vente::where('nomProduit','LIKE','%'.$q.'%')->orWhere('boutique_id','LIKE','%'.$q.'%')->get();
if (count($boutiquier) > 0)
    return view('boutiquier.search_boutiquier')->withDetails($boutiquier)->withQuery($q);
else
    return view('boutiquier.search_boutiquier')->withMessage('No Details found. Try to search again !');
} ); */

Route::view('/layouts/showProduit', 'layouts/showProduit');

Route::get('/user/find', 'SearchController@searchCategories');

//Route::get('/category/find', 'SearchController@searchCategorie');

//Route::get('/categorie/find', 'SearchController@searchCategory');

//Route::get('searcher', 'SearchController@index')->name('searcher');





Route::get('/admin', 'AdminController@login');
//Route::get('/adm', 'AdminController@login');

Route::get('/deconnexion', 'AdminController@deconnexion');
Route::match(['get','post'], '/admin', 'AdminController@login');


Route::resource('product', 'ProductController');
Route::resource('category', 'CategoryController');
Route::resource('cart', 'CartController');
Route::resource('oder', 'OderController');
Route::resource('address', 'AddressController');
Route::resource('boutique', 'BoutiqueController');
Route::resource('quartier', 'QuartierController');
Route::resource('fournisseur', 'FournisseurController');
Route::resource('commercial', 'CommercialController');
Route::resource('fournisseur_vente', 'Fourni_VenteController');
Route::resource('boutique_vente', 'Boutique_VenteController');
Route::resource('posts', 'PostController');
Route::resource('freelance', 'FreelanceController');
Route::resource('livreur', 'LivreurController');



Route::get('quartier', 'QuartierController@index');

//Route::get('getImages', 'ProductController@getImages');



Route::get('product', 'ProductController@lister');

Route::get('boutique', 'BoutiqueController@index');

Route::get('paymentCart', 'CartController@payment');


Route::get('orders/{type?}', 'OderController@Orders');


Route::get('/cart/add-item/{id}', 'CartController@addItem')->name('cart.addItem');
Route::get('cart/removes/{id}', 'CartController@removeItem');
Route::get('cart/modified/','CartController@updateItem');
Route::get('checkCoupon','CheckoutController@checkCoupon');

//Route::get('checkout', 'CheckoutController@index'); 
//Route::post('placeOrder','CheckoutController@placeOrder');
Route::get('/thankyou', 'OderController@thankyou');
Route::DELETE('/annuler/{annuler}','OderController@supprimer')->name('annuler.destroyed');

Route::get('/thankyou/{id}', 'OderController@thankyouOM')->name('orange.money');  



/* Route::get('thankyou',function(){
  return view('thankyou');
});  */ 

Route::get('orderDetails/{id}',function($id){
    return view('myaccount.order');
  });
  
  Route::get('trackOrder/{id}',function($id){
    $orderData = App\Order::where('id',$id)->get();
    return view('myaccount.track',['data' => $orderData]);
  });

//Route::get('/cart/add-item/{id}', 'Fourni_VenteController@addItem')->name('cart.addItem');



Route::get('/', 'TopController@jalo');
Route::get('/catalogues', 'ProductController@getCatalogue');
Route::get('/showProduit', 'ProductController@showProduit');
Route::get('/TESTER' , 'ProductController@TESTER');


//Route::get('/connexion', 'ClientController@formulaire');
//Route::post('/connexion', 'ClientController@traitement');

//Route::get('/inscription', 'ClientController@inscription');


//Route::post('/inscription', 'ClientController@inscri');


//Route::get('/client', 'AdminController@login');
//Route::get('checkout', 'CheckoutController@step1');
Route::post('store-payment', 'CheckoutController@storePayment')->name('payment-store');

Route::get('register', 'Auth\RegisterController@index')->name('register');
Route::post('register', 'Auth\RegisterController@create');

Route::get('login', 'Auth\LoginController@login');
//Route::post('login', 'Auth\LoginController@store')->name('post_login');

Route::get('logout', 'Auth\LoginController@destroy');  

Route::post('toggledeliver/{orderId}', 'OderController@toggledeliver')->name('toggle.deliver');

Route::group(['middleware' => 'auth'], function(){
    Route::get('shipping-info', 'CheckoutController@shipping')->name('checkout.shipping');
    Route::get('commande-info', 'CheckoutController@commande')->name('commande.shipping');
    Route::get('paymentCart', 'CartController@payment');
    Route::get('payment', 'CheckoutController@payment')->name('checkout.payment');
    Route::view('/myaccount', 'myaccount.index');
    Route::get('myaccount/{link}',function($link){
        return view('myaccount.index', ['link' => $link]);
    });
    //start inbox
    Route::view('inbox', 'myaccount.inbox', [
    'data' => App\inbox::all()
 ]);
 Route::get('checkouts', 'CheckoutController@index');   
 Route::get('/checkouts/{id}', 'CheckoutController@showQuartier')->name('quartier.vue');   

 //update inbox
 Route::get('updateInbox', 'profileController@updateInbox');
 Route::post('saveAddress','profileController@saveAddress');
 Route::post('placeOrder','CheckoutController@placeOrder');
 Route::get('/contact', 'MailController@contact');

 Route::post('send/email', 'MailController@sendemail')->name('contact.store');

});


//Route::get('lister/produit', 'ProduitController@listerProduit');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::get('/add-to-cart{id}', [
    'uses'=> 'ProduitController@getAddToCart',
    'as'=> 'produit.addToCart'
]);
Route::get('/shopping-cart', [
    'uses'=> 'ProduitController@getCart',
    'as'=> 'produit.shoppingCart'
]);



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['namespace' => 'Api'], function ()
{
    Route::group(['prefix' => 'api/v1'], function ()
    {
        Route::post('auth/register', 'AuthController@register');
        Route::post('auth/login', 'AuthController@login');
        Route::get('quartiers', 'QuartierController@getAllQuartier');
        Route::get('categories', 'CategorieController@index');
        Route::get('roles', 'UserController@getAllRoles');
        Route::get('clients/{telephone}', 'ProduitController@getClient');
        Route::get('products', 'ProduitController@getAllProduct');
        Route::patch('commande', 'CommandeController@update');




            Route::get('boutiquier/{id}/clients', 'ClientController@index');


            Route::group(['middleware' => 'jwt.auth'], function () {
            Route::get('user', 'AuthController@getAuthUser');  // detail_ user
            Route::get('catalogues/category/{categorie_id}', 'CatalogueController@index');  // liste catalogue
            Route::get('users/fournisseurfmcg','UserController@getUsers');  // liste users fournisseurFMCG
            Route::get('category/products','ProduitController@getListProductToCategory');
            Route::get('product/{id}','ProduitController@show');
            Route::get('client/{number}', 'ClientController@clientNumber');
            Route::post('user', 'UserController@update');
            Route::get('provider/products', 'ProduitController@getListProductsUser');
            Route::get('catalogues/promo', 'CatalogueController@getListCataloguePromo');
            Route::get('stocks', 'StockController@index');
            Route::post('commande', 'CommandeController@create');
            Route::get('client/commandes', 'CommandeController@index');
            Route::get('client/comman', 'CommandeController@index');
            Route::get('client/{number}', 'ClientController@clientNumber');
            Route::post('vente', 'VenteController@create');
            Route::get('boutiquiers', 'UserController@getBoutiquiersByQuartier');
            Route::post('add/boutiquier/favoris', 'MaBoutiqueController@addBoutiquierFavoris');
            Route::get('boutiquier/{boutiquier}/produits', 'UserController@getProductByShoper');
            Route::get('commande/{commande_id}/annulee', 'CommandeController@annuleeCommande');
            Route::get('client/boutiquier/favoris', 'MaBoutiqueController@getBoutiquierFavoris');
            Route::post('stock', 'StockController@create');
            Route::patch('stock', 'StockController@update');
            Route::post('boutiquier/client/add', 'ClientController@createCostumer');
            Route::patch('boutiquier/client', 'ClientController@update');
            //Route::get('products', 'ProduitController@getAllProduct');
            Route::get('boutiquier/{boutiquier_id}/commandes', 'CommandeController@getallOrderByShopper');
            Route::get('boutiquier/{boutiquier_id}/ventes', 'VenteController@getVenteByShopper');
            Route::post('user/update/image', 'UpdateImageController@updateImage');
            Route::get('commercial/commandes', 'CommandeController@getCommandesBycommercial');
            Route::get('fournisseurs', 'UserController@getFournisseur');
            Route::get('commercial/clients', 'ClientController@getClientsByCommercial');
            Route::get('commercial/cl', 'ClientController@getClientsByCommercial');
            Route::get('catalogues', 'CatalogueController@getAllCatalogues');
            Route::get('notification', 'NotificationController@create');
            Route::get('search', 'ProduitController@search');
            //Route::get('clients/{telephone}', 'ProduitController@getClient');
            //Route::get('client/{telephone}', 'CliController@getClient');



            # notification
            Route::post('fcm', 'fcmController@create');
            Route::put('fcm', 'fcmController@update');
        });
    });  
});
Route::get('testss',function(){
    return App\Category::with('childs')
      ->where('product_id',0)
      ->get();
});
Route::get('productsse','ProductController@products');

Route::get('/ajout/news','TopController@ajout_news');
Route::post('/save/news','TopController@save_news')->name('news.save');
Route::get('/news','AdminController@news');



Redirect::to('/')->with('message', 'success|Record updated.');

Route::get('/edit_product/{product_id}','ProductController@edit_product');
Route::GET('/details_vente/{id}','TopController@details_vente')->name('details.vente');
Route::get('/send/email', 'HomeController@mail');
Route::get('/annuler', 'OderController@annuler');
Route::GET('/details_boutiquier/{id}','ProductController@details_boutiquier')->name('details.boutiquier'); 

Route::get('/datas', 'PostController@data');

Route::group(['middleware' => 'AdminMiddleware'], function(){
    Route::get('/produit/boutiquier', 'ProductController@boutiquier');
    Route::get('/lister_commande','AdminController@lister_commande');
    Route::DELETE('/commander/{commander}','AdminController@supprimer')->name('commander.destroyed');

    Route::get('autocomplete', 'ProductController@autocomplete')->name('autocomplete');
    Route::get('/admin/dashboard', 'AdminController@dashboard');



        Route::get('/sliders', 'ProductController@Slider');
        Route::get('listerProduct', 'ProductController@listerProduct');
        Route::get('listerCommande', 'AddressController@listerCommande');
        //Route::get('listerQuartier', ' QuartierController@listerQuartier');
        Route::get('listerCategory', 'CategoryController@listerCategory');
        Route::get('listerBoutiquier', 'BoutiqueController@listerBoutiquier');
        Route::get('listerFournisseur', 'FournisseurController@listerFournisseur');
        Route::get('listerCommercial', 'CommercialController@listerCommercial');
        Route::get('listerFourni_Vente', 'Fourni_VenteController@listerFourni_Vente');
        Route::get('listerBoutique_Vente', 'Boutique_VenteController@listerBoutique_Vente');

        Route::get('/lister_partenaire', 'PartenaireController@listerPartenaire');
        Route::get('/ajout_partenaire', 'PartenaireController@ajoutPartenaire');
        Route::post('/save_partenaire', 'PartenaireController@savePartenaire');
        Route::get('/admin/banPartenaire', 'PartenaireController@banPartenaire');

        Route::get('/lister_visual', 'VisualController@listerVisual');
        Route::get('/ajout_visual', 'VisualController@ajoutVisual');
        Route::post('/save_visual', 'VisualController@saveVisual');
        Route::get('/admin/banVisual', 'VisualController@banVisual');
        Route::DELETE('visual/{visual}', 'VisualController@destroy')->name('visual.destroy');
        Route::patch('visual/{visual}', 'VisualController@update')->name('visual.update');
        Route::get('visual/{visual}/edit', 'VisualController@edit')->name('visual.edit');

        Route::get('ajout/utilisateur', 'AdminController@ajout_utilisateur');
        Route::post('save/utilisateur', 'AdminController@save_utilisateur')->name('ajout.user');

        Route::get('/commandes_pro', 'AdminController@commandes_pro');
        Route::get('/ma_commande', 'OderController@ma_commande');

        Route::get('/commandes_free', 'AdminController@commandes_free');
        Route::get('/order/banOrderFree', 'AdminController@banOrderFree');
        Route::get('/mes_commandes', 'OderController@mes_commandes');
       
        Route::get('/status_product', 'ProductController@status_product');   
        Route::get('/admin/banProduct', 'ProductController@banProduct');
        Route::get('/order/banOrder', 'AdminController@banOrder');
        Route::get('/order/banOrders/fgjj', 'AdminController@banOrder');

        Route::get('/commande/commercial', 'OderController@commercial');   
        Route::get('/commande/freelance', 'OderController@freelance');   
        Route::get('/commande/boutiquier', 'OderController@boutiquier');   
        Route::get('/commande/grpublic', 'OderController@grpublic');   
        Route::get('/mesFreelances', 'OderController@mesFreelances');

        Route::get('/order/banOrderCommercial', 'OderController@banOrderCommercial');
        Route::get('/order/banOrderFreelance', 'OderController@banOrderFreelance');
        Route::get('/order/banOrderFreelances', 'OderController@banOrderFreelances');
        Route::get('/order/banOrderBoutiquier', 'OderController@banOrderBoutiquier');
        Route::get('/order/banOrderGrpublic', 'OderController@banOrderGrpublic');

        Route::get('/order/banOrderGrpublics', 'OderController@banOrderGrpublics');

        Route::GET('/etat_vente_c/{id}','OderController@etat_vente_c')->name('etat_vente_c.vente');
        Route::GET('/etat_vente_f/{id}','OderController@etat_vente_f')->name('etat_vente_f.vente');
        Route::GET('/etat_vente_fs/{id}','OderController@etat_vente_fs')->name('etat_vente_fs.vente');

        Route::GET('/etat_commercial','OderController@etat_commercial');
        Route::GET('/etat_freelance','OderController@etat_freelance');
        Route::GET('/etat_freelances','OderController@etat_freelances');

        Route::post('/approve/{id}', 'OderController@approve')->name('accept.approve');
        Route::post('/decline/{id}', 'OderController@decline')->name('refuse.decline');
        
        Route::get('/recommandations', 'OderController@recommander');       
          
        Route::post('/livreur/status-commande/{id}', 'OderController@status_commande')->name('status.livrer');
        Route::post('/livreur/status-commande_a/{id}', 'OderController@status_commande_a')->name('status.annuler');

        Route::post('/livreur/freelance/{id}', 'OderController@status_freelance')->name('status.freelance');
        Route::post('/livreur/commercial/{id}', 'OderController@status_commercial')->name('status.commercial');
        Route::post('/livreur/boutiquier/{id}', 'OderController@status_boutiquier')->name('status.boutiquier');

        
        
        Route::view('users', 'admin.users', [
            'data' => App\User::paginate(5)
          ]);
        Route::DELETE('user/{user}', 'UserController@supprimer')->name('user.destroy');
        Route::get('/admin/banUser', 'AdminController@banUser');
        Route::get('/orders','AdminController@orders');
        Route::get('/admin/orderStatusUpdate','AdminController@orderStatusUpdate');
        Route::get('/listerQuartier', 'QuartierController@listerQuartier');

        Route::get('/approvisionnements', 'ApprovisionnementController@index');
        Route::get('/admin/banApprovisionnement', 'ApprovisionnementController@banApprovisionnement');       

});



