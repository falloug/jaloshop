<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;

class Commercial extends Model
{
    use Notifiable;
    use SearchableTrait;
    protected $fillable = ['nom', 'phone', 'adresse', 'quartier_id'];

    protected $searchable = [

        'columns' => [
            'commercials.nom' => 10,
            'commercials.adresse' => 5,
            'commercials.id' => 3,

        ]
        
       
    ];
    public function products(){
        return $this->hasMany(Product::class); 
    }

    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }

    public function boutiques(){
        return $this->hasMany(Boutique::class);
    }

    public function type(){
        return $this->belongsTo(type::class);
    }
}
