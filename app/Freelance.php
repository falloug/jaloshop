<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Freelance extends Model
{
    //
    protected $fillable = [
        'adresse', 'phone', 'nom', 'quartier_id', 'user_id', 'parametre_id'
    ];

    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }

    public function parametre(){
        return $this->belongsTo(Parametre::class);
    }
}
