<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
//use Gloudemans\Shoppingcart\Facades\Cart;
//use Illuminate\Support\Facades\Auth;
//use Cviebrock\EloquentSluggable\SluggableTrait;
//use Cviebrock\EloquentSluggable\Sluggable;
//use Illuminate\Support\Facades\DB;  
//use JWTAuth;
use Auth;
use Cart;  

class Order extends Model  
{
    //use Sluggable;
    protected $fillable =['total', 'delivered', 'frais', 'zone_id', 'quartier_id'];


     /* public function user(){
        return $this->belongsTo(User::class);
    }  */
 
    public function product(){
        return $this->belongsTo(Product::class,'product_id');
    }

    public function orderCols(){
        return $this->belongsToMany(Product::class); //->withPivot('qty', 'total');
    }

    public function zone(){
        return $this->belongsTo(Zone::class);
    }

    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }

    public function paiements(){
        return $this->hasMany(Paiment::class);
    }
   

    public static function createOrder(){
        $user = Auth::user();
        $order = $user->orders()->create([
          'total' => Cart::total()   
        ]);
         //$user = new User;
         //$user->orders()->get();
       //$user = Order::create();
         //$order = $user->orders()->create([
        //'total' => Cart::total()
        //'delivered' => 0
    //]);   

   $cartItems=Cart::content();
    foreach($cartItems as $cartItem){
        $order->orderCols()->attach($cartItem->id,[
            'qty'=>$cartItem->qty,
            'total'=>$cartItem->qty*$cartItem->price
        ]);
    } 
    }

    public function order_product(){
        return $this->hasMany('App\Order_Product', 'order_id');
      }

    public static function  getCommandesToUser($categorie)
    {

        if( !isset($categorie))
        {
            $commandes = DB::table('orders as c')
            ->join('order_product as cc', 'cc.order_id', '=' ,'c.id')
            ->join('users', 'users.id', '=', 'c.user_id')
            ->join('products', 'product_id', '=', 'cc.product_id')
            ->join('products', 'products.id', '=', 'products.product_id')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->where('c.user_id', Auth::user()->id)
            ->select('products.name as productLibelle',
                'categories.name as libelleCategorie', 'users.name')
                ->paginate(8);
        }
        else
        {
         $commandes = DB::table('order_product as cc')
            ->join('orders as c', 'c.id', '=' ,'cc.order_id')
            ->join('users', 'users.id', '=', 'c.user_id')
            ->join('products', 'product_id', '=', 'cc.product_id')
            ->join('products', 'products.id', '=', 'products.product_id')
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->where('c.user_id', Auth::user()->id)
            ->where('categories.id', '=', $categorie)
            ->select('products.name as productLibelle',
                'categories.name as libelleCategorie', 'users.name')
                ->paginate(8);
        }
        return $commandes;
    }

    public static function getCommandes($client_id)
    {

        $commandesQuery = DB::table('orders as c')->select('c.date_commande as date',
                        'c.reference', 'etats.libelle', 'c.id')
                         ->join('etats','etats.id', '=', 'c.etat_id')
                         ->where('c.user_id', $client_id)->get();

        foreach ($ventes as $vente)
        {
            $products = DB::table('marchandise_vente as mv')->select('p.libelle as produit', 'mv.quantite as quantite', 'm.prix')
                ->join('marchandises as m', 'm.id', '=', 'mv.marchandise_id')
                ->join('produits as p', 'p.id', '=', 'm.produit_id')
                ->where('mv.vente_id', $vente->id)->get();
            foreach ($products as $product)
            {
                $product->prixTotal = self::calculTotalPrice($product->qty, $product->price);
            }

            $vente->products = $products;
        }

        foreach ($commandesQuery as $commande)
        {
            $products = DB::table('order_product as cc')->select('p.name as product', 'cc.qty', 'ca.price')
            ->join('products as ca', 'ca.id', '=', 'cc.product_id')
            ->join('products as p', 'p.id', '=', 'ca.product_id')
            ->where('cc.order_id', $commande->id)->get();

            foreach ($products as $product)
            {
                $product->prixTotal = self::calculTotalPrice($produit->qty, $produit->price);
            }
            $commande->products = $products;

        }

         return ['commandes' => $commandesQuery, 'ventes' => $ventes];
    }

    public function sluggable()
    {
        /* return [
            'slug' => [
                'source' => 'delivered'
            ]
        ]; */
    }

    public static function quickRandom($length = 16)
    {
        $pool = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

        return substr(str_shuffle(str_repeat($pool, $length)), 0, $length);
    }

    public static function calculTotalPrice($quantite, $prix)
    {
         $prixTotal =  $qty * $price;

         return $prixTotal;
    }

    public static function annleeCommande($commande_id)
    {
        $etat_annule = Etat::where('libelle', 'annule')->get()->first()->id;

        $commande = self::find($order_id);

        $commande->etat_id = $etat_annule;

        $commande->save();

        return 'success';
    }

    public static function getCommandeByCommercial($user_id)
    {
        return DB::table('orders as c')->select('c.id', 'user.name as nomClient',
           'user.phone as phoneClient', 'boutique.nom as nomBoutiquier',
            'boutique.phone as telBou','etats.libelle as etat')->
        join('boutiques as b', 'b.boutiquier_id', '=', 'c.boutiquier_id')->
        leftjoin('users as client', 'client.id', '=', 'c.user_id')->
        join('users as boutique', 'boutique.id', '=', 'c.boutiquier_id')->
        leftjoin('etats', 'etats.id', '=', 'c.etat_id')->where('commercial_id', $user_id)->orderBy('c.date_commande', 'DESC')->get();


    }

}
