<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;

class Approvisionnement extends Model
{
    //

protected $table = 'approvisionnements';
    protected $fillable = ['id', 'vendeur_id','date_ajout','quantite', 'produit_id'];
    public $timestamps = false;

    

    public function produit(){
        return $this->belongsTo(Product::class);
    }

    public function vendeur(){
        return $this->belongsTo(User::class);
    }
}
