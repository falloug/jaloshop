<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dAddress extends Model
{
    //
    protected $table='dAddress';
    //protected $fillable = ['fullname', 'userId', 'phone', 'adresse', 'quartier', 'livraison', 'quartier_id'];


      public function quartier(){
        return $this->belongsTo(Quartier::class);
    } 
}
