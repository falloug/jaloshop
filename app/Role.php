<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{

    protected $fillable = ['nom'];

    public function users(){
        return $this->hasMany(User::class);
    }

    public static function getRoles()
    {
        return self::all();
    }

    public static function getRoleId($role)
    {
        if($role == 'admin'){
            $role_id = self::where('nom', $role)->first()->id;
        }
        elseif($role == 'boutiquier')
        {

            $role_id = self::where('nom', ''.$role)->first()->id;
        }

        elseif($role == 'commercial')
        {

            $role_id = self::where('nom', ''.$role)->first()->id;
        }
        elseif($role == 'fournisseur')
        {

            $role_id = self::where('nom', ''.$role)->first()->id;
        }

          elseif($role == 'freelance')
        {

            $role_id = self::where('nom', ''.$role)->first()->id;
        }
        else
        {
            $role_id = self::where('nom', 'client')->first()->id;
        }
        return $role_id;
    }

    public static function getRoleById($role_id)
    {
        return self::find($role_id)->nom;
    }

}
