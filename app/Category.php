<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
    
class Category extends Model
{
    protected $fillable = ['name','product_id'];

    public static function getAllCategories()
    {  
        return self::where('status',false)->get();
    }
  
     public function childs(){
        return $this->hasMany('App\Category','product_id');
      } 

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'name'
            ]
        ];  
    }

    /* public function products(){
        return $this->hasMany(Product::class);
    } */

    public function sub_cats(){
        return $this->hasMany(Sub_Cat::class);
    }

    public function children(){ 
        return $this->hasMany(Category::class, 'product_id'); 
    } 
    
    public function parent(){   
        return $this->belongsTo(Category::class, 'product_id'); 
    } 
    
    public function products(){ 
        return $this->hasMany(Product::class, 'category_id'); 
    } 
}
