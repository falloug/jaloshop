<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sub_Cat extends Model
{
    //
    public $table = "sub_cats";
    protected $fillable = ['name', 'category_id'];

    public function category(){
        return $this->belongsTo(Category::class);
    }

}
