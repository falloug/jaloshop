<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;
use App\Products;
//use CyrildeWit\EloquentViewable\Viewable;
//use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;



class Product extends Model //implements ViewableContract
{
    //use Sluggable;
    //protected $table = 'products';
    //use Viewable;
    use Notifiable;  
    use SearchableTrait;  
    protected $fillable =['name', 'description', 'price', 'promo_prix', 'image', 'tarif', 'prix_achat', 'category_id','fournisseur_id','marge','pourcentage_om','type_produit','stock'];
    
  
    protected $searchable = [

        'columns' => [
            'products.name' => 10,
            'products.description' => 5,
            'products.promo_prix' => 5,
            'products.id' => 3,
            'c.name' => 2,

        ] ,
        'joins' => [
            'categories as c' => ['c.id', 'products.category_id'],

        ],
         
    ];

    public function category(){ 
        return $this->belongsTo(Category::class, 'category_id'); 
    } 

 public function approvisionnements(){  
        return $this->hasMany(Approvisionnements::class);
    }

    /* $products= Products::select('products.*') 
    ->join('categories', 'categories.id' ,'=', 'products.category_id') 
    ->get();  */
    /* public function category(){
        return $this->belongsTo(Category::class)->latest();
    } */

    public function commercial(){
        return $this->belongsTo(Commercial::class);
    }

    public function commandes(){
        return $this->hasMany(Commande::class);
    }

    public function fournisseur(){
        return $this->belongsTo(Fournisseur::class);
    }

    public static function getAllProduct()
    
        /* return DB::table('products as p')->select('p.id', 'p.name', 'c.name as categorie', 'p.description')
            ->join('categories as c', 'c.id', '=','p.category_id')
            ->get(); */
            {
                return DB::table('products as p')->select('p.id', 'p.name as nomProduit', 'c.name as categorie', 'c.image', 'p.description')
                    ->join('categories as c', 'c.id', '=','p.category_id')
                    ->get();
        

    }

    public static function getAllProducts($category)
    {

        if($category == 'all')   
        {

        $products = DB::table('products')
            //->join('users', 'users.id', '=', 'products.fournisseur_id')
            //->join('products as p', 'p.id', '=', 'products.product_id')
            ->join('categories as c', 'c.id', '=', 'products.category_id')
            ->select('products.*', 'products.name as productLibelle', 'products.description',
                'c.name as libelleCategorie','products.image',
                 'c.image')->paginate(4);
        }

        elseif ($category == 'promo')
        {
            $products = DB::table('products')
                //->join('users', 'users.id', '=', 'products.fournisseur_id')
                //->join('products as p', 'p.id', '=', 'products.product_id')
                ->join('categories as c', 'c.id', '=', 'products.category_id')
                ->select('products.*', 'products.name as productLibelle', 'products.description',
                    'c.name as libelleCategorie',
                    'products.image', 'c.image')
                ->whereNotNull('promo_price')
                ->paginate(8);
        }
        else
        {
            $products = DB::table('products')
            //->join('users', 'users.id', '=', 'products.fournisseur_id')
            //->join('products as p', 'p.id', '=', 'products.product_id')
            ->join('categories as c', 'c.id', '=', 'products.category_id')
            ->select('products.*', 'products.name as productLibelle','c.name as libelleCategorie','products.description',
                'products.image', 'c.image','products.description')
            ->where('c.id', (int)$category)->paginate(8);
        }

        return $products;
    }

    public static function showcatalogue($product_id)
    {
        return DB::table('prducts as c')
            ->select('c.id as product_id', 'c.price', 'c.quantite',
                'c.image','c.date_fin_validite', 'c.valide',
                'c.commercial_id', 'c.fournisseur_id',
                'c.product_id as product_id', 'p.name as libelle_produit', 'p.description',
                'commercial.prenom as prenom_commercial', 'commercial.nom as nom_commercial',
                'commercial.adresse as adresse_commercial', 'commercial.phone as phone_commercial',
                'commercial.avatar as photo_avatar','fournisseur.prenom as prenom_fourniseur',
                'fournisseur.nom as nom_fournisseur', 'fournisseur.adresse as fournisseur_adresse',
                'fournisseur.phone as phone_fournisseur', 'fournisseur.avatar as photo_avatar')
            ->join('products as p', 'p.id', '=', 'c.product_id')
            ->join('users as commercial', 'commercial.id', '=', 'c.commercial_id')
            ->join('users as fournisseur', 'fournisseur.id', '=', 'c.fournisseur_id')->where('c.id', '=', $product_id)
            ->get();
 }

 public function toSearchableArray()
    {
        $array = $this->toArray();

        // Customize array...

        return $array;
    }
/* 
    public function sluggable()
    {
        return [
            'price' => [
                'source' => 'price'
            ]
        ];
    }
 */
    public static function getProduit($product)
    {
        return DB::table('products')
            ->select('products.id', 'products.name', 'categories.name as category', 'categories.image', 'p.description')
            ->where('products.id','=', $product)
            ->join('categories', 'categories.id', '=', 'products.category_id')
            ->get();
    }

    public static function getListProduitToUser($user_id)
    {
        return DB::table('products as c')->select('c.*', 'p.name', 'ca.name', 'ca.image', 'p.description')
            ->join('products as p', 'p.id', '=', 'c.fournisseur_id')
            ->join('categories as ca', 'ca.id', '=','p.category_id')
            ->where('c.fournisseur_id', '=', $user_id)
            ->get();
    }

    
}
