<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TypeVente extends Model
{
    protected $table = 'type_vente';
    protected $fillable = ['id','libelle'];
    public $timestamps = false;
}
