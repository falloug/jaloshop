<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Zone extends Model
{
    //
    protected $fillable =['nom', 'frais'];
    
    public function quartiers(){  
        return $this->hasMany(Quartier::class);
    }

    public function orders(){  
        return $this->hasMany(Order::class);
    }
}
  