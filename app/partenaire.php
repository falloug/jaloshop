<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class partenaire extends Model
{
    //

    protected $fillable = ['nom', 'image', 'description'];

}
