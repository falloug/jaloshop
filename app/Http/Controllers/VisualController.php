<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\visual;
use App\Fournisseur;
use DB;
use Session;

class VisualController extends Controller
{
    //   

    public function visuals(){

        $visuals = DB::table('visuals')->where('status', 'like', 0)->paginate(4);
        return view('visual.visuals', compact('visuals'));

    }
    public function listerVisual(){

        $visuals = visual::all();
        return view('visual.lister_image_visual', compact('visuals'));
        
    }

    public function ajoutVisual()
    {
        $fournisseur = Fournisseur::pluck('nom', 'id');
        return view('visual.ajout_image_visual', compact('fournisseur'));
    }

    public function saveVisual(Request $request){

        $this->validate($request,[

            'nom'=>'required',
            'image'=>'image|mimes:png,jpg,jpeg,pdf|max:10000',

        ]);

        // image upload
       //$image = $request->image;
       //if($image){

        //$imageName=$image->getClientOriginalName();
        //$image->move('imagess', $imageName);
        //$formInput['image']=$imageName;

       //}
       //visual::create($formInput);
       //visual::create($request->all());
       $image = $request->file('image');
        if($image){
        $imageName = $image->getClientOriginalName();
        $image->move(public_path().'/ima/', $imageName);
        } 

       $visuals = new visual;
       $visuals->nom = $request->get('nom');
       $visuals->image = $imageName;
       $visuals->fournisseur_id = $request->get('fournisseur_id');
       $visuals->save();
       $success = 'message success';
       return back()->with(['success' => $success]);


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function edit($id){
        $visual = visual::find($id);
        return view('visual.editer_image_visual', compact('visual'));
        //$visual = DB::table('visuals')->where('id',$id)->first();
        //return view('visual.editer_image_visual', compact('visual'))->with('id',$id);

    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

       /*  $data = ['nom'=>$request->nom,
                 'image'=>$request->image,
    ];
    $visual = DB::table('visuals')->where('id',$request->id)->update($data); */
   /*  $data = ['nom'=>$request->nom,
    'image'=>$request->image,
];
        $visual = DB::table('visuals')->where('id',$request->id)->update($data);
        
        return redirect('lister_visual'); */
        $visual = visual::find($id);
        $visualUpdate = $request->all();
        $visual->update($visualUpdate);
        return redirect('lister_visual');

       /*  $visual= visual::find($id);
        $visual->nom=$request->get('nom');
        $visual->image=$request->get('image');
       
        $visual->save();

        return redirect()->route('lister_visual')->with('success', 'data update') */;
    }

    public function banVisual(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('visuals')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }

      public function destroy($id)
    {
        $visual = visual::find($id);
        $visual->delete();

        return redirect('lister_visual');
    }
      
    

}
