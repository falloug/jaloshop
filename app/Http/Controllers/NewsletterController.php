<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Newsletter; 

class NewsletterController extends Controller
{
    public function newsletter()
        {

            return view('newsletter');
        }
 
        
 public function store(Request $request)
{
    if ( ! Newsletter::isSubscribed($request->user_email) ) {
        Newsletter::subscribePending($request->user_email);
        return redirect('newsletter')->with('status','thanks for subscription');
    }
    return redirect('newsletter')->with('status','you are already subscribed');

}


    
}
