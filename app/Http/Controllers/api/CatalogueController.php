<?php

namespace App\Http\Controllers\Api;

use App\Product;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use JWTAuth;

class CatalogueController extends Controller
{

    /**
     * @SWG\Get(
     *   path="catalogues/category/{categorie_id}",
     *   summary="get all catalogues by categorie_id",
     *   operationId="index",
     *   tags={"catalogues"},
     *   @SWG\Parameter(
     *     name="categorie_id",
     *     in="path",
     *     description="category by catalogue",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index($category_id){

        if($category_id == 0 )
        {
            $category_id = 'all';
        }
        $products = Products::getAllCatalogue($category_id);

        //$product = Products::all();

        return response()->json(['products' => $products, 'status' => 200]);

    }

    /**
     * @SWG\Get(
     *   path="catalogue/{id}",
     *   summary="show catalogue",
     *   operationId="show",
     *   tags={"catalogues"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="id catalogue",
     *     required=true,
     *     type="integer"
     *   ),00
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function show($product_id){

        if($product_id)
        {
            $product = Product::showcatalogue($product_id);

            return response()->json(['product' => $product  ,'status' => 200]);
        }
        else
        {
            return response()->json(['message' => "this product doesn't exist", 'status' => 404],404);
        }
    }

    /**
     * @SWG\Get(
     *   path="categories",
     *   summary="get all categories",
     *   operationId="index",
     *   tags={"categories"},
     *
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getListCataloguePromo()
    {

        $products = Product::getAllCatalogue('promo');

        return response()->json(['productsPromo' => $products, 'status' => 200]);
    }

    /**
     * @SWG\Get(
     *   path="catalogues",
     *   summary="get all catalogues",
     *   operationId="getAllCatalogues",
     *   tags={"catalogues"},
     *
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    /* public function getAllCatalogues()
    {
        $products = DB::table('products')
           
            ->get();

        return response()->json(['products' => $products, 'status' => 200]);

    }
 */
    public function getAllCatalogues()
    {
        $products = DB::table('products')
            ->join('users', 'users.id', '=', 'products.fournisseur_id')
            ->join('categories as c', 'c.id', '=', 'p.category_id')
            ->select('products.*', 'p.name as productLibelle','c.name as libelleCategorie', 'users.name as prenomFournisseur',
               'users.id','products.image', 'c.image', 'c.id as category_id', 'p.description')
           ->get();

        return response()->json(['products' => $products, 'status' => 200]);

    }

}
