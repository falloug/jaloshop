<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use JWTAuth;

class AuthController extends Controller
{

    public function login(Request $request)
    {

        $rules = array(
            'password'   => 'required',
            'phone'   => 'required|min:7'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return response()->json(['error' =>$validator->errors()],400);
        }

        $credentials['phone'] = $request->get('phone');
        $credentials['password'] = $request->get('password');

        $user = User::authentificate($credentials);

        if(isset($user['message']) && isset($user['status']))
        {
            return response()->json(['message' => $user['message']], $user['status']);
        }
        else
        {
            return response()->json($user);
        }


    }


    public function register(Request $request){

        $user = $request->all();

        $auth = User::where('phone',$request->get('phone'))->get()->first();

        if($auth != null)
        {
            return response()->json(['message' => 'user already exist'], 201);
        }

        $rules = array(
            'name'       => 'required|min:2',
            'email'   => 'required',
            'password'   => 'required|min:5',
            'role_id'   => 'required',
            'condiction'   => 'required',
            'phone'   => 'required|unique:users|min:9'
        );
        $validator = Validator::make($user, $rules);

        if ($validator->fails()) {
            return response()->json(['error' =>$validator->errors()],403);
        }
        else {

            $user['password'] = bcrypt($request->get('password'));
            

             if(User::create($user))
             {
                $credentials['phone'] = $request->get('phone');
                $credentials['password'] = $request->get('password');


                 $userAuth = User::authentificate($credentials);

                 if(isset($userAuth['message']) && isset($userAuth['status']))
                 {
                     return response()->json(['message' => $userAuth['message']], $userAuth['status']);
                 }
                 else
                 {
                     return response()->json($userAuth);
                 }
            }
            else
            {
                return response()->json(['message' => 'User not be created'], 403);
            }


        }
    }


    public function getAuthUser(Request $request)
    {
        $userAuth = JWTAuth::toUser($request->header('Authorization'));

        $user = User::getUserDetails($userAuth);

        return response()->json(['user' => $user]);
    }


}


