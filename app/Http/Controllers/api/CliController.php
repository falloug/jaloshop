<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use  App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use JWTAuth;
use Carbon\Carbon;
use App\Quartier;
use App\Notification;

class CliController extends Controller
{
    public function  getClient($telephone)
    {
        $client = User::where('phone', '=', $telephone)->get();

        if($client != []) {
            return response()->json(['data' => $client ,'message' => "success"], 200);
        }
        else
        {
            return response()->json(['data' => $client, 'message' => "Ce client n'existe pas"], 200);
        }
    }
}
