<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Category;
use App\Http\Controllers\Api\ApiController;
use App\Http\Controllers\Controller;

class CategorieController extends Controller
{
    /**
     * @SWG\Get(
     *   path="catalogues/promo",
     *   summary="get list catalogues promo",
     *   operationId="getListCataloguePromo",
     *   tags={"catalogues"},
     *
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public static function index()
    {
        $categories = Category::getAllCategories();

        return response()->json(['categories' => $categories, 'status' => 200]);
    }

}
