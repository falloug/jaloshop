<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Client;
use App\User;
use App\Etat;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Order;
use App\CatalogueCommande;
use Illuminate\Support\Facades\DB;
use JWTAuth;
use App\Notification;



class CommandeController extends Controller
{
    /**
     * @SWG\Get(
     *   path="client/commandes",
     *   summary="orders by customer",
     *   operationId="index",
     *   tags={"orders"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function index(Request $request)
    {
        $user_id = JWTAuth::toUser($request->header('Authorization'))->id;

        $commandes = Order::getCommandes($user_id);

        return response()->json(['commandes_ventes' => $commandes], 200);
    }

    /**
     * @SWG\Post(
     *   path="commande",
     *   summary="create order",
     *   operationId="create",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="command",
     *     in="query",
     *     description="array orders",
     *     required=true,
     *      type="array",
     *      @SWG\Items(
     *             type="integer",
     *             format="int32"
     *         ),
     *         collectionFormat="pipes"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="commande is created"),
     *   @SWG\Response(response=400, description="catalogue does not exit"),
     *   @SWG\Response(response=504, description="catalogues_id not possed"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public static function create(Request $request)
    {

        $products = $request->get('command');

        //dd($catalogues);

        $etat_id = Etat::where('libelle', 'cours')->get()->first()->id;

        if(isset($products))
        {
            $boutiquier = JWTAuth::toUser($request->header('Authorization'));

            $boutiquier_id = $boutiquier->id;

            $reference = Order::quickRandom(14);

            $user_id = $request->get('user')["user_id"];

            $user = User::where('id', $user_id)->first();

//            logger('ok', ['user' => $user]);

            $commande = Order::create([
                'reference' => $reference,'etat_id' => $etat_id, 'user_id' => $user_id, 'boutiquier_id' => $boutiquier_id]);


            foreach ($products as $product)
            {
                $commande_id = Order::where('reference', $commande->reference)->first()->id;

                $product_query = Product::where('id',$product['product_id']);


                if($product_query == [])
                {
                    return response()->json(['message' => ' product does not exit'], 400);
                }

                $productCommande = new Order_Product();

                $productCommande->order_id = $order_id;
                $productCommande->product_id = $product['product_id'];
                $productCommande->qty = $product['qty'];
                $productCommande->save();

            }
            Notification::sendNotif("new_commande", $user, $boutiquier, $order->id);

            return response()->json(['message' => 'commande is created'], 200);

        }
        else
        {
            return response()->json(['message' =>'product_id not possed'],504);
        }

    }
    /**
     * @SWG\Get(
     *   path="commande/{commande_id}/annulee",
     *   summary="annule order",
     *   operationId="annuleeCommande",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="commande_id",
     *     in="path",
     *     description="id order",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="commande annuler avec succes"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function annuleeCommande($commande_id)
    {
        if(Order::annleeCommande($order_id) == 'success')
        {
            return response()->json(['message' => 'commande annuler avec succes'],200);
        }
        else
        {
            return response()->json(['message' => 'erreur commande non annulee'], 400);
        }
    }

    /**
     * @SWG\Get(
     *   path="boutiquier/{boutiquier_id}/commandes",
     *   summary="orders by shopper",
     *   operationId="getallOrderByShopper",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="boutiquier_id",
     *     in="path",
     *     description="id by shopper",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="operation success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getallOrderByShopper($boutiquier_id)
    {


        $commandes = DB::table('orders as c')->select(
            'c.reference', 'etats.libelle', 'c.id' , 'cl.name as nomClient', 'cl.phone1 as phoneClient')
            ->leftjoin('etats','etats.id', '=', 'c.etat_id')
            ->join('users as cl','cl.id', '=', 'c.user_id')
            ->get();

        if($commandes == [])
        {
            return response()->json(['message' => "this boutiquier doesn't have a orders"],200);
        }

        foreach ($commandes as $commande)
        {
            $products = DB::table('order_product as cc')->select('p.name as produit', 'cc.qty', 'ca.price')
                ->join('products as ca', 'ca.id', '=', 'cc.product_id')
                ->join('products as p', 'p.id', '=', 'ca.product_id')
                ->where('cc.order_id', $commande->id)->get();

            foreach ($products as $product)
            {
                $product->prixTotal = self::calculTotalPrice($product->qty, $produit->price);
            }
            $commande->products = $products;

        }

        return response()->json(['commandes' => $commandes], 200);

    }


    public static function calculTotalPrice($quantite, $prix)
    {
        $prixTotal =  $qty * $prix;

        return $prixTotal;
    }

    /**
     * @SWG\Patch(
     *   path="commande",
     *   summary="update order",
     *   operationId="update",
     *   tags={"orders"},
     *   @SWG\Parameter(
     *     name="commande_id",
     *     in="query",
     *     description="id order",
     *     required=true,
     *     type="integer"
     *   ),

     * @SWG\Parameter(
     *     name="etat",
     *     in="query",
     *     description="status order",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="etat changed success"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=504, description="commande_id or etat is empty")
     * )
     *
     */
    public function update(Request $request)
    {
        $order_id = $request->get('order_id');
        $etat= $request->get('etat');

        //dd($order_id, $etat);

        if($order_id != null && $etat !=null)
        {
            $commande = Order::find((int)$order_id);

            if($commande == [])
            {
                return response()->json([message => "ce commmande n'existe pas"]);
            }

            if($etat == "annule")
            {
                $etat_id = Etat::where('libelle', 'annule')->get()->first()->id;
            }
            elseif($etat == "livre")
            {
                $etat_id = Etat::where('libelle', 'livre')->get()->first()->id;
            }
            else
            {
                $etat_id = Etat::where('libelle', 'cours')->get()->first()->id;
            }

            Order::find($order_id)->update(['etat_id' => $etat_id]);

            return response()->json(['message'=> 'etat changed success'],200);

        }
        else
        {
            return response()->json(['message'=> 'order_id or etat is empty'],404);
        }

    }

    /**
     * @SWG\Get(
     *   path="commercial/commandes",
     *   summary="get orders assigned to commercial",
     *   operationId="getCommandesBycommercial",
     *   tags={"orders"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="not order")
     * )
     *
     */
    public static function getCommandesBycommercial(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));

        //dd($user);

        $commandes = Order::getCommandeByCommercial($user->id);

        if($commandes == [])
        {
            return response()->json(['message' => "not order"],404);
        }

        foreach ($commandes as $commande)
        {
            $products = DB::table('order_product as cc')->select('p.name as product', 'cc.qty', 'ca.price')
                ->join('products as ca', 'ca.id', '=', 'cc.product_id')
                ->join('products as p', 'p.id', '=', 'ca.product_id')
                ->where('cc.order_id', $commande->id)->get();

            foreach ($products as $product)
            {
                $product->prixTotal = self::calculTotalPrice($produit->qty, $product->price);
            }
            $commande->products = $products;

        }

        return response()->json(['commandes' => $commandes], 200);
    }


}
