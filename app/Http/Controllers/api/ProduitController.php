<?php

namespace App\Http\Controllers\Api;

use App\Product;
use Illuminate\Http\Request;
use JWTAuth;
use App\Http\Controllers\Controller;
use Tymon\JWTAuth\Exceptions\JWTException;
use Illuminate\Support\Facades\DB;


class ProduitController extends Controller
{
    /**
     * @SWG\Get(
     *   path="category/products",
     *   summary="get all products by category",
     *   operationId="getListProductToCategory",
     *   tags={"products"},
     *   @SWG\Parameter(
     *     name="category",
     *     in="query",
     *     description="id category",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="this category doesn't exist"),
     * )
     *
     */
    public function getListProductToCategory(Request $request)
    {
        $category = $request->get('category');

        if($category)
        {
            
            if(Category::find((int)$category) == null){
                return response()->json(['message' => "this category doesn't exist"],404);
            }

            $products =  Category::where('id', (int)$category)->firstOrFail()->products()->get();

            return response()->json(['products' => $products, 'status' => 200]);
        }
        else
            {
                $products = Product::all();
                return response()->json(['products' => $products, 'status' => 200]);
        }
    }
    /**
     * @SWG\Get(
     *   path="product/{id}",
     *   summary="show product",
     *   operationId="show",
     *   tags={"products"},
     *   @SWG\Parameter(
     *     name="id",
     *     in="path",
     *     description="id by product",
     *     required=true,
     *     type="integer"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=404, description="tthe product does not exist"),
     * )
     *
     */
    public function show($product)
    {
        if($product)
        {

            if(Product::find((int)$product) == null)
            {
                return response()->json(['message' => "this product doesn't exist"],404);
            }
            //dd('ok');
            $products = Product::getProduit((int)$product);

            return response()->json(['product' => $products ,'status' => 200]);
        }
        else
        {
            return response()->json(['message' => 'the product does not exist', 'status' => 404],404);
        }

    }
    /**
     * @SWG\Get(
     *   path="provider/products",
     *   summary="get products by provider",
     *   operationId="getListProductsUser",
     *   tags={"products"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getListProductsUser(Request $request)
    {
        $user = JWTAuth::toUser($request->header('Authorization'));
        $products = Product::getListProduitToUser($user->id);
    
        return response()->json(['products' => $products, 'status' => 200]);
    }

    /**
     * @SWG\Get(
     *   path="products",
     *   summary="get all products",
     *   operationId="getAllProduct",
     *   tags={"products"},
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error")
     * )
     *
     */
    public function getAllProduct()
    {
        return response()->json(['products' => Product::getAllProduct()], 200);
    }

    /**
     * @SWG\Get(
     *   path="search",
     *   summary="search product",
     *   operationId="search",
     *   tags={"products"},
     * @SWG\Parameter(
     *     name="search",
     *     in="path",
     *     description="item search",
     *     required=true,
     *     type="string"
     *   ),
     * @SWG\Parameter(
     *   name="authorization",
     *   type="string",
     *   required=true,
     *   in="header",
     *   description="add token"
     * ),
     *   @SWG\Response(response=200, description="successful operation"),
     *   @SWG\Response(response=400, description="Token is required"),
     *   @SWG\Response(response=500, description="internal server error"),
     *   @SWG\Response(response=403, description="search is not exist")
     * )
     *
     */
    public function search(Request $request)
    {
        $search = $request->get('search');
 
        if($search)
        {
 
            $products = DB::table('products')
                ->join('users', 'users.id', '=', 'products.fournisseur_id')
                ->join('categories as c', 'c.id', '=', 'p.category_id')
                ->select('products.*', 'p.name as productLibelle',
                    'c.name as libelleCategorie', 'users.name as prenomFournisseur', 'products.image',
                   'users.id as fournisseur_id', 'c.image', 'p.description')
                ->where('p.name', 'LIKE', '%' . $search . '%')
                ->orderBy('products.price', 'DESC')->paginate(8);
 
 
            return response()->json(['products' => $products], 200);
 
        }
        else
        {
            return response()->json(['message' => 'search is not exist'], 403);
        }
     }

    public function  getClient($telephone)
    {
        $client = User::where('phone', '=', $telephone)->get();

        if($client != []) {
            return response()->json(['data' => $client ,'message' => "success"], 200);
        }
        else
        {
            return response()->json(['data' => $client, 'message' => "Ce client n'existe pas"], 200);
        }
    }

}
