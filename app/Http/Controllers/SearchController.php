<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;

class SearchController extends Controller
{
    public function searchCategories(Request $request)
{
    return Category::where('name', 'LIKE', '%'.$request->q.'%')->get();
}

public function searchCategorie(Request $request)
{
    return Category::where('name', 'LIKE', '%'.$request->q.'%')->get();
}

public function searchCategory(Request $request)
{
    return Category::where('name', 'LIKE', '%'.$request->q.'%')->get();
}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(4);
        return view('layouts.index', compact('products'));
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function autocomplete(Request $request)
    {
        $data = Product::select("name")
                ->where("name","LIKE","%{$request->input('query')}%")
                ->get();
        return response()->json($data);
    }

}
