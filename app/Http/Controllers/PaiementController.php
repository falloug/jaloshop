<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Paiment;
use App\Order;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;



class PaiementController extends Controller
{
    //  

    public function orange_money(){
        
        //$paiement = new Paiment;
        $dateh = date('c');
        $identifiant = md5(2303388413);
        $ref_commande = 'OM201503WEBP001';
        $site = md5(1431331287);
        $total = 1250;
        $commande = "Paiement Orange Money ";
        $algo = "SHA512";  
        $cle_secret = env('OrangeMoney_API_KEY');
        $cle_bin = pack("H*", $cle_secret);
        $message = "S2M_COMMANDE=$commande"."&S2M_DATEH=$dateh"."&S2M_HTYPE=$algo".
        "&S2M_IDENTIFIANT=$identifiant"."&S2M_REF_COMMANDE=$ref_commande"."&S2M_SITE=$site"."&S2M_TOTAL=$total";
        $hmac = strtoupper(hash_hmac(strtolower($algo), $message, $cle_bin));

       
                return view('front.checkout', compact(
                    'dateh', 'identifiant','ref_commande','total','site','commande','algo','hmac','message'));
        }

    public function orangeMoney(Request $request){
  
               /* $dateh = date('c');

                $identifiant = md5(‘2222222222’);

                $ref_commande = OM201503WEBP001‘;

                $site = md5(‘1111111111’);

                $total = '1250';

                $commande = "TEST Paiement par Orange Money ";

                $algo = "SHA512";  

                $cle_secrete = "e9e92e5da6ab55c6eb238f2af13a07cc2d9683844bd2656dd0e3b1da7f6573f7f329712626a5396b4c11c1946
                4bc05ae8e36cb7da1f5596d237ca64aecf0f458";

                $cle_bin = pack("H*", $cle_secrete);

                $message = "S2M_COMMANDE=$commande"."&S2M_DATEH=$dateh"."&S2M_HTYPE=$algo".
                "&S2M_IDENTIFIANT=$identifiant"."&S2M_REF_COMMANDE=$ref_commande"."&S2M_SITE=$site"."&S2M_TOTAL=$total";

                $hmac = strtoupper(hash_hmac(strtolower($algo), $message, $cle_bin));
                
                $my_api_key = env('API_KEY');
                $my_api_secret = env('API_SECRET');

                if(hash('sha256', $my_api_secret) === $api_secret_sha256 && hash('sha256', $my_api_key) === $api_key_sha256)
                {
                    //from PayExpresse
                }
                else{
                    //not from PayExpresse
                } */
 
        $this->validate($request, [
            'S2M_IDENTIFIANT' => 'required|max:10',
            'S2M_SITE' => 'required|max:10',
            'S2M_REF_COMMANDE' => 'required',
            'S2M_COMMANDE' => 'required',
            'S2M_DATEH' => 'required',
            'S2M_TOTAL' => 'required',
            'S2M_HTYPE' => 'required',
            'S2M_HMAC' => 'required',

            ]);  
        $api_key_sha256 = Input::get('api_key_sha256');
        $api_secret_sha256 = Input::get('api_secret_sha256');
        $my_api_key = env('API_KEY');
        
        //$paiement = new Paiment;
        $paiement->S2M_IDENTIFIANT = Hash::make($request->get('S2M_IDENTIFIANT'));
        //$S2M_IDENTIFIANT = Input::get('S2M_IDENTIFIANT');
        //$S2M_SITE = Input::get('S2M_SITE');
        $paiement->S2M_SITE = Hash::make($request->get('S2M_SITE'));
        $paiement->S2M_REF_COMMANDE = $request->get('S2M_REF_COMMANDE');
        $paiement->S2M_COMMANDE = $request->get('S2M_COMMANDE');
        $paiement->S2M_DATEH = $request->get('S2M_DATEH');
        $paiement->S2M_TOTAL = $request->get('S2M_TOTAL');
        $paiement->S2M_HTYPE = $request->get('S2M_HTYPE');
        $paiement->S2M_HMAC = $request->get('S2M_HMAC');
        //$hashed = Hash::make($S2M_IDENTIFIANT);
        // $hashed = Hash::make($S2M_SITE);
        $paiement->env = $request->get('env');
        $paiement->token = $request->get('token');
        $paiement->api_key_sha256 = $request->get('api_key_sha256');
        $paiement->api_secret_sha256 = $request->get('api_secret_sha256');
        //$api_key_sha256 = Input::get('api_key_sha256');
        //$api_secret_sha256 = Input::get('api_secret_sha256');
        //$my_api_key = env('API_KEY');

        if(hash('sha256', $my_api_secret) === $api_secret_sha256 && hash('sha256', $my_api_key) === $api_key_sha256)
                {
                    //from PayExpresse
                }
                else{
                    //not from PayExpresse
                }

        $paiement->save();

        return back();

    }

    public function myApi(){

                $dateh = date('c');
                $identifiant = md5(2303388413);
                $ref_commande = 'OM201503WEBP001';
                $site = md5(1431331287);
                $total = 1250;
                $commande = "Paiement Orange Money ";
                $algo = "SHA512";  
                $cle_secrete = env('OrangeMoney_API_KEY');
                $cle_bin = pack("H*", $cle_secrete);
                $message = "S2M_COMMANDE=$commande"."&S2M_DATEH=$dateh"."&S2M_HTYPE=$algo".
                "&S2M_IDENTIFIANT=$identifiant"."&S2M_REF_COMMANDE=$ref_commande"."&S2M_SITE=$site"."&S2M_TOTAL=$total";
                $hmac = strtoupper(hash_hmac(strtolower($algo), $message, $cle_bin));

                $dateh->dateh = Input::get('S2M_DATEH');
                $identifiant->identifiant = Input::get('S2M_IDENTIFIANT');
                $ref_commande->ref_commande = Input::get('S2M_REF_COMMANDE');
                $site->site = Input::get('S2M_SITE');
                $total->total = Input::get('S2M_TOTAL');
                $commande->commande = Input::get('S2M_COMMANDE');
                $algo->algo = Input::get('S2M_HTYPE');
                $hmac = Input::get('command_name');
                $cle_bin = pack("H*", $cle_secrete);
                $message = "S2M_COMMANDE=$commande"."&S2M_DATEH=$dateh"."&S2M_HTYPE=$algo".
                "&S2M_IDENTIFIANT=$identifiant"."&S2M_REF_COMMANDE=$ref_commande"."&S2M_SITE=$site"."&S2M_TOTAL=$total";                
                $api_secret_sha256 = Input::get('api_secret_sha256');
                $cle_secrete = env('OrangeMoney_API_KEY');

                if(hash('SHA512', $hmac) === $algo && hash('SHA512', $cle_secrete) === $message)
                {
                    'success';
                }
                else{
                   'not success';
                }

                return view('OrangeMoney.orange_money');
    }
    public function retour_gestion(){

        $paiement = array(
            'ALGO' => 'SHA256',
            'CMD' => 'Sample transaction',
            'HMAC' => 'EC158B8193FFF7E01C768FE320DDBE234234C9D0',
            'ID' => '2570239429.1012158196.92347',
            'MONTANT' => '8500',
            'REF_CMD' => 'SAMPLE_REF_CMD',
            'STATUT' => '200',
            'TRX_ID' => 'MP173453.3454.A12687',
            'UID'
            => '5894f79a37e6d'
            );
            $data = $paiement;
            $secret_key = env('OrangeMoney_API_KEY');//'05E964F202FD9D53DC51F2760C03661JDL93SD0DCCD1A94055639D8545E53D63';
            $bin_key = pack("H*", $secret_key);
            ksort($data);
            $message = urldecode(http_build_query($data));
            $hmac = strtoupper(hash_hmac(strtolower($data['ALGO']), $message, $bin_key));
            if ($hmac === $paiement['HMAC'])
            echo 'Bingo! Valid Data';
            // Process data
            else
            die('Suspicious data!');
    }
}
