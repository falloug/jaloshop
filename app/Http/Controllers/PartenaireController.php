<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\partenaire;
use DB;

class PartenaireController extends Controller
{
    //

    public function Partenaires(){
        //$partenaires = DB::table('partenaires')->paginate(4);
        $partenaires = DB::table('partenaires')->where('status', 'like', 0)->paginate(4);
        return view('partenaire.partenaires', compact('partenaires'));

    }
    public function listerPartenaire(){

        $partenaires = partenaire::all();
        return view('partenaire.lister_partenaire', compact('partenaires'));
        
    }

    public function ajoutPartenaire()
    {
        return view('partenaire.ajout_partenaire');
    }

    public function savePartenaire(Request $request){

        $this->validate($request,[

            'nom'=>'required',
            'image'=>'image|mimes:png,jpg,jpeg,svg|max:10000',
            'description'=>'required',


        ]);

        // image upload
       $image = $request->image;
       if($image){

        $imageName=$image->getClientOriginalName();
        $image->move('partenaireimagess', $imageName);
        $formInput['image']=$imageName;

       }
       partenaire::create($formInput);
       partenaire::create($request->all());

       return back();


    }

    public function banPartenaire(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('partenaires')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }
}
