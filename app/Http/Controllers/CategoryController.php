<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Sub_Cat;
use App\Product;
use Cart;
use Illuminate\Support\Facades\DB;


class CategoryController extends Controller
{   

    public function test(Request $request){

        $categories = Category::getAllCategories();
        $products = Product::getAllProducts($request->get('category')); 
        $catalogues = DB::table('products')->paginate(8);
        //$categories = Category::with('products')->findOrFail( $tester );
        //dd($categories);
        return view('category.tester', compact('categories','products','catalogues'));
    
    
    }

    public function proCat(Request $request){
        $cartItems = Cart::content();
        $categories = Category::all();
        $categorie =  $request->category;
        $produits =  DB::table('products')->select('products.name as name','products.id as id',
        'products.image','products.price', 'products.description', 'products.promo_prix')->join('categories', 'categories.id', 'products.category_id')
        ->where('categories.name',$categorie)->paginate(12);
       return view('version3.layouts.search_produit',[
            'produits' => $produits, 'cartItems' => $cartItems, 'categories' => $categories]);
         /* $produits = DB::table('categories')
        ->join('products','categories.id','products.category_id')
        ->where('categories.name',$categorie)->paginate(4);
        return view('version3.layouts.produit',[
          'produits' => $produits, 'cartItems' => $cartItems
        ]);  */
      
    }

    public function banCategory(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID; 
  
        $update_status = DB::table('categories')
        ->where('id', $userID)  
        ->update([
          'status' => $status
        ]);  
        if($update_status){
          echo "status updated successfully";
        }
      }  

    public function lister_SousCat(){

        $souscats = Sub_Cat::paginate(20);
        return view('category.lister_SousCat', compact('souscats'));
    }

    public function ajout_SousCategorie(){

        $categories = Category::pluck('name', 'id');
        return view('category.ajout_SousCat', compact('categories'));

    }

    public function save_SousCate(Request $request)
    
    {

        $this->validate($request,[

            'name'=>'required',
        ]);
         Sub_Cat::create($request->all());
         return back();
    }
 
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::paginate(4);
        return view('category.lister', compact('categories'));
    }

    public function listerCategory(){

        //$categories = Category::paginate(4);
        $categories = DB::table('categories')->get();

        //$categories = DB::table('categories')->where('name', 'Téléphone')->first();
        //$results = DB::select('select * from categories where id = :id', ['id' => 16]);
        return view('category.lister', compact('categories'));
        //return view('category.lister', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view('category.create');
    }
   
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'name' => 'required',
            'product_id' => 'required',
            ///'image' => 'image|mimes:png,jpg,jpeg|max:10000',
        ]);

        /* $image = $request->image;
        if($image){
        $imageName=$image->getClientOriginalName();
        $image->move('imagess', $imageName);
        $formInput['image']=$imageName; */

       //}
         //Category::create($formInput); 
         Category::create($request->all());
         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        /* $product = Product::findOrFail($id); 
        $categories = Category::with('children')->get(); 
       dd($product, $categories);   */      
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('category.editer', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate( $request, [
            'name'=>'Required',
            'slug'=>'required',
            'product_id'=>'required',
            'image'=>'required',
        ]);

        $category = Category::find($id);
        $categoryUpdate = $request->all();
        $category->update($categoryUpdate);

        return redirect('listerCategory');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect('listerCategory');
    }

    public function supprimer($id)
    {
        $category = Sub_Cat::find($id);
        $category->delete();

        return redirect('lister/SousCat');
    }

    public function cat_sou(){
        /* $products= DB::table('products')->select('products.*') 
        ->join('categories', 'categories.id' ,'=', 'products.category_id') 
        ->get();
            dd($products);
 */

/* Product::whereHas('category', function($q) use($categoryId) { 
    $q->where('category', $categoryId) 
     ->orWhere('parent_id', $categoryId); 
}) 
->get();  */

/* $product = Product::with('categories.parent')->findOrFail($id); 
$categories = $product->categories->groupBy('parent_id'); */

/* @foreach($categories as $parent_id => $children) 

    @if($children->first()->parent}} 

    Parent {{$children->first()->parent->name}} 
    @foreach($children as $child) 
     //print child 
    @endforeach 

    @endif 

@endforeach  */
        /* $res = Category::with([ 
            'products', 
            'children.products' 
            ])->find(28);  */
            $categories = DB::table('categories')->orderBY('name')->get();
            $subcategories = DB::table('sub_cats')->Where('category_id', '=', '?')->orderBY('name')->get();
                return view('category.subcategorie', compact('categories','subcategories'));
            //dd($subcategories);
    }
}
