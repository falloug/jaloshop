<?php

namespace App\Http\Controllers;
  
use Illuminate\Http\Request;
use App\Fournisseur;
use App\Quartier;
use App\Product;
use Illuminate\Support\Facades\DB;


class FournisseurController extends Controller
{
    /**  
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fournisseurs = Fournisseur::paginate(4);
        return view('fournisseur.lister', compact('fournisseurs'));
    }

    public function listerFournisseur(){

        //$fournisseurs = Fournisseur::paginate(4);
        $fournisseurs = Fournisseur::latest()->paginate(6);

        return view('fournisseur.lister', compact('fournisseurs'));
    } 

    public function test(Request $request){

        $fournisseurs = Fournisseur::getAllFournisseurs();
        $products = Product::getAllProducts($request->get('category')); 
        $catalogues = DB::table('products')->paginate(8);
        //$categories = Category::with('products')->findOrFail( $tester );
        dd($fournisseurs);
        //return view('category.tester', compact('categories','products','catalogues'));
    
    
    }

    public function banFournisseur(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID; 
  
        $update_status = DB::table('fournisseurs')
        ->where('id', $userID)  
        ->update([
          'status' => $status
        ]);  
        if($update_status){
          echo "status updated successfully";
        }
      }  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quartiers = Quartier::pluck('nom', 'id');

        return view('fournisseur.ajout_fournisseur', compact('quartiers'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $formInput = $request->except('image');

        $this->validate($request,[
            'nom'=>'required',
            'phone'=>'Required',
            'adresse'=>'Required',
            //'image'=>'image|mimes:png,jpg,jpeg|max:10000',

            ]);
           // image upload
           /* $image = $request->image;
           if($image){
    
            $imageName=$image->getClientOriginalName();
            $image->move('imagess', $imageName);
            $formInput['image']=$imageName;
    
           }
           Product::create($formInput); */
        Fournisseur::create($request->all());
         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fournisseur = Fournisseur::find($id);
        $quartiers = Quartier::pluck('nom', 'id');

        return view('fournisseur.editer', compact('fournisseur', 'quartiers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate( $request, [
            'nom'=>'Required',
            'phone'=>'Required',
            'adresse'=>'Required',
            'image'=>'Required'    

        ]);

        $fournisseur = Fournisseur::find($id);
        $fournisseurUpdate = $request->all();
        $fournisseur->update($fournisseurUpdate);

        return redirect('listerFournisseur');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fournisseur = Fournisseur::find($id);
        $fournisseur->delete();

        return redirect('listerFournisseur');
    }
}
