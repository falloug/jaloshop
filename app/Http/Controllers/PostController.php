<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Posts;
use Auth;
use DB;
use Session;
use Analytics;
use Spatie\Analytics\Period;
use Illuminate\Support\Collection;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function data(){

        $analyticsData = Analytics::fetchTotalVisitorsAndPageViews(Period::days(7));
        //dd($analyticsData[0]['pageViews']);
       dd($analyticsData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('blog.index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //

        $this->validate($request,[
            'title'=>'required',  


        ]);

        Auth::user()->posts()->create($request->all());
         return back();
    }

    /**  
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $post = Posts::find($id);

        $data = array(
            'id' => $id,
            'post' => $post
        );
        $blogkey = 'blog_' . $post->id;
        if(!Session::has($blogkey)){
            $post->increment('visit_count');
            Session::put($blogkey,1);
        }
        return view('blog.view_post', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        /* $post = Posts::find($id);

        $commentCount = $request->commentCount;
        $visitCount = $request->visitCount;

        $post->comment_count = $commentCount;
        $post->visit_count = $visitCount;

        $post->save(); */
        $this->validate( $request, [
           
            'comment_count'=>'required',
            'visit_count'=>'required',
        ]);

        $post = Posts::find($id);

        $commentCount = $request->commentCount;
        $visitCount = $request->visitCount;
        $post->comment_count = $commentCount;
        $post->visit_count = $visitCount;

        $postUpdate = $request->all();
        $category->update($postUpdate);
        $post->save();
        return redirect()->route('posts.show', ['id' => $id]); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
