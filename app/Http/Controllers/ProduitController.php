<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder\lists;
use App\Http\Requests;
use App\Produit;
use App\Categorie;
use App\Cart;
use App\Commande;
use Session;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produits = Produit::all();
        return view('layouts.index', compact('produits'));
    }
    public function listerProduit(){

        $produit = Produit::all();
        $commandes = Commande::all();

        return view('produit.pro', compact('produit', 'commandes'));
    }
    public function lister()
    {
        $produits = Produit::all();
        return view('layouts.index', compact('produits'));
    }

    public function getAddToCart(Request $request, $id){
        $produit = Produit::find($id);
        $oldCart = Session::has('cart') ? Session::get('cart') : null;
        $cart = new Cart($oldCart);
        $cart->add($produit, $produit->id);

        $request->session()->put('cart', $cart);
        return redirect('/produit');
    }

    public function getCart(){
         if(Session::has('cart')) {

            $oldCart = Session::get('cart');
            $cart = new Cart($oldCart);

            return view('shop.shopping-cart', ['produits' => $cart->items, 'totalPrice' => 
            $cart->totalPrice]);
        }
    }

    public function checkout(){

        if(!Session::has('cart')){
            return view('shop.shopping-cart');
        }
        $oldCart = Session::get('cart');
        $cart = new Cart($oldCart);
        $total = $cart->totalPrice;
        return view('client.create', ['total' => $total]);
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {
        $produit = new Produit();
        $categories = Categorie::pluck('nomCategorie', 'id');

        return view('produit.create', compact('produit', 'categories'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate( $request, [
            'nomProduit'=>'Required',
            'description'=>'Required',
            'price'=>'Required',
            'qtstock'=>'Required',
            'image'=>'Required'

        ]);

        $produits = $request->all();
        Produit::create($produits);
        return redirect('/produi.edit');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $produit = Produit::find($id);
        return view('layouts.details', compact('produit'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $produit = Produit::find($id);
        return view('produit.edit', compact('produit'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate( $request, [
            'nomProduit'=>'Required',
            'description'=>'Required',
            'prixBien'=>'Required',
            'units'=>'Required',
            'image'=>'Required'

        ]);

        $produit = Produit::find($id);
        $produitUpdate = $request->all();
        $produit->update($produitUpdate);

        return redirect('produit');
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $produit = Produit::find($id);
        $produit->delete();

        return redirect('produit');
    }

    public function ind()
    {
        $products = Product::paginate(4);
        return view('layouts.index', compact('products'));
    }
}
