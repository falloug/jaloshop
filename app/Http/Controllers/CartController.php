<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use App\Product;
use App\Order;
use App\Address;
use App\Quartier;
class CartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cartItems = Cart::content();

        return view('cart.index', compact('cartItems'));
    }

    public function Checkout(){
        $order = Order::createOrder();
        $quartiers = Quartier::pluck('nom', 'id');
        return view('front.checkout',[
            'data' => Cart::content(),
            'quartiers' => $quartiers
          ]);
    }


    public function payment()
    {
        $cartItems = Cart::content();

        return view('cart.payment', compact('cartItems'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    public function addItem($id){

        $pro = Product::find($id);

        $add = Cart::add(['id' => $pro->id, 'name' => $pro->name,
        'qty' => 1, 'price' => $pro->price, 
        'options' =>[
        'img' => $pro->image,
        'size' => 'large'
        
        ]]);

        return back();
    }

    public function updateItem(Request $request){
        $qty = $request->newQty;
        $rowId = $request->rowID;
        // update cart
        Cart::update($rowId,$qty);
        echo "Cart updated successfully";
      }
  
      public function removeItem($id){
        Cart::remove($id);
        return back();
      }
 
    /**  
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Cart::update($id,['qty' => $request->qty, 'options' =>['size'=>$request->size]]);
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::remove($id);
        return back();
    }
}
