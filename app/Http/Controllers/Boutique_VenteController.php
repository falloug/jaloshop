<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Boutiquier_Vente;
use App\Boutique;
use App\Product;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;



class Boutique_VenteController extends Controller
{
    /** 
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() 
    {
        //
    }

   

    public function mySearch(Request $request)
    {
    	if($request->has('search')){

    		$boutiquiers = Boutiquier_Vente::search($request->get('search'))->with('boutique')->get();	

        }
        else{
    		$boutiquiers = Boutiquier_Vente::get();
    	}
    	return view('boutique_ventes.search_boutiquier', compact('boutiquiers', 'boutiques'));
    }
    
    /* public function searchs()
    {
        return view('boutiquier.search_boutiquier');
    } */
   /*  public function search()
    {
        $q = Input::get('q');
        $boutiquier = Boutiquier_Vente::where('nomProduit','LIKE','%'.$q.'%')->orWhere('boutique_id','LIKE','%'.$q.'%')->get();
    if (count($boutiquier) > 0)
        return view('boutiquier.search_boutiquier')->withDetails($boutiquier)->withQuery($q);
    else
        return view('boutiquier.search_boutiquier')->withMessage('No Details found. Try to search again !');

    } */

    public function listerBoutique_Vente(){

        $boutiquiers = Boutiquier_Vente::paginate(4);
         
        //$boutiquiers = DB::table('boutiquier_ventes')->where('id', '=',  1)->get();

        /* $boutiquiers = DB::table('boutiquier_ventes')
        ->whereDate('created_at', '2016-12-31')
                ->get(); */

        //$boutiquier = Boutiquier_Vente::get(array('total', 'date_vente'));

        return view('boutique_ventes.lister', compact('boutiquiers'));
    } 

    public function addItem($id){

    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $boutiques = Boutique::pluck('nom', 'id');
        return view('boutique_ventes.ajout_boutiquier_vente', compact('boutiques'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation

        $this->validate($request,[

            'nomProduit'=>'required',
            'description'=>'required',
            'date_vente'=>'required',
            'prixProduit'=>'required',
            'quantity'=>'required',
            'total'=>'required',
            
            
        ]);
       // image upload
       
       Boutiquier_Vente::create($request->all());

       return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
