<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use App\User;
use App\Freelance;
use App\Quartier;  
use DB;
use App\Role;
use App\Parametre;

class FreelanceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  
        $freelances = Freelance::paginate(10);
        return view('freelance.lister', compact('freelances'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $quartiers = Quartier::pluck('nom','id');
        $parametres = Parametre::pluck('nom','id');
        $roles = Role::pluck('nom','id');
        return view('freelance.ajouter', compact('quartiers','parametres','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'nom'=>'required',
            'adresse'=>'required',
            'phone'=>'required',
            
        ]);
       // création user;
                        $user = new User;
                    $user->name = $request->get('nom');
                    //$user->password = bcrypt($request->get('password'));
                    $user->email = $request->get('email');
                    $user->phone = $request->get('phone');
                    $user->condiction = $request->get('condiction');
                    $user->role_id = $request->get('role_id');
                    $user->quartier_id = $request->get('quartier_id');
                    $user->password = Hash::make($request->get('password'));
       if($user->save()){
                    error_log('la création a réussi');
                    $freelance = new Freelance;
                    $freelance->nom = $request->get('nom');
                    $freelance->phone = $request->get('phone');
                    $freelance->adresse = $request->get('adresse');
                    $freelance->quartier_id = $request->get('quartier_id');
                    $freelance->parametre_id = $request->get('parametre_id');
                    $freelance->user_id = $user->id;
                    //$freelance->nom = $request->get('name');
                    //$freelance->save();
                    if($freelance->save())  
                    {
                        Auth::login($user);
                        return back();

                    }
                    else
                    {
                        flash('user not saved')->error();

                    }

                    //Boutique::create($request->all());

                }
       //Auth::user()->boutiques()->create($request->all());
       return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $freelance = Freelance::find($id);
        $freelance->delete();

        return redirect('freelance');
    }
}
