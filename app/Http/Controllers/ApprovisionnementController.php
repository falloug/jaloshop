<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Approvisionnement;
use App\Produit;
use App\Product;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use JWTAuth;

class ApprovisionnementController extends Controller
{
    //

public function index(Request $request)
    {
        //$userAuth = JWTAuth::toUser($request->header('Authorization'));

        $approvisionnements = DB::table('approvisionnements')
                    ->leftjoin('products', 'products.id', '=', 'approvisionnements.produit_id')
                    ->leftjoin('categories', 'categories.id', '=', 'products.category_id')
                    ->join('users', 'users.id', '=', 'approvisionnements.vendeur_id')
                    ->leftjoin('quartiers', 'quartiers.id', '=', 'users.quartier_id')
                    ->select('approvisionnements.*', 'products.name as libelle_produit','products.image','products.price', 'categories.name as categorie',
                    'users.name as boutiquier', 'users.phone as boutiquier_phone','quartiers.nom as boutiquier_quartier')
                    ->orderBy('date_ajout','DESC')
                    ->paginate(5);

        return view('approvisionnement.index')->with(['approvisionnements' => $approvisionnements]);
    }

    public function banApprovisionnement(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('approvisionnements')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }    
}
