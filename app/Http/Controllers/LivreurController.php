<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Auth\RegistersUsers;
use Auth;
use App\User;
use App\Livreur;
use App\Quartier;  
use DB;
use App\Role;

class LivreurController extends Controller
{
    //

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //  
        $livreurs = Livreur::paginate(10);
        return view('livreur.lister', compact('livreurs'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $quartiers = Quartier::pluck('nom','id');
        
        $roles = Role::pluck('nom','id');
        return view('livreur.ajouter', compact('quartiers','roles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[

            'nom'=>'required',
            'adresse'=>'required',
            'phone'=>'required',
            
        ]);  
       // création user;
                    $user = new User;
                    $user->name = $request->get('nom');
                    //$user->password = bcrypt($request->get('password'));
                    $user->email = $request->get('email');
                    $user->phone = $request->get('phone');
                    $user->condiction = $request->get('condiction');
                    $user->role_id = $request->get('role_id');
                    $user->quartier_id = $request->get('quartier_id');
                    $user->password = Hash::make($request->get('password'));
       if($user->save()){
                    error_log('la création a réussi');
                    $livreur = new Livreur;
                    $livreur->prenom = $request->get('prenom'); 
                    $livreur->nom = $request->get('nom'); 
                    $livreur->phone = $request->get('phone');
                    $livreur->adresse = $request->get('adresse');
                    $livreur->date_debut = $request->get('date_debut');
                    $livreur->nom_societe = $request->get('nom_societe');
                    $livreur->quartier_id = $request->get('quartier_id');
                    $livreur->user_id = $user->id;
                   

                    if($livreur->save())  
                    {
                        Auth::login($user);
                        return back();

                    }
                    else
                    {
                        flash('user not saved')->error();

                    }

                }
       //Auth::user()->boutiques()->create($request->all());
       return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $livreur = Livreur::find($id);
        $livreur->delete();

        return redirect('livreur');
    }
}
  