<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Boutique;
use App\Fournisseur;
use App\Commercial;
use Cart;
use DB;
use App\Quartier;   

class TypeController extends Controller
{
    //

    public function ajout_fournisseur(){
        $quartiers = Quartier::pluck('nom', 'id');
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();


        return view('version3.layouts.formulaire_fourni', compact('categories', 'cartItems', 'quartiers'));

    }

    public function save_fournisseur(Request $request){

        $this->validate($request,[
            'nom'=>'required',
            'phone'=>'Required',
            'adresse'=>'Required',
        ]);
        Fournisseur::create($request->all());
         return back();
        
    }

    public function ajout_commercial(){
        $quartiers = Quartier::pluck('nom', 'id');
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();


        return view('version3.layouts.formulaire_com', compact('categories', 'cartItems', 'quartiers'));

    }

    public function save_commercial(Request $request){

        $this->validate($request,[
            'nom'=>'required',
            'phone'=>'Required',
            'adresse'=>'Required',
        ]);
         Commercial::create($request->all());
         return back();
        
    }

    public function ajout_boutiquier(){
        $quartiers = Quartier::pluck('nom', 'id');
        $commercials = Commercial::pluck('nom', 'phone', 'id');
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();


        return view('version3.layouts.formulaire_bouti', compact( 'categories', 'cartItems', 'quartiers', 'commercials'));

    }

    public function save_boutiquier(Request $request){

        $this->validate($request,[

            'nom'=>'required',
            'adresse'=>'required',
            'phone'=>'required',
            
        ]);
       // image upload
       
       Boutique::create($request->all());

       return back();
        
    }
    public function a_propos()
    {
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();

        return view('version3.layouts.a-propos', compact( 'categories', 'cartItems'));
    }

    public function vendeur(Request $request)
    {
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        if($request->has('search')){

    		$vendeurs = Commercial::search($request->get('search'))->with('quartier')->paginate(9);	

        }
        else{
    		$vendeurs = Commercial::paginate(9);
        } 

        return view('version3.layouts.vendeur', compact('vendeurs', 'categories', 'cartItems'));
    }

    public function fournisseur(Request $request)
    {
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();

        if($request->has('search')){

    		$fournisseurs = Fournisseur::search($request->get('search'))->with('quartier')->paginate(9);	

        }
        else{
    		$fournisseurs = Fournisseur::paginate(9);
        } 

        return view('version3.layouts.fournisseur', compact('fournisseurs', 'categories', 'cartItems'));
    }


}
