<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Fournisseur;
use App\Commercial;
use App\Boutique;
use Cart;
use TESTER; 
//use Excel;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProductExport;
use Illuminate\Support\Facades\DB;
use App\Exports\ProductQueryExport;  
use App\Exports\FRomtQueryExport;
use PDF;

//use CyrildeWit\EloquentViewable\Support\Period;
//use Auth;


class ProductController extends Controller  
{  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::paginate(4);
        $boutiquiers = Boutique::paginate(9);
        return view('layouts.index', compact('products','boutiquiers')); 
    }

    public function export($type)
    {
        /* $data = Product::get()->toArray();
        return Excel::create('itsolutions_example', function($excel) use ($data){
            $excel->sheet('mySheet', function($sheet)
            use ($data){
               $sheet->fromArray($data);
            });
        })->download($type); */
     return Excel::download(new ProductQueryExport(2018), 'products.' .$type);
    }

    public function generatePDF()

    {

        $product = Product::all();
        $pdf = PDF::loadView('produit.lister', $product);
        return $pdf->download('itsolutionstuff.pdf');

    }
    
    public function excel(){
        $products = DB ::table('products')->get()->toArray();
        $products_array[] = array('name', 'description', 'price', 'promo_prix', 'image', 'tarif', 'category_id','fournisseur_id');

        foreach($products as $product){

            $products_array[] = array(
                'name' => $product->name,
                'description' => $product->description,'price' => $product->price,'promo_prix' => $product->promo_prix,'image' => $product->image,
                'tarif' => $product->tarif,'category_id' => $product->category_id,'fournisseur_id' => $product->fournisseur_id,
            );
        }
        Excel::create('Product Data', function($excel) use ($products_array){
          
            $excel->setTitle('Product Data');
            $excel->sheet('Product Data', function($sheet)
            use ($products_array){
               $sheet->fromArray($products_array, null, 'A1', false, false);
            });
        })->download('xlsx');

    }

    public function autocomplete(Request $request)
    {
        $data = Product::select("name")
                ->where("name","LIKE","%{$request->input('query')}%")
                ->get();
        return response()->json($data);
    }

    public function getCatalogue(Request $request)
    {
        $categories = DB::table('categories')->where('status',false)->get();
        $cartItems = Cart::content();   
        $category_id = $request->category_id;  
        $products = Product::getAllProducts($request->get('category'));  
  
   
        $data = DB::table('products')->select('products.name as name','products.id as id',
        'products.image','products.price', 'products.description')
            ->join('categories','categories.id','products.category_id')
            ->where('products.category_id',$category_id)->get();

        if($request->has('search')){

    		$produits = Product::search($request->get('search'))->with('category')->paginate(12);	

        }
        else{
    		$produits = Product::latest()->paginate(12);
        }
       

        return view('version3.layouts.produit')
        ->with(['produits' => $produits, 'categories'  
          => $categories, 'cartItems'  => $cartItems, 'products' => $products,
          'data' => $data]);
        
    }

    public function productsCat(Request $request){
        //$categories = DB::table('categories')->get();
        ///$cartItems = Cart::content();
        $category = $request->category_id;
        $produits = DB::table('products')->select('products.name as name','products.id as id',
        'products.image','products.price', 'products.description')
        ->join('categories', 'categories.id', 'products.category_id')
        ->where('products.category_id', $category)
        ->get();
        //return view('version3.layouts.search_produit', compact('produits', 'categories', 'cartItems'));

        dd($produits);
    }

    public function showProduit()
    {
        return view('layouts.showProduit');
    }

    public function mySearch(Request $request)

    {
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();  
        $categorie =  $request->category;
        $products = Product::getAllProducts($request->get('category')); 

     	if($request->has('search')){

    		$produits = Product::search($request->get('search'))->with('category')->paginate(12);	

        }
        else{
            //$produits = Product::get();
            $produits = Product::latest()->paginate(12);
        }  
        
        
    	return view('version3.layouts.search_produit', compact('produits','products', 'categories', 'cartItems'));
    }

    public function getImages(){

        $images = Product::pluck('image');
        return view('layouts.showProduit', compact('images'));

    }

    public function listerProduct(Request $request){

        $products = Product::paginate(4);

            $categorie =  $request->category;
           
            //$views =  DB::table('views')->get();
            //$products = Product::withCount('views')->get();
            if($request->has('search')){

                $products = Product::search($request->get('search'))->with('category')->paginate(200);	
    
            }
            else{
                //$produits = Product::get();
                $products =  Product::latest()->paginate(10);
            }  
        return view('produit.lister', compact('products'));
    }


    public function status_product(Request $request){

        $products = Product::paginate(4);

        $categorie =  $request->category;

            if($request->has('search')){

                $products = Product::search($request->get('search'))->with('category')->paginate(10);	
    
            }
            else{
                
                $products =  Product::latest()->paginate(10);
            }  
        return view('admin.status_product', compact('products'));
    }

    public function banProduct(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('products')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::pluck('name', 'id');
        $fournisseurs = Fournisseur::pluck('nom', 'id');
        $commercials = Commercial::pluck('nom', 'id');

        return view('produit.ajouter', compact('categories','fournisseurs','commercials'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $formInput = $request->except('image');
        // validation

        $this->validate($request,[
            'image.*' => 'mimes:doc,pdf,docx,zip,png,jpeg,odt,jpg,svc,csv,mpeg,ogg,mp4,webm,3gp,mov,flv,avi,wmv,ts',
            'name'=>'required',
            'description'=>'required',
            'price'=>'required',
            'promo_prix' => 'required',
            'image'=>'image|mimes:png,jpg,jpeg|max:10000',

        ]);
       // image upload
      /* $image = $request->image;
       if($image){

        $imageName=$image->getClientOriginalName();
        $image->move('imagess', $imageName);
        $formInput['image']=$imageName;

       }*/
                $image = $request->file('image');
                if($image){
                $imageNameP = $image->getClientOriginalName();
                $image->move(public_path().'/jaloimage/', $imageNameP);
                $formInput['image']=$imageNameP;
                    }
       Product::create($formInput);
       Product::create($request->all());

       return back();
    }
  
    /**
     * Display the specified resource.
     *   
     * @param  int  $id  
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $id)
    {
       $produit = Product::find($id);
       $produits = Product::paginate(6);
       $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        /* views($produit)->record();
         views($produit)->count();
         views($produit)->delayInSession(now()->addHours(2))->record();
         views($produit)->period(Period::create('2018-01-24', '2018-05-22'))->remember()->count(); */
         //views($produit)->increment();

        //views()->countByType(Product::class);
        /* views($produit)
        ->overrideIpAddress('127.0.0.1')
    ->record(); */
    
    
        if($request->has('search')){

    		$produits = Product::search($request->get('search'))->with('category')->paginate(3);	

        }
        else{
    		$produits = Product::paginate(3);
    	}


       return view('version3.layouts.details', compact('produit', 'produits', 'categories', 'cartItems'));

    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);
       /*  views($product)->record();
        views($product)->count(); */
        $categories = Category::pluck('name', 'id');
        $fournisseurs = Fournisseur::pluck('nom', 'id');
        
        return view('produit.edit', compact('product', 'categories', 'fournisseurs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate( $request, [
            'name'=>'Required',
            'description'=>'Required',
            'price'=>'Required',
            'image'=>'Required'

        ]);

        $product = Product::find($id);
        $productUpdate = $request->all();
        $product->update($productUpdate);
        return redirect('/listerProduct');
   
    }
  
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);
        $product->delete();

        return redirect('listerProduct');
    }

    public function products()
    {
        return view('master');
    }

    public function TESTER()
    {
        return view('layouts.TESTER');
    }

    public function edit_product($product_id)
    {
                $product_info=DB::table('products')
                    ->where('product',$product)
                    ->first();
                    $product_info=view('admin.layouts.edit_product')
                    ->with('product_info',$product_info);
                    return view('admin.layouts.edit_product')
                    ->with('admin_edit_product',$product_info);
                    
    }

    public function boutiquier(Request $request)
    {
        $fournisseurs = DB::table('fournisseurs')->get();
        $cartItems = Cart::content(); 
        $category_id = $request->category_id;
        $products = Product::getAllProducts($request->get('category')); 
  

        $data = DB::table('products')->select('products.name as name','products.id as id',
        'products.image','products.price', 'products.description')
            ->join('categories','categories.id','products.category_id')
            ->where('products.category_id',$category_id)->get();

        if($request->has('search')){

    		$produits = Product::search($request->get('search'))->with('category')->paginate(12);	

        }
        else{
    		$produits = DB::table('products')->where('status', 'like', 4)->paginate(12);
        }
       

        return view('version3.produit.boutiquier')
        ->with(['produits' => $produits, 'fournisseurs'  
          => $fournisseurs, 'cartItems'  => $cartItems, 'products' => $products,
          'data' => $data]);
        
    }

    public function details_boutiquier(Request $request,$id)
    {
        $produit = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       if($request->has('search')){

        $produits = Product::search($request->get('search'))->with('category')->paginate(12);	

    }
    else{
        $produits = Product::paginate(12);
    }
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
      return view('version3.layouts.boutiquier_details', compact('produits',
       'produit', 'categories', 'cartItems'
              ));

              /* if($request->has('search')){

                $commandes = Product::search($request->get('search'))->with('category')->paginate(3);	
    
            }
            else{
                $commandes = Product::paginate(3);
            } */
    }

    
}
