<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder\lists;
session_start();

use Auth;
use Session;
use App\User;
use Cart;
use App\Order_Product;
use App\News;
use App\Category;
use App\Product; 
use App\Quartier; 



class AdminController extends Controller
{
    public function login(Request $request){

        if($request->isMethod('post')){

            $data = $request->input();
            if(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password'], 'status'=>'2'])){

                //echo "succes"; die;
                return redirect('/admin/dashboard');
            }
            elseif(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password'], 'status'=>'3'])){
                return redirect('/listerCommercial');
            }
            elseif(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password'], 'status'=>'4'])){
                return redirect('/commande/commercial');
            }elseif(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password'], 'status'=>'5'])){
                return redirect('/listerFournisseur');
            }
            elseif(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password'], 'status'=>'6'])){
                return redirect('/admin/dashboard');
            }     

            elseif(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password'], 'status'=>'7'])){
                return redirect('/lister/c_commercesup');
            }

            elseif(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password'], 'status'=>'8'])){
                return redirect('/produit/boutiquier');
            }

            elseif(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password'], 'status'=>'9'])){
                return redirect('/mesFreelances');
            }

            elseif(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password']])){
                return redirect('/');
            }
            else{
                //echo "failed"; die;

                return redirect('/admin')->with('flash_message_error', 'Mot de Passe ou phone Incorrect');  
            }
        }
        return view('admin.admin_login');
    }


    public function ajout_utilisateur(){
        //$quartiers = Quartier::pluck('nom', 'id');
        //$categories = DB::table('categories')->get();
        //$cartItems = Cart::content();


        return view('admin.ajout_user');

    }

    public function save_utilisateur(Request $request){

        $this->validate($request,[
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255',
            'condiction' => 'required|string|max:255',
            'password' => 'required|string|min:6|confirmed',
        ]);
        User::create($request->all());
         return back();
        
    }
    public function news()
    {
        $news = News::paginate(10);
        return view('admin.newsletter', compact('news')); 

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    /* public function index()
    {
        $this->AdminAuthCheck();
        return redirect('/admin'); 
    } */

    public function deconect()
    {
        Session::flush();
        return redirect('/admin'); 

    }

    public function lister_commande()
    {

        $commandes = DB::table('order_product')->
       /*  Select('products.name as name','products.id as id',
         'products.image','products.price', 'products.description')
          ->leftJoin('products', 'products.id', 'order_product.product_id')
          ->orderBy('order_product.id','DESC')
          -> */paginate(6);
          $categories = DB::table('categories')->get();
         $cartItems = Cart::content();
       return view('version3.layouts.lister_commande', compact('commandes',
        'vente', 'categories', 'cartItems'));
    }

     public function supprimer($id)
    {
        $commandes = Order_Product::find($id);
        $commandes->delete();
        return redirect('lister_commande');
    }
 


    
   /*  public function AdminAuthCheck()
    {
      $admin=Session::get('id');
     if($admin) {
         return;
     } else{
        return redirect('/admin')->send();

     }  */
    //}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    

    public function dashboard(){
        return view('admi.dashboard');
    }

    public function listerQuartier()
    {

        $quartiers = Quartier::paginate(4);
        return view('quartier.lister', compact('quartiers')); 
    }

   /*  public function __construct()
    {
        $this->middleware('guest');
    } */

    public function deconnexion(){

        Session::flush();
        //$this->middleware('guest')->except('/deconnexion');

        return redirect('/admin')->with('flash_message_success', 'Logged out Successfully');  

    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function banProduct(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('products')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }


    public function banUser(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('users')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }      
    
    
        public function orders(){
          $orders = DB::table('orders')
         ->Select('users.name as username','users.id as userId',
         'orders.id','orders.status','orders.total','orders.created_at')
          ->leftJoin('users', 'users.id', 'orders.user_id')
          ->orderBy('orders.id','DESC')
          ->get();
          return view('admin.orders',compact('orders'));
        }
  
        public function orderStatusUpdate(Request $request){
          if(isset($request->order_id) && isset($request->order_status)){
            //save order status
            $uptStatus =DB::table('orders')->where('id',$request->order_id)
            ->update(['status' => $request->order_status]);
    
            if($uptStatus){
              echo "Order " . $request->order_status;
            }
            else{
              echo "error";
            }
          }
        }


        public function commandes_pro(){
            $orders = DB::table('orders')
           ->Select('users.name as username','users.id as userId','users.phone as telephone',
           'orders.id','orders.status','orders.total','orders.created_at')
            ->leftJoin('users', 'users.id', 'orders.user_id')
            ->orderBy('orders.id','DESC')
            ->paginate(6);
            $addresse_livraisons = DB::table('dAddress')
       ->Select('users.name as username','users.id as userId','users.phone as telephone',
       'quartiers.nom as nomComplet','quartiers.id as id',
       'dAddress.id','dAddress.adresse','dAddress.livraison','dAddress.created_at')
        ->leftJoin('users', 'users.id', 'dAddress.userId')
        ->leftJoin('quartiers', 'quartiers.id', 'dAddress.quartier_id')
        ->orderBy('dAddress.id','DESC')
        ->paginate(10);
            return view('admin.commandes_pro',compact('orders', 'addresse_livraisons'));
          }



        public function banOrder(Request $request){
            //return $request->all();
            $status = $request->status;
            $userID = $request->userID;
      
            $update_status = DB::table('orders')
            ->where('id', $userID)
            ->update([
              'status' => $status
            ]);
            if($update_status){
              echo "status updated successfully";
            }
          }


          public function commandes_free(){
            $orders = DB::table('order_freelance')
           ->Select('freelances.nom as username','freelances.id as userId','freelances.phone as telephone',
           'client_freelances.nom as nom','client_freelances.id as id','client_freelances.phone as telephone',
           'order_freelance.id','order_freelance.status','order_freelance.total','order_freelance.created_at')
            ->leftJoin('freelances', 'freelances.id', 'order_freelance.freelance_id')
            ->leftJoin('client_freelances', 'client_freelances.id', 'order_freelance.client_freelance_id')
            ->orderBy('order_freelance.id','DESC')
            ->paginate(6);
            $addresse_livraisons = DB::table('client_freelances')
       ->Select('client_freelances.nom as username','client_freelances.id as userId','client_freelances.phone as telephone',
       'quartiers.nom as nomComplet','quartiers.id as id',
       'client_freelances.adresse','client_freelances.created_at')
        ->leftJoin('freelances', 'freelances.id', 'client_freelances.freelance_id')
        ->leftJoin('quartiers', 'quartiers.id', 'client_freelances.quartier_id')
        ->orderBy('client_freelances.id','DESC')
        ->paginate(10);
            return view('admin.commande_freelance',compact('orders', 'addresse_livraisons'));
          }


          
        public function banOrderFree(Request $request){
            //return $request->all();
            $status = $request->status;
            $userID = $request->userID;
      
            $update_status = DB::table('order_freelance')
            ->where('id', $userID)
            ->update([
              'status' => $status
            ]);
            if($update_status){
              echo "status updated successfully";
            }
          }
  
}
