<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Carbon\Carbon;
use App\Order_Product;
use App\Order_Product_Freelance;
use Auth;
use Cart;
Use App\Category;
Use App\Commercial;
Use App\User;
use App\dAddress;
use App\Paiment;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;  
use \Datetime;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\ProductExport;
use App\Exports\ProductQueryExport; 
use App\Exports\ClientComQueryExport;  
use App\Exports\ClientFreeQueryExport;  
use App\Exports\ClientBoutQueryExport;  
use App\Exports\ClientQueryExport;  
use App\Exports\FRomtQueryExport;
use PDF;

class OderController extends Controller  
{
  public function export_commerciaux($type)
  {
  return Excel::download(new ClientComQueryExport(2018), 'Client des Commerciaux.' .$type);
  }

  public function export_freelance($type)
  {
  return Excel::download(new ClientFreeQueryExport(2018), 'Client des Freelances.' .$type);
  }

  public function export_boutiquier($type)
  {
  return Excel::download(new ClientBoutQueryExport(2018), 'Client des boutiquiers.' .$type);
  }

  public function export_client($type)
  {
  return Excel::download(new ClientQueryExport(2018), 'Client.' .$type);
  }
  
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }  

    public function test(){ 
        $cartItems = Cart::content();
        $categories = Category::all();
        return view('commander.test', compact('cartItems','categories'));
    }

    public function Orders($type=''){

        if($type=='pending'){
   
            $orders = Order::where('status', '0')->get();
             
        } elseif($type=='status'){

            $orders = Order::where('status', '1')->get();

        } else{
           $orders = Order::all();
        }
        return view('commande.orders', compact('orders'));

    }

    public function toggledeliver(Request $request, $orderId){

       $order = Order::find($orderId);

       if($request->has('status')){
        $order->status=$request->status;

       } else {
         $order->status="0";

       }

       $order->save();

       return back();
    }

    public function commercial()
    {
     
        $products = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price',
                    'order_product_freelance.id as ID','order_freelance.id','order_product_freelance.livreur_id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                    'client_freelances.nom as clientName','client_freelances.livraison as AddressLivraison','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                            -> join('products', 'products.id', 'order_product_freelance.product_id')
                            -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                            -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                            -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                            -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                            -> where('roles.id', '=', 4 )
                            ->orderBy('order_freelance.id', 'DESC')
                            -> paginate(5);  
                            $livreurs = DB::table('livreurs')->get();

                            return view('commande.commercial', compact('products','livreurs'));
    }

       
    public function freelance()
    {


        $products = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price',
                    'order_product_freelance.id as ID','order_freelance.id','order_product_freelance.livreur_id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                    'client_freelances.nom as clientName','client_freelances.livraison as AddressLivraison','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                            -> join('products', 'products.id', 'order_product_freelance.product_id')
                            -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                            -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                            -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                            -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                            -> where('roles.id', '=', 1 )
                            -> orderBy('order_freelance.id', 'DESC')
                            -> paginate(5);  
                            $livreurs = DB::table('livreurs')->get();

                            return view('commande.freelance', compact('products','livreurs'));
    }

    public function mesFreelances()
    {


        $products = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price',
                    'order_product_freelance.id as ID','order_product_freelance.livreur_id','order_freelance.id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                    'client_freelances.nom as clientName','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                            -> join('products', 'products.id', 'order_product_freelance.product_id')
                            -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                            -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                            -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                            -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                            -> where('roles.id', '=', 1 )
                            ->orderBy('order_freelance.id', 'DESC')
                            -> paginate(5);  

                            return view('commande.mesFreelances', compact('products'));
    }

    public function boutiquier()
    {


        $products = DB::table('order_product')->select('order_product.qty as quantity','order_product.total as total_commande', 'products.name','products.image', 'products.price',
                'order_product.id as ID','order_product.livreur_id','orders.user_id','orders.id','roles.nom','users.name as Myname','users.phone as Myphone','orders.created_at as date','orders.status','orders.total', 
                'dAddress.fullname as clientName', 'dAddress.phone as clientPhone','dAddress.livraison as AddressLivraison','dAddress.adresse as ClientAdresse')
                        -> join('products', 'products.id', 'order_product.product_id')
                        -> join('orders', 'orders.id', 'order_product.order_id')
                        -> leftjoin('dAddress', 'dAddress.id', '=', 'orders.client_id')
                        -> leftjoin('users', 'users.id', '=', 'orders.boutiquier_id')
                        -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                        -> where('roles.id', '=', 9)
                        ->orderBy('orders.id', 'DESC')
                        -> paginate(5);
                        $livreurs = DB::table('livreurs')->get();

                        return view('commande.boutiquier', compact('products','livreurs'));
    }

    public function grpublic()
    {


        $products = DB::table('order_product')->select('order_product.qty as quantity','order_product.total as total_commande',
        'orders.status','order_product.livreur_id', 'products.name','products.image', 'products.price',
                'order_product.id as ID','order_product.status as myStatus','orders.user_id','orders.id','quartiers.nom as quartier',
                'roles.nom','users.name as Myname','users.phone as Myphone','orders.created_at as date','orders.total', 
                'dAddress.fullname as clientName', 'dAddress.phone as clientPhone','dAddress.livraison as AddressLivraison',
                'dAddress.adresse as ClientAdresse')
                        -> join('products', 'products.id', 'order_product.product_id')
                        -> join('orders', 'orders.id', 'order_product.order_id')
                        -> leftjoin('dAddress', 'dAddress.id', '=', 'orders.client_id')
                        -> leftjoin('users', 'users.id', '=', 'orders.user_id')
                        -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                        -> leftjoin('quartiers', 'quartiers.id', '=', 'users.quartier_id')
                        -> where('roles.id', '=', 2 )
                        ->orderBy('orders.id', 'DESC')
                        -> paginate(5);
        $livreurs = DB::table('livreurs')->get();
                        return view('commande.grpublic', compact('products','livreurs'));
    }

    public function banOrderGrpublic(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('orders')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }

      public function banOrderGrpublics(Request $request){
        //return $request->all();
        $livreur_id = $request->livreur_id;
        $userID = $request->userID;
  
        $update_livreur = DB::table('order_product')->select('order_product.id as ID')
        ->where('order_product.ID', $userID)
        ->update([
          'livreur_id' => $livreur_id
        ]);
        if($update_livreur){
          echo "livreur updated successfully";
        }
      }

      public function banOrderFreelance(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('order_freelance')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }

      public function banOrderFreelances(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('order_freelance')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }

      public function banOrderCommercial(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('order_freelance')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }

      public function banOrderBoutiquier(Request $request){
        //return $request->all();
        $status = $request->status;
        $userID = $request->userID;
  
        $update_status = DB::table('orders')
        ->where('id', $userID)
        ->update([
          'status' => $status
        ]);
        if($update_status){
          echo "status updated successfully";
        }
      }

      public function approve($id){
        $product = Order_Product::findOrFail($id);
        $product->status = 1; //Approved
        $product->save();
        return redirect()->back(); //Redirect user somewhere
     }
     
     public function decline($id){
        $product = Order_Product::findOrFail($id);
        $product->status = 0; //Declined
        $product->save();
        return redirect()->back(); //Redirect user somewhere
     }

     public function etat_commercial()

     {
        $commercials = DB::table('users')
                    ->select('users.id','users.name','users.phone',
                            'roles.nom','roles.id as ID',
                            'quartiers.nom as quartier_nom')
                    ->join('roles', 'roles.id', 'users.role_id')
                    ->join('quartiers', 'quartiers.id', 'users.quartier_id')
                    ->where('roles.nom', '=', 'commercial')
                    ->paginate(10);

        return view('commande.etat_commercial', compact('commercials'));

     }
      /**
       * Display the specified resource.
       *
       * @param  int  $id
       * @return \Illuminate\Http\Response
       */
      public function etat_vente_c($id)
      {
          //
          $commercial = User::find($id);
 
          $products = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price','products.marge',
                    'order_product_freelance.id as ID','order_freelance.id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                    'client_freelances.nom as clientName','users.id as MyId','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                            -> join('products', 'products.id', 'order_product_freelance.product_id')
                            -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                            -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                            -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                            -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                            -> where('users.id', '=', $commercial->id)
                            -> where('order_freelance.status', '=', 1)
                            ->orderBy('order_freelance.created_at', 'DESC')
                            -> get();

          $years = date('Y');
          $mois = date('m');
          
          $startdate = new DateTime('2020-09-15 00:00:00');
          $overdate = new DateTime('2020-09-30 00:00:00');
          //$initial = strtotime($startdate);
          //$finish = strtotime($overdate);
          $marges = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price','products.marge',
                            'order_product_freelance.id as ID','order_freelance.id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                            'client_freelances.nom as clientName','users.id as MyId','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                                    -> join('products', 'products.id', 'order_product_freelance.product_id')
                                    -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                                    -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                                    -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                                    -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                                    -> where('users.id', '=', $commercial->id)
                                    -> where('order_freelance.status', '=', 1)
                                    ->orderBy('order_freelance.created_at', 'DESC')
                                    -> sum('products.marge');   
                                    //-> where('order_freelance.created_at', '>=', $startdate )
                                    //-> where('order_freelance.created_at', '<=', $overdate )
                                    //-> orderBy('order_freelance.created_at', 'DESC')
                                    //-> sum('products.marge');                  
          
          
          return view('commande.etat_vente_c', compact('commercial','products','marges','years','mois'));
      }

      public function etat_freelance()

      {
        $freelances = DB::table('users')
                     ->select('users.id','users.name','users.phone',
                             'roles.nom','roles.id as ID',
                             'quartiers.nom as quartier_nom')
                             ->join('roles', 'roles.id', 'users.role_id')
                             ->join('quartiers', 'quartiers.id', 'users.quartier_id') 
                     ->where('roles.nom', '=', 'freelance')
                     ->paginate(10);

        return view('commande.etat_freelance', compact('freelances'));
             
      }

      public function etat_freelances()

      {
        $freelances = DB::table('users')
                     ->select('users.id','users.name','users.phone',
                             'roles.nom','roles.id as ID',
                             'quartiers.nom as quartier_nom')
                             ->join('roles', 'roles.id', 'users.role_id')
                             ->join('quartiers', 'quartiers.id', 'users.quartier_id') 
                     ->where('roles.nom', '=', 'freelance')
                     ->paginate(10);

        return view('commande.etat_freelances', compact('freelances'));
             
      }
       /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
       public function etat_vente_f($id)
       {
           //
           $freelance = User::find($id);
  
            $products = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price','products.marge',
                     'order_product_freelance.id as ID','order_freelance.id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                     'client_freelances.nom as clientName','users.id as MyId','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                             -> join('products', 'products.id', 'order_product_freelance.product_id')
                             -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                             -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                             -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                             -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                             -> where('users.id', '=', $freelance->id)
                             -> where('order_freelance.status', '=', 1)
                             ->orderBy('order_freelance.created_at', 'DESC')
                             -> get();

            $clients = DB::table('client_freelances')->select('client_freelances.id','client_freelances.nom',
                                 'client_freelances.phone','client_freelances.freelance_id','client_freelances.adresse')   
                             ->where('client_freelances.freelance_id', '=', $freelance->user_id)
                             ->get();              

            $marges = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price','products.marge',
                            'order_product_freelance.id as ID','order_freelance.id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                            'client_freelances.nom as clientName','users.id as MyId','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                                    -> join('products', 'products.id', 'order_product_freelance.product_id')
                                    -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                                    -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                                    -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                                    -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                                    -> where('users.id', '=', $freelance->id)
                                    -> where('order_freelance.status', '=', 1)
                                    ->orderBy('order_freelance.created_at', 'DESC')
                                    -> sum('products.marge');      
 
           return view('commande.etat_vente_f', compact('freelance','products','marges','clients'));
       }

       /**
        * Display the specified resource.
        *
        * @param  int  $id
        * @return \Illuminate\Http\Response
        */
        public function etat_vente_fs($id)
        {
            //
            $freelance = User::find($id);
   
             $products = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price','products.marge',
                      'order_product_freelance.id as ID','order_freelance.id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                      'client_freelances.nom as clientName','users.id as MyId','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                              -> join('products', 'products.id', 'order_product_freelance.product_id')
                              -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                              -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                              -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                              -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                              -> where('users.id', '=', $freelance->id)
                              -> where('order_freelance.status', '=', 1)
                              ->orderBy('order_freelance.created_at', 'DESC')
                              -> get();
 
             $clients = DB::table('client_freelances')->select('client_freelances.id','client_freelances.nom',
                                  'client_freelances.phone','client_freelances.freelance_id','client_freelances.adresse')   
                              ->where('client_freelances.freelance_id', '=', $freelance->id)
                              ->get();              
 
             $marges = DB::table('order_product_freelance')->select('order_product_freelance.qty as quantity','order_product_freelance.total as total_commande', 'products.name','products.image', 'products.price','products.marge',
                             'order_product_freelance.id as ID','order_freelance.id','order_freelance.created_at as date','order_freelance.status','order_freelance.total', 
                             'client_freelances.nom as clientName','users.id as MyId','users.name as Myname','users.phone as Myphone', 'roles.nom as role','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                                     -> join('products', 'products.id', 'order_product_freelance.product_id')
                                     -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                                     -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                                     -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                                     -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                                     -> where('users.id', '=', $freelance->id)
                                     -> where('order_freelance.status', '=', 1)
                                     ->orderBy('order_freelance.created_at', 'DESC')
                                     -> sum('products.marge');      
  
            return view('commande.etat_vente_fs', compact('freelance','products','marges','clients'));
        }
 

    public function recommander()

    {
        $recommandations =  DB::table('recommandations')
        ->select('recommandations.id', 'recommandations.nom_client as client', 'recommandations.phone_client as phone',
        'products.name as produit','products.price as prix','products.image', 
        'users.name','users.phone as Myphone','quartiers.nom as quartier'
        )
        ->join('products','products.id', '=', 'recommandations.produit_id')
        ->join('users','users.id', '=', 'recommandations.boutiquier_id')
        ->leftjoin('quartiers','quartiers.id', '=', 'users.quartier_id')
        ->paginate(5);

        return view('commande.recommander', compact('recommandations'));
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function status_commande($id)
    {
        //    

        $product = Order_Product::findOrFail($id);
        $product->status = 1; //Approved
        $product->save();
        return redirect()->back(); 
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function status_freelance(Request $request,$id)
    {
        //    

        $product = Order_Product_Freelance::findOrFail($id);
        $product->livreur_id = $request->get('livreur_id'); //Approved
        $product->save();
        return redirect()->back(); 
    }

     /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function status_commercial(Request $request,$id)
    {
        //    

        $product = Order_Product_Freelance::findOrFail($id);
        $product->livreur_id = $request->get('livreur_id'); //Approved
        $product->save();
        return redirect()->back(); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function status_boutiquier(Request $request,$id)
    {
        //    

        $product = Order_Product::findOrFail($id);
        $product->livreur_id = $request->get('livreur_id'); //Approved
        $product->save();
        return redirect()->back(); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function status_commande_a($id)
    {
        //

        $product = Order_Product::findOrFail($id);
        $product->status = 0; //Approved
        $product->save();
        return redirect()->back(); 
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }
  
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /* $products = $request->get('product');
        $quantites = $request->get('qty');

        $etat_id = Etat::where('libelle', 'cours')->get()->first()->id;

        $total = Order::quickRandom(14);

        $commande = Order::create(['user_id' => Auth::user()->id, 'total' => $total]);

        for($i=0; $i < count($products); $i++)
        {
            $catalogueCommande = new CatalogueCommande();

            $catalogueCommande->order_id = $commande->id;
            $catalogueCommande->product_id = (int)$products[$i];
            $catalogueCommande->qty = (int)$quantites[$i];
            $catalogueCommande->save();

        }
        return 'success'; */
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function thankyou()
    {
        
        $payment = DB::table('order_product')->Select('orders.total')
         ->join('orders', 'orders.id', 'order_product.order_id')
         ->orderBy('order_product.id','DESC')->get();
        $dateh = date('c');
        $identifiant = md5(2303388413);
        $ref_commande = 'OM201503WEBP001';
        $site = md5(1431331287);
        $total = str_replace(",","","25,000");
        $commande = "Paiement Orange Money ";
        $algo = "SHA512";  
        $cle_secret = env('OrangeMoney_API_KEY');
        $cle_bin = pack("H*", $cle_secret);
        $message = "S2M_COMMANDE=$commande"."&S2M_DATEH=$dateh"."&S2M_HTYPE=$algo".
        "&S2M_IDENTIFIANT=$identifiant"."&S2M_REF_COMMANDE=$ref_commande"."&S2M_SITE=$site"."&S2M_TOTAL=$total";
        $hmac = strtoupper(hash_hmac(strtolower($algo), $message, $cle_bin));
       
        $commandes = Order::latest()->where('user_id', Auth::user()->id)->orderBY('created_at','DESC')->paginate(1);
        $livraison = dAddress::all();
        
        //var_dump($total);
        //$total->save();
        //echo $total;
       return view('Merci.thankyou')->with([
              'identifiant' => $identifiant,
               'dateh' => $dateh,
               'ref_commande' => $ref_commande,'total' => $total,'site' => $site,
               'commande' => $commande,'algo' => $algo,'hmac' => $hmac,'message' => $message, 
               'commandes' => $commandes,
               'livraison' => $livraison,
               
             ]);
             
    }

    public function thankyouOM(Request $request,$id)
    {
        //
        //$reduction = 500;
        $dateh = date('c');
        $identifiant = md5(2303388413);
        $site = md5(1431331287);
        $ref_commande = 'OMJALOSAS201903WEBP001';
        $total =  Order::find($id);
        $montant = str_replace(",","",$total['total']);
        //$somme = $montant - $reduction;
        $commande = "JALOSAS Paiement Orange Money ";
        $algo = "SHA512";
        $cle_secret = env('OrangeMoney_API_KEY');
        $cle_bin = pack("H*", $cle_secret);
        $message = "S2M_COMMANDE=$commande"."&S2M_DATEH=$dateh"."&S2M_HTYPE=$algo".
        "&S2M_IDENTIFIANT=$identifiant"."&S2M_REF_COMMANDE=$ref_commande"."&S2M_SITE=$site"."&S2M_TOTAL=$montant";
        $hmac = strtoupper(hash_hmac(strtolower($algo), $message, $cle_bin));

        $commandes = Order::latest()->where('id', $id)->orderBY('created_at','DESC')->paginate(1);
       // $commandes = Order::latest()->where('user_id', Auth::user()->id)->orderBY('created_at','DESC')->paginate(1);
        $livraison = dAddress::all();

        //ser = User::where('id', '=', $total->user_id)->paginate(1);
        $u = User::find($total->user_id);
              //echo $message;
        return view('Merci.om')->with([
              'identifiant' => $identifiant,
               'dateh' => $dateh,
               'montant' => $montant,  
               //'somme' => $somme,
               'ref_commande' => $ref_commande,'total' => $total,'site' => $site,
               'commande' => $commande,'algo' => $algo,'hmac' => $hmac,'message' => $message,
               'commandes' => $commandes,
               'livraison' => $livraison,
               'user' => $u,

             ]);
    }



    public function retour_gestion(){

        $paiement = array(
            'ALGO' => 'SHA256',
            'CMD' => 'Sample transaction',
            'HMAC' => 'EC158B8193FFF7E01C768FE320DDBE234234C9D0',
            'ID' => '2570239429.1012158196.92347',
            'MONTANT' => '8500',
            'REF_CMD' => 'SAMPLE_REF_CMD',
            'STATUT' => '200',
            'TRX_ID' => 'MP173453.3454.A12687',
            'UID'
            => '5894f79a37e6d'
            );
            $data = $paiement;
            $secret_key = env('OrangeMoney_API_KEY');//'05E964F202FD9D53DC51F2760C03661JDL93SD0DCCD1A94055639D8545E53D63';
            $bin_key = pack("H*", $secret_key);
            ksort($data);
            $message = urldecode(http_build_query($data));
            $hmac = strtoupper(hash_hmac(strtolower($data['ALGO']), $message, $bin_key));
            if ($hmac === $paiement['HMAC'])
            echo 'Bingo! Valid Data';
            // Process data
            else
            die('Suspicious data!');
    }

    public function annuler() 
    {
        //
        $commandes = Order::where('user_id', Auth::user()->id)->orderBY('created_at','DESC')->paginate(1);
        return view('Merci.annuler', compact('commandes'));
    }

    public function supprimer($id)
    {
        //
        $commande = Order::find($id);
        $commande->delete();

        return redirect('/annuler');
    }

    public function ma_commande()
    {
        $commandes = DB::table('order_product')->
        Select('products.name as name','products.id as id',
         'products.image','products.price', 'products.description', 'orders.total as total', 'orders.created_at', 'order_product.qty')
          ->leftJoin('products', 'products.id', 'order_product.product_id')
          ->leftJoin('orders', 'orders.id', 'order_product.order_id')
          ->orderBy('order_product.id','DESC')
          ->paginate(4);
          return view('admin.ma_commande', compact('commandes'));

    }

    public function mes_commandes()
    {
        $commandes = DB::table('order_product_freelance')->
        Select('products.name as name','products.id as id',
         'products.image','products.price', 'products.description', 'order_freelance.total as total', 'order_freelance.created_at', 'order_product_freelance.qty')
          ->leftJoin('products', 'products.id', 'order_product_freelance.product_id')
          ->leftJoin('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
          ->orderBy('order_product_freelance.id','DESC')
          ->paginate(4);
          return view('admin.mes_commandes', compact('commandes'));

    }
}
