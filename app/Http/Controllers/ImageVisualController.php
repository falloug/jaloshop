<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\visual;
use DB;


class ImageVisualController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        //$visual = visual::find($id);
        $visual = DB::table('visuals')->where('id',$id)->first();
        return view('visual.editer_image_visual', compact('visual'))->with('id',$id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $data = ['nom'=>$request->nom,
                 'image'=>$request->image,
    ];
    $visual = DB::table('visuals')->where('id',$request->id)->update($data);
    return redirect('lister_visual');
       /*  $this->validate( $request, [
            'nom'=>'required',
            'image'=>'image|mimes:png,jpg,jpeg|max:10000',

        ]);
 */
        /* $visual = visual::findOrFail($id);
        $visualUpdate = $request->all();
        $visual->update($visualUpdate); 
 */
       /*  $visual= visual::find($id);
        $visual->nom=$request->get('nom');
        $visual->image=$request->get('image');
       
        $visual->save();  */

        //return redirect('lister_visual');
        //return redirect()->route('lister_visual')->with('success', 'data update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
