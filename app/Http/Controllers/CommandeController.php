<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Commande;
use Auth;

class CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(Commande::with(['product'])->get(),200);
    }


    public function deliverOrder(Commande $commande)
    {
        $commande->is_delivered = true;
        $status = $commande->save();
        
        return response()->json([
            'status'    => $status,
            'data'      => $order,
            'message'   => $status ? 'Commande Delivered!' : 'Error Delivering Commande'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $commande = Commande::create([
            'product_id' => $request->product,
            'user_id' => Auth::id(),
            'quantity' => $request->quantity,
            'address' => $request->address
        ]);
        
        return response()->json([
            'status' => (bool) $commande,
            'data'   => $commande,
            'message' => $commande ? 'Commande Created!' : 'Error Creating Commande'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Commande $commande)
    {
        return response()->json($commande,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commande $commande)
    {
        $status = $commande->update(
            $request->only(['quantity'])
        );
        
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Commande Updated!' : 'Error Updating Commande'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commande $commande)
    {
        $status = $commande->delete();
        
        return response()->json([
            'status' => $status,
            'message' => $status ? 'Commande Deleted!' : 'Error Deleting Commande'
        ]);
    }
}
