<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Role;
use App\Boutique;
use App\Boutiquier_Vente;
use App\Quartier;
use App\Commercial;
use App\Product;
use DB;
use Cart;
use Auth;
//use Carbon;
use App\User;
class BoutiqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)

    {
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();

        if($request->has('search')){

    		$boutiquiers = Boutique::search($request->get('search'))->with('quartier')->paginate(9);	

        }
        else{
    		$boutiquiers = Boutique::paginate(9);
        }        
        return view('version3.layouts.boutique', compact('boutiquiers', 'categories', 'cartItems'));
    }

     public function listerBoutiquier(){

        $boutiquiers = Boutique::latest()->paginate(4);
        return view('boutiquier.lister', compact('boutiquiers'));
    } 

    public function mySearch(Request $request)  
    {
    	if($request->has('search')){

    		$boutiquiers = Boutique::search($request->get('search'))->with('boutiquier_ventes')->get();	

        }
        else{
    		$boutiquiers = Boutique::get();
    	}
    	return view('boutiquier.search_boutiquier', compact('boutiquiers'));
    }

    public function lister_commercesup()
    {
        $boutiquiers = Boutique::paginate(4);
        return view('boutiquier.lister_commercesup', compact('boutiquiers'));

    }
    public function ajout_commercesup()
    {
        $quartiers = Quartier::pluck('nom', 'id');
        $commercials = Commercial::pluck('nom', 'phone', 'id');


        return view('boutiquier.ajout_commercesup', compact('quartiers', 'commercials'));

    }

    public function save_commercesup(Request $request)
    {
                // validation

                $this->validate($request,[

                    'nom'=>'required',
                    'adresse'=>'required',
                    'phone'=>'required',
                    
                ]);
                // image upload
                $user = new User;
                $user->name = $request->get('name');
                $user->password = bcrypt($request->get('password'));
                $user->phone = $request->get('phone');
                $user->condiction = $request->get('condiction');
                //$user->role_id = $request->get('role_id');
                $user->quartier_id = $request->get('quartier_id');
                //$user->created_at = Carbon::now();

                if($user->save()){
                    error_log('la création a réussi');
                    $boutiquier->nom = $request->get('name');
                    $boutiquier->phone = $request->get('phone');
                    $boutiquier->adresse = $request->get('adresse');
                    $boutiquier->quartier_id = $request->get('quartier_id');
                    $boutiquier->user_id = $user->id;
                    //$boutiquier->nom = $request->get('name');
                    $boutiquier->save();

                    //Boutique::create($request->all());

                }


                return back();
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quartiers = Quartier::pluck('nom', 'id');
        $commercials = Commercial::pluck('nom','id');
        $users = User::pluck('name','id');
        $roles = Role::pluck('nom','id');


        return view('boutiquier.create', compact('quartiers', 'commercials','roles','users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation

        $this->validate($request,[

            'nom'=>'required',
            'adresse'=>'required',
            'phone'=>'required',
            
        ]);
         $message = "saved";
       // création user;
                    $user = new User;
                    $user->name = $request->get('nom');
                    //$user->password = bcrypt($request->get('password'));
                    $user->email = $request->get('email');
                    $user->phone = $request->get('phone');
                    $user->condiction = $request->get('condiction');
                    $user->role_id = $request->get('role_id');
                    $user->quartier_id = $request->get('quartier_id');
                    $user->password = Hash::make($request->get('password'));
       //$user->created_at = Carbon::now();
       if($user->save()){
                    error_log('la création a réussi');
                    $boutiquier = new Boutique;
                    $boutiquier->nom = $request->get('nom');
                    $boutiquier->phone = $request->get('phone');
                    $boutiquier->adresse = $request->get('adresse');
                    $boutiquier->quartier_id = $request->get('quartier_id');
                    $boutiquier->commercial_id = $request->get('commercial_id');
                   //$boutiquier->user_id = $user->id;
                    $boutiquier->type_boutiquier = $request->get('type_boutiquier');
                   //$boutiquier->save();
                    $boutiquier->user_id = $user->id;
                    //$agents->save();
                    if($boutiquier->save())  
                    {
                        Auth::login($user);
                        return back();

                    }
                    else
                    {
                        flash('user not saved')->error();

                    }

                    //Boutique::create($request->all());

                }
       //Auth::user()->boutiques()->create($request->all());
       return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //

        $top_promo = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       $top_promos = DB::table('products')->where('status', 'like', 2)->paginate(8);
       $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        //views($top_promo)->record();


        if($request->has('search')){

    		$top_promos = Product::search($request->get('search'))->with('category')->where('status', 'like', 2)->paginate(12);	

        }
        else{
            $top_promos = DB::table('products')->where('status', 'like', 2)->paginate(12);
        }
       return view('version3.layouts.promo_details', compact('top_promos', 'top_promo', 'categories', 'cartItems'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $boutiques = Boutique::find($id);
        $quartiers = Quartier::pluck('nom', 'id');
        $commercials = Commercial::pluck('nom', 'phone', 'id');

        return view('boutiquier.editer', compact('boutiques', 'quartiers', 'commercials'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate( $request, [
            'nom'=>'Required',
            'adresse'=>'Required',
            'phone'=>'Required',
            

        ]);

        $boutique = Boutique::find($id);
        $boutiqueUpdate = $request->all();
        $boutique->update($boutiqueUpdate);

        return redirect('listerBoutiquier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $boutique = Boutique::find($id);
        $boutique->delete();

        return redirect('listerBoutiquier');
    }
}
