<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Support\Facades\Auth;
use App\Quartier;
use App\Order; 
use App\Product;
use App\dAddress;
use App\Zone;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Content;
use Mail;
use App\Mail\SendMail;
use Session;

class CheckoutController extends Controller
{  
    
  

public function index(){ 

  $quartiers = Quartier::get(['nom','id']);
  $zones = Zone::with('quartiers')->orderBy('nom', 'asc')->get();
 /*  $dateh = date('c');
  $identifiant = md5(2303388413);
  $ref_commande = 'OM201503WEBP001';
  $site = md5(1431331287);
  $total = 1250;
  $commande = "Paiement Orange Money ";
  $algo = "SHA512";  
  $cle_secret = env('OrangeMoney_API_KEY');
  $cle_bin = pack("H*", $cle_secret);
  $message = "S2M_COMMANDE=$commande"."&S2M_DATEH=$dateh"."&S2M_HTYPE=$algo".
  "&S2M_IDENTIFIANT=$identifiant"."&S2M_REF_COMMANDE=$ref_commande"."&S2M_SITE=$site"."&S2M_TOTAL=$total";
  $hmac = strtoupper(hash_hmac(strtolower($algo), $message, $cle_bin)); */

 
  return view('front.checkout')->with([
   /*  'identifiant' => $identifiant,
      'dateh' => $dateh,
      'ref_commande' => $ref_commande,'total' => $total,'site' => $site,
      'commande' => $commande,'algo' => $algo,'hmac' => $hmac,'message' => $message, */  
      'data' => Cart::content(),
      'quartiers' => $quartiers,
      'zones' => $zones,
      
    ]);
} 

public function showQuartier(Request $request,$id){

  $frais = Quartier::find($id);
  $quartiers = Quartier::all();
 /*  $dateh = date('c');
  $identifiant = md5(2303388413);
  $ref_commande = 'OM201503WEBP001';
  $site = md5(1431331287);
  $total = 1250;
  $commande = "Paiement Orange Money ";
  $algo = "SHA512";  
  $cle_secret = env('OrangeMoney_API_KEY');
  $cle_bin = pack("H*", $cle_secret);
  $message = "S2M_COMMANDE=$commande"."&S2M_DATEH=$dateh"."&S2M_HTYPE=$algo".
  "&S2M_IDENTIFIANT=$identifiant"."&S2M_REF_COMMANDE=$ref_commande"."&S2M_SITE=$site"."&S2M_TOTAL=$total";
  $hmac = strtoupper(hash_hmac(strtolower($algo), $message, $cle_bin)); */

   return view('front.checkout', compact('frais'))
   ->with([
   /*  'identifiant' => $identifiant,  
    'dateh' => $dateh,
    'ref_commande' => $ref_commande,'total' => $total,'site' => $site,
    'commande' => $commande,'algo' => $algo,'hmac' => $hmac,'message' => $message,   */
    'data' => Cart::content(),
    'frais' => $frais,
    'quartiers' => $quartiers,
    
    ]) ;
}
public function placeOrder(Request $request){

  $this->validate($request, [
      'fullname' => 'required',
      'phone' => 'required|numeric',
      'adresse' => 'required|min:2|max:25',
      'livraison' => 'required',
      'quartier_id' => 'required',
      //'frais' => 'required',

      ]);  

  $address = new dAddress;
 
  $address->userid = Auth::user()->id;
  $address->fullname = $request->fullname;  
  $address->phone = $request->phone;
  $address->adresse = $request->adresse;
  $address->livraison = $request->livraison;
  $address->quartier = $request->quartier;
  $address->quartier_id = $request->has('quartier_id');
  $address->save();

        $name = $request->name;
       // $email = $request->email;
        $subject = $request->subject;
        $message = $request->message;
        $email = ['fallougueye197@gmail.com', 'fallou.g@jaloshops.com','axel.n@jaloshops.com','amary.d@jaloshops.com','oumar.c@jaloshops.com' ];
        Mail::to($email)->send(new SendMail($subject, $message));
        Session::flash("success");
  //$orders = new Order;
  //$orders->frais = $request->frais;


  Order::createOrder();

  Cart::destroy();
  return redirect('thankyou');
}

public function shipping(){
    $quartiers = Quartier::pluck('nom', 'id');
    $quartiers = Quartier::all();

    return view('connexion.shipping-info', compact('quartiers'));
}

public function commande(){

    return view('cart.payment');
}

public function payment(){

        $cartItems = Cart::content();
        return view('cart.ok', compact('cartItems'));
}

public function storePayment(Request $request){
   

    $token = $request->stripeToken;

    try {
        $charge = \Stripe\Charge::create(array(
            "amount" => Cart::total()*100, // Amount in cents
            "currency" => "usd",
            "source" => $token,
            "description" => "Example charge"
        ));
    } catch (\Stripe\Error\Card $e) {

        // The Card has been declined
    }
    //create order

    Order::createOrder();

        // redirect user to some page
        return "Order Completed";
    

    
}


public function checkCoupon(Request $res)
    {
        $code = $res->code;

        $check = DB::table('coupons')
        ->where('coupon_code',$code)
        ->get();
        if(count($check)=="1"){
            //ok
            $user_id = Auth::user()->id;
            $check_used = DB::table('used_coupons')
            ->where('user_id',$user_id)
            ->where('coupon_id',$check[0]->id)
            ->count();
           if($check_used=="0"){
                //insert used one
            $used_add = DB::table('used_coupons')
            ->insert([
                    'coupon_id' => $check[0]->id,
                    'user_id' => $user_id
            ]);
            $insert_cart_total = DB::table('cart_total')
            ->insert([
                    'cart_totol' => Cart::total(),
                    'discount' => $check[0]->discount,
                    'user_id' => $user_id,
                    'gtotal' =>  Cart::total() - (Cart::total() * $check[0]->discount)/100,
            ]);
            $disnew = $check[0]->discount;
            $gtnew = Cart::total() - (Cart::total() * $check[0]->discount)/100;
            
            //echo "applied";

            ?>
 <div class="cart-total" >
                                <h4>Total Amount</h4>
                                <table>
                                  <tbody>
                                    <tr>
                                      <td>Sub Total</td>
                                      <td>$ <?php echo Cart::subtotal(); ?></td>
                                    </tr>
                                    <tr>
                                      <td>Tax (%)</td>
                                      <td>$ <?php echo Cart::tax(); ?></td>
                                    </tr>
                                   
                                  
                                    <tr>
                                      <td>Grand Total </td>
                                      <td>$ <?php echo Cart::total(); ?></td>
                                    </tr>

                                    <tr><td colspan="2"><hr></td></tr>
                                    <tr>
                                      <td>Discount(%) </td>
                                      <td> <?php echo $disnew; ?></td>
                                    </tr>

                                     <tr>
                                      <td>Grand Total (After discount) </td>
                                      <td>$ <?php echo $gtnew; ?></td>
                                    </tr>
                                  </tbody>
                                </table>
                           <input type="submit" class="btn update btn-block" value="Continue Shopping">
                         <a href="{{url('checkout')}}" class="btn check_out btn-block">checkout</a>
                                </div>

            <?php 
           }
           else{?>

               <div class="alert alert-warning">you already used this coupon</div>
               <div class="cart-total" >
                                <h4>Total Amount</h4>
                                <table>
                                  <tbody>
                                    <tr>
                                      <td>Sub Total</td>
                                      <td>$ <?php echo Cart::subtotal(); ?></td>
                                    </tr>
                                    <tr>
                                      <td>Tax (%)</td>
                                      <td>$ <?php echo  Cart::tax(); ?></td>
                                    </tr>
                                   
                                  
                                    <tr>
                                      <td>Grand Total</td>
                                      <td>$ <?php echo Cart::total(); ?></td>
                                    </tr>
                                  </tbody>
                                </table>
                           <input type="submit" class="btn update btn-block" value="Continue Shopping">
                         <a href="{{url('checkout')}}" class="btn check_out btn-block">checkout</a>
                                </div>
           <?php }
        }else{
            //echo "wrong coupon"; 
            ?>
            <div class="alert alert-danger">Wrong Coupon code you entered</div>

            <div class="cart-total" >
            <h4>Total Amount</h4>
            <table>
              <tbody>
                <tr>
                  <td>Sub Total</td>
                  <td>$ <?php echo Cart::subtotal(); ?></td>
                </tr>
                <tr>
                  <td>Tax (%)</td>
                  <td>$ <?php echo  Cart::tax(); ?></td>
                </tr>
               
              
                <tr>
                  <td>Grand Total</td>
                  <td>$ <?php echo Cart::total(); ?></td>
                </tr>
              </tbody>
            </table>
       <input type="submit" class="btn update btn-block" value="Continue Shopping">
     <a href="{{url('checkout')}}" class="btn check_out btn-block">checkout</a>
            </div>
        <?php }
    }       
}
