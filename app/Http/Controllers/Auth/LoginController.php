<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;

   
class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */  

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request){

        if($request->isMethod('post')){

            $data = $request->input();
            if(Auth::attempt(['phone'=>$data['phone'], 'password'=>$data['password']])){
                return redirect('/checkout');
            }
            
            else{
                
                return redirect('/login')->with('flash_message_error', 'Mot de Passe ou phone Incorrect');  
            }
        }
        //return view('admin.admin_login');
    }
}
