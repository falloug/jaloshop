<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;
use App\Quartier;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
  
    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/checkouts';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {  
        return Validator::make($data, [
            'genre' => 'string|max:255',
            'name' => 'required|string|max:255',
            'tranche_age' => 'string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'phone' => 'required|string|max:255',
            'condiction' => 'required|string|max:255',
            'quartier_id' => 'required|',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *  
     * @param  array  $data
     * @return \App\User
     */
       protected function create(array $data)
    {
        return User::create([
            'genre' => $data['genre'],
            'name' => $data['name'],
            'tranche_age' => $data['tranche_age'],
            'email' => $data['email'],
            'phone' => $data['phone'],
            'quartier_id' => $data['quartier_id'],
            'condiction' => $data['condiction'],
            'password' => Hash::make($data['password']),
        ]);
    }
    
}
