<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Product;

class DetailsController extends Controller
{
    //

    public function details($id)
    {
      $produit_jour = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       $produit_jours = DB::table('products')->where('status', 'like', 0)->paginate(3);

       return view('version3.layouts.produits_details', compact('produit_jours', 'produit_jour'));

    }
    

    public function promo($id)
    {
       $top_promo = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       $top_promos = DB::table('products')->where('promo_prix', 'like', 25)->paginate(6);
       return view('version3.layouts.promo_details', compact('top_promos', 'top_promo'));

    }

    public function vente($id)
    {
       $product = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       $commande = DB::table('order_product')->
        Select('products.name as name','products.id as id',
         'products.image','products.price', 'products.description')
          ->leftJoin('products', 'products.id', 'order_product.product_id')
          ->orderBy('order_product.id','DESC')
          ->paginate(3);
       return view('version3.layouts.produits_details2', compact('product'));

    }
}
