<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Category;
use App\Product;
use Cart;

class TestController extends Controller
{
    //

    public function proCat(Request $request){
        $cat = $request->cat;
  
        $data= DB::table('categories')
        ->join('products','cats.id','products.category_id')
        ->where('categories.name',$cat)->get();
        return view('front.products',[
          'data' => $data, 'catByUser' => $cat
        ]);
      }
   
      public function test_productsCat(Request $request){
         $category_id = $request->category;
  
         $priceCount = count([$request->price]);
         $cartItems = Cart::content();
         // price are array
        /* if($category_id!="" && $priceCount!="0"){
          $price = explode("-",$request->price);
  
            $start = $price[0];
            $end = $price[1];
       
            echo "both are selected";
            $data = DB::table('products')
            ->join('categories','categories.id','products.category_id')
            ->where('products.category_id',$category_id)
            ->where('products.price', ">=", $start)
            ->where('products.price', "<=", $end)
            ->get();
  
        } */
         /* else if($priceCount!="0"){
           $price = explode("-",$request->price);
           $start = $price[0];
           $end = $price[0];
  
           //echo "price is selected";
           $data = DB::table('products')
           ->join('categories','categories.id','products.category_id')
           ->where('products.price', ">=", $start)
           ->where('products.price', "<=", $end)
           ->get();
  
         } */
         /* else if($category_id!=""){ */
           //echo "cat is selected";
           //$data = DB::table('products')
           $data = Product::with('category')
           /* ->join('categories','categories.id','products.category_id')
           ->where('products.category_id',$category_id) */
           ->get();
/*          }
         else{
           //echo "nothing is slected";
           return "<h1 align='center'>Please select atleast one filter from dropdown</h1>";
  
         }
  
         if(count($data)=="0"){
           echo "<h1 align='center'>no products found under this Category</h1>";
         }else{  */
          //dd($data);
          return view('tester.test',[
           'data' => $data, 'cartItems' => $cartItems  /* 'catByUser' => $data[0]->name */
         ]); 
      //}
  
      }
  
  
      public function search(Request $request){
        $searchData= $request->searchData;
  
        //start query for search
        $data = DB::table('products')
        ->join('categories','categories.id','products.category_id')
        ->where('products.name', 'like', '%' . $searchData . '%')
        ->get();
        return view('version3.layouts.index',[
          'data' => $data, 'catByUser' => $searchData
        ]);
      }
  
      public function details($id){
        $data = products::find($id);
      if(count($data)!=0){
        return view('front.details',[
         'data' => $data
        ]);
      }
      
      else{
        return view('front.products',[
          'data' => products::all(),
          'catByUser' => 'Product not found',
        ]);
      }
      
      }
}
