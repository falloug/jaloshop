<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Category;
use App\Order;
use App\Order_Product;
use DB;
use App\Boutique;
use Cart;
use App\Sub_Cat;
use Newsletter;   
use App\News;
use Auth;
use App\visual;

class TopController extends Controller
{
    //

    public function actualite()
    {
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        return view('version3.layouts.actualite', compact('categories', 'cartItems'));

    }

    public function mentions()
    {
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        return view('version3.layouts.mentions', compact('categories', 'cartItems'));

    }

    public function top_vente(Request $request){
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        if($request->has('search')){

    		$commandes = Product::search($request->get('search'))->with('category')->paginate(12);	

        }
        else {
            $commandes = Product::latest()->where('status', 'like', 3)->paginate(12);
               
        } 
        return view('version3.layouts.top_ventes', compact('commandes', 'categories', 'cartItems'));

    }

  
    public function made_senegal(Request $request){
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        if($request->has('search')){

    		$made_sénégal = Product::search($request->get('search'))->with('category')->paginate(12);	

        }
        else {
            $made_sénégal = Product::latest()->where('status', 'like', 5)->paginate(12);
               
        } 
        return view('version3.layouts.made_sénégal', compact('made_sénégal', 'categories', 'cartItems'));

    }

    public function produits(){

        $produits = Product::latest()->paginate(12);
        return view('version3.layouts.produit', compact('produits'));
    }

    public function top_promo(Request $request){
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        //$promos = DB::table('products')->where('promo_prix', 'like', 25)->paginate(8);
        if($request->has('search')){

    		$promos = Product::search($request->get('search'))->with('category')->where('status', 'like', 2)->paginate(12);	

        }
        else {
            $promos = Product::latest()->where('status', 'like', 2)->paginate(12);
               
        } 
        return view('version3.layouts.top_promos', compact('promos', 'categories', 'cartItems'));

    }

    public function produit_jour(){

        $produit_jours = Product::latest()->where('status', 'like', 0)->get();
        return view('version3.produit.produit', compact('produit_jours'));

    }


    public function mySearchss(Request $request)
    {
        $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        $products = Product::all();
    	if($request->has('search')){

    		$produit_jours = Product::search($request->get('search'))->with('category')->get();	

        }   
        else{
    		$produit_jours = Product::latest()->get();
    	}
    	return view('version3.layoutsss.index', compact('produit_jours', 'categories', 'cartItems', 'products'));
    }


    public function store(Request $request)
        {
            if ( ! Newsletter::isSubscribed($request->email) ) 
            {
                Newsletter::subscribePending($request->email);
                return redirect('/')->with('success', 'Merci pour votre inscription');
            }
            return redirect('/')->with('failure', 'Désolé! vous etes déja connecté! merci  ');
                
        }

    public function jalo(Request $request){
  
        
        if($request->has('search')){

    		$produit_jours = Product::search($request->get('search'))->with('category')->where('status', 'like', 0)->paginate(3);	

        }
        else {
            $produit_jours = Product::latest()->where('status', 'like', 0)->paginate(3);
               
        } 

        if($request->has('search')){

    		$promo = Product::search($request->get('search'))->with('category')->where('status', 'like', 2)->paginate(6);	

        }
       /* else if($request->has('search')){

    		$promo = Product::search($request->get('search'))->with('category')->where('promo_prix', '!=', 0)->paginate(4);	

        }  */
        else {
            $promo = Product::latest()->where('status', 'like', 2)->paginate(6);
               
        } 


        //$produit_jours = DB::table('products')->where('status', 'like', 0)->paginate(4);
        $boutiquiers = Boutique::paginate(6);
        //$promo = DB::table('products')->paginate(4)->where('promo_prix', 'like', 25);y
        if($request->has('search')){

    		$commandes = Product::search($request->get('search'))->with('category')->where('status', 'like', 3)->paginate(6);	

        }
        else {
            $commandes =  Product::latest()->where('status', 'like', 3)->paginate(6);
               
        } 

        if($request->has('search')){

    		$made_sénégal = Product::search($request->get('search'))->with('category')->where('status', 'like', 5)->paginate(6);	

        }
        else {
            $made_sénégal = Product::latest()->where('status', 'like', 5)->paginate(6);
               
        } 
       
        
     
        
        $cartItems = Cart::content();
        $products = Product::all()->where('status', 'like', 1);

        $visuals = visual::latest()->where('status', 'like', 0)->paginate(10);
        $categories = DB::table('categories')->get();
        $sub_cats = Sub_Cat::with('category')->get();
        $categorie =  $request->category;
        $produits =  DB::table('products')->join('categories', 'categories.id', 'products.category_id')
        ->where('categories.name',$categorie)->get();

        /* $category_id = $request->category_id;

        $produit_jours = DB::table('products')
            ->join('categories','categories.id','products.category_id')
            ->where('products.category_id',$category_id)->get();
 */
//$categoriesss = DB::table('categories')->where('product_id',1)->get();
  
        return view('version3.layouts.jalomarket' , compact('produit_jours',
        'produit_jours',
         'boutiquiers',
          'promo',
           'commandes',
            'visuals',
             'categories',
             'sub_cats',
             'produits',
             //'categoriesss',
             'products',
            'cartItems',
            'made_sénégal'
      ));
    }

    public function details_vente(Request $request,$id)
    {
        $vente = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       if($request->has('search')){

        $commandes = Product::search($request->get('search'))->with('category')->paginate(6);	

    }
    else {
        $commandes = DB::table('products')->where('status', 'like', 3)->paginate(6);
           
    } 
         $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
      return view('version3.layouts.vente_details', compact('commandes',
       'vente', 'categories', 'cartItems'
              ));

              /* if($request->has('search')){

                $commandes = Product::search($request->get('search'))->with('category')->paginate(3);	
    
            }
            else{
                $commandes = Product::paginate(3);
            } */
    }

    public function details_made(Request $request,$id)
    {
        $produit = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       if($request->has('search')){

        $made_sénégal = Product::search($request->get('search'))->with('category')->paginate(6);	

    }
    else {
        $made_sénégal = DB::table('products')->where('status', 'like', 5)->paginate(6);
           
    } 
         $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
      return view('version3.layouts.details_made', compact('made_sénégal',
       'produit', 'categories', 'cartItems'
              ));

              /* if($request->has('search')){

                $commandes = Product::search($request->get('search'))->with('category')->paginate(3);	
    
            }
            else{
                $commandes = Product::paginate(3);
            } */
    }

    public function ajout_news(){

        //categories = Category::pluck('name', 'id');
        return view('version3.layouts.index');

    }

    public function save_news(Request $request)
    
    {

        $this->validate($request,[

            'name'=>'required',
        ]);
        $auth = News::where('name', $request->get('name'))->get()->first();

        if($auth != null)
        {

            //$message = 'cet utiliseur existe déja';
            return back()->with('success', 'Cet utiliseur existe déja');
        }  
        else{
            //$message = 'Merci Votre inscription a bien été enregistré';
            News::create($request->all());

            return back()->with('success', 'Merci Votre inscription a bien été enregistré');
        };
         //News::create($request->all());
         //return back();
    }

    
}
