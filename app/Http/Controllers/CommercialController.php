<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Notification;
use Illuminate\Foundation\Auth\RegistersUsers;
use App\Role;
use Cart;
use Auth;
//use Carbon;    
use App\User;
use App\Commercial;
use App\Quartier;
use Illuminate\Support\Facades\DB;

class CommercialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commercials = Commercial::paginate(4);
        return view('commercial.lister', compact('commercials'));
    }

    public function listerCommercial(){

        $commercials = Commercial::paginate(4);
        return view('commercial.lister', compact('commercials'));
    }

    public function lister_commercesup()
    {
        $commercials = Commercial::paginate(4);
        return view('commercial.lister_commercesup', compact('commercials'));

    }

    public function ajout_commercesup()
    {
        $quartiers = Quartier::pluck('nom', 'id');

        return view('commercial.ajout_commercesup', compact('quartiers'));

    }

    public function save_commercesup(Request $request)
    {
       
        $this->validate($request,[
            'nom'=>'required',
            'phone'=>'Required',
            'adresse'=>'Required',
        ]);
         Commercial::create($request->all());
         return back();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quartiers = Quartier::pluck('nom', 'id');
        $roles = Role::pluck('nom','id');

        return view('commercial.ajout_commercial', compact('quartiers','roles'));

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

$this->validate($request,[   

            'nom'=>'required',
            'adresse'=>'required',
            'phone'=>'required',
            
        ]);  
        $message = "saved";
       // création user;  
                    $user = new User;
                    $user->name = $request->get('nom');
                    //$user->password = bcrypt($request->get('password'));
                    $user->email = $request->get('email');
                    $user->phone = $request->get('phone');
                    $user->condiction = $request->get('condiction');
                    $user->role_id = $request->get('role_id');
                    $user->quartier_id = $request->get('quartier_id');
                    $user->password = Hash::make($request->get('password'));
       if($user->save()){  
                    error_log('la création a réussi');
                    $commercial = new Commercial;
                    $commercial->nom = $request->get('nom');
                    $commercial->phone = $request->get('phone');
                    $commercial->adresse = $request->get('adresse');
                    $commercial->quartier_id = $request->get('quartier_id');
                    $commercial->user_id = $user->id;
                    if($commercial->save())  
                    {
                        Auth::login($user);
                        return back();

                    }
                    else
                    {
                        flash('user not saved')->error();

                    }
                
                }
       return back();
       /* $this->validate($request,[
            'nom'=>'required',
            'phone'=>'Required',
            'adresse'=>'Required',
        ]);
         Commercial::create($request->all());
         return back();*/
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $commercials = Commercial::find($id);
        $quartiers = Quartier::pluck('nom', 'id');

        return view('commercial.editer', compact('commercials', 'quartiers'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate( $request, [
            'nom'=>'Required',
            'phone'=>'Required',
            'adresse'=>'Required',

            
            

        ]);

        $commercial = Commercial::find($id);
        $commercialUpdate = $request->all();
        $commercial->update($commercialUpdate);

        return redirect('listerCommercial');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $commercial = Commercial::find($id);
        $commercial->delete();

        return redirect('listerCommercial');
    }
}
