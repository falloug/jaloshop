<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Fournisseur_Vente;
use App\Fournisseur;
use Gloudemans\Shoppingcart\Facades\Cart;

class Fourni_VenteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function mySearch(Request $request)
    {
    	if($request->has('search')){

    		$fournisseurs = Fournisseur_Vente::search($request->get('search'))->with('fournisseur')->get();	

        }
        else{
    		$fournisseurs = Fournisseur_Vente::get();
    	}
    	return view('fourni_ventes.search_fournisseur', compact('fournisseurs', 'boutiques'));
    }

    public function listerFourni_Vente(){

        $fournisseurs = Fournisseur_Vente::paginate(4);
        $fournisseur = Fournisseur_Vente::get(array('total', 'date_vente'));
        
        return view('fourni_ventes.lister', compact('fournisseurs', 'fournisseur'));
    } 

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $fournisseur_vente = Fournisseur::pluck('nom', 'id');
        return view('fourni_ventes.ajout_fournisseur_vente', compact('fournisseur_vente'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // validation

        $this->validate($request,[

            'nomProduit'=>'required',
            'description'=>'required',
            'date_vente'=>'required',
            'price'=>'required',
            'quantity'=>'required',
            'total'=>'required',
            
            
        ]);
       // image upload
       
       Fournisseur_Vente::create($request->all());

       return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $fournisseur = Fournisseur_Vente::find($id);
        $fournisseur->delete();

        return redirect('fournisseur_vente');
    }
}
