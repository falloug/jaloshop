<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Address;
use App\dAddress;
use App\Quartier;
use App\User;
use Illuminate\Support\Facades\Auth;
use DB;
use App\Product;
use Cart;



class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response   
     */
    public function index()
    {
        //
    }

    public function Addresse_livraison(){
        $addresse_livraisons = DB::table('dAddress')
       ->Select('users.name as username','users.id as userId','users.phone as telephone',
       'quartiers.nom as nom', 'quartiers.id as quartierId',
       'dAddress.id','dAddress.adresse','dAddress.livraison','dAddress.created_at')
        ->leftJoin('users', 'users.id', 'dAddress.user_id')
        ->leftJoin('quartiers', 'quartiers.id', 'dAddress.quartier_id')
        ->orderBy('dAddress.id','DESC')
        ->paginate(10);
        return view('admin.commandes_pro',compact('addresse_livraisons'));
      }

    public function listerCommande(){

        $commandes = Address::all();
        $commandes = Address::paginate(4);

        $users = User::all();
        $quartiers = Quartier::all();
        return view('addresses.lister', compact('commandes', 'users', 'quartier', 'commandes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $quartiers = Quartier::pluck('nom', 'id');
        return view('connexion.shipping-info', compact('quartiers'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [

            'adresse' => 'required',
            'phone' => 'required|integer|nullable',
            'livraison' => 'required',
            'name' => 'required|nullable',
            'quartier_id' => 'required',

        ]);
        Auth::user()->address()->create($request->all());
        return redirect()->route('checkout.payment');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //
        $vente = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       /* $commandes = DB::table('order_product')->
       Select('products.name as name','products.id as id',
        'products.image','products.price', 'products.description')
         ->leftJoin('products', 'products.id', 'order_product.product_id')
         ->orderBy('order_product.id','DESC')
         ->paginate(6);
         $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
      return view('version3.layouts.vente_details', compact('commandes',
       'vente', 'categories', 'cartItems' */
              //));

             /*  if($request->has('search')){

                $commandes = Product::search($request->get('search'))->with('category')->paginate(3);	
    
            }
            else{
                $commandes = Product::paginate(3);
            } */
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      //

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
