<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Quartier;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Query\Builder\lists;
//session_start();
use Auth;
use Session;
use App\Product;
use Cart;
use App\Zone;

class QuartierController extends Controller
{
    /**
     * Display a listing of the resource.
     * 
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $quartiers = Quartier::paginate(4);
        return view('quartier.lister', compact('quartiers'));
    }

    public function listerQuartier(){

        $quartiers = Quartier::latest()->paginate(4);
        return view('quartier.lister', compact('quartiers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //$zones = DB::table('zones')->get(['nom']);
        $zones = Zone::pluck('nom', 'id');
       return view('quartier.index', compact('zones'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request,[
            'nom'=>'required',
        ]);
         Quartier::create($request->all());
         return back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request,$id)
    {
        //

        $produit_jour = Product::find($id);
       //$images = Product::pluck(array($id,'image'));
       $produit_jours = DB::table('products')->where('status', 'like', 0)->paginate(3);
       $categories = DB::table('categories')->get();
        $cartItems = Cart::content();
        //views($produit_jour)->record();

        if($request->has('search')){

            $produit_jours = Product::search($request->get('search'))->with('category')->paginate(3);	

        }
        else{
            $produit_jours = Product::paginate(3);
        }

       return view('version3.layouts.produits_details', compact('produit_jours', 'produit_jour', 'categories', 'cartItems'));

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $quartier = Quartier::find($id);
        $zones = Zone::pluck('nom', 'id');
        return view('quartier.editer', compact('quartier','zones'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate( $request, [
            'nom'=>'Required',
            
            

        ]);

        $quartier = Quartier::find($id);
        $quartierUpdate = $request->all();
        $quartier->update($quartierUpdate);
  
        return redirect('listerQuartier');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category = Category::find($id);
        $category->delete();

        return redirect('listerCategory');
    }
}
