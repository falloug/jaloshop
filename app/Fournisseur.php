<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;


class Fournisseur extends Model
{  
    use Notifiable;
    use SearchableTrait;
    protected $fillable = ['nom', 'phone', 'adresse', 'quartier_id'];
      

    protected $searchable = [
  
        'columns' => [
            'fournisseurs.nom' => 10,
            'fournisseurs.adresse' => 5,
            'fournisseurs.id' => 3,
            'q.nom' =>2,

        ],
        'joins' => [
            'quartiers as q' => ['q.id', 'fournisseurs.quartier_id'],

            //'boutique' => ['boutiquier_ventes.id','boutique.boutiquier_vente_id'],
            //'boutiquier_ventes' => ['boutiques.id','boutiquier_ventes.boutique_id'],

            //->join('boutiques as b', 'b.id', '=', 'boutiquier_ventes.boutique_id')

        ],
        
       
    ];

    public static function getAllFournisseurs()
    {  
        return self::where('status',true)->get();
    }


    public function products(){
        return $this->hasMany(Product::class);
    }

    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }

    public function type(){
        return $this->belongsTo(type::class);
    }

    public function fournisseur_ventes(){
        return $this->hasMany(Fournisseur_Vente::class);
    }

    public function visuals(){
        return $this->hasMany(visual::class);
    }
}
