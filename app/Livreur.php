<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Livreur extends Model
{
    protected $fillable = [
        'adresse', 'phone', 'nom','prenom','date_debut','nom_societe', 'quartier_id', 'user_id', 'status'
    ];

    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }
}   
