<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Parametre extends Model
{
    //
    protected $fillable = [
        'nom', 'pourcentage'
    ];

    public function freelances(){
        return $this->hasMany(Freelance::class);
    } 
}
