<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_Product_Freelance extends Model
{
    //
    protected $table = 'order_product_freelance';
    protected $fillable =['product_id', 'order_freelance_id', 'qty', 'total','livreur_id','status'];
}
