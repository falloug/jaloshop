<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class type extends Model
{
    //

    public function fournisseurs(){
        return $this->hasMany(Fournisseur::class);
    }

    public function boutiques(){
        return $this->hasMany(Boutique::class);
    }

    public function commercials(){
        return $this->hasMany(Commercial::class);
    }
}
