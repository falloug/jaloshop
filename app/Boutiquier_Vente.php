<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;

class Boutiquier_Vente extends Model
{
    use Notifiable;
    use SearchableTrait;
    public $table = "boutiquier_ventes";
    protected $fillable = ['nomProduit', 'description', 'date_vente', 'prixProduit', 'quantity', 'boutique_id', 'total'];

  
    protected $searchable = [

        'columns' => [
            'boutiquier_ventes.nomProduit' => 10,
            'boutiquier_ventes.description' => 5,
            'boutiquier_ventes.id' => 3,
            'b.nom' => 2,

        ] ,
        'joins' => [
            'boutiques as b' => ['b.id', 'boutiquier_ventes.boutique_id'],

            //'boutique' => ['boutiquier_ventes.id','boutique.boutiquier_vente_id'],
            //'boutiquier_ventes' => ['boutiques.id','boutiquier_ventes.boutique_id'],

            //->join('boutiques as b', 'b.id', '=', 'boutiquier_ventes.boutique_id')

        ],
       
    ];

    
    public function boutique(){
        return $this->belongsTo(Boutique::class);
    }

}  
