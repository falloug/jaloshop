<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class visual extends Model
{
    //
    protected $fillable = ['nom', 'image'];

    public function fournisseur(){
        return $this->belongsTo(Fournisseur::class);
    }
}
  