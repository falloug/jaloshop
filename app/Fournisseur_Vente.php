<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;

class Fournisseur_Vente extends Model
{
    use Notifiable;
    use SearchableTrait;
    public $table = "fournisseur_ventes";
    protected $fillable =['nomProduit', 'description', 'date_vente', 'price', 'quantity', 'fournisseur_id', 'total'];


    protected $searchable = [

        'columns' => [
            'fournisseur_ventes.nomProduit' => 10,
            'fournisseur_ventes.description' => 5,
            'fournisseur_ventes.id' => 3,
            'f.nom' => 2,

        ] ,  
        'joins' => [
            'fournisseurs as f' => ['f.id', 'fournisseur_ventes.fournisseur_id'],

        ],
       
    ];
    public function fournisseur(){
        return $this->belongsTo(Fournisseur::class);
    }

}
