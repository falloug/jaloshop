<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class ProductQueryExport implements FromCollection, WithHeadings
{
    use Exportable;  
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('products')->select('products.name as name', 'products.price',
         'products.prix_achat','fournisseurs.nom as nom'
       )->leftJoin('fournisseurs', 'fournisseurs.id', 'products.fournisseur_id')
       ->orderBy('products.id','DESC')
       ->get();
    }

    public function __construct(int $year)
    {
        $this->year = $year;
    }

    public function query(int $year)
    {
        return Product::query()->select('products.name as name', 'products.price',
        'products.prix_achat','fournisseurs.nom as nom'
      )->leftJoin('fournisseurs', 'fournisseurs.id', 'products.fournisseur_id')
      ->orderBy('products.id','DESC')->whereYear('created_at', $this->year);
    }

    public function headings(): array{
        return[
            'nom du produit',
            'prix de Vente du produit',
            'prix d Achat du produit',
            'fournisseur',
        ];

    }

}
