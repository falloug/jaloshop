<?php

namespace App\Exports;

use App\Product;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class ProductViewsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): view
    {
        return views('produit.lister', [
            'products' => DB::table('products')->select('id',
            'name',
            'price',
            'fournisseur_id',
            'pourcentage_boutiquier',
            'pourcentage_freelance')->get()
        ]);
    }

}
