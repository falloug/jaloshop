<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;


class ProductExport implements FromCollection, WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()  
    {
        return DB::table('products')->select('id',
        'name',
        'price',
        'fournisseur_id',
        'pourcentage_boutiquier',
        'pourcentage_freelance')->get();
    }  

    public function headings(): array{
        return[
            'id',
            'name',
            'price',
            'fournisseur_id',
            'pourcentage_boutiquier',
            'pourcentage_freelance',
        ];

    }
}
