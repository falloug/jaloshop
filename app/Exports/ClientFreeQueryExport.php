<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class ClientFreeQueryExport implements FromCollection, WithHeadings
{
    use Exportable;  
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('order_product_freelance')->select(
        'client_freelances.nom as clientName','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                -> join('products', 'products.id', 'order_product_freelance.product_id')
                -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                -> where('roles.id', '=', 1 )
                ->orderBy('order_freelance.id', 'DESC')
                -> get();  
    }

    public function __construct(int $year)
    {
        $this->year = $year;
    }

    public function query(int $year)
    {
        return DB::table('order_product_freelance')->select(
            'client_freelances.nom as clientName','client_freelances.phone as clientPhone','client_freelances.adresse as ClientAdresse')
                    -> join('products', 'products.id', 'order_product_freelance.product_id')
                    -> join('order_freelance', 'order_freelance.id', 'order_product_freelance.order_freelance_id')
                    -> leftjoin('client_freelances', 'client_freelances.id', '=', 'order_freelance.client_freelance_id')
                    -> leftjoin('users', 'users.id', '=', 'order_freelance.freelance_id')
                    -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                    -> where('roles.id', '=', 1 )
                    ->orderBy('order_freelance.id', 'DESC')
      ->orderBy('order_product_freelance.id','DESC')->whereYear('created_at', $this->year);
    }

    public function headings(): array{
        return[
            'Nom du Client',
            'Phone du Client',
            'Adresse du Client',
        ];

    }

}
