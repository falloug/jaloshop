<?php

namespace App\Exports;

use App\Product;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithHeadings;
use DB;

class ClientBoutQueryExport implements FromCollection, WithHeadings
{
    use Exportable;  
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return DB::table('order_product')->select(
        'dAddress.fullname as clientName', 'dAddress.phone as clientPhone','dAddress.adresse as ClientAdresse')
                -> join('products', 'products.id', 'order_product.product_id')
                -> join('orders', 'orders.id', 'order_product.order_id')
                -> leftjoin('dAddress', 'dAddress.id', '=', 'orders.client_id')
                -> leftjoin('users', 'users.id', '=', 'orders.boutiquier_id')
                -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                -> where('roles.id', '=', 9)
                ->orderBy('orders.id', 'DESC')
                -> get();  
    }

    public function __construct(int $year)
    {
        $this->year = $year;
    }

    public function query(int $year)
    {
        return DB::table('order_product')->select(
            'dAddress.fullname as clientName', 'dAddress.phone as clientPhone','dAddress.adresse as ClientAdresse')
                    -> join('products', 'products.id', 'order_product.product_id')
                    -> join('orders', 'orders.id', 'order_product.order_id')
                    -> leftjoin('dAddress', 'dAddress.id', '=', 'orders.client_id')
                    -> leftjoin('users', 'users.id', '=', 'orders.boutiquier_id')
                    -> leftjoin('roles', 'roles.id', '=', 'users.role_id')
                    -> where('roles.id', '=', 9)
      ->orderBy('order_product.id','DESC')->whereYear('created_at', $this->year);
    }

    public function headings(): array{
        return[
            'Nom du Client',
            'Phone du Client',
            'Adresse du Client',
        ];

    }

}
