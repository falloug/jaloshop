<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    protected $fillable = [
        'adresse', 'phone','livraison', 'name', 'quartier_id'
    ];

    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }
}
