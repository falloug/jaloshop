<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
//use CyrildeWit\EloquentViewable\Viewable;
//use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;

    

class Quartier extends Model //implements ViewableContract
{
    //sprotected $table = 'users';
    //use Viewable;
    protected $fillable =['nom', 'zone_id'];

    public static function getQuartier()
    {
        return self::all();
    }  
   
    public static function getQuartierById($quartier_id)   
    {
      return self::find($quartier_id)->nom;
    }
    
    public function boutiques(){  
        return $this->hasMany(Boutique::class);
    }

    public function orders(){  
        return $this->hasMany(Order::class);
    }

    public function addresses(){
        return $this->hasMany(Address::class);
    }

     public function dAddress(){
        return $this->hasMany(dAddress::class);
    } 

    public function freelances(){
        return $this->hasMany(Freelance::class);
    } 

    public function livreurs(){
        return $this->hasMany(Livreur::class);  
    }

    public function commercials(){
        return $this->hasMany(Commercial::class);
    }

    public function fournisseurs(){
        return $this->hasMany(Fournisseur::class);
    }

    public function users(){
        return $this->hasMany(User::class);
    }

    public function zone(){
        return $this->belongsTo(Zone::class);
    }

}
