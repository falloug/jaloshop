<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Nicolaslopezj\Searchable\SearchableTrait;
//use CyrildeWit\EloquentViewable\Viewable;
//use CyrildeWit\EloquentViewable\Contracts\Viewable as ViewableContract;

class Boutique extends Model //implements ViewableContract
{  
    //use Viewable;
    use Notifiable;
    use SearchableTrait;
    //protected $table = 'boutiques';
    protected $fillable =['nom', 'adresse', 'phone', 'quartier_id', 'commercial_id', 'type_boutiquier','user_id'];
  

    protected $searchable = [

        'columns' => [
            'boutiques.nom' => 10,
            'boutiques.adresse' => 5,
            'boutiques.id' => 3,

        ],
        'joins' => [
            'boutiquier_ventes' => ['boutiques.id','boutiquier_ventes.boutique_id'],
        ],
       
    ];  
    
    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }

    public function user(){
        return $this->belongsTo(User::class);

}

    public function commercial(){
        return $this->belongsTo(Commercial::class);
    }

    public function boutiquier_ventes(){
        return $this->hasMany(Boutiquier_Vente::class);
    }

    public function type(){
        return $this->belongsTo(type::class);
    }
   
}
