<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Cviebrock\EloquentSluggable\Sluggable;
use Tymon\JWTAuth\Exceptions\JWTException;
use JWTAuth;
use Cache;
  
class User extends Authenticatable
{

    use Notifiable;
    use Sluggable;
    //protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array  
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'quartier_id', 'condiction','genre','tranche_age','information
','acheter'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function isOnline(){
        return Cache::has('user-is-online-' . $this->id);
    }

    public function isAdmin(){

        return $this->admin;
    }

    public function address(){

        return $this->hasMany(Address::class);
    }

    public function orders(){  
  
        return $this->hasMany(Order::class);
    }

 public function approvisionnements(){  
        return $this->hasMany(Approvisionnement::class);
    }

    public function commandes(){
        return $this->hasMany(Commande::class);
    }
    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function posts(){

        return $this->hasMany(Posts::class);
    }

    public function boutiques(){

        return $this->hasMany(Boutique::class);
    }

    public function evaluations(){

        return $this->hasMany(Evaluation::class);
    }

    public function quartier(){
        return $this->belongsTo(Quartier::class);
    }


    public static function getUsers($role)
    {
        return DB::table('users')->select('name', 'condiction', 'phone')
            ->join('roles', 'roles.id','=', 'users.role_id')
            ->where('roles.nom', $role)->get();
    }


    public static function authentificate($credentials)
    {
        try
        {

            if (!$token = JWTAuth::attempt($credentials))
            {
                return (['message' =>'invalid_email_or_password', 'status' => 401]);
            }
        } 
        catch (JWTAuthException $e)
        {
            return (['message' =>'failed_to_create_token', 'status' => 401]);
        }
        
        $userAuth = JWTAuth::toUser($token);

        $user = self::getUserDetails($userAuth);

        return ['token' => $token, 'user' => $user];
    }

    public $timestamps = false;

    /* public static function getUsers($role)
    {
        return DB::table('users')->select('name','email','password','phone', 'condiction');
            
    } */

    public function sluggable()
    {
        return [
            'name' => [
                'source' => 'name'
            ]
        ];
    }

    public static function getUserDetails($userAuth)
    {
        $role = Role::getRoleById($userAuth->role_id);

        $user = ['id' => $userAuth->id,'name' => $userAuth->name, 'email' => $userAuth->email,
            'condiction' => $userAuth->condiction, 'role' =>$role,
            'phone' => $userAuth→phone];

        return $user;
    }

    public static function getBoutiquiersByQuartier($user)  // list boutiquier de son quartier
    {
        $role_id_boutiquier = Role::where('nom', 'boutiquier')->get()->first()->id;

        return DB::table('users')->select('users.name','users.phone', 'users.id', 'users.condiction')
                    ->where('role_id', $role_id_boutiquier)->get();

    }

    public static function verifyUser($client)
    {
        $query = DB::table('users')->where('phone', $client['phone'])->get();

        if($query->count() == 0)
        {
            return false;
        }
        else
        {
            return true;
        }

    }

    public static function getClientsByBoutiquier($boutiquier_id)
    {
       return DB::table('users as c')->select('c.name', 'c.phone', 'c.condiction', 'c.phone',
                    'c.id')
              ->get();
    }


    
}











    

   



    


    

    

    

   


