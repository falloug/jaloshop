<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFournisseurVentesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fournisseur_ventes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nomProduit');
            $table->text('description');
            $table->date('date_vente');
            $table->integer('price');
            $table->integer('quantity');
            $table->integer('fournisseur_id');
            $table->integer('total');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fournisseur_ventes');
    }
}
