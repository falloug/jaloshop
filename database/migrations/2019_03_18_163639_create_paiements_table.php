<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePaiementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */   
    public function up()
    {
        Schema::create('paiements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('S2M_IDENTIFIANT');
            $table->integer('S2M_SITE');
            $table->string('S2M_REF_COMMANDE');
            $table->string('S2M_COMMANDE');
            $table->datetime('S2M_DATEH');
            $table->float('S2M_TOTAL');
            $table->string('S2M_HTYPE');
            $table->string('S2M_HMAC');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('paiements');
    }
}
