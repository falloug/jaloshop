<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDAddressTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dAddress', function (Blueprint $table) {
            $table->increments('id');
            $table->string('fullname');
            $table->integer('userid');
            $table->string('phone');
            $table->string('adresse');
            $table->string('quartier');
            $table->string('livraison');
            $table->integer('quartier_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('dAddress');
    }
}
